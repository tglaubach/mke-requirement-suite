import base64
import json
import re
import textwrap
import io
from ReqTracker.io_files import nextcloud_interface
from flask import Flask, request, jsonify, send_file, send_from_directory, render_template, render_template_string
import urllib
import shutil

import numpy as np

import traceback


import datetime
import time

import dash
from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update, get_asset_url, ClientsideFunction, clientside_callback
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

import dash_auth, dash_cytoscape

# from dash_bootstrap_templates import load_figure_template


# load_figure_template("litera")

# add to path for testing
import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, current_dir)
    sys.path.insert(0, parent_dir)


import ReqTracker

from ReqTracker.core import node_factory, schema, graph_main, graph_base, helpers

from ReqTracker.ui import ui_state
from ReqTracker.ui.helpers_ui import A, A2, idfromlink, get_log_sys_modal, get_log_changes_modal, get_sysinfo, mk_href

from ReqTracker.ui_overlays import config_offcanvas, log_offcanvas, shownode_modal
from ReqTracker.ui_vmc import vmc_docmaker, html_render
from ReqTracker.api import routes, uib, uim, uidf

from OLD.io import woosh_api

log = helpers.log
tostr = helpers.tostr







# DEFAULT_DB_PATH = parent_dir + r'\scripts\simple_test_db.json'



"""
 ██████  ██       ██████  ██████   █████  ██      ███████ 
██       ██      ██    ██ ██   ██ ██   ██ ██      ██      
██   ███ ██      ██    ██ ██████  ███████ ██      ███████ 
██    ██ ██      ██    ██ ██   ██ ██   ██ ██           ██ 
 ██████  ███████  ██████  ██████  ██   ██ ███████ ███████ 
"""                                        







# pull_projvars = ui_state.pull_projvars
# push_projvars = ui_state.push_projvars





#########################################################################0
#########################################################################0
#########################################################################0
#########################################################################0
#########################################################################0



"""
 █████  ██████  ██████  
██   ██ ██   ██ ██   ██ 
███████ ██████  ██████  
██   ██ ██      ██      
██   ██ ██      ██      
"""



title_str = "MKE-ReqTrack"
if ui_state.IS_PLAYGROUND:
    title_str += ' (PLAYGR)'

# , assets_folder='src/ReqTracker/assets'

app = Dash(title_str, pages_folder = current_dir + '/pages', use_pages=True,
    external_stylesheets=[dbc.themes.FLATLY ])#, dbc.themes.LITERA SUPERHERO ])

# needed for debugging in vscode
application = app.server

app.title = 'ReqTracker'
if ui_state.USERNAME_PW_PAIRS:
    auth_http = dash_auth.BasicAuth(
        app,
        ui_state.USERNAME_PW_PAIRS
    )




#########################################################################0
# HELPERS 
#########################################################################0

"""

 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌          ▐░▌          ▐░▌     ▐░▌            ▐░▌
▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                            
"""

GetGraphHold = ui_state.GetGraphHold
update_tags = ui_state.update_tags


add_log_alert = ui_state.add_log_alert
make_node = ui_state.make_node

def handle_command(graph, cmd, searchmode):

    key = ''
    ret = graph
    info = ''
    cmd = cmd.strip()
    inp_search = ''
    log.debug((graph, cmd, searchmode))
    if cmd.startswith('search '):
        inp_search = cmd[len('search '):]
        key = 'search' if inp_search else ''
    elif cmd.startswith('s '):
        inp_search = cmd[len('s '):]
        key = 'search' if inp_search else ''
    elif cmd.startswith('get '):
        key = 'get'
        inp = cmd[len('get '):]
        ret = None if inp not in graph else graph[inp]
        if ret is None:
            info = info = f"get {graph}[{inp_search}] not found"
        else:
            info = f"get {graph}[{inp_search}]"

    elif cmd.startswith('eval '):
        key = 'eval'
        inp = cmd[len('eval '):]
        ret = inp
    elif cmd.startswith('add '):
        key = 'add'
        inp = cmd[len('add '):]
        log.debug('adding ' + inp)
        if inp in graph:
            ret = None
            info = 'already exists!'
        else:
            ret, info = make_node(graph, inp, ret_info=True)
    elif cmd.startswith('del '):
        key = 'del'
        inp = cmd[len('del '):]
        log.info('deleting ' + inp)
        ret = graph.del_node(inp, remove=False)
        info = f'deleted {ret} nodes'
    elif cmd:
        key = 'search'
        inp_search = cmd
    else:
        ret = None
        key = ''
        inp = ''
        info = 'command not found'

    if key == 'search' and inp_search:
        ret = inp_search
        inp = inp_search
        
    return ret, key, inp, info









"""
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          ▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀█░█▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌     ▐░▌  ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌                    ▐░▌
▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
       
"""


## routes are now in api/routes.py
routes.add_routes(app.server)
uib.add_routes(app.server)
uim.add_routes(app.server)
uidf.add_routes(app.server)

"""

 ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ 
▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░▌       ▐░▌     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     
▐░░░░░░░░░░░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     
 ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀      
                    
"""


with GetGraphHold() as tmp:
    tabs = ui_state.make_tabs(tmp)
    ids = tmp.get_viewnodes(as_id=True)
    # edlg = editnode_modal.make_inp_modal(tmp.root_node, tmp, ui_state.tags_possible)





"""
██   ██ ███████  █████  ██████  ███████ ██████      ██████   █████  ██████  
██   ██ ██      ██   ██ ██   ██ ██      ██   ██     ██   ██ ██   ██ ██   ██ 
███████ █████   ███████ ██   ██ █████   ██████      ██████  ███████ ██████  
██   ██ ██      ██   ██ ██   ██ ██      ██   ██     ██   ██ ██   ██ ██   ██ 
██   ██ ███████ ██   ██ ██████  ███████ ██   ██     ██████  ██   ██ ██   ██ 
                                                                            """
                                                                            

help_txt = """**THIS IS THE COMMAND INTERFACE**

**Commands:** A command consists of a command and a text or ID:

- searching: 
   - `"s POC"` -> will search and show for everything containing the string `"POC"`
   - `"search POC"` -> same as above
   - `"POC"` -> same as above, if there is no node with ID `"POC"`
- opening nodes for edit: 
   - `"R.DS.SOW.1"` -> will open the node with ID `"R.DS.SOW.1"` from the root "nodes" graph
   - `"get R.DS.SOW.1"` -> will open this node from the currently active graph
- deleting nodes:
   - `"del VAS.TEST"` -> will delete the node with ID `"VAS.TEST"` if found. **Be careful** this can not easily be undone and no prompt to confirm is given!
- adding nodes:
   - `"add VAS.TEST"` -> will add a new node with the ID `"VAS.TEST"` to the root "nodes" graph and open a dialog to edit it. If already exist it will not do anything (but return a info). 

**Making new Nodes (Nodes):** The following node categories are allowed:

{}

**Making new Nodes (Documents):** Alternatively you can input a matching file schema like to add a new Document type node to the root "nodes" graph:

- `"MKE-316-000000-ODC-PL-0110"` -> will create DOC node with ID `"MKE-316-000000-ODC-PL-0110"`
- `"MKE-316-000000-ODC-RP-0066_05_MKE_FE_Performance_Report.pdf"` -> will create DOC node with ID `"MKE-316-000000-ODC-RP-0066"`
- `"MKE-316-060000-ODC-TR-0134_#05"` -> will create DOC node with ID "MKE-316-060000-ODC-TR-0134_#05"
- `"MKE-316-060000-ODC-TR-0134_#05_02_ACU Remote Interface.pdf"` -> will create DOC node with ID `"MKE-316-060000-ODC-TR-0134_#05"`
- `"301-000000-010"` -> will create DOC node with ID `"301-000000-010"`
- `"301-000000-010_Rev3_DishStates_Modes - signed.pdf"` -> will create DOC node with ID `"301-000000-010"`
- `"SKA-TEL-DSH-0000013"` -> will create DOC node with ID `"SKA-TEL-DSH-0000013"`
- `"SKA-TEL-DSH-0000013_Rev5_SPFRx_RS Signed.pdf"`  -> will create DOC node with ID `"SKA-TEL-DSH-0000013"`

if the matched ID is already contained within the node. The given text will be added as a version. If the version already exists the command will be ignored.

""".format(' | '.join('`"{}":{}`'.format(k, v) for k, v in graph_main.get_possible_node_info()))


# navlinks = dbc.Col(        
#         dbc.DropdownMenu(
#             children=[dbc.DropdownMenuItem(f"{page['name']} - {page['path']}", href=page["relative_path"]) 
#                         for page in dash.page_registry.values()] + [dbc.DropdownMenuItem('test', href='/vis_plot/all/reversed/none')],
#             nav=True,
#             in_navbar=True,
#             label="Select UI",
#             size='sm'
#         )
#     , style={'margin': '5px', "marginLeft": "15px"}, width='auto')


cmd_bar = dbc.Col([
    html.Div(children=[], id='on_render'),
    html.Div(children=[], id='on_render2'),    
    dbc.InputGroup([

        dbc.Button("?", id='cmd_help_btn', size='sm', color='info'),
        dbc.Popover(
            dbc.PopoverBody(dcc.Markdown(f'{help_txt}'), style={'minWidth': 1400}),
            target="cmd_help_btn",
            trigger="click",
            placement="bottom",
            style={'minWidth': 1400}
        ),
        dbc.Tooltip("Click to show help", target="cmd_help_btn", placement='bottom'),

        dcc.Store(id='mem-command', data=''),
        dbc.Input(id="inp-search", placeholder="command... (ENTER)", debounce=False, type="text", size='sm', style={'minWidth':'250px'}),
        dbc.Popover(dbc.ListGroup([], id=f"popover-search-suggestions"),
            id=f"popover-search",
            target=f"inp-search",
            placement='bottom',
            is_open=False,
            style={'maxHeight': '400px', "overflow": "scroll", "minWidth": "300px"},
            trigger="legacy",
        ),

        dbc.Button('\u2315', id='btn-command', n_clicks=0, size='sm'),
        dbc.Tooltip("submit the input search or command to the left", target="btn-command", placement='bottom'),
        dbc.Button('\u27F3', id='btn-refresh', n_clicks=0, size='sm'),
        dbc.Tooltip("Reload the tabs and search index", target="btn-refresh", placement='bottom'),

        # dbc.Tooltip("Type a command and submit by pressing enter (press ? to the left for help on commands)", target="inp-search", placement='bottom'),
        dbc.Button('cls', id='btn-clearsearch', n_clicks=0, size='sm'),
        dbc.Tooltip("clear all command inputs and temp views below", target="btn-clearsearch", placement='bottom'),


    ], size="sm")
    ], style={'margin': '5px'}, width='auto')

header_bar = dbc.Row([
        cmd_bar,   

        # dbc.Col(dbc.Select(options=ui_state.DISPLAY_STYLES_POSSIBLE, value=ui_state.display_style, id={'type': "par", 'index':"display_style"}, style={'width': '110px'}, size='sm'),style={'margin': '2px'}, width='auto'),
        # dbc.Tooltip("Display Style: Select a UI view to work in", target={'type': "par", 'index':"display_style"}, placement='bottom'),

        dbc.Col(dbc.InputGroup([
            dbc.InputGroupText("dlg"),
            dbc.Button('cng', id='btn-show-changes', n_clicks=0, size='sm'),
            dbc.Tooltip("Show the change logging dialog", target="btn-show-changes", placement='bottom'),
            dbc.Button(html.Div('sys', id='syslogtxt'), id='btn-show-syslog', n_clicks=0, size='sm'),
            dbc.Tooltip("Show the system logs dialog", target="btn-show-syslog", placement='bottom'),            
        ], size="sm"),style={'margin': '2px'}, width='auto'),
        
        dbc.Col(dbc.InputGroup([
            dbc.Button(html.Div('log', id='logtxt'), id='btn-show-offcanvas_R', n_clicks=0, size='sm'),
            dbc.Tooltip("Show the alert logging dialog on the bottom", target="btn-show-offcanvas_R", placement='bottom'),
        ], size="sm"),style={'margin': '2px', 'marginLeft': '10px'}, width='auto'),
], style={'margin': '5px'}, className="g-0 ms-auto flex-nowrap mt-3 mt-md-0", align="center")


navlinks = dbc.Nav(
    [dbc.NavLink(name, href=f"vis-{name}") for name in ui_state.DISPLAY_STYLES_POSSIBLE], 
style={'marginLeft': '15px'}, id='navlinks')

navbar = dbc.Navbar(
    dbc.Container(
        [
            dbc.Button('\u2699', id='btn-show-offcanvas_L', n_clicks=0, size='sm', style={'margin': '2px'}),
            dbc.Tooltip("Show the configuration dialog on the left", target="btn-show-offcanvas_L", placement='bottom'),

            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        # dbc.Col(html.Img(src=get_asset_url('minerva.jpg'), height="30px")),
                        dbc.Col(html.Img(src=get_asset_url('mpifr.png'), height="30px", style={'margin': '2px'})),
                        dbc.Col(dbc.NavbarBrand(title_str, style={'margin': '2px'}), width='auto'),
                    ],
                    align="center",
                    className="g-0",
                ),
                href="#",
                style={"textDecoration": "none"},
                id='logo',
            ),
            navlinks,
            dbc.Tooltip("Click one of the options to chose the visualisation style for below", target="navlinks", placement='bottom'),

            dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
            dbc.Collapse(
                header_bar,
                id="navbar-collapse",
                is_open=False,
                navbar=True,
            ),
        ]
    ),
    color="dark",
    dark=True,
    sticky='top',
    id='main-navbar'
)


# for some reason the dash app does not load without a dummy element in the main layout
cytoscape_dummy = dash_cytoscape.Cytoscape(
        id='cytoscape-dummy',
        layout={'name': 'preset'},
        style={'display': 'none'},
        elements=[
            {'data': {'id': 'one', 'label': 'Node 1'}, 'position': {'x': 75, 'y': 75}},
        ],
    )



"""
███    ███  █████  ██ ███    ██     ██       █████  ██    ██  ██████  ██    ██ ████████ 
████  ████ ██   ██ ██ ████   ██     ██      ██   ██  ██  ██  ██    ██ ██    ██    ██    
██ ████ ██ ███████ ██ ██ ██  ██     ██      ███████   ████   ██    ██ ██    ██    ██    
██  ██  ██ ██   ██ ██ ██  ██ ██     ██      ██   ██    ██    ██    ██ ██    ██    ██    
██      ██ ██   ██ ██ ██   ████     ███████ ██   ██    ██     ██████   ██████     ██    """
                                                                                        
                                                                                        


offcanvas_L = config_offcanvas.mk_layout()
offcanvas_B = log_offcanvas.layout

app.layout = html.Div([
    dcc.Location(id='url', refresh='callback-nav'),
    dcc.Store(id='mem-t_last', data=None),
    
    dcc.Store(id='plotupdate'),
    dcc.Store(id='showlog'),
    dcc.Interval(id="log-interval", interval=5_000, n_intervals=0),    
    # model_edit,
    dbc.Modal(id='dialogs', is_open=False, fullscreen=False, scrollable=True, size='xl'),
    
    
    shownode_modal.layout(),

    # html.Div(id='dialogs'),
    offcanvas_L,
    offcanvas_B,
    navbar,
    cytoscape_dummy,
    # dbc.CardHeader(dbc.Breadcrumb(items=tabs, id='tabs-container')),
    dbc.CardHeader(tabs, id='tabs-container', style={'marginLeft': '5px', "backgroundColor": "#f8f9fa",}),
    dcc.Store(id='mem-ids', data=[]),

    # dbc.CardBody(plots)#, style={"maxHeight": "100%", "overflow": "scroll"}),
    
    dbc.CardBody(dash.page_container),

], id='root-container')




"""

 ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌          ▐░▌       ▐░▌▐░▌               ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌ ▄▄▄▄▄▄▄▄      ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌▐░░░░░░░░▌     ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌ ▀▀▀▀▀▀█░▌     ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌ ▄▄▄▄█░█▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 

 """     


"""

 ██████  ███    ██     ██████  ███████ ███    ██ ██████  ███████ ██████  
██    ██ ████   ██     ██   ██ ██      ████   ██ ██   ██ ██      ██   ██ 
██    ██ ██ ██  ██     ██████  █████   ██ ██  ██ ██   ██ █████   ██████  
██    ██ ██  ██ ██     ██   ██ ██      ██  ██ ██ ██   ██ ██      ██   ██ 
 ██████  ██   ████     ██   ██ ███████ ██   ████ ██████  ███████ ██   ██ 
                                                                         
                                                                        
"""


@app.callback(
    Output("on_render", "style"),
    Input("on_render", "children"),
)
def on_render(dummy):
    try:
            
        log.info('ON_RENDER triggered.')
        ui_state.setv(t_render_last=helpers.nowiso())

    except Exception as err:
        log.error(err, exc_info=True)
        add_log_alert(A("ERROR on_click_showlog_cb: {}".format(err), color="danger"))

    # switch_graph_cb(graph_selected)
    # outp = tuple([projvars_dc] + [projvars_dc[k] for k in ui_state.cng_config_cb_keys])
    # update_tags()
    return no_update


clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='attach_btn_callback'
    ),
    Output("mem-command", "data"),
    Input("btn-command", "id"),
)



"""
 ██████  ██████  ███████ ███    ██      ██████ ██       ██████  ███████ ███████ 
██    ██ ██   ██ ██      ████   ██     ██      ██      ██    ██ ██      ██      
██    ██ ██████  █████   ██ ██  ██     ██      ██      ██    ██ ███████ █████   
██    ██ ██      ██      ██  ██ ██     ██      ██      ██    ██      ██ ██      
 ██████  ██      ███████ ██   ████      ██████ ███████  ██████  ███████ ███████ 

"""
# add callback for toggling the collapse on small screens
@app.callback(
    Output("navbar-collapse", "is_open"),
    Input("navbar-toggler", "n_clicks"),
    State("navbar-collapse", "is_open"),
)
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("offcanvas_config", "is_open"),
    Input("btn-show-offcanvas_L", "n_clicks"),
    State("offcanvas_config", "is_open"),
    prevent_initial_call=True
)
def toggle_offcanvas_L(n1, is_open):
    return not is_open

@app.callback(
    Output("offcanvas_log", "is_open", allow_duplicate=True),
    Input("btn-show-offcanvas_R", "n_clicks"),
    State("offcanvas_log", "is_open"),
    prevent_initial_call=True
)
def toggle_offcanvas_R(n1, is_open):
    return not is_open





"""
 ██████ ██████      ██       ██████   ██████   ██████  ██ ███    ██  ██████  
██      ██   ██     ██      ██    ██ ██       ██       ██ ████   ██ ██       
██      ██████      ██      ██    ██ ██   ███ ██   ███ ██ ██ ██  ██ ██   ███ 
██      ██   ██     ██      ██    ██ ██    ██ ██    ██ ██ ██  ██ ██ ██    ██ 
 ██████ ██████      ███████  ██████   ██████   ██████  ██ ██   ████  ██████  
                                                                             
"""                                                    


@app.callback(
    Output('dialogs', 'children', allow_duplicate=True),
    Output('dialogs', 'is_open', allow_duplicate=True),
    Input("btn-show-syslog", "n_clicks"),
    Input("btn-show-changes", "n_clicks"),
    Input("btn-show-info", "n_clicks"),
    Input("btn-show-config", "n_clicks"),

    Input("logo", "n_clicks"),
    prevent_initial_call = True,
)
def on_click_showlog_cb(n1, n2, n3, n4, n5):
    ret = no_update
    is_open = no_update
    try:
        state = ui_state.get()
        button_clicked = ctx.triggered_id
        log.debug('button_clicked ({}, {})'.format((n1, n2, n3, n4, n5), button_clicked))
        if button_clicked == 'btn-show-syslog':
            ret = get_log_sys_modal()
            is_open = True
        elif button_clicked == 'btn-show-changes':
            with GetGraphHold() as graph:
                ret = get_log_changes_modal(graph)
                is_open = True
        elif button_clicked == 'btn-show-info' or button_clicked == 'logo':
            usr = ui_state.NEXTCLOUD_LOGIN_DATA[0] if ui_state.NEXTCLOUD_LOGIN_DATA is not None and len(ui_state.NEXTCLOUD_LOGIN_DATA) > 1 else 'None'
            n_woosh = woosh_api.get_n_docs(ui_state.DEFAULT_FINDEX_PATH) if os.path.exists(ui_state.DEFAULT_FINDEX_PATH) else -1
            with GetGraphHold() as graph:
                ret = get_sysinfo(graph, ui_state.T_STARTED, usr, state.t_last, n_woosh=n_woosh) 
                is_open = True
        elif button_clicked == 'btn-show-config':
            txt = json.dumps(state.to_dict(), indent=2)
            st = {
                'margin': '15px', 
                "minHeight": '100px', 
                "maxHeight": '500px', 
                'overflow':'scroll', 
                'width': '100%', 
                'fontFamily': "Lucida Console, Courier New, monospace", 
                'fontSize': '11px'}

            ret = [
                dbc.ModalHeader("System Info: "),
                dbc.ModalBody(html.Pre(txt, style=st)),
            ]
            is_open= True
            
    except Exception as err:
        log.error(err, exc_info=True)
        add_log_alert(A("ERROR on_click_showlog_cb: {}".format(err), color="danger"))

    return ret, is_open






"""
 ██████ ██████       ██████  ██████  ███    ███ ███    ███  █████  ███    ██ ██████  ███████ 
██      ██   ██     ██      ██    ██ ████  ████ ████  ████ ██   ██ ████   ██ ██   ██ ██      
██      ██████      ██      ██    ██ ██ ████ ██ ██ ████ ██ ███████ ██ ██  ██ ██   ██ ███████ 
██      ██   ██     ██      ██    ██ ██  ██  ██ ██  ██  ██ ██   ██ ██  ██ ██ ██   ██      ██ 
 ██████ ██████       ██████  ██████  ██      ██ ██      ██ ██   ██ ██   ████ ██████  ███████ 

"""

clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='get_suggestions'
    ),
    Output("popover-search-suggestions", 'children', allow_duplicate=True),
    Output("popover-search", 'is_open', allow_duplicate=True),
    Input("inp-search", 'value'),
    Input('mem-command', 'data'),
    State("mem-ids", 'data'),
    prevent_initial_call = True,
)


clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='submit_suggestion'
    ),
    Output("mem-command", 'data', allow_duplicate=True),
    Output("popover-search", 'is_open', allow_duplicate=True),
    Output("inp-search", 'value', allow_duplicate=True),
    Input({'type': 'suggestion', 'index': ALL}, 'n_clicks'),
    State({'type': 'suggestion', 'index': ALL}, 'children'),
    prevent_initial_call = True,
)

         
@callback(
    Output("inp-search", "value", allow_duplicate=True),
    Input("btn-clearsearch", "n_clicks"),
    prevent_initial_call=True
)
def cb_clear_search(n1):
    return ''

         
@callback(
    Output("mem-command", "data", allow_duplicate=True),
    Input("btn-command", "n_clicks"),
    State("inp-search", "value"),
    prevent_initial_call=True
)
def btn_command_cb(n1, val):
    log.debug(('btn_command_cb', val))
    return val



@callback(
    Output("id_toshow", "data", allow_duplicate=True),
    Output('tabs-container', 'children'),
    Output('mem-ids', 'data'),
    Output('url', 'search', allow_duplicate=True),
    Output('inp-search', 'value'),
    Output('inp-search', 'valid'),
    Output('inp-search', 'invalid'),
    Output('showlog', 'data', allow_duplicate=True),
    Output('plotupdate', 'data', allow_duplicate=True),
    Input('mem-command', 'data'),
    Input('btn-refresh', 'n_clicks'),
    # Input({'type': "par", 'index':"display_style"}, "value"),
    State('url', 'pathname'),
    State('inp-search', 'valid'),
    State('inp-search', 'invalid'),
    prevent_initial_call=True
)
def update_data(inp_search, n, disp_style, inp_valid, inp_invalid):
    tabs = []
    id_ = no_update
    ids = no_update
    showlog = no_update
    plotupdate = no_update
    urlqry = no_update
    txt = no_update

    try:
        tabid = None
        state = ui_state.get()
        changed_id = [p['prop_id'] for p in callback_context.triggered][0]
        tstart = time.time()
        log.debug(tostr(round(tstart, 2), ' | update_data', changed_id))

        # if 'display_style' in changed_id:
        #     log.debug('update_data --> clear (display_style={})'.format(disp_style))
        #     push_projvars(display_style=disp_style)
        #     plotupdate = 'clear'


        with GetGraphHold() as graph:
            if 'mem-command' in changed_id:
                if inp_search and inp_search in graph:
                    id_ = graph[inp_search].id
                    s = f"COMMAND: lookup graph {graph}[{inp_search}] --> {graph[inp_search]}"
                    add_log_alert(A(s, color='info'))  
                elif inp_search:
                    ret, key = None, ''
                    log.debug(f'handle_command("{inp_search}")')
                    ret, key, inp, info = handle_command(graph, inp_search, state.searchmode)
                    log.debug(f'handle_command --> "{ret, key}"')                
                    if key == 'search':
                        tabid = key + ':' + ret
                    if key == 'add' and ret is not None and ret in graph:
                        id_ = graph[ret].id
                    if key == 'eval':
                        tabid = f'eval:{ret}'                    
                    s = "COMMAND: {}({}) --> {}: {}".format(key, inp, ret, info)
                    add_log_alert(A(s, color='info'))

                    if key != 'search':
                        showlog = helpers.make_zulustr(helpers.get_utcnow())
                    if not ret:
                        inp_valid = False
                        inp_invalid = True
                    else:
                        inp_valid = True
                        inp_invalid = False
                        txt = f'{key} {ret}'

                    if plotupdate == no_update:
                        plotupdate = helpers.nowiso()

                else:
                    inp_valid = False
                    inp_invalid = False
                    plotupdate = 'clear'

            else:
                inp_valid = False
                inp_invalid = False


            dt = time.time() - tstart
            log.debug(tostr(round(dt, 2), 's | update_data --> making tabs', disp_style))

            tabs = ui_state.make_tabs(graph, tabid)
            ids = graph.get_viewnodes(as_id=True)
            log.debug(f'Set mem-ids to {len(ids) = } elements')
            if tabid:
                log.debug(('TABID: ', type(tabid), tabid, plotupdate))
                urlqry = '?' + urllib.parse.urlencode({'g':tabid})
            
            dt = time.time() - tstart
            
            log.debug(tostr(round(dt, 2), 's | update_data --> DONE '))
            
    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        add_log_alert(A("ERROR update_data: {}".format(err), color="danger"))
        try:
            if 'inp-search' in changed_id:
                inp_valid = False
                inp_invalid = True
        except Exception as err:
            raise

    return id_, tabs, ids, urlqry, txt, inp_valid, inp_invalid, showlog, plotupdate


"""
 ██████ ██████      ██    ██ ██ ███████     ██    ██ ██ ███████ ██     ██ ███████ 
██      ██   ██     ██    ██ ██ ██          ██    ██ ██ ██      ██     ██ ██      
██      ██████      ██    ██ ██ ███████     ██    ██ ██ █████   ██  █  ██ ███████ 
██      ██   ██      ██  ██  ██      ██      ██  ██  ██ ██      ██ ███ ██      ██ 
 ██████ ██████        ████   ██ ███████       ████   ██ ███████  ███ ███  ███████ 

"""
@callback(
    Output('url', 'search', allow_duplicate=True),
    Input('plotupdate', 'data'),
    State('url', 'search'),
    prevent_initial_call=True
)
def update_page(plotupdate, qm):
    log.debug('plotupdate! ' + str(plotupdate) + str(qm))
    if plotupdate.lower() == 'clear':
        return ''
    elif not plotupdate:
        return no_update
    else:
        return qm



"""

 ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄        ▄       ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄        ▄ 
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░▌      ▐░▌     ▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░▌      ▐░▌
▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌▐░▌░▌     ▐░▌     ▐░▌░▌   ▐░▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░▌░▌     ▐░▌
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌▐░▌    ▐░▌     ▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌▐░▌    ▐░▌
▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌ ▐░▌   ▐░▌     ▐░▌ ▐░▐░▌ ▐░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░▌ ▐░▌   ▐░▌
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌  ▐░▌  ▐░▌     ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░▌  ▐░▌  ▐░▌
▐░█▀▀▀▀█░█▀▀ ▐░▌       ▐░▌▐░▌   ▐░▌ ▐░▌     ▐░▌   ▀   ▐░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░▌   ▐░▌ ▐░▌
▐░▌     ▐░▌  ▐░▌       ▐░▌▐░▌    ▐░▌▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌    ▐░▌▐░▌
▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄█░▌▐░▌     ▐░▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌ ▄▄▄▄█░█▄▄▄▄ ▐░▌     ▐░▐░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌      ▐░░▌     ▐░▌       ▐░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌      ▐░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀       ▀         ▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀        ▀▀ 
                                                                                                

"""

if __name__ == '__main__':
    log.setLevel(helpers.logging.DEBUG)


    log.info('__main__ --> app.run_server(...)')
    app.run_server(debug=True, use_reloader=True)  # Turn off reloader if inside Jupyter

    