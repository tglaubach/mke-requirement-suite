import warnings 
import numpy as np


import numpy as np

# def _get_dmin(xy, i):
#     import scipy.spatial
#     tmp = xy.copy()
#     v = np.unique(xy[:,i])
#     tmp = np.array([v, v*0]).T
#     #tmp[:,i] *= 0
#     d = scipy.spatial.distance.cdist( tmp, tmp )
#     d[d == 0] = np.nan
#     dmin = np.nanmin(d)
#     return dmin

# def spread_xy(xy, x_min, y_min):
#     dx_min = _get_dmin(xy, 1)
#     dy_min = _get_dmin(xy, 0)

#     fact_x = x_min / dx_min
#     xy[:, 0] = xy[:, 0] * fact_x + 2*dx_min

#     fact_y = y_min / dy_min
#     xy[:, 1] = xy[:, 1] * fact_y + 2*dy_min

#     # print('I AM HERE!')
#     # print(xy.astype(int))

#     xy = np.clip(xy, 0, 5000)

#     return xy


def nodes2moduledata(g, max_x=2000, max_y=2200, filtfun = None, min_x=150, min_y=60, no_root=True, id_lut=None, new_layout=False):


    # xy = xy
    idx = g.get_viewnodes(as_id=True)

    if filtfun:
        ii = [i for i, idxi in enumerate(idx) if filtfun(idxi)]
        idx = [idx[i] for i in ii]

    rid = g.root_node.id

    while no_root and rid in idx:
        idx.remove(rid)


    view = g.view.copy()
    nodes_to_pop_i = [view.vs['name'].index(id_) for id_ in view.vs['name'] if not id_ in idx]
    
    view.delete_vertices(nodes_to_pop_i)
    idx = view.vs['name']
    # view = None
    if not id_lut:
        id_lut = {nid: i for i, nid in enumerate(idx)}

    if not idx:
        return {}
    
    xy = g.get_xy(normalize=True, view=view)

    
    xy = xy[:, ::-1]

    # max_x = min_x * len(np.unique((xy[:, 0]*100).astype(int)))
    # max_y = min_y * len(np.unique((xy[:, 1]*100).astype(int)))

    max_x = min(max(len(np.unique(xy[:, 0]*100)) * 20, max_x), 6000)
    max_y = min(max(len(np.unique(xy[:, 1]*100)) * 30, max_y), 6000)

    xy[:, 0] *= max_x
    xy[:, 1] *= max_y

    xy[:, 0] += min_x
    xy[:, 1] += min_y

    funxy = lambda x: [getattr(x, 'data', {}).get(p, None) for p in ('pos_x', 'pos_y')] 
    func = lambda n: [id_lut.get(el.id) for el in n.children if el.id in idx]
    funp = lambda n: [id_lut.get(el.id) for el in n.parents if el.id in idx]

    # xy = spread_xy(xy, min_x, min_y)
    #print(g, ' new_layout=', new_layout)
    xy = xy.astype(int).tolist()
    if new_layout:
        xy_dc = dict(zip(idx, xy))
    else:

        xy_pre = {nid:funxy(g[nid]) for nid in idx }
        xy_pre = {k:[x,y] for k, (x,y) in xy_pre.items() if not x is None and not y is None}
        #print(f'{xy_pre=}')

        xy_dc = {**dict(zip(idx, xy)), **xy_pre}
    
    parents_dc = {nid:funp(g[nid]) for nid in idx }
    children_dc = {nid:func(g[nid]) for nid in idx }

    recs = {id_lut.get(nid):{'parents': parents_dc[nid], 'children': children_dc[nid], 'pos_x': xy_dc[nid][0], 'pos_y': xy_dc[nid][1]} for nid in idx if nid != rid}
    return recs




def nodes2drawflow(g, max_x=2000, max_y=2200, filtfun = None, min_x=150, min_y=60, no_root=True, id_lut=None, new_layout=False):


    # xy = xy
    idx = g.get_viewnodes(as_id=True)

    if filtfun:
        ii = [i for i, idxi in enumerate(idx) if filtfun(idxi)]
        idx = [idx[i] for i in ii]

    rid = g.root_node.id

    while no_root and rid in idx:
        idx.remove(rid)

    view = g.view.copy()
    nodes_to_pop_i = [view.vs['name'].index(id_) for id_ in view.vs['name'] if not id_ in idx]
    
    view.delete_vertices(nodes_to_pop_i)
    idx = view.vs['name']

    html_str = '''<div>
    <div class="title-box">
            <input type="text" df-name>
    </div>
</div>'''
    xy = g.get_xy(normalize=True, view=view)
    xy = xy[:, ::-1]

    # max_x = min_x * len(np.unique((xy[:, 0]*100).astype(int)))
    # max_y = min_y * len(np.unique((xy[:, 1]*100).astype(int)))

    max_x = min(max(len(np.unique(xy[:, 0]*100)) * 20, max_x), 6000)
    max_y = min(max(len(np.unique(xy[:, 1]*100)) * 30, max_y), 6000)

    xy[:, 0] *= max_x
    xy[:, 1] *= max_y

    xy[:, 0] += min_x
    xy[:, 1] += min_y



    # xy = spread_xy(xy, min_x, min_y)
    xy = xy.astype(int).tolist()

    nodes_drawflow = {}

    #idx = idx[:10]
    handled = set()

    for i, nid, (pos_x, pos_y) in zip(range(len(idx)), idx, xy):
        if nid == rid:
            continue

        if nid in handled:
            warnings.warn(f'WARNING {nid=} is duplicate! --> SKIPPING!')
            continue
        
        if id_lut and nid in id_lut:
            i = id_lut[nid]

        n = g[nid]
        children = [idx.index(el.id) for el in n.children if el.id in idx]
        parents = [idx.index(el.id) for el in n.parents if el.id in idx]
        dci = {
            "id": i,
            "name": "newnode",
            "data": { "name":  nid}, 
            "class": "newnode",
            "html": html_str,
            "typenode": False,
            "inputs": {
            "input_1": {
                "connections": [dict(node=str(node_id), input="output_1") for node_id in children]
            }
            },
            "outputs": {
            "output_1": {
                "connections": [dict(node=str(node_id), output="input_1") for node_id in parents]
            }
            },
            "pos_x": pos_x,
            "pos_y": pos_y
        }
        
        nodes_drawflow[str(i)] = dci
        handled.add(nid)

    return nodes_drawflow


        