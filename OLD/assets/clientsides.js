window.dash_clientside = Object.assign({}, window.dash_clientside, {
    clientside: {
        get_suggestions: get_suggestions_sub,
        submit_suggestion: submit_suggestion_sub,
        attach_btn_callback: attach_btn_callback_sub,
    }
});

function attach_btn_callback_sub (id_) { 

    // Get the input field
    var input = document.getElementById('inp-search');

    // Execute a function when the user presses a key on the keyboard
    input.addEventListener("keypress", function(event) {
    // If the user presses the "Enter" key on the keyboard
    if (event.key === "Enter") {
        console.log('submit "btn-command" by enter from "inp-search"')
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementById('btn-command').click();
    }
    }); 

    console.log('added EventListener "btn-command" by enter from "inp-search"')

}


function submit_suggestion_sub(n, suggestions) { 
    console.debug({'path': 'submit_suggestion_sub', 'n': n, 'suggestions': suggestions})
    for (let i = 0; i < n.length; i++) {
        
        if ((n[i] > 0) && (typeof suggestions[i] === 'string' || suggestions[i] instanceof String)){
            if (suggestions[i].startsWith('submit -> ')){
                const ret = [suggestions[i].slice(10), false, suggestions[i].slice(10)];
                console.log(ret);
                return ret; 
            } else if (n[i] > 0 && typeof suggestions[i] === 'string' || suggestions[i] instanceof String && $(suggestions[i].startsWith('...'))){
                const ret = [suggestions[i], false, suggestions[i]]; 
                console.log(ret);
                return ret; 
            }
        } 
        
      }
    ret =  [window.dash_clientside.no_update, true, window.dash_clientside.no_update];
    console.log(ret);

    return ret;
}

function get_suggestions_sub(input, cmd_trigger, array) {
    const triggered = dash_clientside.callback_context.triggered.map(t => t.prop_id)[0];
    // console.debug({'path': 'get_suggestions_sub', 'input': input, 'array': array, 'triggered_id': triggered})

    if (input != null && input.length > 0){
        
        var searchstr = input.trim().toLowerCase();
        var cmd = '';

        const parts = searchstr.split(' ');
        if (parts.length > 1){
            cmd = parts[0] + ' ';
            searchstr = parts.slice(1, parts.length).join(' ');
        }
        
        // console.debug(searchstr)
        // console.debug(cmd)

        // console.log(searchstr);
        var matches = ['submit -> ' + input];
        for (const element of array) {
            
            if (element.match(RegExp(searchstr, 'i')) != null){
                matches.push(element);
            }
            if(matches.length >= 10){
                matches.push('... first k=10 from n=' + array.length.toString() + ' ids')
                break;
            }
        }

        // var matches = res.filter(element => element.toLowerCase().includes(searchstr));

        // if (matches.length == 0){
        //     matches = get_closest(searchstr, array, 5);
        // }
        // console.log(matches)
        const elements = matches.map((el, i) => makeSuggestion((cmd + el).trim(), i));
        const is_open = cmd_trigger.includes('inp-search')

        return [elements, is_open];
    }
    else {
        return [[], false];
    }
}


function makeSuggestion(el, i) {
    const ret = {
        'type': 'Button', 
        'namespace': 'dash_bootstrap_components', 
        'props': {
            'children': el, 
            'id': {'type': 'suggestion', 'index': i}, 
            'size':'sm', 
            'style': {'width': '100%'}, 
            'color':'light', 
            'disabled': el.startsWith('...')
        }
    };

    console.log(ret)
    return ret
}

function get_closest(target, arr, n) {
    // Calculate distances for all strings in the array
    const distances = arr.map(str => ({ str, distance: levenshteinDistance(target, str) }));

    // Sort distances in ascending order
    distances.sort((a, b) => a.distance - b.distance);

    // Get the n closest matches
    const closestMatches = distances.slice(0, n).map(item => item.str);
    return closestMatches;
}

function levenshteinDistance(s1, s2) {
    const len1 = s1.length;
    const len2 = s2.length;
    const matrix = [];

    for (let i = 0; i <= len1; i++) {
        matrix[i] = [i];
    }

    for (let j = 0; j <= len2; j++) {
        matrix[0][j] = j;
    }

    for (let i = 1; i <= len1; i++) {
        for (let j = 1; j <= len2; j++) {
            const cost = s1.charAt(i - 1) === s2.charAt(j - 1) ? 0 : 1;
            matrix[i][j] = Math.min(
                matrix[i - 1][j] + 1, // deletion
                matrix[i][j - 1] + 1, // insertion
                matrix[i - 1][j - 1] + cost // substitution
            );
        }
    }

    return matrix[len1][len2];
}
