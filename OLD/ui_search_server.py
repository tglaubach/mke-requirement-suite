#!/usr/bin/python3
""" 
the search bot server (uses direct caching and )
"""

import traceback
import time, datetime
import numpy as np
import logging


import os, inspect, sys



from sentence_transformers import SentenceTransformer, util



model = SentenceTransformer("multi-qa-MiniLM-L6-cos-v1")



log = logging.getLogger()
level = logging.INFO
fmt = "[ %(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
frmatter = logging.Formatter(fmt, datefmt='%Y-%m-%d %H:%M:%S%z')
log.setLevel(level)
streamHandler = logging.StreamHandler(sys.stdout)


log.setLevel(logging.INFO)
logging.getLogger('werkzeug').setLevel(logging.ERROR)


from flask import Flask, request, jsonify

t_started = datetime.datetime.utcnow()

app = Flask('ReqTrackSearchBot')

content_ids = []
content_dict = []
content_embedded = []

@app.route('/api/v1/index', methods=['POST'])
def do_index():
    
    try:
        dc_index = request.json

        global content_ids, content_embedded

        content_ids = list(dc_index.keys())
        sentences = list(dc_index.values())
        content_embedded = model.encode(sentences)

        return jsonify({'OK': len(content_embedded)})

        
    except Exception as err:
        return jsonify({"error": str(traceback.format_exception(err))}), 500

@app.route('/')
@app.route('/info')
def info():
    
    try:
        global content_ids, content_embedded
        return jsonify({'version': 'v1', 'n_indices': len(content_ids), 'running_since': t_started.isoformat()})

    except Exception as err:
        return jsonify({"error": str(traceback.format_exception(err))}), 500


@app.route('/api/v1/search')
def search():
    

    args = request.args
    
    try:    
        global content_ids, content_embedded
        args = request.args
        
        if len(content_ids) == 0:
            return jsonify({"error": 'This server has no data stored to search in!'}), 404
        
        i_start = args['from'] if "from" in args else 0
        n_max = args['size'] if 'size' in args else 10
        i_start = max(0, min(len(content_ids), i_start))
        i_end = max(0, min(len(content_ids), i_start + n_max))

        query = args['query']
        query_embedding = model.encode(query)
        cosine_scores = util.cos_sim(query_embedding, content_embedded)
        idx_sort = np.argsort(cosine_scores)[::-1]

        idx_sort = idx_sort[i_start:i_end]
        
        res = [(content_ids[i], cosine_scores[i]) for i in idx_sort[i_start:i_end]]
        return jsonify(res)
    
    except Exception as err:
        return jsonify({"error": str(traceback.format_exception(err))}), 500

if __name__ == '__main__':
    app.run('0.0.0.0', 9100)
