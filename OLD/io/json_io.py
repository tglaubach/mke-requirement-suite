import json

from ReqTracker.io.base_io import BaseSerializeSource

def save(dc_serialized, path=''):
    if isinstance(path, str):
        with open(path, 'w') as fp:
            json.dump(dc_serialized, fp, indent=2)
    else:
        json.dump(dc_serialized, path, indent=2)

def load(pth):
    if isinstance(pth, str):
        with open(pth, 'r') as fp:
            return json.load(fp)
    else:
        return json.load(pth)



class src_jsondb(BaseSerializeSource):

    sinktype = 'file'
    
    def push(self, dc_serialized:dict, **kwargs):
        return save(dc_serialized, path=self.path)
    
    def pull(self, **kwargs):
        return load(self.path)



if __name__ == "__main__":
    
    import os, inspect, sys
    current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    parent_dir = os.path.dirname(os.path.dirname(current_dir))

    sys.path.insert(0, parent_dir)
    

    import json 
    import time

    from ReqTracker.core import graph_main, graph_base, schema

    t = time.time()
    pth = r'C:\temp\temp_db' + f'_{int(t)}.json'
    pth_in = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.json'

    with open(pth_in, 'r') as fp:
        dc = json.load(fp)
    
    graph = graph_main.Graph.deserialize(dc)

    save(pth, graph.serialize())
    
    
    k = 'VS.TEST'
    graph.add_node(graph_main.construct(dict(id=k, text='SOME TEST SCRIPT')))
    graph[k].add_note('This is only for testing!')

    save(pth, graph.serialize())

    g2 = graph_main.Graph.deserialize(load(pth))

    print(g2.get_changes(since_construction=False))

    print(g2[k].to_dict(add_parents=True))

    assert json.dumps(g2.serialize()) == json.dumps(graph.serialize())