from abc import ABC, abstractmethod
import os

import time

import multiprocessing as mp
from queue import Queue

from ReqTracker.core import helpers

log = helpers.log


# class BaseIoInterface(ABC):

    

#     @abstractmethod
#     def save(self, dc_serialized:dict, **kwargs):
#         pass
    
#     @abstractmethod
#     def load(self, **kwargs):
#         pass

#     @abstractmethod
#     def get_node(self, id_:str, **kwargs):
#         pass

#     @abstractmethod
#     def add_node(self, node_dc:dict, **kwargs):
#         pass

#     @abstractmethod
#     def del_node(self, id_:str, **kwargs):
#         pass
    
#     @abstractmethod
#     def get_change(self, id_, **kwargs):
#         pass

#     @abstractmethod
#     def add_change(self, change_dc:str, **kwargs):
#         pass

#     @abstractmethod
#     def del_change(self, id, **kwargs):
#         pass

    
#     def get_node_as_dict(self, id_, **kwargs):
#         return self.get_node(id_, **kwargs)
    
#     def get_nodes(self, ids:list, **kwargs):
#         return [self.get_node(node_id, **kwargs) for node_id in ids]
    
#     def set_nodes(self, nodes_dc:list, **kwargs):
#         return [self.add_node(dc, **kwargs) for dc in nodes_dc]

#     def del_nodes(self, ids:list, **kwargs):
#         return [self.del_node(node_id, **kwargs) for node_id in ids]
    
#     def add_changes(self, changes_dc:list, **kwargs):
#         return [self.add_change(change_dc, **kwargs) for change_dc in changes_dc]
    
#     def del_changes(self, ids, **kwargs):
#         return [self.del_change(e, **kwargs) for e in ids]
    


class BaseSerializeSource(object):

    tbl_changes = 'changes'
    tablename = 'nodes'

    sinktype = ''

    def __init__(self, path, tablename='nodes', **kwargs) -> None:
        self.path = path
        self.tablename = tablename
        self.tbl_changes = 'changes'

    @property
    def name(self):
        return os.path.basename(self.path)
    
    @property
    def key(self):
        return self.path
    
    def __str__(self) -> str:
        return self.name
    
    def get_data_dc(self):
        return {v['id']:v for v in self.pull()[self.tablename]}

    def push(self, dc_serialized:dict, **kwargs):
        raise NotImplementedError('This is an abstract class! Any inherited class must overwrite this!')
    
    def pull(self, **kwargs):
        raise NotImplementedError('This is an abstract class! Any inherited class must overwrite this!')




    def get_node(self, id_:str, **kwargs):
        return self.get_nodes(id_, **kwargs)[0]

    def get_nodes(self, ids=None, **kwargs):
        dc = self.get_data_dc()
        if not ids:
            return list(dc.values())
        else:
            return [dc[i] for i in ids]
        
    def get_change(self, id_, **kwargs):
        return self.get_changes([id_], **kwargs)
    
    def get_changes(self, ids=None, **kwargs):
        dc_ser = self.pull()
        if not ids:
            return dc_ser[self.tbl_changes]
        else:
            return [dc_ser[self.tbl_changes][i] for i in ids]
        


    def push_changes(self, nodes_dc_to_update, changes_to_append):
        self.set_nodes(nodes_dc_to_update)
        self.set_changes(changes_to_append)


    def set_node(self, node_dc:dict, **kwargs):
        return self.set_nodes([node_dc], **kwargs)[0]
    
    def set_nodes(self, nodes_dc:list, **kwargs):
        dc = self.get_data_dc()
        
        for n in nodes_dc:
            dc[n['id']] = n

        if self.tbl_changes in kwargs:
            self.set_changes(self.path, kwargs[self.tbl_changes], do_save=False)

        self.push(dc)

        return [True for node in nodes_dc]
    
    def del_node(self, id_:str, **kwargs):
        return self.del_nodes([id_], **kwargs)
    
    def del_nodes(self, ids:list=None, **kwargs):
        dc_ser = self.pull()
        if ids is None:
            ids = dc_ser[self.tablename]

        dc_ser[self.tablename] = [n for n in dc_ser[self.tablename] if n['id'] not in ids ]
        if self.tbl_changes in kwargs:
            self.set_changes(self.path, kwargs[self.tbl_changes], do_save=False)
        self.push(dc_ser)
        return [True for id_ in ids]



    def set_change(self, change_dc:str, **kwargs):
        return self.set_changes([change_dc], **kwargs)
    
    def set_changes(self, changes_dc:list, **kwargs):
        if not changes_dc:
            return [False]
        dc_ser = self.pull()
        changes = dc_ser['nodes']
        
        for c in changes_dc:
            i = None if 'id' not in c else c['id']
            tpl = (c['timestamp'], c['text']) if isinstance(c, dict) else c

            if i is None or i > len(changes):
                changes.append(tpl)
            else:
                changes[i] = tpl
        dc_ser[self.tbl_changes] = changes

        if 'do_save' in kwargs and not kwargs['do_save']:
            pass
        else:
            self.push(dc_ser)

        return [True for change in changes_dc]

    def del_change(self, id_, **kwargs):
        self.del_changes([id_], **kwargs)
        return True
    
    def del_changes(self, ids=None, **kwargs):
        if not ids:
            ids = list(range(len(dc_ser[self.tbl_changes])))
        dc_ser = self.pull()
        dc_ser[self.tbl_changes] = [e for i, e in enumerate(dc_ser[self.tbl_changes]) if i not in ids]
        self.push(dc_ser)
        return [True for i in ids]
    



class AsyncWritingSource(BaseSerializeSource):

    def __init__(self, base_source, verb=False, **kwargs) -> None:
        assert issubclass(type(base_source), BaseSerializeSource), 'the given base source {} (type: {}) is not inherited from BaseSerializeSource'.format(base_source, type(base_source))
        super().__init__(base_source, '', **kwargs)
        self.logfun = log.info if verb else log.debug
        self.procs = []

    @property
    def base_source(self):
        return self.path
    
    @property
    def name(self):
        return self.base_source.name + '_async'
    
    @property
    def key(self):
        return self.base_source.key + '_async'
    
    def __run_as_process(self, data):
        
        procs_err = [(p.pid, p.exitcode) for p in self.procs if p.exitcode is not None and p.exitcode != 0]
        assert not procs_err, f'found processes which exited with errors! exitcodes: ' + str(procs_err)
        self.procs = [p for p in self.procs if p.exitcode is None]

        p = mp.Process(target=self.run_async, args=(data,), daemon = True)
        p.start()


    def get_change(self, id_, **kwargs):
        return self.get_changes([id_], **kwargs)
    
    def get_changes(self, ids=None, **kwargs):
        return self.base_source.get_changes(ids, **kwargs)

    def get_node(self, id_:str, **kwargs):
        return self.get_nodes(id_, **kwargs)[0]

    def get_nodes(self, ids=None, **kwargs):
        return self.base_source.get_nodes(ids, **kwargs)

    def pull(self, **kwargs):
        return self.base_source.pull(**kwargs)



    def set_node(self, node_dc:dict, **kwargs):
        return self.set_nodes([node_dc], **kwargs)[0]
    
    def del_node(self, id_:str, **kwargs):
        return self.del_nodes([id_], **kwargs)
    
    def set_change(self, change_dc:str, **kwargs):
        return self.set_changes([change_dc], **kwargs)
    
    def del_change(self, id_, **kwargs):
        self.del_changes([id_], **kwargs)
        return True
    

    def set_nodes(self, nodes_dc:list, **kwargs):
        self.__run_as_process(('set_nodes', [nodes_dc], kwargs))
        return [True for node in nodes_dc]
    
    def set_changes(self, changes_dc:list, **kwargs):
        if not changes_dc:
            return []

        self.__run_as_process(('set_changes', [changes_dc], kwargs))
        return [True for change in changes_dc]

    def del_nodes(self, ids:list=None, **kwargs):
        self.__run_as_process(('del_nodes', [ids], kwargs))
        return [True for id_ in ids]
    
    def del_changes(self, ids=None, **kwargs):
        self.__run_as_process(('del_changes', [ids], kwargs))
        return [True for i in ids]

    def push(self, dc_serialized:dict, **kwargs):
        self.__run_as_process(('push', [dc_serialized], kwargs))
        return None

    # Define the consumer function
    def run_async(self, data):
        try:
            self.logfun(self.name + ' | STARTING run_async')

            if not data:
                return
        
            methodname, args, kwargs = data
            fun = getattr(self.base_source, methodname)
            fun(*args, **kwargs)

        except Exception as err:
            self.logfun(self.name + ' | ERROR run_async! {}'.format(err))
            log.error(err, exc_info=True)

        self.logfun(self.name + ' | ENDING run_async')