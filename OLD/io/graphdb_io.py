import re
import sqlite3
import time
import json


pattern = re.compile('[\W_-]+')


import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    


from ReqTracker.core import helpers
from ReqTracker.io.base_io import BaseSerializeSource

log = helpers.logging.getLogger('graphdb')
log.setLevel(helpers.logging.WARNING)

KEEP_CHANGELOG = True
IS_READONLY = os.environ.get('IS_READONLY', '')

KWs = "ABORT ACTION ADD AFTER ALL ALTER ALWAYS ANALYZE AND AS ASC ATTACH AUTOINCREMENT BEFORE BEGIN BETWEEN BY CASCADE CASE CAST CHECK COLLATE COLUMN COMMIT CONFLICT CONSTRAINT CREATE CROSS CURRENT CURRENT_DATE CURRENT_TIME CURRENT_TIMESTAMP DATABASE DEFAULT DEFERRABLE DEFERRED DELETE DESC DETACH DISTINCT DO DROP EACH ELSE END ESCAPE EXCEPT EXCLUDE EXCLUSIVE EXISTS EXPLAIN FAIL FILTER FIRST FOLLOWING FOR FOREIGN FROM FULL GENERATED GLOB GROUP GROUPS HAVING IF IGNORE IMMEDIATE IN INDEX INDEXED INITIALLY INNER INSERT INSTEAD INTERSECT INTO IS ISNULL JOIN KEY LAST LEFT LIKE LIMIT MATCH MATERIALIZED NATURAL NO NOT NOTHING NOTNULL NULL NULLS OF OFFSET ON OR ORDER OTHERS OUTER OVER PARTITION PLAN PRAGMA PRECEDING PRIMARY QUERY RAISE RANGE RECURSIVE REFERENCES REGEXP REINDEX RELEASE RENAME REPLACE RESTRICT RETURNING RIGHT ROLLBACK ROW ROWS SAVEPOINT SELECT SET TABLE TEMP TEMPORARY THEN TIES TO TRANSACTION TRIGGER UNBOUNDED UNION UNIQUE UPDATE USING VACUUM VALUES VIEW VIRTUAL WHEN WHERE WINDOW WITH WITHOUT".split()

def test_no_kws(s:str):
    for word in s.split():
        found_keywords = [k for k in KWs if k in word]
        assert not found_keywords, f'your string {word=} contains invalid SQL keywords: {found_keywords=}'


def __connect(sqlite_file):

    if IS_READONLY:
        p = sqlite_file.strip()
        if not p.startswith('file:'):
            p = 'file:' + p
        if not p.endswith('?mode=ro'):
            p = p + '?mode=ro'
        try:
            return sqlite3.connect(p, uri=True)
        except Exception as err:
            log.error(f"error while trying to connect to database: {p} (file exists?: {os.path.exists(sqlite_file)})")
            log.error(str(err))
            raise
        
    else:
        try:
            return sqlite3.connect(sqlite_file)
        except Exception as err:
            log.error(f"error while trying to connect to database: {sqlite_file} (file exists?: {os.path.exists(sqlite_file)})")
            log.error(str(err))
            raise
        

        

def __qry_sub(sqlite_file, create_sql, qry_sql, unpack, verb=True):

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            if not IS_READONLY:
                cur.execute(create_sql)
                if verb:
                    log.debug(qry_sql)
                    
            cur = cur.execute(qry_sql)
            ret = cur.fetchall()

            if unpack:
                ret = [json.loads(jsons) for (id_, jsons, ts) in ret]
            else:
                ret = [tpl[1:] for tpl in ret]
            if verb:
                log.debug(f'len(qry) = {len(ret)}')
            return ret
    except Exception as err:
        log.error(f"error while trying to query database: {sqlite_file} (file exists?: {os.path.exists(sqlite_file)})\n'{qry_sql}'")
        log.error(str(err))
        raise
    finally:
        if 'con' in locals() and con:
            con.close()
    

def query_nodes(sqlite_file, id_='', tablename='graph_nodes', verb=True, wheres=None):

    create_sql = f"CREATE TABLE IF NOT EXISTS {tablename} ( id TEXT PRIMARY KEY NOT NULL UNIQUE, json TEXT, last_time_changed TEXT NOT NULL);"
    qry_sql = f'SELECT * FROM {tablename}'

    wheres = [] if wheres is None else wheres
    if id_ or wheres:
        qry_sql += ' WHERE '

    if wheres:
        qry_sql += ' (' + ' AND '.join([str(s) for s in wheres]) + ') '

    if id_ and isinstance(id_, str):
        qry_sql += f' AND id="{id_}"'
    
    elif id_ and hasattr(id_, '__len__'):
        qry_sql += ' AND id in ({})'.format(','.join((f'"{i}"' for i in id_)))
    
    qry_sql += ';'
    return __qry_sub(sqlite_file, create_sql, qry_sql, unpack=True, verb=verb)
    

def __query_changes(sqlite_file, id_ ='', tablename='graph_changes'):
    create_sql = f"CREATE TABLE IF NOT EXISTS {tablename} ( id INTEGER PRIMARY KEY ASC, timestamp TEXT, text TEXT);"
    qry_sql = f'SELECT * FROM {tablename}'

    if id_ and isinstance(id_, str):
        qry_sql += f' WHERE id="{id_}"'
    elif id_ and hasattr(id, '__len__'):
        qry_sql += ' WHERE id in ({})'.format(','.join((f'"{i}"' for i in id_)))

    qry_sql += ';'
    return __qry_sub(sqlite_file, create_sql, qry_sql, unpack=False)
    
    
def __query(sqlite_file, wheres=None):
    nodes = query_nodes(sqlite_file, id_ ='', tablename='graph_nodes', wheres=wheres)
    # changes = __query_changes(sqlite_file, id_ ='', tablename='graph_changes')
    changes = [(helpers.nowiso(), 'querying changes was disabled for performance reasons. Contact admin if you need this feature!')]
    return {'nodes':nodes, 'changes':changes}
    
def insertr_changelog(sqlite_file, nodes, tablename='graph_nodes', verb=False, ts=None):
    
    create_sql = f"CREATE TABLE IF NOT EXISTS {tablename} ( id TEXT, json TEXT, last_time_changed TEXT NOT NULL, PRIMARY KEY (id, json));"
    insertr_sql = f'INSERT OR REPLACE INTO {tablename} (id, json, last_time_changed) VALUES (?, ?, ?);'

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            if verb:
                log.debug(create_sql)
            cur.execute(create_sql)
            if verb:
                log.debug(insertr_sql)
                log.debug(f'len(nodes) = {len(nodes)}')
            
            if nodes:
                if ts is None:
                    ts = helpers.make_zulustr(helpers.get_utcnow(), remove_ms=False)
                fun = lambda x: (x['id'], json.dumps(x, indent=2), ts)
                cur.executemany(insertr_sql, [fun(node) for node in nodes])
            con.commit()
            if verb:
                log.info('INSERT OR REPLACE n={} nodes in {}'.format(len(nodes), sqlite_file))
    finally:
        if 'con' in locals() and con:
            con.close()
    
    

def fs_info(sqlite_file, tablename_v='graph_nodes_search', verb=False):
    assert sqlite_file, 'the sqlite file path can not be empty'
    try:            

        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            cur = cur.execute(f'SELECT COUNT(*) FROM {tablename_v};')
            n, = next(iter(cur.fetchall()))
            return {'table:': {'name': tablename_v, 'count': n}}
    finally:
        if 'con' in locals() and con:
            con.close()


def fs_setup(sqlite_file, tablename='graph_nodes', tablename_v='graph_nodes_search', do_update=True, verb=False):
    create_sql = f"CREATE VIRTUAL TABLE IF NOT EXISTS {tablename_v} USING fts5 ( id, json, last_time_changed);"


    triggers_sql = f"""CREATE TRIGGER IF NOT EXISTS {tablename_v}_update AFTER INSERT ON {tablename}
BEGIN
    INSERT INTO {tablename_v} (id, json, last_time_changed) VALUES (new.id, new.json, new.last_time_changed);
END;

CREATE TRIGGER IF NOT EXISTS {tablename_v}_delete AFTER DELETE ON {tablename}
BEGIN
    INSERT INTO {tablename_v} ({tablename_v}, id, json, last_time_changed) VALUES ('delete', old.id, old.json, old.last_time_changed);
END;

CREATE TRIGGER IF NOT EXISTS {tablename_v}_update AFTER UPDATE ON {tablename}
BEGIN
    INSERT INTO {tablename_v} ({tablename_v}, id, json, last_time_changed) VALUES ('delete', old.id, old.json, old.last_time_changed);
    INSERT INTO {tablename_v} (id, json, last_time_changed) VALUES (new.id, new.json, new.last_time_changed);
END;"""


    insertr_sql = f'INSERT OR REPLACE INTO {tablename_v} SELECT id, json, last_time_changed FROM {tablename};'

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()

            scripts = [create_sql, triggers_sql]
            if do_update:
                scripts.append(insertr_sql)

            for script in scripts:
                if verb:
                    log.debug(script)
                cur.executescript(script)

    finally:
        if 'con' in locals() and con:
            con.close()



def fs_search(sqlite_file, searchtext, tablename='graph_nodes', tablename_v='graph_nodes_search', search_column='json', verb=False, limit=20, allow_fallback=True):
    qry = f"SELECT DISTINCT	id, snippet({tablename_v}, 1, '<mark>', '</mark>', '(...)', 15), rank FROM {tablename_v} WHERE {search_column} MATCH ? ORDER BY rank"
    

    if allow_fallback:
        try:
            return fs_search(sqlite_file, searchtext, tablename, tablename_v, verb=verb, limit=limit, allow_fallback=False)
        except (sqlite3.OperationalError, FileNotFoundError) as err:
            log.info('Failed to search. Will call fs_setup() for search tables and then retry')
            fs_setup(sqlite_file, tablename, tablename_v, do_update=False, verb=verb)

    
    if limit > 0:
        qry += f' LIMIT {limit:.0f}'

    if not qry.endswith(';'):
        qry += ';'

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:            
        st = pattern.sub(' ', searchtext).strip()
        
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            cur = cur.execute(f'SELECT COUNT(*) FROM {tablename_v};')
            n, = next(iter(cur.fetchall()))
            if n <= 1: # for some reason a virtual table has excatly 1 entry!
                log.info('searchtable does not contain data. Will call fs_setup() for search tables and then retry')
                fs_setup(sqlite_file, tablename, tablename_v, do_update=True, verb=verb)

            cur = cur.execute(qry, (st,))
            els = cur.fetchall()            

            return {obj[0]: {'id': obj[0], 'snippet': obj[1], 'score': obj[2], 'keywords': list(set(list(re.findall(r"<mark>(.*?)</mark>", obj[1]))))} for obj in els}
        
        
    finally:
        if 'con' in locals() and con:
            con.close()


def insertr_nodes(sqlite_file, nodes, tablename='graph_nodes', verb=True, ts=None):
    
    create_sql = f"CREATE TABLE IF NOT EXISTS {tablename} ( id TEXT PRIMARY KEY NOT NULL UNIQUE, json TEXT, last_time_changed TEXT NOT NULL);"
    insertr_sql = f'INSERT OR REPLACE INTO {tablename} (id, json, last_time_changed) VALUES (?, ?, ?);'

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            if verb:
                log.debug(create_sql)
            cur.execute(create_sql)
            if verb:
                log.debug(insertr_sql)
                log.debug(f'len(nodes) = {len(nodes)}')
            
            if nodes:
                if ts is None:
                    ts = helpers.make_zulustr(helpers.get_utcnow(), remove_ms=False)
                fun = lambda x: (x['id'], json.dumps(x, indent=2), ts)
                cur.executemany(insertr_sql, [fun(node) for node in nodes])
            con.commit()
            if verb:
                log.info('INSERT OR REPLACE n={} nodes in {}'.format(len(nodes), sqlite_file))
    finally:
        if 'con' in locals() and con:
            con.close()



def insertr_changes(sqlite_file, changes, verb=True):

    create_changes_sql = f"CREATE TABLE IF NOT EXISTS graph_changes ( id INTEGER PRIMARY KEY ASC, timestamp TEXT, text TEXT);"
    insertr_changes_sql = f'INSERT OR REPLACE INTO graph_changes (id, timestamp, text) VALUES (?, ?, ?);'

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            if verb:
                log.debug(create_changes_sql)
            cur.execute(create_changes_sql)
            if verb:
                log.debug(insertr_changes_sql)
                log.debug(f'len(changes) = {len(changes)}')
            
            if changes:
                if isinstance(changes, list) and len(changes[0]) == 2:
                    tmp = [(i, ts, txt) for i, (ts, txt) in enumerate(changes, 1)]
                    
                if isinstance(changes, list) and isinstance(changes[0], dict):
                    cols = 'id timestamp text'.split()
                    tmp = [tuple((dci[c] for c in cols)) for dci in changes]

                elif isinstance(changes, dict):
                    tmp = [(i, ts, txt) for i, (ts, txt) in changes.items()]

                cur.executemany(insertr_changes_sql, tmp)
            con.commit()

            if verb:
                log.debug('INSERT OR REPLACE n={} changes in {}'.format(len(changes), sqlite_file))

    finally:
        if 'con' in locals() and con:
            con.close()
    

def delrow(sqlite_file, ids, tablename='graph_nodes'):
    if isinstance(ids, str):
        ids = [ids]

    del_node_sql += 'DELETE FROM {} WHERE id in ({})'.format(tablename, ','.join((f'"{i}"' for i in ids)))

    assert sqlite_file, 'the sqlite file path can not be empty'
    try:
        con = __connect(sqlite_file)
        with con:
            cur = con.cursor()
            for id in ids:
                sql = del_node_sql.format(tablename, id)
                log.debug(sql)
                cur.execute(sql)

            con.commit()
            
            log.debug('DELETE n={} rows in {}.{}'.format(len(ids), sqlite_file, tablename))

    finally:
        if 'con' in locals() and con:
            con.close()

def push_changes(pth, nodes_dc_to_update, changes_to_append):
    ts = helpers.make_zulustr(helpers.get_utcnow(), remove_ms=False)
    insertr_nodes(pth, nodes_dc_to_update, ts=ts)
    if KEEP_CHANGELOG:
        insertr_changelog(pth, nodes_dc_to_update, tablename='changelog', ts=ts)
    insertr_changes(pth, changes_to_append)

def save(dc_serialized, pth):
    ts = helpers.make_zulustr(helpers.get_utcnow(), remove_ms=False)
    insertr_nodes(pth, dc_serialized['nodes'], ts=ts)
    if KEEP_CHANGELOG:
        insertr_changelog(pth, dc_serialized['nodes'], tablename='changelog', ts=ts)
    insertr_changes(pth, dc_serialized['changes'])
    
def load(pth, wheres=None):
    return __query(pth, wheres=wheres)

def del_row(pth, node_ids_to_delete):
    delrow(pth, node_ids_to_delete)

def get_project_var(path, key, tablename='project_variables', **kwargs):
    v = next(iter(query_nodes(path, key, tablename, verb=False)), {})
    if v and 'value' in v:
        log.debug('get_project_var({}, {}, {}) -> {}'.format(helpers.limit_len(path, 15, 'R'), key, tablename, v['value']))
        return v['value']
    elif 'default' in kwargs:
        default = kwargs['default']
        set_project_var(path, key, default, tablename)
        return default
    else:
        raise KeyError('"{} not found in project variables and no default given'.format(key))
    
def set_project_var(path, key, value, tablename='project_variables'):
    insertr_nodes(path, [{'id': key, 'value': value}], tablename, verb=False)
    log.debug('set_project_var({}, {}, {}, {})'.format(helpers.limit_len(path, 15, 'R'), key, value, tablename))
    
class src_graphdb(BaseSerializeSource):

    sinktype = 'file'
    
    def __init__(self, path, tablename='graph_nodes', wheres=None, **kwargs) -> None:
        self.path = path
        self.tablename = tablename
        self.wheres = wheres

    def push(self, dc_serialized:dict, **kwargs):
        return save(dc_serialized, self.path)

    def pull(self, **kwargs):
        return load(self.path, wheres=self.wheres)

    def push_changes(self, nodes_dc_to_update, changes_to_append):
        ts = helpers.make_zulustr(helpers.get_utcnow(), remove_ms=False)
        insertr_nodes(self.path, nodes_dc_to_update, ts=ts)
        if KEEP_CHANGELOG:
            insertr_changelog(self.path, nodes_dc_to_update, tablename='changelog', ts=ts)
        insertr_changes(self.path, changes_to_append)

    def get_node(self, id_:str, **kwargs):
        return next(query_nodes(self.path, id_, self.tablename), None)

    def get_nodes(self, ids, **kwargs):
        return query_nodes(self.path, ids, self.tablename)

    def set_node(self, node_dc:dict, **kwargs):
        insertr_nodes(self.path, [node_dc], self.tablename)
        if 'changes' in kwargs:
            insertr_changes(self.path, kwargs['changes'])
        return True
    
    def set_nodes(self, nodes_dc:dict, **kwargs):
        insertr_nodes(self.path, nodes_dc, self.tablename)
        if 'changes' in kwargs:
            insertr_changes(self.path, kwargs['changes'])
        return [True for node in nodes_dc]
    
    def del_node(self, id_:str, **kwargs):
        delrow(self.path, id_, tablename=self.tablename)
        return True
    
    def del_nodes(self, ids:list, **kwargs):
        delrow(self.path, ids, tablename=self.tablename)
        return [True for id_ in ids]

    def get_change(self, id_, **kwargs):
        return __query_changes(self.path, id_, self.tablename)

    def set_change(self, change_dc:str, **kwargs):
        insertr_changes(self.path, [change_dc])
        return True
    
    def set_changes(self, changes_dc:list, **kwargs):
        insertr_changes(self.path, changes_dc)
        return [True for change in changes_dc]

    def del_change(self, id_, **kwargs):
        delrow(self.path, [id_], 'graph_changes')
        return True
    
    def del_changes(self, ids, **kwargs):
        delrow(self.path, ids, 'graph_changes')
        return [True for i in ids]



# if __name__ == "__main__":
    
#     import json 
#     import time

#     from ReqTracker.core import graph_main, graph_base, schema

#     t = time.time()
#     pth = r'C:\temp\temp_db' + f'_{int(t)}.sqlite'
#     pth_in = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'

#     dc = get_project_var(pth_in, 'display_style', default='fallback') 

#     print(dc)

    # with open(pth_in, 'r') as fp:
    #     dc = json.load(fp)
    
    # graph = graph_main.Graph.deserialize(dc)

    # save(pth, graph.serialize())

    
    
    # k = 'VS.TEST'
    # graph.add_node(graph_main.construct(dict(id=k, text='SOME TEST SCRIPT')))
    # graph[k].add_note('This is only for testing!')

    # nodes, changes = graph.get_changed_objects_since_constructed()

    # push_changes(pth, nodes, changes)
    # graph.mark_updated()

    # g2 = graph_main.Graph.deserialize(load(pth))

    # print(g2.get_changes(since_construction=False))

    # print(g2[k].to_dict(add_parents=True))

    # assert json.dumps(g2.serialize()) == json.dumps(graph.serialize())

    # with open(pth_in, 'r') as fp:
    #     dc = json.load(fp)
    
    # graph = graph_main.Graph.deserialize(dc)

    # pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'
    # save(pth, graph.serialize())