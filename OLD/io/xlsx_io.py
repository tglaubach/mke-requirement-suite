import re
import warnings
import json

import openpyxl

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    


from ReqTracker.core import helpers
from ReqTracker.core.graph_base import limit_len, STATUS, colors

from ReqTracker.io.base_io import BaseSerializeSource

log = helpers.log


splitnote = '\n\n' + '-'*100 +  '\n\n'



def load(pth):
    warnings.warn('loading an excel file will not load any data attached to a node!')
    wb = openpyxl.load_workbook(pth, read_only=True, keep_vba=False, data_only=True)
    sheetname_main = wb.sheetnames[0]
    sheetname_hist = wb.sheetnames[1] if len(wb.sheetnames) > 1 else ''

    ws = wb[sheetname_main]

    mat = list(ws.values)
    
    split_str = lambda x, y: x.split(y) if x else list()

    map_str = lambda x: '' if x is None else x

    def fnotes(txt):
        notes = split_str(txt, splitnote)
        notes_dc = {}
        for note in notes:
            parts = note.split('\n')
            if parts:
                key = parts[0]
                text = '\n'.join(parts[1:])
                notes_dc[key] = text
        return notes_dc
    
    def check_has(x):
        assert x, 'the given object must not be empty or None'
        return x
    
    mapper = [  (1, 'id', check_has),
                (2, 'text', map_str),
                (3, '_status', check_has),
                (4, 'tags', lambda x: x.split() if x else list()),
                (5, 'notes', fnotes),
                (6, 'parents', lambda x: x.split() if x else list()),
                (7, 'error', map_str),
                (8, 'data', lambda x: json.loads(x) if x else {})
              ]
    



    fun = lambda r: {k:f(r[i]) for i, k, f in mapper}
    recs = [fun(r) for r in mat[1:]]
    
    if sheetname_hist:
        ws = wb[sheetname_hist]
        mat = list(ws.values)
        changes = [(t, s) for (t, s) in mat[1:]]

    return {'nodes': recs, 'changes': changes}
        
        

def save(dc_serialized, pth):

    def to_row_xls(row):
        if 'parents' in row and row['parents']:
            parents = [c for c in row['parents']]
        else:
            parents = []

        return [
            row['type'],
            row['id'], 
            row['_text'],
            (row['_status'], colors[STATUS[row['_status']]]),
            '\n'.join(row['tags']),
            splitnote.join('{}\n{}'.format(k, v) for k, v in row['notes'].items()),
            '\n'.join(parents),
            limit_len(row['error'], 120),
            json.dumps(row['data'])
        ]
    
    cols = {
        'type': {'width': 20},
        'id': {'width': 18},
        'text': {'width': 80},
        'status': {'width': 18},
        'tags': {'width': 20},
        'notes': {'width': 80},
        'parents': {'width': 20},
        'error': {'width': 40},
        'data': {'width': 40},
    }

    rows = [to_row_xls(n) for n in dc_serialized['nodes']]
    stati = [str(s.name) for s in STATUS]
    # stati = '"{}"'.format(','.join(stati))
    # dv = openpyxl.worksheet.datavalidation.DataValidation(type="list", formula1=stati, allow_blank=True)
    
    
    wb = openpyxl.Workbook()
    
    # ws = wb.worksheets[0]
    wb.create_sheet('main')
    del wb['Sheet']


    ws = wb['main']


    #ws['A1'] = 1
    for j, h in enumerate(cols.keys()):
        ws.cell(row=1, column=j+1).font = openpyxl.styles.Font(bold=True)
        ws.cell(row=1, column=j+1).value = h

    for i, row in enumerate(rows):
        for j, v in enumerate(row):
            if isinstance(v, tuple):
                v, color = v
                color = color.replace('#', '')
                color = '80' + color if len(color) == 6 else color
                color = openpyxl.styles.colors.Color(rgb=color)
                color = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=color)
                ws.cell(row=i+2, column=j+1).fill = color
                

            ws.cell(row=i+2, column=j+1).alignment = openpyxl.styles.Alignment(wrapText=True)
            ws.cell(row=i+2, column=j+1).value = v
    
        
    for i, c in enumerate(cols.values(), 1):  # ,1 to start at 1
        ws.column_dimensions[openpyxl.utils.get_column_letter(i)].width = c['width']

    c = ws['B2']
    ws.freeze_panes = c
    ws.auto_filter.ref = ws.dimensions

    # ws.add_data_validation(dv)
    # c = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[list(cols.keys()).index('status')]
    # dv.add(c + '2:' + c + '1048576')
    
    wb.create_sheet('history')
    ws = wb['history']
    
    changes = dc_serialized['changes']
    if changes:
        ws.cell(row=1, column=1).value = 'time'
        ws.cell(row=1, column=1).font = openpyxl.styles.Font(bold=True)

        ws.cell(row=1, column=2).value = 'changenote'
        ws.cell(row=1, column=2).font = openpyxl.styles.Font(bold=True)

        for i, (t, s) in enumerate(changes, 2):
            ws.cell(row=i, column=1).value = helpers.make_zulustr(t) if not isinstance(t, str) else t
            ws.cell(row=i, column=2).value = s



    log.info(f'writing -> {pth}')

    wb.save(pth)


class src_xlsxdb(BaseSerializeSource):
    
    sinktype = 'file'
    
    def push(self, dc_serialized:dict, **kwargs):
        return save(dc_serialized, path=self.path)
    
    def pull(self, **kwargs):
        return load(self.path)


if __name__ == '__main__':

    import json 
    import time

    from ReqTracker.core import graph_main, graph_base, schema

    t = time.time()
    pth = r'C:\temp\temp_db' + f'_{int(t)}.xlsx'
    pth_in = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.json'

    with open(pth_in, 'r') as fp:
        dc = json.load(fp)
    
    graph = graph_main.Graph.deserialize(dc)

    save(pth, graph.serialize())
    
    
    k = 'VS.TEST'
    graph.add_node(graph_main.construct(dict(id=k, text='SOME TEST SCRIPT')))
    graph[k].add_note('This is only for testing!')

    save(pth, graph.serialize())

    g2 = graph_main.Graph.deserialize(load(pth))

    print(g2.get_changes(since_construction=False))

    print(g2[k].to_dict(add_parents=True))

    if json.dumps(g2.serialize()) != json.dumps(graph.serialize()):
        with open('tmp_b.json', 'w') as fp:
            json.dump( graph.serialize(), fp, indent=2)
        with open('tmp_a.json', 'w') as fp:
            json.dump( g2.serialize(), fp, indent=2)
            
    assert json.dumps(g2.serialize()) == json.dumps(graph.serialize())
