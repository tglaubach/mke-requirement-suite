import requests
import json
import io
import getpass

from urllib.parse import urlencode



from ReqTracker.core import graph_main, graph_base, helpers

from ReqTracker.io.base_io import BaseSerializeSource

log = helpers.log


is_test_client = False


def make_url(lst):
    return '/'.join([str(c).strip('/') for c in lst if c])

def __chk_ret(r):
    if hasattr(r.json, '__call__'):
        assert r.status_code < 300, f"{r.status_code} {r.text}"
        return r.json()
    else:
        assert r.status_code < 300, f"{r.status_code} {json.dumps(r.json)}"
        return r.json




def get(url:str, params:dict=None, auth=None) -> dict:
    
    log.debug(f'GET   | {url}' + '' if not auth else auth[0])
    # log.trace(f'{json.dumps(params)}')
    r = requests.get(url, params=params, auth=auth)
    return __chk_ret(r)


def patch(url:str, dc:dict=None, params:dict=None, auth=None) -> dict:
    log.debug(f'PATCH | {url}' + '' if not auth else auth[0])
    # log.trace(f'{json.dumps(params)} | {json.dumps(dc)}')
    r = requests.patch(url, params=params, json=dc, auth=auth)
    return __chk_ret(r)


def post(url:str, dc:dict=None, params:dict=None, auth=None) -> dict:
    log.debug(f'POST | {url}' + '' if not auth else auth[0])
    # log.trace(f'{json.dumps(params)} | {json.dumps(dc)}')
    r = requests.post(url, params=params, json=dc, auth=auth)
    return __chk_ret(r)




def get_by_id(base_url, id):
    url = make_url((base_url, 'api', 'v1', 'node'))
    return get(url, params=dict(id=id))
    

def save(base_url, dc_serialized):
    url = make_url((base_url, 'api', 'v1', 'graph'))
    post(url, dc=dc_serialized['nodes'])
    log.info(f'posting -> {url}')

def push_changes(base_url, nodes_dc_to_update, changes_to_append):
    url = make_url((base_url, 'api', 'v1', 'graph', 'push_changes'))
    dc = {}
    dc['nodes_dc_to_update'] = nodes_dc_to_update
    dc['changes_to_append'] = changes_to_append
    patch(url, dc=dc)
    log.info(f'patching -> {url}')
    
def load(base_url):
    url = make_url((base_url, 'api', 'graph'))
    ret =  get(url)
    log.info(f'getting -> {url}')
    return ret

def login():
    usr = getpass.getuser()
    usr2 = input('username (empty for default={}): '.format(usr))
    if usr2:
        usr = usr2

    return (usr, getpass.getpass('pw'))

class api():

    def __init__(self,base_url, auth=()) -> None:
        if auth == 'login':
            auth = login()

        self.url = base_url
        self.auth = auth

    def add_node(self, **kwargs):
        url = make_url((self.url, 'api', 'node', 'add_node'))
        log.info(f'posting -> {url}')
        return post(url, dc=kwargs, auth=self.auth)

    def add_note(self, id, text):
        url = make_url((self.url, 'api', 'node', 'add_note'))
        log.info(f'patching -> {url}')
        return patch(url, dc=dict(id=id, text=text), auth=self.auth)
    
    def add_data(self, id, key, value):
        url = make_url((self.url, 'api', 'node', 'add_data'))
        log.info(f'patching -> {url}')
        return patch(url, dc=dict(id=id, key=key, value=value), auth=self.auth)
    
    def set_status(self, id, status_new):
        url = make_url((self.url, 'api', 'node', 'set_status'))
        log.info(f'patching -> {url}')
        return patch(url, dc=dict(id=id, status=status_new), auth=self.auth)
    
class src_api(BaseSerializeSource):

    sinktype = 'api'

    def __init__(self, path, auth=(), nochanges=False, **kwargs) -> None:
        if auth == 'login':
            auth = login()
        
        self.url = path
        self.auth = auth
        self.nochanges = nochanges

        log.debug('CONSTRUCTED: ' + self.name + ' with:  NO_AUTH' if not self.auth else ' with: ' + self.auth[0])
        
    @property
    def name(self):
        return self.url
    
    @property
    def path(self):
        return self.url
    

    def get_by_id(self, id):
        url = make_url((self.url, 'api', 'v1',  'node'))
        return get(url, params=dict(id=id), auth=self.auth)

    
    def push(self, dc_serialized):
        url = make_url((self.url, 'api', 'v1',  'graph'))
        post(url, dc=dc_serialized, auth=self.auth)
        log.debug(f'posting -> {url}')

    def pull(self):
        url = make_url((self.url, 'api', 'v1',  'graph'))
        if self.nochanges:
             url += '?nochanges=1'

        ret =  get(url, auth=self.auth)
        log.debug(f'getting -> {url}')
        return ret
    
    def ping(self):
        url = make_url((self.url, 'api', 'v1', 'ping'))
        r = requests.get(url, auth=self.auth)
        log.debug(f'getting -> {url}')
        return r.status_code == 200
    
    def push_changes(self, nodes_dc_to_update, changes_to_append):
        url = make_url((self.url, 'api', 'v1', 'graph', 'push_changes'))
        patch(url, dc={'nodes_dc_to_update': nodes_dc_to_update, 'changes_to_append': changes_to_append}, auth=self.auth)
        log.debug(f'posting -> {url}')
