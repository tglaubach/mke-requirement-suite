import os
from whoosh import index, highlight, writing
from whoosh.fields import Schema, ID, TEXT, NUMERIC, STORED
from whoosh.index import create_in, open_dir, FileIndex
from whoosh.qparser import QueryParser

from whoosh.query import FuzzyTerm, Variations
# from whoosh.analysis import StemmingAnalyzer, CharsetFilter
# from whoosh.support.charset import accent_map

from ReqTracker.core import helpers

log = helpers.log



DEFAULT_INDEX_DIR = 'wooosh_index'

fields = 'id title content source page n_pages ltc link checksum'.split()

# my_analyzer = StemmingAnalyzer() | CharsetFilter(accent_map)

def get_schema():
    return Schema(
        id=ID(unique=True, stored=True), 
        title=TEXT(stored=True), 
        content=TEXT(stored=True), #analyzer=my_analyzer, 
        source=TEXT(stored=True), 
        page=NUMERIC(stored=True), 
        n_pages=NUMERIC(stored=True), 
        ltc=TEXT(stored=True), 
        link=TEXT(stored=True),
        checksum=TEXT(stored=True))

def get_indexer(pth = DEFAULT_INDEX_DIR, force_clean=False):
    if isinstance(pth, FileIndex):
        return pth
    elif not os.path.exists(pth):
        os.mkdir(pth)
        return create_in(pth, schema=get_schema())
    else:
        return create_in(pth, schema=get_schema()) if force_clean else open_dir(pth)

class AttrDict(dict):
    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value

def add_doc(writer, node, doc):

    for i, page in enumerate(doc, 1):
        if isinstance(page, str):
            tmp = AttrDict()
            tmp.page_content = page
            tmp.metadata = {'source': node.get_latest_ver()['path']}
            page = tmp
            

        writer.add_document(id=node.id, 
                            title=node.title, 
                            ltc=node.last_time_changed, 
                            link = node.link,

                            content=page.page_content, 
                            source=page.metadata['source'] if 'source' in page.metadata else '', 
                            page=page.metadata['page'] +1 if 'page' in page.metadata else i, 
                            n_pages=page.metadata['total_pages'] if 'total_pages' in page.metadata else 0)
        
def clean_index(index_dirname, new_docs:dict):
    # Always create the index from scratch
    ix = get_indexer(index_dirname, force_clean=True)
    writer = ix.writer()

    for id_, (node, content) in new_docs:
        add_doc(writer, node, content)

    writer.commit()



def incremental_index(index_dirname, new_docs:dict, force_clean=False, verb=False):
    
    ix = get_indexer(index_dirname, force_clean=force_clean)
    # The set of all paths in the index
    indexed_ids = set()
    # The set of all paths we need to re-index
    to_index = set()

    with ix.searcher() as searcher:
        writer = ix.writer()

        # Loop over the stored fields in the index
        for fields in searcher.all_stored_fields():
            indexed_id = fields['id']
            indexed_ids.add(indexed_id)

            if indexed_id in new_docs:
                # The file has changed, delete it and add it to the list of files to reindex
                writer.delete_by_term('id', indexed_id)
                to_index.add(indexed_id)

        cnt = 1
        for id_, (node, content) in new_docs.items():
            if verb:
                v = os.path.basename(node.get_latest_ver()['path'])
                log.info(f'{cnt}/{len(new_docs)}: {id_} ({v})')
            cnt += 1
            if id_ in to_index or id_ not in indexed_ids:
                # This is either a file that's changed, or a new file that wasn't indexed before. So index it!
                add_doc(writer, node, content)

        writer.commit()

def index_my_docs(index_dirname, clean=False):
    if clean:
        clean_index(index_dirname)
    else:
        incremental_index(index_dirname)




def get_n_docs(ix=DEFAULT_INDEX_DIR):
    if isinstance(ix, str):
        ix = open_dir(ix)
    with ix.searcher() as searcher:
        n = searcher.doc_count()
        log.debug(f'TOTAL N= {n} ELEMENTS IN WOOOSH DB')
        return n


def get_doc(id_, ix=DEFAULT_INDEX_DIR):
    if isinstance(ix, str):
        ix = open_dir(ix)

    with ix.searcher() as searcher:
        return searcher.stored_fields(id_)

def hit2dc(hit, **kwargs):
    dc = {k:hit[k] for k in fields}
    dc['content'] = hit.content
    dc['highlights'] = hit.highlights("content")
    dc['score'] = hit.score
    dc = {**dc, **kwargs}
    return dc

class HtmlMarkFormatter(highlight.Formatter):
    """Puts html.mark around the matched terms.
    """
    def format_token(self, text, token, replace=False):
        tokentext = highlight.get_text(text, token, replace)
        return "<mark>%s</mark>" % tokentext



def wsearch(searchstring, field='content', formatter=None, fragmenter=None, ix=DEFAULT_INDEX_DIR):

    if fragmenter is None:
        fragmenter = highlight.SentenceFragmenter(sentencechars='.?!\n', charlimit=1_000)
    if formatter is None:
        formatter = HtmlMarkFormatter() #highlight.UppercaseFormatter()

    with ix.searcher() as searcher:
        query = QueryParser(field, ix.schema, termclass=Variations).parse(searchstring)
        
        results = searcher.search(query, terms=True)
        results.fragmenter = fragmenter
        results.formatter = formatter
        
        res = []
        for hit in results:
            dc = searcher.stored_fields(hit.docnum)
            dc['highlights'] = hit.highlights("content")
            dc['score'] = hit.score
            res.append(dc)

        return res, results.runtime
    






