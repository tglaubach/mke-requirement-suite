#!/usr/bin/python3
""" 
the ui server providing a plotly dash UI
"""


import os, sys, inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from ReqTracker.core import helpers

if os.environ.get('IS_PRODUCTIVE'):
    helpers.log.setLevel(os.environ.get('LOGGING_LVL', 'INFO'))
    dbg = False
else:
    dbg = True


helpers.logging.getLogger('werkzeug').setLevel(helpers.logging.ERROR)

from ReqTracker.ui_server import app

if __name__ == "__main__":
    app.run_server(host='0.0.0.0', port=8080, debug=dbg, use_reloader=False)  # Turn off reloader if inside Jupyter