



import base64
import json
import re
import textwrap
import io
from ReqTracker.io_files import nextcloud_interface
from flask import Flask, request, jsonify, send_file, send_from_directory, render_template, render_template_string, Response
import urllib
import shutil

import numpy as np

import traceback


import datetime
import time
import markdown

# from dash_bootstrap_templates import load_figure_template


# load_figure_template("litera")

# add to path for testing
import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, current_dir)
    sys.path.insert(0, parent_dir)


import ReqTracker

from ReqTracker.core import node_factory, schema, graph_main, graph_base, helpers

from ReqTracker.ui import helpers_ui, ui_state
from ReqTracker.ui_vmc import vmc_docmaker, html_render
from ReqTracker.api import uib

log = helpers.log
tostr = helpers.tostr



GetGraphHold = ui_state.GetGraphHold
add_log_alert = ui_state.add_log_alert



"""
 ██████  ██       ██████  ██████   █████  ██      ███████ 
██       ██      ██    ██ ██   ██ ██   ██ ██      ██      
██   ███ ██      ██    ██ ██████  ███████ ██      ███████ 
██    ██ ██      ██    ██ ██   ██ ██   ██ ██           ██ 
 ██████  ███████  ██████  ██████  ██   ██ ███████ ███████ 
"""                                        


from ReqTracker.api.static_content import NAVBAR, CSS, HEADER

COLORS = '\n'.join([f'classDef {state} stroke:{col},stroke-width:2px;' for state, col in graph_base.colorss.items()])

"""

 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌          ▐░▌          ▐░▌     ▐░▌            ▐░▌
▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                            
"""

state_map_dc = {
    'FAIL': '\u274C',
    'CHECK': '\u2705',
    'WIP': '\u23F3',
    'QM': '\u2753',
    'OPEN': '\u21CB'
}



def mk_link(id_, label=None, pth='main'):
    return f'<a href="/uim/v1/{pth}/{urllib.parse.quote_plus(id_)}" target="_self">{label if label else id_}</a>'


def rid2mid(n, pre='N_'):
    s = n.id if (not isinstance(n, str) and hasattr(n, 'id'))  else n  
    return pre + re.sub('[^0-9a-zA-Z]+', '_', s)

def node_el2node(n, id_only=False, render_state=False, as_url=True, with_color=True):
    id_ = rid2mid(n)
    
    if id_only :
        s = n.id
        txt = newline = ''
    else:
        s = f'{n.id}  ({n.status.name})'
        newline = '\\n'
        txt = helpers.limit_len(n.text, 35).replace("\n", " ")

    s = f'{s}{newline}{txt}'
    if as_url:
        s = mk_link(n.id, s)

    if render_state:
        raise NotImplementedError("still working on showing state")
    
    ret = f'{id_}["{s}"]'

    if with_color:
        ret += f':::{n.status.name}'

    return ret + ';'





def node_el2state(graph, id_, container_types=None):
    fid2 = lambda x: rid2mid(x, '')
    fun = lambda a, b: f'   {fid2(b)} --> {fid2(a)}'

    container = graph[id_]

    children = container.children
    if children:
        edges = []
        subcs = []

        for child in children:
            if container_types and isinstance(child, container_types):
                subcs.append(node_el2state(graph, child, container_types))
            else:
                edges += child.to_graph().get_edges(as_id=True)

        flow = '\n'.join([fun(*e) for e in edges])
        flow += '\n'+'\n'.join(f'{fid2(c)} --> [*]' for c in children)
        flow += '\n\n'.join(subcs)

    else:
        flow = '\n[*] --> [*]'
    proto = f'state {fid2(container.id)} ' + '{\n' + flow + '\n}'
    return proto


def node_el2sysml(n):
    mid = rid2mid(n.id, '')

    # type: requirement, functionalRequirement, interfaceRequirement, performanceRequirement, physicalRequirement, designConstraint
    
    if (n.data.get('sysml', {}).get('type', None)):
        tp = n.data.get('sysml', {}).get('type', None)
    elif 'requirement' in n.classname.lower():
        tp = 'requirement'
    else:
        tp = 'element'

    if tp == 'element':
        
        url = f'/uib/edit/{n.id}' if not issubclass(type(n), schema.BaseVersionedDocument) else n.link

        tpl = (mid, n.id, url, False)
        
    else:
        id_ = n.id
        risk = n.data.get('sysml', {}).get('risk', 'medium')
        verifymethod = n.data.get('sysml', {}).get('verifymethod', None)

        if verifymethod is None:
            if 'test' in n.classname.lower():
                verifymethod = 'test'
            elif 'design' in n.classname.lower():
                verifymethod = 'analysis'
        
        if len(n.text) > 100:
            text = helpers.limit_len(n.text, 100)
        else:
            text = n.text
        text = text.replace('\n', ' ')

        # defaults
        if not text:
            text = id_
        if not verifymethod:
            verifymethod = 'demonstration' 

        tpl = (tp, mid, id_, text, risk, verifymethod, True)
        
    return tpl


def mmhelp_wrap_graph(s):
    
    return f"""<pre class="mermaid">
{s}

{COLORS}
    
</pre>
"""

def mmg_graph(g, TDLR = 'LR', graphtype='graph', do_warp=True):
    edges = g.get_edges(as_id=True, with_root=False)
    nodes = g.vn

    fun = lambda a, b: f'{rid2mid(b)} --> {rid2mid(a)}'
    flow = '\n'.join([fun(*e) for e in edges])
    defs = '\n'.join([node_el2node(n) for n in nodes if n.id != g.root_node.id])
    s = f'{graphtype} {TDLR}\n{defs}\n\n{flow}'
    return mmhelp_wrap_graph(s) if do_warp else s

def mmg_statediagram(graph, start_ids, container_types=None):
    fun = lambda x: node_el2state(graph, x, container_types)
    elss = '\n\n'.join([fun(x) for x in start_ids])
    return mmhelp_wrap_graph(f"stateDiagram-v2\n\n{elss}")



classes = [graph_base.BaseNode, schema.RequirementContainer, schema.Requirement, 
            schema.VerificationDesignAnalysis, schema.VerificationRequirement, 
            schema.VerificationAnalysis, schema.VerificationEvent, 
            schema.VerificationTest,
            schema.VerificationExperimentScript, schema.VerificationAnalysisScript,
            schema.VerificationRequirementTest, schema.VerificationRequirementDesign,
            schema.WorkPackage,
            schema.Document,
            schema.RequestForDeviation, schema.RequestForWaiver, schema.Defect]

def mmg_sysml(g):
    # contains copies derives satisfies verifies refines traces
    couplings = {
        (schema.Requirement, schema.VerificationRequirement): '{child} - copies -> {parent}',
        (schema.VerificationRequirement, schema.VerificationRequirementDesign): '{child} - refines -> {parent}',
        (schema.VerificationRequirement, schema.VerificationRequirementTest): '{child} - refines -> {parent}',
        (schema.VerificationRequirementTest, schema.VerificationAnalysis): '{child} - satisfies -> {parent}',
        (schema.VerificationRequirementDesign, schema.VerificationDesignAnalysis): '{child} - satisfies -> {parent}',
        (schema.VerificationTest, schema.VerificationEvent): '{child} - contains -> {parent}',
        }
    allother = '{child} - contains -> {parent}'

    f2 = lambda p, c: couplings.get((type(p), type(c)), allother)
    fun = lambda p, c: '   ' + f2(c, p).format(parent=rid2mid(p, ''), child=rid2mid(c, ''))
    flow = '\n'.join([fun(*t) for t in g.get_edges()])

    lst = [node_el2sysml(n) for n in g.vn]
    gid = f'G_{id(g)}'
    tmplt = """

requirementDiagram

{% for n in REPLACEME %}
    {% if n[-1] %}
    {{ n[0] }} {{ n[1] }} {
        id: "{{ n[2] }}"
        text: "{{ n[3] }}"
        risk: {{ n[4] }}
        verifymethod: {{ n[5] }}
        }
    {% else %}
    element {{ n[0] }} {
        type: "{{ n[1] }}"
        docRef: "{{ n[2] }}"
        }
    {% endif %}
{% endfor %}

""".replace('REPLACEME', gid) + flow
    
    return mmhelp_wrap_graph(tmplt), {gid:lst}





    

def make_page(content, js='', nohead=False):
    mm = """<script type="module">
    import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
    mermaid.initialize({ startOnLoad: true });
  </script>
"""
    
    page = f'''<!DOCTYPE html>
    <html>

    <head>
    <title>ReqTracker-UI MeerMaid</title>
        <script src="https://cdn.plot.ly/plotly-2.32.0.min.js" charset="utf-8"></script>
        {"" if nohead else HEADER}
    </head>

        <body>
            <div>{"" if nohead else NAVBAR}</div>
            <div id="main-content">
{content}
            </div>

{mm}
            <script>
{js}
            </script>
        </body>
    </html>'''
    return page


"""
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          ▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀█░█▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌     ▐░▌  ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌                    ▐░▌
▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
       
"""

def add_routes(server):
    
    
    @server.route('/api/v1/graphb', methods=['GET'])
    @server.route('/api/v1/graphb/<string:id>', methods=['GET'])
    def r_api_graphb(id=None):
        try:    
            with GetGraphHold() as graph:
                id = request.args.get('id', id)
                nn_max = request.args.get('nn', 25)
                TDLR = request.args.get('TDLR', 'LR')
                graphtype = request.args.get('graphtype', 'graph')
                if not id:
                    id = graph.root_node.id
                if not id in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                
                n = graph[id]
                gv = n.nng(nn_max)

                content = mmg_graph(gv, TDLR, graphtype, do_warp=False)
            return jsonify({'mm': content, 'id': id})
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500


    @server.route('/uim/v1/graph', methods=['GET'])
    @server.route('/uim/v1/graph/<string:id>', methods=['GET'])
    def r_uim_graph(id=None):
        print('/uim/v1/graph/')
        try:    
            with GetGraphHold() as graph:
                id = request.args.get('id', id)
                nn_max = request.args.get('nn', 25)
                TDLR = request.args.get('TDLR', 'LR')
                graphtype = request.args.get('graphtype', 'graph')
                if not id:
                    fun = lambda s: f'<div style="padding:1px; border:1px black solid; border-radius:5px">{s}</div>'
                    

                    content = '\n\n'.join([fun(mmg_graph(graph[n].nng(nn_max), TDLR, graphtype)) for n in graph.vn if isinstance(n, schema.Requirement)])
                elif not id in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                else:
                    content = mmg_graph(graph[id].nng(nn_max), TDLR, graphtype)

                
            tmpl = make_page(content)
            return render_template_string(tmpl)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500

    
    @server.route('/uim/v1/reqs', methods=['GET'])
    def r_uim_reqs():

        print('/uim/v1/reqs/')
        try:    
            with GetGraphHold() as graph:
                nmax = int(request.args.get('nmax', 0))
                dish = request.args.get('dish', '')
                gv = graph.filt_by_dish(dish_id=dish) if dish else graph
                
                reqs = [n.id for n in gv.vn if isinstance(n, schema.Requirement)]
                container_types= (schema.VerificationRequirementTest, schema.VerificationRequirementDesign, schema.VerificationRequirement, schema.Requirement)
                reqs = reqs[:nmax] if nmax else reqs
                content = '\n\n'.join([mmg_statediagram(gv, [cc], container_types) for cc in reqs])
            tmpl = make_page(content)
            return render_template_string(tmpl)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500


    @server.route('/uim/v1/sysml', methods=['GET'])
    def r_uim_sysml():

        print('/uim/v1/reqs/')
        try:    
            with GetGraphHold() as graph:
                nmax = int(request.args.get('nmax', 0))
                id_ = request.args.get('id', None)
                dish = request.args.get('dish', '')
                use_reqs = int(request.args.get('reqs', 1 ))

                g = graph
                if id_:
                    g = graph[id].to_graph()
                if dish:
                    g = graph.filt_by_dish(dish_id=dish)
                
                if use_reqs:
                    reqs = [n.id for n in g.vn if isinstance(n, schema.Requirement)]
                else:
                    reqs = g.get_lvl0_nodes()

                reqs = reqs[:nmax] if nmax else reqs

                content, kwargs = '', {}
                for nid in reqs:
                    c, k = mmg_sysml(g[nid].to_graph())
                    content += '\n\n' + c
                    for key in k:
                        kwargs[key] = k[key]
                
            tmpl = make_page(content)
            return render_template_string(tmpl, **kwargs)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500

    
    
    @server.route('/uim/v1/show', methods=['GET'])
    @server.route('/uim/v1/show/<string:id>', methods=['GET'])
    def r_uim_show(id=None):
        print('/uim/v1/graph/')
        try:    
            with GetGraphHold() as graph:
                id = request.args.get('id', id)
                nn_max = request.args.get('nn', 25)
                TDLR = request.args.get('TDLR', 'LR')
                graphtype = request.args.get('graphtype', 'graph')
                if not id:
                    id = graph.root_node.id
                if not id in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                
                n = graph[id]
                gv = n.nng(nn_max)

                right = mmg_graph(gv, TDLR, graphtype)

                lst = [html_render.mk_tpl(n.id, str(n), p0='uim', v='v1') for n in graph.nodes]
                left = """<div id="links">
    {% for (h, t) in urllist %}
        <div><a id="{{ h }}" href="{{ h }}">{{ t }} </a></div>
    {% endfor %}
    </div>"""
                
            content = f'''<div style="width: 100%; overflow: hidden;">
    <div id="left" style="width: 50%; float: left; height: 100%; overflow-y: scroll;  overflow-x: scroll;"> {left} </div>    
    <div id="right" style="margin-left: 52%; height: 100%; overflow-y: scroll;  overflow-x: scroll;"> {right} </div>
</div>'''
                
            tmpl = make_page(content)
            return render_template_string(tmpl, urllist=lst)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        

        
    @server.route('/uim/v1/main', methods=['GET'])
    @server.route('/uim/v1/main/<string:id>', methods=['GET'])
    def uim_main(id=None):

        try:    
        

            args = request.args
            id = args.get('id', id)
            params_dc = {**dict(p0='uim', v='v1', pth='main', nohead='1'), **args}

            js = ''
            kwargs = {}

            with GetGraphHold() as graph:
                id = request.args.get('id', id)
                nn_max = request.args.get('nn', 15)
                TDLR = request.args.get('TDLR', 'LR')
                graphtype = request.args.get('graphtype', 'graph')
                if not id:
                    id = graph.root_node.id
                if not id in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                
                n = graph[id]

                right = mmhelp_wrap_graph(mmg_graph(n.nng(nn_max), TDLR, graphtype, do_warp=False))
                

                # left = mk_iframe(src=f'{request.host_url}/uib/v1/table?{params}', size='100%', id='iframe_table')
                # left = f'<div style="padding:5px;border:1px black solid; border-radius:5px">{left}</div>'

                content, jsi, kwargsi = uib.get_table_all(graph, **params_dc)
                left = content
                js += '\n\n' + jsi
                kwargs = {**kwargs, **kwargsi}

                content, jsi, kwargsi = uib.get_edit_all(graph, id, **params_dc)
                right += '\n\n' + content
                js += '\n\n' + jsi
                kwargs = {**kwargs, **kwargsi}


            js += '\n\n' + """function toggle_iframe(ele) {
    var source = document.getElementById("iframe_path");
    var target = document.getElementById("iframe_graph_ploty");
    target.src = window.location.origin + source.value;

    if (target.hidden) {
        target.hidden=false;
    } else {
        target.hidden=true;
    }
}

async function refresh_data(ele) {
    const apiUrl = window.location.origin + '/api/v1/plotlydata/sunburst' + window.location.search;
    var default_s = "Click here to refresh plot data...";
    document.getElementById('btn_refresh_data').innerText = "Fetching..."
    const response = await fetch(apiUrl);
    if (!response.ok) {
      document.getElementById('btn_refresh_data').innerText = "ERROR: Network response was not OK"
    }
    const userData = await response.json();
    var graph_ploty = document.getElementById('graph_ploty');
    
    Plotly.newPlot( graph_ploty, userData.data, userData.layout);
    document.getElementById('btn_refresh_data').innerText = default_s;
}"""

            # figsize = int(request.args.get('figsize', '300'))
            tmp = f'<div id="graph_ploty" style="overflow-y: scroll;"></div>'
            tmp = '<div><button id="btn_refresh_data" onclick="refresh_data(this)">Click here to refresh plot data...</button></div>' + tmp

            # plottype = request.args.get('type', 'sunburst')
            # maxdepth = int(request.args.get('maxdepth', '10'))
            # figsize = int(request.args.get('figsize', '600'))
            # lyoutg = request.args.get('layoutg', 'auto')
            # iframe_src = f'/uib/v1/plot_ly?figsize={figsize}&type={plottype}&maxdepth={maxdepth}&lyoutg={lyoutg}'
            # sel = f'<div><input type="text" id="iframe_path" value="{iframe_src}" style="min-width:60%;" ><button onclick="toggle_iframe(this)">Click here to toggle graph visibility...</button></div>'
            # tmp = sel + f'\n\n<div><iframe id="iframe_graph_ploty" style="width:98%; min-height:400px; height:{figsize//3*2}px;" hidden="true" name="graph_ploty" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" ></iframe></div>'
            left = tmp + left
            
            right = f'<div style="padding:5px; border:1px black solid; border-radius:5px"><h3>Graph for Node: {id}<h3>{right}</div>'
            left = f'<div style="padding:5px; border:1px black solid; border-radius:5px">{left}</div>'

        
            content = f'''<div style="width: 100%; overflow: hidden;">
<div id="left" style="width: 50%; float: left; height: 100%; overflow-y: scroll;  overflow-x: scroll;"> {left} </div>    
<div id="right" style="margin-left: 52%; height: 100%; overflow-y: scroll;  overflow-x: scroll;"> {right} </div>
</div>'''
            tmpl = make_page(content, js, nohead=args.get('nohead', False))                    
            return render_template_string(tmpl, **kwargs)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        
