# <ul class="horizontal">
#   <li><a href="/uib/v1/show">Main</a></li>
#   <li><a href="/uim/v1/main">mm-Main</a></li>
#   <li></li>
#   <li><a href="/uib/v1/table">Table</a></li>
#   <li><a href="/uib/v1/plot_ly">Graph</a></li>
#   <li><a href="/uim/v1/show">mm-Show</a></li>
#   <li><a href="/uim/v1/graph">mm-Graph</a></li>
#   <li><a href="/uim/v1/reqs">mm-Reqs</a></li>
#   <li><a href="/uim/v1/sysml">mm-SysML</a></li>
# </ul> 

NAVBAR = '''

 <div class="topnav">
  <a href="/uib/v1/show">Main</a>
  <a href="/uim/v1/main">mm-Main</a>
  
  <a href="/uib/v1/table">Table</a>
  <a href="/uib/v1/plot_ly">Graph</a>
  <a href="/uim/v1/show">mm-Show</a>
  <a href="/uim/v1/graph">mm-Graph</a>
  <a href="/uim/v1/reqs">mm-Reqs</a>
  <a href="/uim/v1/sysml">mm-SysML</a>
  <div class="search-container">
    <form action="/uib/v1/search">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div>
'''

CSS = '''
<style>
* {box-sizing: border-box;}

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}

/* Add a black background color to the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

/* Style the links inside the navigation bar */
.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

/* Change the color of links on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Style the "active" element to highlight the current page */
.topnav a.active {
  background-color: #2196F3;
  color: white;
}

/* Style the search box inside the navigation bar */
.topnav input[type=text] {
  float: right;
  padding: 6px;
  border: none;
  margin-top: 8px;
  margin-right: 16px;
  font-size: 17px;
}

/* When the screen is less than 600px wide, stack the links and the search field vertically instead of horizontally */
@media screen and (max-width: 600px) {
  .topnav a, .topnav input[type=text] {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;
  }
}

table {
  border-collapse: collapse;
}

ul.navbar2 {
  list-style-type: none;
  margin: 0;
  padding: 0;
}


ul.horizontal {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

ul.horizontal li {
    float: left;
}

ul.horizontal li a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

ul.horizontal li a:hover:not(.active) {
    background-color: #000;
}

ul.horizontal li a.active {
    background-color:#04AA6D;
}

ul.horizontal2 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    border: 1px solid #e7e7e7;
    background-color: #f3f3f3;
}

ul.horizontal2 li {
    float: left;
}

ul.horizontal2 li a {
    display: inline-block;
    color: #666;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

ul.horizontal2 li a:hover:not(.active) {
    background-color: #ddd;
}

ul.horizontal2 a.active {
    color: white;
    background-color: #04AA6D;
}
.width94 {
width:94%;
}
@media screen and (max-width: 600px) {
    .width94 {
       width:100%;
    }
}

ul.vertical {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 200px;
    background-color: #f1f1f1;
}

ul.vertical li a {
    display: block;
    color: #000;
    padding: 8px 0 8px 16px;
    text-decoration: none;
}

ul.vertical li a:hover:not(.active) {
    background-color: #555;
    color:white;
}

ul.vertical a.active {
    background-color: #04AA6D;
    color:white;
}

ul.gray {
border: 1px solid #e7e7e7;
    background-color: #f3f3f3;
}

ul.gray li a {
    display: block;
    color: #666;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

ul.gray li a:hover:not(.active) {
    background-color: #ddd;
}

ul.gray li a.active {
    color: white;
    background-color: #008CBA;
}
.rightli {
float:right;
}

@media screen and (max-width: 408px) {
    .rightli {
       display:none;
    }
}

ul.ex {
width:90%;
}
@media screen and (max-width: 600px) {
    ul.ex {
       width:100%;
    }
}

ul.divider li {
    float: left;
    border-right:1px solid #bbb;
}

ul.divider li:last-child {
    border-right: none;
}
ul.border {
    border: 1px solid #555;
}

ul.border li a {
    padding: 8px 16px;
}

ul.border li {
    text-align: center;
    border-bottom: 1px solid #555;
}

ul.border li:last-child {
    border-bottom: none;
}

'''

# <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
# <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
# <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
# body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
#   line-height: 1.8;

# <meta charset="UTF-8">
# <meta name="viewport" content="width=device-width, initial-scale=1">

HEADER = '''

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>


body, html {
  height: 100%;

}
''' + CSS + '\n</style>'