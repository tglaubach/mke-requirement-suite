



import base64
import json
import re
import textwrap
import io
from ReqTracker.io_files import nextcloud_interface
from flask import Flask, request, jsonify, send_file, send_from_directory, render_template, render_template_string, Response
import urllib
import shutil

import numpy as np

import traceback


import datetime
import time
import markdown

# from dash_bootstrap_templates import load_figure_template


# load_figure_template("litera")

# add to path for testing
import sys, inspect, os

from ReqTracker.io.graphdb_io import fs_search

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, current_dir)
    sys.path.insert(0, parent_dir)


import ReqTracker

from ReqTracker.core import node_factory, schema, graph_main, graph_base, helpers

from ReqTracker.ui import ui_state
from ReqTracker.ui_vmc import vmc_docmaker, html_render

log = helpers.log
tostr = helpers.tostr



GetGraphHold = ui_state.GetGraphHold
add_log_alert = ui_state.add_log_alert


"""
 ██████  ██       ██████  ██████   █████  ██      ███████ 
██       ██      ██    ██ ██   ██ ██   ██ ██      ██      
██   ███ ██      ██    ██ ██████  ███████ ██      ███████ 
██    ██ ██      ██    ██ ██   ██ ██   ██ ██           ██ 
 ██████  ███████  ██████  ██████  ██   ██ ███████ ███████ 
"""                                        


from ReqTracker.api.static_content import NAVBAR, CSS, HEADER


"""

 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌          ▐░▌          ▐░▌     ▐░▌            ▐░▌
▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                            
"""


def get_nn_plot_bytes(node, lyout, nn, format='jpg'):

    if not 'plt' in globals():
        try:
            
            from matplotlib import pyplot as plt
            import matplotlib
            matplotlib.use('svg')
            print(matplotlib.get_backend())
            
        except Exception as err:
            log.exception(err)
            raise
    
    f = plt.figure(frameon=False, figsize=(12,6))
    grid = plt.GridSpec(1, 1, left=0.1, right=0.8)
    ax = plt.subplot(grid[0, 0])

    ax.axis('off')
    f.tight_layout()
    ax = node.plot(lyout, nn_max=nn, ax=ax)
    with io.BytesIO() as fp:
        f.savefig(fname=fp, format=format)
        fp.seek(0)
        bts = fp.read()
    return bts

def get_fig_html(f):
    return f.to_html(
                config=None,
                auto_play=False,
                include_plotlyjs=True,
                include_mathjax="cdn",
                post_script=None,
                full_html=True,
                animation_opts=None,
                default_width="100%",
                default_height="100%",
                validate=False,
            )

def node2tr(n, **kwargs):

    fun = lambda x: html_render.mk_tpl(x.id, x.id)
    c = [fun(c) for c in n.children]
    p = [fun(c) for c in n.parents]

    more_children = ''
    if len(c) > 10:
        more_children = f' ... {len(c)-10} more.'
        c = c[:10]

    more_parents = ''
    if len(p) > 10:
        more_parents = f' ... {len(p)-10} more.'
        p = p[:10]
    

    status = n.status.name.replace('_', ' ')
    notes = [(k, v) for k
    , v in n.notes.items()] # v.replace('->', '--')
    #print(notes)
    txt = helpers.limit_len(n.text, 75).strip().strip('#').strip()

    if txt.startswith(n.id):
        txt = txt[len(n.id):]

    lnk, _ = html_render.mk_tpl(n.id, txt, **kwargs)

    return (n.id, (lnk, txt), (n.color, status), ' '.join(n.tags), c, p, notes, more_children, more_parents)

def get_table_html(graph, filt='', i=0, n=None, **kwargs):
    if filt:
        nodes = graph.filti(filt, ignore_case = True)
    else:
        nodes = list(graph.nodes)

    N = len(nodes)
    if not n:
        n = N

    il = max(0, int(i))
    iu = min(N, il+int(n))

    lst = [node2tr(n, **kwargs) for n in nodes[il:iu]]


    tmplt = """<table id="table">
<tr id="header_row">
    <th>Node</th>
    <th>Status</th>
    <th>Tags</th>
    <th>Children</th>
    <th>Parents</th>
    <th>Notes</th>
</tr>

{% for id_, (h, t), (scolor, status), tagss, children, parents, notes, more_children, more_parents in lst %}
    
    <tr id="trow_{{ id_ }}" style="border:1px black solid">
        <td style="border:1px black solid">
            <a id="{{ h }}" href="{{ h }}" target="self">{{ id_ }}</a><div>{{ t }}</div>
        </td>
        <td style="border:1px black solid">
            <div style="color:{{ scolor }}">{{ status }}</div>
        </td>
        <td style="border:1px black solid">{{tagss}}</td>
        <td style="border:1px black solid">
            {% for h2, t2 in children %}
                <div><a id="{{ h2 }}" href="{{ h2 }}" style="font-size: 0.8em;" target="self">{{ t2 }} </a></div>
            {% endfor %}
            {{ more_children }}
        </td>
        <td style="border:1px black solid">
            {% for h3, t3 in parents %}
                <div><a id="{{ h3 }}" href="{{ h3 }}" style="font-size: 0.8em;" target="self">{{ t3 }} </a></div>
            {% endfor %}
            {{ more_parents }}
        </td>
        <td style="border:1px black solid">
            {% for h4, t4 in notes %}
                <div style="font-size: 0.8em;"><b>{{ h4 }}</b>: {{ t4 }}</div>
            {% endfor %}
        </td>
    </tr>

{% endfor %}

    </table>"""
    return tmplt, lst

def get_table_all(graph=None, filt='', i=0, n=None, **kwargs):

    if not graph is None:
        tmplt, lst = get_table_html(graph, filt, i, n, **kwargs)
    else:
        with GetGraphHold() as graph:
            tmplt, lst = get_table_html(graph, filt, i, n, **kwargs)

# function search(ele) {
# if(event.key === 'Enter') {
    
#     const urlParams = new URLSearchParams(window.location.search);
#     urlParams.set('filt', ele.value);
#     window.location.search = urlParams;
# }
# }
    js = '''
function search(ele) {
        if(event.key === 'Enter') {
            
            const urlParams = new URLSearchParams(window.location.search);
            urlParams.set('filt', ele.value);
            window.location.search = urlParams;
        } else {
        var s = ele.value;
        if (event.key.length == 1) {
            s = s + event.key;
        }
        var myElement = document.getElementById("table");
        var allIds = myElement.querySelectorAll("[id^='trow_']");
        var matches = 0;
        var nonmatches = 0;
        for (var i = 0, n = allIds.length; i < n; ++i) {
            if ((s.length == 0) || (allIds[i].id.includes(s))) { 
                allIds[i].style.display = 'block';
                allIds[i].hidden = false;
                matches += 1;
            } else {
                allIds[i].style.display = 'none';
                allIds[i].hidden = true;
                nonmatches += 1;
            }
        }
        var res_info = document.getElementById("search-resultinfo");
        res_info.innerText = 'Found K='+matches+' matches, and M='+nonmatches+' non-matches for search="'+s+'" (K='+allIds.length+' nodes total)'
        }
    }
'''
    content = f'''
<h2>ALL NODES graph: {graph.name} </h2>

<input type="text" id="search_inp" placeholder="search here... (enter)" class="search" onkeydown="search(this)" value="{ filt }"><span id="res_info"></span>
<div id="search-resultinfo"> N={len(lst)} entries found for search: "{filt}"</div>
<hr>
{tmplt}
'''
    return content, js, dict(lst=lst)
                
def get_edit_all(graph, id, **params):
    if not id:
        id = next((n for n in graph if n.do_serialize))

    if not id in graph:
        return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
    
    n = graph[id]
    options = [(s.name, graph_base.colors[s], ('selected' if n.status == s else '')) for s in graph_base.STATUS]
    content = """        
        <div>
            <h2>Edit Node: {{ id_ }} </h2>
            
            <hr>
            <div id="nid">{{ id_ }}</div>
            <label for="status-sel">STATUS: {{ status }} </label>
            <select id="status-sel" name="status">
            {% for (s, c, s2) in options %}
                <option value="{{ s }}" style="color:{{ c }}" {{ s2 }}>{{ s }}</option>
            {% endfor %}
            </select> 
            <button onclick="set_status(this)">Commit (Status)</button>
        </div>
        
        <hr>

        <label for="status">TEXT:</label>
        <textarea id="text-area" style="width:90%; min-height:300px; overflow-x: scroll; overflow-y: none; margin:5px;display:block;font-family: Lucida Console, Courier New, monospace;font-size: 0.8em;">{{ text }}</textarea>
        <button onclick="set_text(this)">Commit (Text)</button>

        <hr>

        <div>
            <label for="note-add">ADD NOTE:</label>
            <input type="text" id="note-add" placeholder="type any text to add as note..." style="min-width:60%;">
            <button onclick="add_note(this)">Commit (Note)</button>
        </div>

<div><alert id="commit-result"></alert></div>


"""
    kwargs = dict(id_=n.id, statu=n.status.name, text=n.text, options=options)

    content += '\n\n<hr><h2>Original Node Info</h2><div>' + html_render.html_node2html(graph[id], with_doc=True, **params) + '</div>'

    js = '''

function helper(apiUrl, obj) {
    try {
        var json = JSON.stringify(obj);
        console.log(apiUrl);
        console.log(obj);
        console.log(json);
        fetch(apiUrl, {
            method: 'PATCH',
            headers: {
            'Content-Type': 'application/json',
            },
            body: json,
            })
            .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
            })
            .then(userData => {
                const t = new Date().toISOString();
                document.getElementById('commit-result').innerText = t + '| SUCCESS! Commited by PATCH to "' + apiUrl + '"! Please reload page!';
                document.getElementById('commit-result').style.color = 'green'
                
            })
            .catch(error => {
                const t = new Date().toISOString();
                console.error('Error:', error);
                document.getElementById('commit-result').innerText = t + '| ERROR: ' + error;
                document.getElementById('commit-result').style.color = 'red'
            });
    } catch (error) {
        document.getElementById('commit-result').innerText = t + '| ERROR: ' + error;
        document.getElementById('commit-result').style.color = 'red'
    }

}
function add_note(ele) {
    const apiUrl = window.location.origin + '/api/v1/node/add_note';
    const to_upload = document.getElementById('note-add').innerText;
    const id_ = document.getElementById('nid').innerText;
    helper(apiUrl, {text:to_upload, id:id_});
}

function set_text(ele) {
    const apiUrl = window.location.origin + '/api/v1/node/set_text';
    const to_upload = document.getElementById('text-area').value;
    const id_ = document.getElementById('nid').innerText;
    helper(apiUrl, {text:to_upload, id:id_});
}

function set_status(ele) {
    const apiUrl = window.location.origin + '/api/v1/node/set_status';
    const id_ = document.getElementById('nid').innerText;
    const e = document.getElementById('status-sel');
    var value = e.value;
    var to_upload = e.options[e.selectedIndex].text;
    helper(apiUrl, {status:to_upload, id:id_});
}
'''
    
    
    return content, js, kwargs


def get_search_all(search_text, g=None, **kwargs):    

    
    ret = []
    t = time.time()
    state = ui_state.get()
    kwargs = dict(node_vms=[])
    nmax = kwargs.get('n', kwargs.get('kwargs', state.n_search_results))

    if search_text:
        assert state.db_path.endswith('.sqlite'), 'can only search on a sqlite database source!'
        sres1 = fs_search(state.db_path, search_text, search_column='id', limit=nmax)
        sres2 = fs_search(state.db_path, search_text, limit=nmax)
        hits = list({**sres1, **sres2}.values())

        if g is None:
            with ui_state.GetGraphHold() as g:
                kwargs = html_render.sub_searchhits_kwargs(hits, g)
        else:
            kwargs = html_render.sub_searchhits_kwargs(hits, g)
            
    runtime = time.time() - t
    status = f'<div>Searchtext: "{search_text}" | -> {len(kwargs.get("node_vms"))} Docs in {runtime*1000:.1f}ms</div>\n\n'
    tmplt = html_render.sub_searchhits_tmplt()
    content = status + tmplt
    js = ''
    return content, js, kwargs


def make_page(content, js='', nohead=False):
    page = f'''<!DOCTYPE html>
    <html>
{"" if nohead else HEADER}
        <body>
            <div>{"" if nohead else NAVBAR}</div>
            <div id="main-content">
{content}
            </div>
            <script>
{js}
            </script>
        </body>
    </html>'''
    return page


"""
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          ▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀█░█▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌     ▐░▌  ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌                    ▐░▌
▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
       
"""

def add_routes(server):

    @server.route('/uib/v1/search', methods=['GET'])
    def uib_search():
        
        try:    
            qry = request.args.get('qry', request.args.get('search', ''))
            content, js, kwargs = get_search_all(qry)
            tmpl = make_page(content, js, nohead=request.args.get('nohead', False))
            return render_template_string(tmpl, **kwargs)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
  



    @server.route('/uib/v1/edit/<string:id>', methods=['GET'])
    def edit(id=None):
        
        try:    
            with GetGraphHold() as graph:
                id = request.args.get('id', id)
            content, js, kwargs = get_edit_all(graph, id, **request.args)
            tmpl = make_page(content, js, nohead=request.args.get('nohead', False))
            return render_template_string(tmpl, **kwargs)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
  



    @server.route('/uib/v1/table', methods=['GET'])
    def show_table():

        try:    

            filt = request.args.get('filt', '')
            i = request.args.get('i', '0')
            n = request.args.get('n', request.args.get('N', None))
            content, js, kwargs = get_table_all(None, filt, i, n)
            return render_template_string(make_page(content, js), **kwargs)
            
    
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        

    @server.route('/uib/v1/plotnn/<string:id>', methods=['GET'])
    def plot_nn(id=None):
        
        try:    
            with GetGraphHold() as graph:
                id = request.args.get('id', id)
                nn = request.args.get('nn', 25)
                lyout = request.args.get('layout', 'tree')
                if not id:
                    return "must give an id to plot", 402
                elif id not in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                
                bts = get_nn_plot_bytes(graph[id], lyout, nn, 'jpg')
                img =b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + bts + b'\r\n'

            print('returning!', len(img), len(bts))

            return Response(img, mimetype='multipart/x-mixed-replace; boundary=frame')
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        


    @server.route('/uib/v1/node/<string:id>', methods=['GET'])
    def uib_node(id=None):

        try:    
            with GetGraphHold() as graph:
                args = request.args
                id = args.get('id', id)
                with_doc = args.get('with_doc', False)

                if not id:
                    id = graph.root_node.id

                if not id in graph:
                    return f'<div style="color:red;>"{id=}" not found in graph "{graph=}"</div>', 404
                else:
                    content = html_render.html_node2html(graph[id], with_doc=with_doc)
                    tmpl = make_page(content)                    
                    return render_template_string(tmpl)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        

    @server.route('/uib/v1/plot_ly', methods=['GET'])
    def uib_plot_ly():
        try:
            plottype = request.args.get('type', 'sunburst')
            maxdepth = int(request.args.get('maxdepth', '10'))
            figsize = int(request.args.get('figsize', '1200'))
            lyout = request.args.get('layout', 'auto')

            with GetGraphHold() as graph:
                if plottype == 'sunburst':
                    f = graph.plot_sunburst(figsize=figsize, maxdepth=maxdepth)
                elif plottype == ' treemap':
                    f = graph.plot_treemap(figsize=figsize, maxdepth=maxdepth)
                else:
                    f = graph.plot_ly(figsize=figsize, lyout=lyout)

            f.update_layout()
            content = get_fig_html(f)
            page = make_page(content)
            return Response(page)
        
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        

    @server.route('/uib/v1/editn', methods=['GET'])
    @server.route('/uib/v1/editn/<string:id>', methods=['GET'])
    @server.route('/uib/v1/editj', methods=['GET'])
    @server.route('/uib/v1/editj/<string:id>', methods=['GET'])
    def uib_editn(id=None):
        try:
            
            with_doc = request.args.get('with_doc', True)


            with GetGraphHold() as graph:
                
                if not id:
                    id = graph.root_node.id

                if not id in graph:
                    return f'<div style="color:red;>"{id=}" not found in graph "{graph=}"</div>', 404
                
                dc = graph[id].to_dict(add_parents=True, no_doc= False if with_doc else True)
                
            
                jsons = json.dumps(dc, indent=2)

                content = f'''<h2>Edit Node: {id}</h2>
                <div><input type="text" id="get-path" value="/api/v1/set_node/{id}" style="min-width:60%;">
                <button onclick="patch(this)">Upload (Patch)</button>
                <textarea id="json-data" style="width:90%; min-height:600px; overflow-x: scroll; overflow-y: none; margin:5px;display:block;font-family: Lucida Console, Courier New, monospace;font-size: 0.8em;">\n\n{jsons}\n\n</textarea>
                <alert id="action-result"></alert>
                '''
                
                js = '''
function patch(ele){
    const apiUrl = window.location.origin + document.getElementById('get-path').value;
    
    try {
        const json = JSON.stringify(document.getElementById("json-data").innerText);
        fetch(apiUrl, {
            method: 'PATCH',
            headers: {
            'Content-Type': 'application/json',
            },
            body: json,
            })
            .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
            })
            .then(userData => {
                const t = new Date().toISOString();
                const myJSON = JSON.stringify(userData[0], null, 2); 
                document.getElementById("json-data").innerText = myJSON;
                document.getElementById('action-result').innerText = t + '| SUCCESS! Updated data by GET!';
                document.getElementById('action-result').style.color = 'green'
                
            })
            .catch(error => {
                const t = new Date().toISOString();
                console.error('Error:', error);
                document.getElementById('action-result').innerText = t + '| ERROR: ' + error;
                document.getElementById('action-result').style.color = 'red'
            });
    } catch (error) {
        document.getElementById('action-result').innerText = t + '| ERROR: ' + error;
        document.getElementById('action-result').style.color = 'red'
    }
}
'''

            page = make_page(content, js)
            return Response(page)
        
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        
        
    @server.route('/uib/v1/show', methods=['GET'])
    @server.route('/uib/v1/show/<string:id>', methods=['GET'])
    def uib_show(id=None):

        try:    
            with GetGraphHold() as graph:
                args = request.args
                
                # log.debug((args, id))

                id = args.get('id', id)
                filt = args.get('filt', '')
                with_doc = args.get('with_doc', False)
                nn = args.get('nn', 25)
                lyout = request.args.get('layout', 'tree')
                show_graph = args.get('show_graph', False)
                plottype = request.args.get('type', 'sunburst')
                maxdepth = int(request.args.get('maxdepth', '10'))
                figsize = int(request.args.get('figsize', '600'))
                lyoutg = request.args.get('layoutg', 'auto')
                

                lst = [html_render.mk_tpl(n.id, str(n)) for n in graph.nodes]

                if filt:
                    lst = [(h, t) for h, t in lst if filt in t]

                left = """
    <div style="padding:5px; border:1px black solid; border-radius:5px">
    <h2>ALL NODES graph: {{ gname }} </h2>
    <alert id="search-resultinfo"></alert>
    <input type="text" id="search_inp" placeholder="search here... (enter)" class="search" onkeydown="search(this)" value="{{ filt }}"><span id="res_info"></span>
    <hr>
    <div id="links">
    {% for (h, t) in lst %}
        <div><a id="{{ h }}" href="{{ h }}">{{ t }} </a></div>
    {% endfor %}
    </div>
    </div>
    """
                iframe_src = f'/uib/v1/plot_ly?figsize={figsize}&type={plottype}&maxdepth={maxdepth}&lyoutg={lyoutg}'
                sel = f'<div><input type="text" id="iframe_path" value="{iframe_src}" style="min-width:60%;" ><button onclick="toggle_iframe(this)">Click here to toggle graph visibility...</button></div>'
                tmp = sel + f'\n\n<div><iframe id="iframe_graph_ploty" style="width:98%; min-height:400px; height:{figsize//3*2}px;" hidden="true" name="graph_ploty" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" ></iframe></div>'
                tmp = f'<div style="padding:5px;border:1px black solid; border-radius:5px">{tmp}</div>'    
                left = tmp + left

                if not id:
                    id = graph.root_node.id

                if not id in graph:
                    right = f'<div style="color:red;>"{id=}" not found in graph "{graph=}"</div>'

                else:
                    imageblob = get_nn_plot_bytes(graph[id], lyout, nn, 'png')
                    s = base64.b64encode(imageblob).decode('utf-8')
                    if not s.startswith('data:image'):
                        s = 'data:image/png;base64,' + s
                    img = f"<div><img src=\"{s}\" style=\"max-width:95%\" /></div>"
                    right = '\n\n' + html_render.html_node2html(graph[id], with_doc=with_doc, graph_plot=img)
                    
                    right = f'<div style="padding:5px; border:1px black solid; border-radius:5px">{right}</div>'

                    dc = graph[id].to_dict(add_parents=True, no_doc= False if with_doc else True)
                    content = json.dumps(dc, indent=2)

                    body = f'''<h2>Edit Node: {id}</h2>
                    <div><input type="text" id="get-path" value="/api/v1/set_node/{id}" style="min-width:60%;" />
                    <button onclick="patch(this)">Upload (Patch)</button>
                    <textarea id="json-data" style="width:90%; min-height:600px; overflow-x: scroll; overflow-y: none; margin:5px;display:block;font-family: Lucida Console, Courier New, monospace;font-size: 0.8em;"> {{content}} </textarea>
                    <alert id="action-result"></alert>
                    '''

                    right += f'<div style="padding:5px; border:1px black solid; border-radius:5px">{body}</div>'
                    
                js = """

function patch(ele){
    const apiUrl = window.location.origin + document.getElementById('get-path').value;
    
    try {
        const json = JSON.stringify(document.getElementById("json-data").innerText);
        fetch(apiUrl, {
            method: 'PATCH',
            headers: {
            'Content-Type': 'application/json',
            },
            body: json,
            })
            .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
            })
            .then(userData => {
                const t = new Date().toISOString();
                const myJSON = JSON.stringify(userData[0], null, 2); 
                document.getElementById("json-data").innerText = myJSON;
                document.getElementById('action-result').innerText = t + '| SUCCESS! Updated data by GET!';
                document.getElementById('action-result').style.color = 'green'
                
            })
            .catch(error => {
                const t = new Date().toISOString();
                console.error('Error:', error);
                document.getElementById('action-result').innerText = t + '| ERROR: ' + error;
                document.getElementById('action-result').style.color = 'red'
            });
    } catch (error) {
        document.getElementById('action-result').innerText = t + '| ERROR: ' + error;
        document.getElementById('action-result').style.color = 'red'
    }
}

function toggle_iframe(ele) {
    var source = document.getElementById("iframe_path");
    var target = document.getElementById("iframe_graph_ploty");
    target.src = window.location.origin + source.value;

    if (target.hidden) {
        target.hidden=false;
    } else {
        target.hidden=true;
    }
}
    function search(ele) {
        if(event.key === 'Enter') {
            
            const urlParams = new URLSearchParams(window.location.search);
            urlParams.set('filt', ele.value);
            window.location.search = urlParams;
        } else {
        var s = ele.value;
        if (event.key.length == 1) {
            s = s + event.key;
        }
        var myElement = document.getElementById("links");
        var allIds = myElement.querySelectorAll('*[id]');
        var matches = 0;
        var nonmatches = 0;
        for (var i = 0, n = allIds.length; i < n; ++i) {
            if ((s.length == 0) || (allIds[i].id.includes(s))) { 
                allIds[i].style.display = 'block';
                allIds[i].hidden = false;
                matches += 1;
            } else {
                allIds[i].style.display = 'none';
                allIds[i].hidden = true;
                nonmatches += 1;
            }
        }
        var res_info = document.getElementById("res_info");
        res_info.innerText = 'Found K='+matches+' matches, and M='+nonmatches+' non-matches for search="'+s+'" (K='+allIds.length+' nodes total)'
        }
    }

    // onchange="filter(this)"
    function filter(ele) {
        var myElement = document.getElementById("links");
        var allIds = myElement.querySelectorAll('*[id]');
        count = 0;
        for (var i = 0, n = allIds.length; i < n; ++i) {
            if ((ele.value.length == 0) || (allIds[i].id.includes(ele.value))) { 
                allIds[i].style.display = 'block';
                allIds[i].hidden = false;
                count += 1;
            } else {
                allIds[i].style.display = 'none';
                allIds[i].hidden = true;
            }
        }
        const s = 'Found N=' + count + ' matches from M=' + allIds.length + 'nodes in total'
        document.getElementById('action-result').style.color = 'blue'
        
    }"""
                
                content = f'''<div style="width: 100%; overflow: hidden;">
    <div id="left" style="width: 50%; float: left; height: 100%; overflow-y: scroll;  overflow-x: scroll;"> {left} </div>    
    <div id="right" style="margin-left: 52%; height: 100%; overflow-y: scroll;  overflow-x: scroll;"> {right} </div>
</div>'''
                tmpl = make_page(content, js)                    
                return render_template_string(tmpl, lst=lst, filt=filt, gname=graph.name, id_=id, content_json=content)
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        
