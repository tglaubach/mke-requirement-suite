

import base64
import json
import re
import textwrap
import io
from ReqTracker.io_files import nextcloud_interface
from flask import Flask, request, jsonify, send_file, send_from_directory, render_template, render_template_string, Response, url_for
import urllib
import shutil

import numpy as np

import traceback


import datetime
import time
import markdown
import matplotlib

# from dash_bootstrap_templates import load_figure_template


# load_figure_template("litera")

# add to path for testing
import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, current_dir)
    sys.path.insert(0, parent_dir)


import ReqTracker

from ReqTracker.core import node_factory, schema, graph_main, graph_base, helpers

from ReqTracker.ui import ui_state
from ReqTracker.ui_vmc import vmc_docmaker, html_render
from ReqTracker.ui.ui_state import GetGraphHold


log = helpers.log
tostr = helpers.tostr




add_log_alert = ui_state.add_log_alert


"""
 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          ▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀█░█▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌     ▐░▌  ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌                    ▐░▌
▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
       
"""

def m_ping():
    return f'ReqTracker Server, status: RUNNING, DB-connection: "{ui_state.get().db_path}" local time: {helpers.nowiso()} running since: {helpers.make_zulustr(ui_state.T_STARTED)}'


def add_routes(server):

    @server.route("/ping")
    @server.route("/")
    @server.route("/api/v1/ping")
    def ping():
        return m_ping()

    @server.route('/api/v1/node', methods=['GET'])
    @server.route('/api/v1/node/<string:id>', methods=['GET'])
    def api_get_nodes(id=None):
        
        try:    
            source, sinks = request.args.get('source', None), request.args.get('sinks', None)
            if source and not sinks:
                sinks = [source]
            with GetGraphHold(source, sinks) as graph:
                args = request.args
                # log.debug((args, id))

                if 'id' in args:
                    id = args['id']

                if not id:
                    return jsonify([n.to_dict(add_parents=True) for n in graph.nodes]), 200
                elif id not in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                else:
                    return jsonify(graph[id].to_dict(add_parents=True)), 200
        except Exception as err:
            return jsonify({"error": traceback.format_exc()}), 500
        
    @server.route('/api/v1/plotlydata', methods=['GET'])
    @server.route('/api/v1/plotlydata/<string:style>', methods=['GET'])
    def api_plotlydata(style='sunburst'):

        

        try:    
            source, sinks = request.args.get('source', None), request.args.get('sinks', None)
            if source and not sinks:
                sinks = [source]
            with GetGraphHold(source, sinks) as graph:
                args = request.args
                id = args.get('id', None)
                # log.debug((args, id))

                if id and id not in graph:
                    return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
                if style == 'nn':
                    g = graph[id].nng(args.get('nnmax', 25)) if id else graph
                else:
                    g = graph[id].to_graph() if id else graph
                
                if style == 'sunburst':
                    dc = g.plot_sunburst(as_dict=True, **args)
                elif style == 'graph':
                    dc = g.plot_ly(as_dict=True, **args)
                elif style == 'treemap':
                    dc = g.plot_ly(as_dict=True, **args)
                else:
                    return jsonify({"error": f'wrong input {style=} was given but must be one of {"sunburst treemap graph".split()}'}), 422
                response = server.response_class(
                    response=dc,
                    status=200,
                    mimetype='application/json'
                )
                            
                return response
                
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        
    @server.route('/api/v1/node/set_text', methods=['POST', 'PATCH'])
    @server.route('/api/v1/node/set_status', methods=['POST', 'PATCH'])
    @server.route('/api/v1/node/add_data', methods=['POST', 'PATCH'])
    @server.route('/api/v1/node/add_note', methods=['POST', 'PATCH'])
    @server.route('/api/v1/node/append_to_doc', methods=['POST', 'PATCH'])
    @server.route('/api/v1/node/set_doc', methods=['POST', 'PATCH'])
    def api_graph_nodes():
        


        try:    
            try:
                source, sinks = request.json.get('source', None), request.json.get('sinks', None)
                if source and not sinks:
                    sinks = [source]
            except Exception as err:
                source, sinks = None, None

            with GetGraphHold(source, sinks) as graph:
                dc = request.json
                

                if 'id' not in dc:
                    return jsonify({"error": '"id" must be given in args'}), 422
                id = dc['id']
                
                if id not in graph:
                    return jsonify({"error": f'"{id}" could not be found in graph'}), 404
                else:
                    node = graph[id]
                
                if 'set_status' in request.path:
                    status = dc['status']
                    if isinstance(status, str):
                        found = status in graph_base.STATUS.__members__
                    elif isinstance(status, int):
                        found = status in graph_base.STATUS
                    else:
                        found = False
                    if not found:
                        allowed = ' '.join([s.name for s in graph_base.STATUS])
                        return jsonify({"error": f'the given status "{status}" could not be found in allowed status list: {allowed}'}), 404
                    node.set_status(status)
                elif 'set_text' in request.path:
                    node.set_text(dc['text'])
                elif 'add_note' in request.path:
                    node.add_note(dc['text'])
                elif 'add_data' in request.path:
                    node.add_data(dc['key'], dc['value'])
                elif 'append_to_doc' in request.path:
                    node.doc_append_part(dc['value'])

                elif 'set_doc' in request.path:
                    if not isinstance(dc['value'], list):
                        typ = type(dc['value'])
                        return jsonify({"error": f'can only set a document with a list type object, but got {typ=}'}), 400
                    
                    node.doc_set_value(dc['value'])

                return jsonify(node.to_dict(add_parents=True))

            
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500



    @server.route('/api/v1/node/set_node', methods=['POST'])
    @server.route('/api/v1/node/add_node', methods=['POST'])
    @server.route('/api/v1/node/del_node', methods=['POST'])
    def api_graph_getset_nodes():
        


        try:    
            try:
                source, sinks = request.json.get('source', None), request.json.get('sinks', None)
                if source and not sinks:
                    sinks = [source]
            except Exception as err:
                source, sinks = None, None

            with GetGraphHold(source, sinks) as graph:
                dc = request.json
                if not dc:
                    dc = graph.serialize()
                    return jsonify(dc), 200

                if 'id' not in dc:
                    return jsonify({"error": '"id" must be given in args'}), 422

                id_ = dc['id']
                
                if 'add_node' in request.path:
                    if id_ in graph:
                        return jsonify({"error": f'"{id_}" already exists and can not be added!'}), 403
                    else:
                        node = graph_main.construct(dc)
                        if node:
                            graph.add_node(node)
                        else:
                            return jsonify({"error": f'"{id_}" could not be constructed from dict'}), 404
                        
                elif 'set_node' in request.path:
                    node = graph_main.construct(dc)
                    if node is None:
                        return jsonify({"error": f'"{id_}" could not be constructed from dict'}), 404
                    
                    if id_ in graph:
                        node = graph[id_] = node
                    else:
                        graph.add_node(node)
                        
                elif 'del_node' in request.path:
                    graph.del_node(node, remove=False)

                return jsonify(node.to_dict(add_parents=True))

            
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500


    @server.route('/api/graph/push_changes/', methods=['POST', 'PATCH'])
    @server.route('/api/v1/graph/push_changes/', methods=['POST', 'PATCH'])
    def push_changes_api():
        

        try:    
            state = ui_state.get()
            try:
                source, sinks = request.json.get('source', None), request.json.get('sinks', None)
                if source and not sinks:
                    sinks = [source]
            except Exception as err:
                source, sinks = None, None

            
            source = state.db_path if source is None else source

            if not (request.method == 'PATCH' or request.method == 'POST'):
                return jsonify({"error": 'incorrect HTTP message'}), 404
            dc = request.json

            nodes_dc_to_update = dc['nodes_dc_to_update']
            changes_to_append = dc['changes_to_append']
            if source == ':memory:':
                g = ui_state._in_memory_graph
                dc = g.serialize()
                for k, v in nodes_dc_to_update.items():
                    dc['nodes'][k] = v
                dc['changes'] += changes_to_append
                graph_main.Graph.deserialize(dc, g, mode='overwrite')
                log.debug('push_changes_api | Pushed N={} nodes and N={} changes to memory graph "{}"'.format(len(nodes_dc_to_update), len(changes_to_append), source))
            else:
                src = graph_main.get_io_source(source)

                assert not src is None and hasattr(src, 'push_changes'), f'no source given, or the source "{src}" has no method "push_changes"'
                src.push_changes(nodes_dc_to_update, changes_to_append)
                log.debug('push_changes_api | Pushed N={} nodes and N={} changes to sink "{}"'.format(len(nodes_dc_to_update), len(changes_to_append), src.name))

            return jsonify({}), 200
            
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500



    @server.route('/api/graph/', methods=['POST', 'PATCH'])
    @server.route('/api/v1/graph/', methods=['GET', 'POST', 'PATCH'])
    def graph_api():

        try:    
            state = ui_state.get()
            log.debug(f'graph_api {request.path} {request.method}')
            if request.method == 'GET':
                args = request.args
                # log.debug((args, id))

                nochanges = True if args.get('nochanges', None) else False
                no_docs = True if args.get('no_docs', False) else False

                source, sinks = request.args.get('source', None), request.args.get('sinks', None)
                if source and not sinks:
                    sinks = [source]

                with GetGraphHold(source, sinks) as graph:
                    dc = graph.serialize()
                if nochanges:
                    dc['changes'] = []
                return jsonify(dc), 200
            elif request.method == 'PATCH' or request.method == 'POST':
                dc = request.json
                if 'nodes' not in dc or 'changes' not in dc:
                    return jsonify({"error": '"nodes" and  must be given in dict'}), 422
                
                try:
                    source, sinks = request.json.get('source', None), request.json.get('sinks', None)
                    if source and not sinks:
                        sinks = [source]
                except Exception as err:
                    source, sinks = None, None
                if source or sinks:
                    raise NotImplementedError("source and sink params are not implemented yet for graph pushing!")
                
                g = graph_main.Graph.deserialize(dc)
                if ui_state.MAKE_BACKUPS_ON_PUSH:
                    tmp = ui_state.save_backup()
                    add_log_alert(tmp)
                if os.path.exists(state.db_path):
                    os.remove(state.db_path)
                g.set_new_datasource(state.db_path)
                g.push()
                return jsonify({}), 200
            else:
                return jsonify({"error": 'incorrect HTTP message'}), 404
            
        
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500


    return server

