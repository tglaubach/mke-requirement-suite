



import base64
import json
import re
import textwrap
import io
from flask import Flask, request, jsonify, send_file, send_from_directory, render_template, render_template_string, Response
import urllib
import shutil

import numpy as np

import traceback


import datetime
import time
import markdown
import matplotlib


# from dash_bootstrap_templates import load_figure_template


# load_figure_template("litera")

# add to path for testing
import sys, inspect, os

from ReqTracker.io.graphdb_io import fs_search

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, current_dir)
    sys.path.insert(0, parent_dir)


import ReqTracker

from ReqTracker.core import node_factory, schema, graph_main, graph_base, helpers

from ReqTracker.ui import ui_state
from ReqTracker.ui_vmc import vmc_docmaker, html_render
from ReqTracker.io_files import nextcloud_interface, redmine_api

log = helpers.log
tostr = helpers.tostr



GetGraphHold = ui_state.GetGraphHold
add_log_alert = ui_state.add_log_alert



def add_routes(server):

    @server.route('/uidf/v1/main', methods=['GET'])
    def uidf_main():
        
        try:    
            return render_template('drawflow_graph_template.html')
        except Exception as err:
            log.exception(err)
            return jsonify({"error": traceback.format_exc()}), 500
        

    @server.route('/uidf/v1/push_changes_drawflow/', methods=['POST', 'PATCH'])
    @server.route('/uidf/v1/push_changes_drawflow', methods=['POST', 'PATCH'])
    def push_changes_drawflow_api():
        
        log.debug(f'push_changes_drawflow {request.path} {request.method}')
        try:    
            state = ui_state.get()
            if not (request.method == 'PATCH' or request.method == 'POST'):
                return jsonify({"error": 'incorrect HTTP message'}), 404
            
            changes = request.json.get('changes', {})
            print(changes)
            source, sinks = request.json.get('source', None), request.json.get('sinks', None)
            if source and not sinks:
                sinks = [source]
            
            with GetGraphHold(source, sinks) as graph:  
                for i, change in enumerate(changes, 1):
                    cmd = change.get('command', None)
                    node_id = change.get('node_id', None)
                    if not cmd:
                        log.error(f'no command given for change no={i} {change=}')
                        return jsonify({"error": f'no command given for change no={i} {change=}'}), 404
                    elif node_id and not node_id in graph:
                        log.error(f'node_id {node_id=} for change no={i} is not in graph {change=}')
                        return jsonify({"error": f'node_id {node_id=} for change no={i} is not in graph {change=}'}), 404
                    elif node_id and not hasattr(graph[node_id], cmd):
                        otype = type(graph[node_id])
                        log.error(f'command {cmd=} for type="{otype}" of {node_id=} and change no={i} is not recognized {change=}')
                        return jsonify({"error": f'command {cmd=} for type="{otype}" of {node_id=} and change no={i} is not recognized {change=}'}), 404
                    elif not node_id and not hasattr(graph, cmd):
                        log.error(f'command {cmd=} for change no={i} is not recognized {change=}')
                        return jsonify({"error": f'command {cmd=} for change no={i} is not recognized {change=}'}), 404
                    
                results = []

                for change in changes:
                    node_id = change.get('node_id', None)
                    cmd = change.get('command', None)
                    args = change.get('args', [])
                    kwargs = change.get('kwargs', {})
                    if node_id:
                        fun = getattr(graph[node_id], cmd, None)
                    else:
                        fun = getattr(graph, cmd, None)
                    assert fun is not None, f'{cmd=} was not found and passed by sanity check!'
                    results.append(fun(*args, **kwargs))

                changelog = graph.get_changes(since_construction=True, dt_as_string=True)

                res = {'requested_changes': changes, 'results':results, 'changelog': changelog}

            return jsonify(res), 200
            
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500


    @server.route('/uidf/v1/push_layout/', methods=['POST', 'PATCH'])
    def push_layout_api():
        
        log.debug(f'push_layout {request.path} {request.method}')
        try:    
            state = ui_state.get()
            if not (request.method == 'PATCH' or request.method == 'POST'):
                return jsonify({"error": 'incorrect HTTP message'}), 404
            layout = request.json.get('layout', {})

            try:
                source, sinks = request.json.get('source', None), request.json.get('sinks', None)
                if source and not sinks:
                    sinks = [source]
            except Exception as err:
                source, sinks = None, None

            with GetGraphHold(source, sinks) as graph:
                results = {}
                for nid, row in layout.items():
                    if nid in graph:
                        n = graph[nid] 
                        for k in ['pos_x', 'pos_y']:
                            n.add_data(k, row.get(k))
                        results[nid] = 200
                    else:
                        results[nid] = 404
                results = {'requested': layout, 'results': results}

            return jsonify(results), 200
            
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500


    server.json.sort_keys = False

    @server.route('/uidf/v1/graph_drawflow', methods=['GET', 'POST', 'PATCH'])
    @server.route('/uidf/v1/graph_drawflow/', methods=['GET', 'POST', 'PATCH'])
    def graph_drawflow():

        times = {}
        times['t0'] = time.time()


        try:    
            from ReqTracker.exporters.drawflow_interface import nodes2drawflow, nodes2moduledata

            log.debug(f'graph_api {request.path} {request.method}')
            if request.method == 'GET':
                args = request.args
                # log.debug((args, id))

                max_x = args.get('max_x', args.get('x', 2200))
                max_y = args.get('max_y', args.get('y', 2200))
                root_node = args.get('root', args.get('root_node', args.get('node', None)))
                keep_loose_docs = args.get('keep_loose_docs', args.get('keep_loose', args.get('with_docs', None)))
                split_categories = args.get('split_categories', args.get('in_cat', args.get('split', 'DS')))
                full_data = int(args.get('full_data', args.get('all_data', args.get('all', 0))))
                with_template = int(args.get('with_template', args.get('templates', 0)))
                new_layout = int(args.get('new_layout', args.get('clear', 0)))
                no_graph = int(args.get('no_graph', 0))

                log.debug(args)
                #log.debug(f'{new_layout=}, {type(new_layout)=}')


                if not keep_loose_docs:
                    f = lambda nid: not (isinstance(graph[nid], schema.Document) and not any((p.do_serialize for p in graph[nid].linked)))
                else:
                    f = None

                dc = {}

                source, sinks = args.get('source', None), args.get('sinks', None)
                if source and not sinks:
                    sinks = [source]

                with GetGraphHold(source, sinks) as graph:
                    times['getgraph'] = time.time()
                    if root_node and not root_node in graph:
                        return jsonify({'error': f'{root_node=} not found in {graph=}'}), 404
                    elif root_node:
                        g = graph[root_node].to_graph()
                    else:
                        g = graph

                    if not no_graph:
                        dc['graph'] = {el.id: el.to_dict(add_parents=True, graph=g, no_doc=True) for el in g.nodes if el.do_serialize and el._status != graph_base.STATUS.DELETED}
                    else:
                        dc['graph'] = None

                    if full_data:
                        dc['nodes'] = {n.id: n.to_dict(n.has_graph) for n in g.nodes}

                    else:
                        get_link = lambda n: n.link if hasattr(n, 'link') and n.link else html_render.mk_tpl(n.id)[0]
                        # , 'parents': [p.id for p in n.parents], 'children': [p.id for p in n.children]

                        fun = lambda n: {'id': n.id, 'text': n.hint, 'link': get_link(n), 'status': n.status.name, 'color': n.color, 'notes': n.notes}
                        dc['nodes'] = {n.id:fun(n) for n in g.nodes}

                    times['nodes'] = time.time()

                    dc['node_ids'] = {i:nid for i, nid in enumerate(g.get_viewnodes(as_id=True))}
                    id_lut = {v:k for k, v in dc['node_ids'].items()}

                    times['node_ids'] = time.time()

                    if full_data:
                        dc['categories'] = {}
                        if len(g) > 1:
                            dc['categories']['ALL'] = nodes2drawflow(g, int(max_x), int(max_y), filtfun=f, id_lut=id_lut, new_layout=new_layout)

                        times['categories-ALL'] = time.time()

                        if split_categories:
                            cats = g.filtt(split_categories)
                            for cat in cats:
                                if '##' in cat.id and not with_template:
                                    continue
                        
                                if cat.id == 'DS.REQS':
                                    filtfun = lambda nid: (nid.startswith('R.DS') or nid.startswith('DS.') or nid.startswith('VR.')) and f(nid)
                                else:
                                    filtfun = f

                                gi = cat.to_graph()
                                if len(gi) > 1:
                                    dc['categories'][cat.id] = nodes2drawflow(cat.to_graph(), int(max_x), int(max_y), filtfun=filtfun, id_lut=id_lut, new_layout=new_layout)
                                times[f'categories-{cat.id}'] = time.time()

                    else:
                        dc['modviews'] = {}
                        dc['modviews_progress'] = {}

                        if len(g) > 1:
                            dc['modviews']['ALL'] = nodes2moduledata(g, int(max_x), int(max_y), filtfun=f, id_lut=id_lut, new_layout=new_layout)

                        times['modviews-ALL'] = time.time()
                        if split_categories:
                            cats = list(g.filtt(split_categories))
                            cats.sort(key=lambda x: x.id)

                            cats = [c for c in cats if c.dish_id] + [c for c in cats if not c.dish_id]

                            cmap = matplotlib.cm.get_cmap('seismic') # RdYlGn

                            for cat in cats:
                                if '##' in cat.id and not with_template:
                                    continue

                                if cat.id == 'DS.REQS':
                                    filtfun = lambda nid: (nid.startswith('R.DS') or nid.startswith('DS.') or nid.startswith('VR.')) and f(nid)
                                else:
                                    filtfun = f

                                gi = cat.to_graph()
                                if len(gi) > 1:
                                    dc['modviews'][cat.id] = nodes2moduledata(gi, int(max_x), int(max_y), filtfun=filtfun, id_lut=id_lut, new_layout=new_layout)
                                times[f'modviews-{cat.id}'] = time.time()


                                weights = [redmine_api.weight_status(node.status.name) for node in gi.vn]
                                progress = np.mean(weights)
                                col = cmap(1-progress)
                                dc['modviews_progress'][cat.id] = {'progress': progress, 'col':col, 'color':matplotlib.colors.to_hex(col)}
                                
                                
                                

                log.debug(f'returning dict with n={len(dc)} drawflow entries for graph {g=}')

                times[f'DONE'] = time.time()

                log.debug(str(times))
                log.debug([c for c in dc['modviews'].keys()])

                #k = list(times.keys())[1:]
                #dtimes = dict(zip(k, np.round(np.diff(list(times.values())), 10)))
                # log.debug(json.dumps(dtimes))
                          
                return jsonify(dc), 200
            
            elif request.method == 'PATCH' or request.method == 'POST':
                raise NotImplementedError('WIP!')
            else:
                return jsonify({"error": 'incorrect HTTP message'}), 404
            
        
        except Exception as err:
            log.error(err, exc_info=True)
            return jsonify({"error": traceback.format_exc()}), 500

    return server
