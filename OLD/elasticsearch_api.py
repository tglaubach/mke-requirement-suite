

import numpy as np
from elasticsearch import Elasticsearch, NotFoundError


import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    

from ReqTracker.core import helpers


log = helpers.log


es_index = "test_index"
es_doc_type = "pdf_pages"
es_host = "http://localhost:9200"


# default_settings = {
#     'es_index': ES_DEFAULT_INDEX,
#     'es_doc_type': ES_DEFAULT_DOC_TYPE,
#     'es_host': ES_DEFAULT_HOST
# }

es = None 

def init(basic_auth, host, do_test=True, **kwargs):
    global es
    es = Elasticsearch(host, basic_auth=basic_auth, **kwargs, timeout=100, max_retries=2)

    if do_test:
        log.info('ELASTICSEARCH INFO')
        log.info(es.info())
        
        try:    
            n = es.count(index=es_index)
        except NotFoundError as err:
            log.error('Index not found creating a new one!')
            es.indices.create(index=es_index)
            n = 0

        log.info('ELASTICSEARCH COUNT {}={} entries'.format(es_index, n))


def es_info(**kwargs):
    try:    
        node_ids = {}
        info = {}
        count = {}
        node_info = {}

        global es
        n = es.count(index=es_index)
        info = {k:v for k, v in es.info().body.items()}
        count = {k:v for k, v in n.body.items()}

        r = es.sql.query(body={'query': f'select node_id, n_pages from {es_index}'})
        node_ids = {tpl[0]:tpl[1] for tpl in r.body['rows'] if tpl}

        node_info = {'n_node_ids': len(node_ids), 'n_pages_total': int(np.sum(list(node_ids.values())))}

    except NotFoundError as err:
        log.error('Index not found creating a new one!')
        es.indices.create(index=es_index)
        ret = {'ERROR': str(err)}

    
    ret = {
        'info': info,
        'index': es_index,
        'count': count,
        'node_info': node_info,
        'node_ids': node_ids,
    }
        

    return ret


def es_search_raw(search_str, field="content", n_max = 10, i_start = 0):
    body = {
            "from": i_start,
            'size' : n_max,
            "query": {'match': {field: search_str}},
            "highlight": {
                "fields": {
                "content": {}
                },
                "pre_tags": ["<mark>"],
                "post_tags": ["</mark>"],
            }
        }
    log.debug(body)
    ret = es.search(index=es_index, body=body)
    log.debug(len(ret.body['hits']['hits']))

    return ret

def es_more_like_this(search_str, fields=["content"], n_max = 10, i_start = 0, filename_filt=''):

    qry = {
        'more_like_this': {
            'fields': fields,
            'like': search_str,
            "min_term_freq" : 1,
            "max_query_terms" : 12
            }
    }

    if filename_filt:
        qry = {"bool": {

            'must': qry,  
            "filter": {
                "regexp": {
                    "node_id": {
                        "value": filename_filt,
                    }
                }
            }
        }}

    body = {
            "from": i_start,
            'size' : n_max,
            "query": qry

            # "highlight": {
            #     "fields": {
            #     "content": {}
            #     },
            #     "pre_tags": ["<mark>"],
            #     "post_tags": ["</mark>"],
            # }
        }
    
    log.debug(body)
    ret = es.search(index=es_index, body=body)
    log.debug(len(ret.body['hits']['hits']))

    mapper_source = {
        'id': 'node_id',
        'title': 'title',
        'link': 'link',
        'ltc': 'ltc',
        'source': 'source',
        'page': 'page',
        'n_pages': 'n_pages',
        'content': 'content',
    }
    hits = []
    for hit in ret.body['hits']['hits']:
        dc = {k_new:hit['_source'][k_old] for k_new, k_old in mapper_source.items()}
        dc['score'] = hit['_score']
        # print('\n')
        # import json
        # print(json.dumps(hit, indent=2))
        # print('\n')  
        dc['highlights'] = hit["_source"]['content']
        hits.append(dc)

    return hits

def es_search(search_str, field="content", n_max = 10, i_start = 0):
    ret = es_search_raw(search_str, field=field, n_max=n_max, i_start=i_start)


    mapper_source = {
        'id': 'node_id',
        'title': 'title',
        'link': 'link',
        'ltc': 'ltc',
        'source': 'source',
        'page': 'page',
        'n_pages': 'n_pages',
        'content': 'content',
    }

    hits = []
    for hit in ret.body['hits']['hits']:
        dc = {k_new:hit['_source'][k_old] for k_new, k_old in mapper_source.items()}
        dc['score'] = hit['_score']
        dc['highlights'] = '\n'.join(hit['highlight']['content'])
        hits.append(dc)

    return hits

def _get_ids_matching(node_ids):
    body = {
        "size": 10_000,
        "query": {"bool": {"should": [ {"match": {"node_id": id_}} for id_ in node_ids]}}            
    }
    r = es.search(index=es_index, body=body)
    return [el['_id'] for el in r.body['hits']['hits']]

def _do_bulk_delete(_ids):
    body = [{"delete": {"_index": es_index, "_id": str(_id)}} for _id in _ids]
    r = es.bulk(index=es_index, body=body, timeout='1000s')

def update_index(docs_dc):
    
    fun = lambda p: {"index": {"_index": es_index, "_id": '{}.PAGE_{}'.format(p['node_id'], p['page'])}}
    to_upload = []
    node_ids = []
    for doc in docs_dc:
        if not doc is None:
            for p in doc:
                if not p is None:
                    to_upload += [fun(p), p]
                    node_ids.append(p['node_id'])

    for i in range(0, len(node_ids), 1_000):
        s = slice(i, min(len(node_ids), i+1_000))
        node_ids_sub = node_ids[s]
        # delete old docs, since they might have more pages than the new ones!
        # we need to go iteratively here in case there is more than 10k documents in one query
        while _ids := _get_ids_matching(node_ids_sub):
            _do_bulk_delete(_ids)
        
    # upload the new documents using bulk upload
    r = es.bulk(index=es_index, body=to_upload, timeout='1000s')
    return len(r.raw['items'])


if __name__ == "__main__":

    pass

    # import time, getpass
    # from ReqTracker.core import graph_main, schema, helpers
    # from ReqTracker.io_files import langchain_nextcloud_io
    # from ReqTracker.ui import ui_state

    # # helpers.logging.getLogger('elastic_transport.transport').setLevel(helpers.logging.ERROR)
    
    # init(('elastic', ui_state.get_es_info()), ui_state.ES_DEFAULT_HOST, do_test=True)

    # es_info()

    # # g = graph_main.Graph(r"C:\Users\tglaubach\repos\mke-requirement-suite\scripts\20240217_tmp.sqlite")

    # # nodes = g.get_filelike()

    # # docs = langchain_nextcloud_io.get_nc_docs_dc(nodes, ui_state.nextcloud_login_data, verb=True, use_threading=True)

    # # pages = []
    # # for d in docs:
    # #     pages += d

    # # update_index(pages)

    