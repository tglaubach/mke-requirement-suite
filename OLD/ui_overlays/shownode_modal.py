
import urllib


import dash
import dash_bootstrap_components as dbc

from dash import html, dcc, callback, Input, Output, State, no_update, ALL, MATCH, callback_context, ctx

from ReqTracker.core import graph_main, helpers, schema, node_factory
from ReqTracker.io_files import elasticsearch_api, langchain_nextcloud_io
from ReqTracker.ui import ui_state, helpers_ui
from ReqTracker.ui.vis import cyto_vis
from ReqTracker.ui_vmc import vmc_docmaker as vmc

log = helpers.log

A2, A = helpers_ui.A2, helpers_ui.A
add_log_alert = ui_state.add_log_alert
GetGraphHold = ui_state.GetGraphHold

def layout():
    return html.Div([
        dcc.Store(id='id_toshow'), dbc.Modal(children=[
            html.Div([], id='sdlg_body'),
            html.Center(dbc.ModalFooter([
                dbc.Button('Show Similar Files', id='sdlg_morelikethis', color='info', size='xl'),
                dbc.Button('Close', id='sdlg_bclose', size='xl'),
            ]))
        ], id='sdlg', is_open=False, fullscreen=False, size='xl')])



@callback(
    Output('sdlg', 'is_open', allow_duplicate=True),
    Output('sdlg_body', 'children', allow_duplicate=True),
    Input('id_toshow', 'data'),
    prevent_initial_call = True,
)
def open_smodal_cb(data):
    is_open = no_update
    ret = no_update
    log.debug(f"open_smodal_cb: starting with {data}")


    if not data:
        return is_open, ret
    
    
    try:
        with ui_state.GetGraphHold() as graph:
            assert isinstance(data, (dict, str)), f'data ({type(data)} is of unknown type! {data}'
            if isinstance(data, dict):
                id_ = data['id']
                gname = data['gname']
            else:
                id_ = data
                gname = ''

            if gname:
                gv = ui_state.get_graph_view(graph, gname, apply_contraction=False)
                log.debug(f'opened: {gname}')
            else:
                gv = graph

            is_open = True
            if id_ in gv:
                el = gv[id_]
                subg = gv.filt_by_nn(el, el.id, n_max=ui_state.NODE_SHOW_NNMAX)
                el.is_match = True

                if len(subg) <= 5:
                    figsize = ui_state.NODE_SHOW_FIGSIZE 
                else:
                    figsize = min(len(subg) * 15 + 80, 250)

                ui_state.setv(classes_to_plot=ui_state.ALL_CLASSNAMES,
                                size_fun=ui_state.NODE_SHOW_SIZEFUN,
                                scale_xy=ui_state.NODE_SHOW_SCALE_XY,
                                network_plot_style = 'tree')
                

                fig = cyto_vis.get_fig(subg, 
                                       figsize=figsize, 
                                       no_root=False, 
                                       group_loose=False, 
                                       sep_reqs=False, 
                                       hide_reqs=False, 
                                       state=ui_state.get())

                dashdoc = vmc.DocFactory.from_doc(el.doc_get()).to_dash(disabled=True, add_header=False, can_select=False)
                dashdoc_row = html.Div([
                        dbc.Label("Doc Content:"),
                        html.Center(dashdoc),
                ])

                ret = [
                    dbc.ModalHeader(dbc.ModalTitle(helpers_ui.node2title(el))),
                    dbc.ModalBody([
                        fig,
                        html.Hr(),
                        *helpers_ui.node2item(el, add_header=False),
                        dashdoc_row
                    ], style={"maxHeight": 1000, "overflow": "auto"})
                ]

                # log.debug(f"open_smodal_cb: returning with: {is_open} and {len(ret)}")

    except Exception as err:
        e = A('{} open_smodal_cb --> ERROR: {}'.format(id_, err), color='danger')
        add_log_alert(e)
        log.error(err, exc_info=True)
        ret = helpers_ui.A2(*e)
    return is_open, ret


@callback(
    Output('sdlg', 'is_open', allow_duplicate=True),
    Input('sdlg_bclose', 'n_clicks'),
    prevent_initial_call = True,
)
def close_modal_cb(n):
    return False


@callback(
    Output('dialogs', 'children', allow_duplicate=True),
    Output('dialogs', 'is_open', allow_duplicate=True),
    Input(('sdlg_morelikethis'), 'n_clicks'),
    State('id_toshow', 'data'),
    prevent_initial_call = True,
)
def sdlgmorelikethis_cb(n1, data):
    ret = no_update
    is_open = no_update
    try:
        state = ui_state.get()
        if isinstance(data, dict):
            id_ = data['id']
            gname = data['gname']
        else:
            id_ = data
            gname = ''
        log.debug('sdlgmorelikethis_cb ' + str(id_))
        if n1 and id_:
            with GetGraphHold() as graph:
                tempstore = set()
                txt = helpers_ui.node2md(graph[id_])
                filename_filt = 'MKE*' if state.only_search_mke_docs else ''
                if elasticsearch_api.es is None:
                    ret = [dbc.Alert(f'can not search similar to "{id_}" because there is no connection connection to Elasticsearch', color='warning')]
                else:
                    hits = elasticsearch_api.es_more_like_this(txt, filename_filt=filename_filt, n_max=state.n_search_results)

                    def helpr(hit):
                        pi = hit['source']
                        uname = ui_state.NEXTCLOUD_LOGIN_DATA[0] if ui_state.NEXTCLOUD_LOGIN_DATA else ''
                        lnk = f'{langchain_nextcloud_io.DEFAULT_NC_URL}/remote.php/dav/files/{uname}/{pi}'
                        return helpers_ui.doc2item(hit, tempstore, lnk)
                    
                    els = [helpr(hit) for hit in hits]

                    st = {'margin': '15px', "minHeight": '100px', "maxHeight": '500px', 'overflow':'scroll', 'width': '100%'}

                    ret = [
                        dbc.ModalHeader(helpers_ui.node2title(graph[id_], prefix='Similar Files to ')),
                        dbc.ModalBody(html.Div(els, style=st)),
                    ]
                is_open = True

    except Exception as err:
        add_log_alert(A('sdlgmorelikethis_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

    return ret, is_open
