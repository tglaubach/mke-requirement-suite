

import urllib, os, datetime, io, os, json


from ReqTracker.io_files import nextcloud_interface
import dash

from dash import html, dcc, callback, Input, Output, State, no_update, ALL, MATCH, callback_context, ctx
import dash_bootstrap_components as dbc

from ReqTracker.core import graph_main, helpers, schema, node_factory
from ReqTracker.ui import ui_state, helpers_ui
from ReqTracker.ui.visualisation_main import LAYOUTS_POSSIBLE, SIZEFUNS_POSSIBLE, SIZEFUNS_ALLOWED

log = helpers.log

A2, A = helpers_ui.A2, helpers_ui.A
add_log_alert = ui_state.add_log_alert
GetGraphHold = ui_state.GetGraphHold





"""
██████   █████  ███    ██ ███████ ██          ██       ██████   ██████  
██   ██ ██   ██ ████   ██ ██      ██          ██      ██    ██ ██       
██████  ███████ ██ ██  ██ █████   ██          ██      ██    ██ ██   ███ 
██      ██   ██ ██  ██ ██ ██      ██          ██      ██    ██ ██    ██ 
██      ██   ██ ██   ████ ███████ ███████     ███████  ██████   ██████  
                                                                        """
                                                                        




layout = dbc.Offcanvas([
                html.Div([], id='logitems'),
                html.Hr(),
                dbc.Button('Clear Log', id='clearlog'),
            ],
            id="offcanvas_log",
            title='Event Log',
            is_open=False,
            placement='bottom',
        )



"""
 ██████ ██████      ██       ██████   ██████   ██████  ██ ███    ██  ██████  
██      ██   ██     ██      ██    ██ ██       ██       ██ ████   ██ ██       
██      ██████      ██      ██    ██ ██   ███ ██   ███ ██ ██ ██  ██ ██   ███ 
██      ██   ██     ██      ██    ██ ██    ██ ██    ██ ██ ██  ██ ██ ██    ██ 
 ██████ ██████      ███████  ██████   ██████   ██████  ██ ██   ████  ██████  
                                                                             

"""                     


@callback(Output('logitems', 'children', allow_duplicate=True),
          Input("clearlog", "n_clicks"),
          prevent_initial_call=True)
def on_log_change_cb(n):
    return []

@callback(Output('logitems', 'children', allow_duplicate=True),
          Output("offcanvas_log", "is_open", allow_duplicate=True),
          Output('logtxt', 'children', allow_duplicate=True),
          Output('syslogtxt', 'children', allow_duplicate=True),
          Output('mem-t_last', 'data'),
          Input("log-interval", "n_intervals"),
          Input("showlog", "data"),   
          State('logitems', 'children'),
          State('mem-t_last', 'data'),
          prevent_initial_call=True)
def on_log_change_cb(n, new_msg, oldlog, t_last_s):
    is_open = no_update
    logalerts = no_update
    syslog_alarm = no_update

    try:
        triggered_id = ctx.triggered_id

        if triggered_id == 'showlog':
            ui_state.logdata.append(new_msg)
            
        if not ui_state.IS_SUBSCRIBER and not ui_state.IS_PLAYGROUND:
            t_last, updated_ids = ui_state.update_nextcloud_tick(helpers.parse_zulutime(t_last_s))
            if t_last is not None:
                t_last_s = helpers.make_zulustr(t_last, remove_ms=False)

        # log.debug(f'on_log_change_cb FINISHED! {t_last_s}')
        if not (oldlog is None or isinstance(oldlog, (list))):
            log.warning(f'Log is of type: {type(oldlog)} with value: {oldlog}')

        oldlog, is_open, logalerts = helpers_ui.on_log_change(ui_state.logdata, oldlog)
        
        if triggered_id == 'showlog':
            is_open = True

        n, color = helpers.fifo_log.get_err_info()
        if n:
            n = "99+" if n > 99 else str(n)
            syslog_alarm = html.Div([
                    'sys',
                    dbc.Badge(
                        n,
                        color=color,
                        pill=True,
                        text_color="white",
                        className="position-absolute top-0 start-100 translate-middle",
                        style={'display': 'none' if n == 0 else 'block'}
                    )
                ])
        else:
            syslog_alarm = 'sys'
            
    except Exception as err:
        log.error(err, exc_info=True)
        oldlog.insert(0, dbc.Alert(str(err), color='danger'))
        while len(oldlog) > 50:
            oldlog.pop()
        is_open = True

    # log.debug((oldlog, is_open, t_last_s))
    return oldlog, is_open, logalerts, syslog_alarm, t_last_s


    

