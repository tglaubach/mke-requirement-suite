
import urllib, os, datetime, io, os, json


import dash

from dash import html, dcc, callback, Input, Output, State, no_update, ALL, MATCH, callback_context, ctx
import dash_bootstrap_components as dbc

from ReqTracker.core import graph_main, helpers, schema, node_factory
from ReqTracker.io import base_io, graphdb_io

from ReqTracker.io_files import langchain_nextcloud_io, nextcloud_interface
from ReqTracker.io_files.elasticsearch_api import update_index
from ReqTracker.ui import ui_state, helpers_ui
from ReqTracker.ui.visualisation_main import LAYOUTS_POSSIBLE, SIZEFUNS_POSSIBLE, SIZEFUNS_ALLOWED

log = helpers.log

A2, A = helpers_ui.A2, helpers_ui.A
add_log_alert = ui_state.add_log_alert
GetGraphHold = ui_state.GetGraphHold



"""
██████   █████  ███    ██ ███████ ██           ██████  ██████  ███    ██ ███████ ██  ██████  
██   ██ ██   ██ ████   ██ ██      ██          ██      ██    ██ ████   ██ ██      ██ ██       
██████  ███████ ██ ██  ██ █████   ██          ██      ██    ██ ██ ██  ██ █████   ██ ██   ███ 
██      ██   ██ ██  ██ ██ ██      ██          ██      ██    ██ ██  ██ ██ ██      ██ ██    ██ 
██      ██   ██ ██   ████ ███████ ███████      ██████  ██████  ██   ████ ██      ██  ██████  
                                                                                             """
                                                                                             


def mk_layout():
        
    state = ui_state.get()
    layout = dbc.Offcanvas(dbc.Container([
                dbc.Row([
                    dbc.Label('Save / Load'),
                    dbc.InputGroup([dbc.Input(id='inp-savefile', value=state.db_path, debounce=True, style={'minWidth': 140}, size="sm")]),
                    dbc.InputGroup([
                        dbc.Button('Save Backup', id='btn-save-backup-file', n_clicks=0),
                        dbc.Button('Default', id='btn-default-file', n_clicks=0),
                        dbc.Button(':memory:', id='btn-inmemory-db', n_clicks=0),
                    ], style={'margin': '2px'}, size="sm"),
                    ], style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),

                dbc.Row([
                    dbc.Label('DANGER ZONE:', style={'color': 'red'}),
                    dbc.Row(dcc.Loading(dbc.InputGroup([
                        dbc.InputGroupText('remotepath: ', style={'color': 'red'}),
                        dbc.Input(
                            id="inp-remotepath",
                            placeholder='e.g. "http://10.98.76.45:8020"',
                            value=ui_state.DEFAULT_PULL_PATH,
                            type='text', 
                            size="sm",
                            disabled=ui_state.DISABLE_DANGERZONE
                        ),
                    ], style={'margin': '2px'}, size="sm"), style={'width': '100%'})),
                    dbc.Row(dbc.InputGroup([
                        dcc.ConfirmDialogProvider(
                            children=dbc.Button('PULL Remote!', n_clicks=0, size="sm", color='danger', disabled=ui_state.DISABLE_DANGERZONE),
                            id='confirm-btn-pullremote',
                            message='This will pull data from a remote api and COMPLETLY OVERWRITE THE LOCAL DATABASE. Are you sure you want to continue?',
                        ),
                        dcc.ConfirmDialogProvider(
                            children=dbc.Button('Delete Local!', n_clicks=0, size="sm", color='danger', disabled=ui_state.DISABLE_DANGERZONE),
                            id='confirm-btn-dellocal',
                            message='DANGER!!! DANGER!!! This will COMPLETLY DELETE THE LOCAL DATABASE! Are you sure you want to continue?',
                        ),
                        dcc.ConfirmDialogProvider(
                            children=dbc.Button('Pull Example!', n_clicks=0, size="sm", color='warning', disabled=ui_state.DISABLE_DANGERZONE),
                            id='confirm-btn-pullexample',
                            message='This will pull data from a remote api and COMPLETLY OVERWRITE THE LOCAL DATABASE. Are you sure you want to continue?',
                        ),
                    ], style={'margin': '2px'}, size="sm"), style={'width': '100%'}),
                    dbc.Row(dbc.InputGroup([
                        dbc.InputGroupText("link_after_pull", style={'width': 140, 'color': 'red'}),
                        dbc.Checkbox(value=state.link_after_pull, id={"type": "par", "index": "link_after_pull"}, style={'margin': '8px'}, disabled=ui_state.DISABLE_DANGERZONE or ui_state.IS_PLAYGROUND),
                    ], style={'margin': '2px'}, size="sm"), style={'width': '100%'}),
                    ],
                    style={"border":"1px gray solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"}
                ),
                dbc.Row([
                    dbc.Label('Upload / Download'),
                    dcc.Upload(
                        id='upload-data',
                        children=html.Div([
                            'Upload Req-DB ',
                        ]),
                        style={
                            'width': '100%',
                            'height': '40px',
                            'lineHeight': '40px',
                            'borderWidth': '1px',
                            'borderStyle': 'dashed',
                            'borderRadius': '5px',
                            'textAlign': 'center',
                            'margin': '10px'
                        },
                        # Allow multiple files to be uploaded
                        multiple=False
                    ), 
                    dbc.InputGroup([
                        dbc.InputGroupText("Download", style={'width': 140}),
                        dbc.Button("json", id="btn-download-graph", n_clicks=0, style={'width': 50}),
                        dbc.Button("xlsx", id="btn-download-graph-xlsx", n_clicks=0, style={'width': 50}),
                        dbc.Button("sqlite", id="btn-download-graph-sqlite", n_clicks=0, style={'width': 50}),
                        dcc.Download(id="download-graph"),
                        ], style={'margin': '2px'}, size="sm"),

                    ],
                    style={"border":"1px gray solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),
                dbc.Row([
                    dbc.Label("Make New Dish from Form"),
                    dbc.InputGroup([
                        dbc.InputGroupText("Dish No.", style={'width': 140}),
                        dbc.Input(placeholder="P119", id="inp-newdish"),
                        dbc.Button('CREATE', id='btn-newdish', n_clicks=0),
                    ], style={'margin': '2px'}, size="sm"),
                    dcc.ConfirmDialog(
                        id='confirm-btn-newdish',
                        message='This will add a lot of new nodes. Are you sure you want to continue?',
                    )],
                    style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),
                dbc.Row([
                    dbc.Label("db_sinks"),
                    dbc.Textarea(value='\n'.join(state.db_sinks), id={"type": "par", "index": "db_sinks"}, size="sm", placeholder="insert newline separated sink pathes here...", debounce=True, style={'minHeight': 100, 'fontFamily': "Lucida Console, Courier New, monospace"}),
                ], 
                style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),

                dbc.Row([
                        dbc.Label("TAGS"),
                        dbc.Textarea(value=' '.join(state.tags_possible), id="inp-possibletags", size="sm", placeholder="insert space separated tags here...", style={'minHeight': 100, 'fontFamily': "Lucida Console, Courier New, monospace"}),
                    
                    dbc.InputGroup([
                        dbc.InputGroupText("Update:", style={'width': 140}),
                        dbc.Button('RESET', id='btn-resettags', n_clicks=0, style={'width': '30%'}),
                        dbc.Button('UPDATE', id='btn-updatetags', n_clicks=0, style={'width': '30%'}),
                    ], style={'margin': '2px'}, size="sm"),
                    ],
                    style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),
                dbc.Row([
                    dbc.Label("Settings"),
                    dbc.InputGroup([
                        dbc.Button('Show', id='btn-show-config', n_clicks=0),
                        dbc.Button('Reset to Default', id='btn-reset-config', n_clicks=0),
                        dbc.Button('Reload', id='btn-reload-config', n_clicks=0),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("sql_filter:", style={'width': 140}),
                        dbc.Input(value=state.sql_filter, id={"type": "par", "index": "sql_filter"}, type="text", size='sm', debounce=True),
                        dbc.Tooltip('loading filter e.G. "119A" to only load nodes with "119A" in the ID', target={"type": "par", "index": "sql_filter"}, placement='bottom'),
                    ], style={'margin': '2px'}, size="sm"),

                    dbc.InputGroup([
                        dbc.InputGroupText("searchmode:", style={'width': 140}),
                        dbc.Select(options=ui_state.SEARCHMODES_POSSIBLE, value=state.searchmode, id={"type": "par", "index": "searchmode"}),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("default tabid", style={'width': 140}),
                        dbc.Select(options=ui_state.TABIDS_DEFAULT, value=state.default_tabid, id={"type": "par", "index": "default_tabid"}),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("default filtmode", style={'width': 140}),
                        dbc.Select(options=ui_state.FILTMODES_POSSIBLE, value=state.filtmode, id={"type": "par", "index": "filtmode"}),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("only_search_mke_docs", style={'width': 140}),
                        dbc.Checkbox(value=state.only_search_mke_docs, id={"type": "par", "index": "only_search_mke_docs"}, style={'margin': '8px'}),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("hide_loose_docs", style={'width': 140}),
                        dbc.Checkbox(value=state.hide_loose_docs, id={"type": "par", "index": "hide_loose_docs"}, style={'margin': '8px'}),
                    ], style={'margin': '2px'}, size="sm"),
                    
                    dbc.InputGroup([
                        dbc.InputGroupText("start_contracted", style={'width': 140}),
                        dbc.Checkbox(value=state.start_contracted, id={"type": "par", "index": "start_contracted"}, style={'margin': '8px'}),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("scale_xy", style={'width': 140}),
                        dbc.Input(type="number", value=state.scale_xy, id={"type": "par", "index": "scale_xy"}, min=0., step=.1, max=2.),
                    ], style={'margin': '2px'}, size="sm"),
                    dbc.InputGroup([
                        dbc.InputGroupText("n_search_results", style={'width': 140}),
                        dbc.Input(type="number", value=state.n_search_results, id={"type": "par", "index": "n_search_results"}, min=5, step=5),
                    ], style={'margin': '2px'}, size="sm"),

                ], 
                    style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),

                dbc.Row([
                        dbc.Row(html.Label('Classtypes to Show')),
                        dbc.Row(dbc.InputGroup(dbc.Checklist(options=[x.__name__ for x in graph_main.classes], value=state.classes_to_plot, id={"type": "par", "index": "classes_to_plot"}, style={"marginLeft": "10px"}), style={'margin': '2px'}, size="sm")),
                        dbc.Row(dbc.InputGroup(dbc.Button('Set All', id='btn-show-all-classes', size='sm', n_clicks=0, style={'width': '100%'}), style={'margin': '2px'}, size="sm")),
                    ], 
                    style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                ),
                dcc.Loading(dbc.Row([
                    dbc.Row(html.Label('Nextcloud')),
                    dbc.Row(dbc.InputGroup([
                        dbc.Button('Sync Now!', id='btn-updatenextcloud', n_clicks=0, disabled=ui_state.IS_PLAYGROUND or ui_state.IS_READONLY),
                        dbc.Button('Re-Index files!', id='btn-index_nextcloud_all', n_clicks=0, disabled=ui_state.IS_PLAYGROUND or ui_state.IS_READONLY),
                    ], style={'margin': '2px'}, size="sm"), style={'width': '100%'}),
                    
                    dbc.Row(html.Label('Crawl Folder:')),
                    dbc.Row(dbc.InputGroup([
                        dbc.Button('GO!', id='btn-crawlnextcloud', n_clicks=0, disabled=ui_state.IS_PLAYGROUND or ui_state.IS_READONLY),
                        dbc.Input(
                            id="inp-foldercrawl",
                            placeholder='e.g. "/Shared/MeerKAT Extension/10_schedule"',
                            type='text', 
                            size="sm",
                        ),
                    ], style={'margin': '2px'}, size="sm"), style={'width': '100%'}),

                    ],style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                )),
                dbc.Row([
                    dbc.Label("About"),
                    dbc.Button('Show Version and System Info', id='btn-show-info', n_clicks=0, size='sm'),
                    ],style={"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "margin": "5px"} 
                )
            ]),
                id="offcanvas_config",
                title="Settings",
                is_open=False,
                placement='start',
                style={'width': '30%', 'minWidth': '350px'}
            )
    
    return layout


"""
 ██████ ██████       ██████  ██████  ███    ██ ███████ ██  ██████      ██████  ██       ██████  ███████ 
██      ██   ██     ██      ██    ██ ████   ██ ██      ██ ██           ██   ██ ██      ██       ██      
██      ██████      ██      ██    ██ ██ ██  ██ █████   ██ ██   ███     ██   ██ ██      ██   ███ ███████ 
██      ██   ██     ██      ██    ██ ██  ██ ██ ██      ██ ██    ██     ██   ██ ██      ██    ██      ██ 
 ██████ ██████       ██████  ██████  ██   ████ ██      ██  ██████      ██████  ███████  ██████  ███████ 
                                                                                                        
                     """                                                                                   

cng_config_cb_keys = ui_state.CHNG_CONFIG_CB_KEYS
# push_projvars = ui_state.push_projvars
# pull_projvars = ui_state.pull_projvars
update_tags = ui_state.update_tags


 

@callback(
        Output({"type": "par", "index": ALL}, 'value'),
        Input('btn-reload-config', 'n_clicks'),
        Input('btn-reset-config', 'n_clicks'),
        State({"type": "par", "index": ALL}, 'value'),
        State({"type": "par", "index": ALL}, 'id'),
        prevent_initial_call=False)
def btn_reload_state(n, n2, matches, indices):

    new_props = no_update

    try:
        if ctx.triggered_id == 'btn-reload-config':
            state = ui_state.get()
        elif n2 > 0:
            state = ui_state.reset_default()
        else:
            state = None

        if not state is None:
            new_props = [getattr(state, k['index']) for k in indices]
        return new_props
    
    except Exception as err:
        log.error(err, exc_info=True)
        add_log_alert(A('ERROR while setting params "{}"'.format(err), color="danger"))
    return new_props



@callback(
        Output('showlog', 'data', allow_duplicate=True),
        Output('plotupdate', 'data', allow_duplicate=True),
        Input({"type": "par", "index": ALL}, 'value'),
        prevent_initial_call=True)
def cng_config_cb(vals):
    plotupdate = no_update
    res = no_update

    try:
        state = ui_state.get()
        alldata = [{**ctx.triggered_prop_ids[d['prop_id']], **d} for d in ctx.triggered if d['prop_id'] in ctx.triggered_prop_ids]

        dc_to_update = {d['index']: d['value'] for d in alldata if d['index'] in state.to_dict()}
        

        if 'db_sinks' in dc_to_update and isinstance(dc_to_update['db_sinks'], str) and dc_to_update['db_sinks']:
            dc_to_update['db_sinks'] = [s.strip() for s in dc_to_update['db_sinks'].split('\n')]
        
        log.debug(f'updating: {dc_to_update}')
        ui_state.setv(**dc_to_update)

        # plotupdate = helpers.make_zulustr(helpers.get_utcnow())

        return res, plotupdate
    except Exception as err:
        log.error(err, exc_info=True)
        res = A('ERROR while setting params "{}"'.format(err), color="danger")

    return res, no_update


@callback(             
    Output('showlog', 'data', allow_duplicate=True),
    Input('btn-save-backup-file', 'n_clicks'),
    prevent_initial_call=True
)
def save_backup_cb(n_clickssave):
    try:
        res = no_update
        if n_clickssave:
            res = ui_state.save_backup()
    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        add_log_alert(A("ERROR save_backup_cb: {}".format(err), color="danger"))
    return res

@callback(
        Output('btn-refresh', 'n_clicks', allow_duplicate=True), # HACK?
        Output({"type": "par", "index": "sql_filter"}, 'valid', allow_duplicate=True),
        Output({"type": "par", "index": "sql_filter"}, 'invalid', allow_duplicate=True),
        Input({"type": "par", "index": "sql_filter"}, 'value'),
        State('btn-refresh', 'n_clicks'), # HACK?
        prevent_initial_call=True)
def update_(val, n):
    try:
        res = no_update
        valid = no_update
        invalid = no_update

        graphdb_io.test_no_kws(val)
        res = n + 1 # HACK?
        valid = True
        invalid = False
        add_log_alert(A(f"successfully updated sql pre filter to: '{val}'", color="success"))
    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        add_log_alert(A("ERROR save_backup_cb: {}".format(err), color="danger"))
        valid = False, 
        invalid = True

    return res, valid, invalid


 
@callback(
        Output('inp-search', 'value', allow_duplicate=True),
        Output({"type": "par", "index": "classes_to_plot"}, 'value', allow_duplicate=True),
        Output('showlog', 'data', allow_duplicate=True),
        Input('btn-show-all-classes', 'n_clicks'),
        prevent_initial_call=True)
def btn_default_classes_to_plot_cb(n1):
    s = no_update
    classes_to_plot = no_update
    res = no_update

    try:
        defs = [x.__name__ for x in graph_main.classes]
        log.debug(f'btn_default_classes_to_plot_cb to ALL')
        ui_state.setv(classes_to_plot=defs)
        classes_to_plot = defs
        s = ''
    except Exception as err:
        log.error(err, exc_info=True)
        res = A('ERROR while btn_default_classes_to_plot_cb "{}"'.format(err), color="danger")
        add_log_alert(res)
        
    return s, classes_to_plot, res


@callback(
        Output('inp-savefile', 'value', allow_duplicate=True),
        Output('showlog', 'data', allow_duplicate=True),
        Input('btn-default-file', 'n_clicks'),
        State('inp-savefile', 'value'),
        prevent_initial_call=True)
def btn_default_pth_cb(n1, pth):
    try:
        db_path=ui_state.DEFAULT_DB_PATH
        log.debug(f'Setting db_path from "{pth}" to "{db_path}"')
        res = no_update
        ui_state.setv(db_path=db_path)
        return db_path, res
    except Exception as err:
        log.error(err, exc_info=True)
        res = A('ERROR while db_path "{}"'.format(err), color="danger")
        add_log_alert(res)
        
    return no_update, res


@callback(
        Output('inp-savefile', 'value', allow_duplicate=True),
        Output('showlog', 'data', allow_duplicate=True),
        Input('btn-inmemory-db', 'n_clicks'),
        State('inp-savefile', 'value'),
        prevent_initial_call=True)
def btn_default_pth_mem_cb(n1, pth):
    try:
        log.debug(f'Setting db_path from "{pth}" to ":memory:"')
        res = no_update
        db_path=':memory:'
        ui_state.setv(db_path=db_path)
        return db_path, res
    except Exception as err:
        log.error(err, exc_info=True)
        res = A('ERROR while db_path "{}"'.format(err), color="danger")
        add_log_alert(res)
        
    return no_update, res




@callback(
    Output('inp-search', 'value', allow_duplicate=True),
    Output('showlog', 'data', allow_duplicate=True),
    Output('inp-savefile', 'valid'),
    Output('inp-savefile', 'invalid'),
    Input("inp-savefile", 'value'),
    prevent_initial_call=True,
)
def new_savefile_cb(new_db_path):
    s = no_update
    res = no_update
    try:
        
        if new_db_path != ':memory:':

            datasource = graph_main.get_io_source(new_db_path, auth=ui_state.AUTH)

            assert datasource is not None, f'The new DB path {new_db_path} could not be resolved as a proper database source.'
            if not datasource.name.startswith('http'):
                with open(new_db_path, 'rb') as fp:
                    pass # raises File exception if not found
            else:
                r = datasource.ping()
                add_log_alert(A(f'{datasource.name}: PING -> {r}', color='info'))

        db_path=new_db_path
        ui_state.setv(db_path=db_path)
        add_log_alert(A(f'successfully set db_path="{db_path}"', color='success'))
        
        res = no_update
        invalid = False
        valid = True
        s = ''

    except Exception as err:
        log.error(err, exc_info=True)
        res = A('ERROR while setting param "{}"'.format(err), color="danger")
        add_log_alert(res)
        invalid = True
        valid = False

    return s, res, valid, invalid

@callback(
        Output('showlog', 'data', allow_duplicate=True),
        Output('inp-possibletags', 'valid'),
        Output('inp-possibletags', 'invalid'),
        Input('btn-resettags', 'n_clicks'),
        Input('btn-updatetags', 'n_clicks'),
        State('inp-possibletags', 'value'),
        prevent_initial_call=True)
def cng_config_cb(n1, n2, tags):
    try:
        valid = no_update
        invalid = no_update
        
        if ctx.triggered_id == 'btn-updatetags':
            tt, newtags = update_tags(tags.split())
            tags = ' '.join(set(tt))
            if newtags:
                valid = True
        else:
            tags = ' '.join(set(ui_state.get().tags_possible))

    except Exception as err:
        log.error(err, exc_info=True)
        res = A('ERROR while setting param "{}"'.format(err), color="danger")
        add_log_alert(res)
        invalid = True
        valid = False
        
    return tags, valid, invalid


@callback(
        Output('confirm-btn-newdish', 'displayed'),
        Input('btn-newdish', 'n_clicks'),
        State('inp-newdish', 'value'))
def display_confirm(value, dish_name):
    if value:
        return True if dish_name else False
    else:
        False
    


@callback(Output('showlog', 'data', allow_duplicate=True),
            Input('confirm-btn-newdish', 'submit_n_clicks'),
            State('inp-newdish', 'value'),
            prevent_initial_call=True)
def update_newdish_cb(submit_n_clicks, dish_name):
    res = None
    try:
        if submit_n_clicks:
            ui_state.raise_on_readonly()
            if dish_name:
                with GetGraphHold() as graph:
                    graph.clone_new_dish(dish_name)
                    res = A('generated new nodes for dish: ' + dish_name, color='success')
    except Exception as err:
        res = A('ERROR while generating new dish nodes (dish="{}"): "{}"'.format(dish_name, err), color="danger")
    if res:
        add_log_alert(res)
    return res
    

    

@callback(
    Output("download-graph", "data"),
    Input("btn-download-graph-sqlite", "n_clicks"),
    Input("btn-download-graph-xlsx", "n_clicks"),
    Input("btn-download-graph", "n_clicks"),
    prevent_initial_call=True,
)
def download_graph(n_clicks, n2, n3):
    try:
        ret = no_update
        with GetGraphHold() as graph:
            
            changed_id = [p['prop_id'] for p in callback_context.triggered][0]
            
            filename = datetime.datetime.now().strftime('%Y%m%d_%H%M_reqdb')
            log.debug(('download_graph', changed_id, filename))

            if 'btn-download-graph-xlsx' in changed_id:
                x = io.BytesIO()
                graph.save((x, 'xlsx'))
                x.seek(0)
                content = x.read()
                filename += '.xlsx'

                ret = dcc.send_bytes(content, filename, type='.xlsx')
            elif 'btn-download-graph-sqlite' in changed_id:
                
                fname = datetime.datetime.now().strftime('%Y%m%d_%H%M_reqdb.sqlite')
                g = graph.clone()
                filename = os.path.join(ui_state.DEFAULTUPLOAD_PATH, fname)
                g.save(filename)
                with open(filename, 'rb') as fp:
                    content = fp.read()
                os.remove(filename)
                ret = dcc.send_bytes(content, fname, type='.sqlite')
            else:
                content = json.dumps(graph.serialize(), indent=2)
                filename += '.json'
                ret = dict(content=content, filename=filename)
            
            log.debug('sending ' + ret['filename'])

    except Exception as err:
        ret = A('download_graph({}) --> ERROR: {}'.format(graph, err), color='danger')
        add_log_alert(ret)
        log.error(err, exc_info=True)
    
    return ret
    

@callback(
    Output('showlog', 'data', allow_duplicate=True),
    Output("mem-t_last", "data", allow_duplicate=True),
    Input("btn-updatenextcloud", "n_clicks"),
    State("mem-t_last", "data"),
    prevent_initial_call=True,
)
def updatenextcloud_cb(n3, t_last_s):
    try:
        ui_state.raise_on_readonly()
        if not ui_state.NEXTCLOUD_LOGIN_DATA:
            ret = A('updatenextcloud_cb --> ERROR: {}'.format('no Nextcloud login given!'), color='danger')
        else:
            t_last, updated_ids = ui_state.update_nextcloud_tick(helpers.parse_zulutime(t_last_s), do_tick=True)
            t_last_s = helpers.make_zulustr(t_last, remove_ms=False)

            ret = helpers_ui.A2(A(f'Successfully synced from Nextcloud server! ({t_last_s})', color='success'))

    except Exception as err:
        ret = A('updatenextcloud_cb --> ERROR: {}'.format(err), color='danger')
        add_log_alert(ret)
        log.error(err, exc_info=True)
    return ret, t_last_s




@callback(
    Output('showlog', 'data', allow_duplicate=True),
    Input("btn-index_nextcloud_all", "n_clicks"),
    prevent_initial_call=True,
)
def index_nextcloud_all_cb(n3):
    valid, invalid = no_update, no_update

    try:
        ui_state.raise_on_readonly()
        if not ui_state.NEXTCLOUD_LOGIN_DATA:
            ret = A('index_nextcloud_all_cb --> ERROR: {}'.format('no Nextcloud login given!'), color='danger')
        else:

            with GetGraphHold() as graph:
                files = graph.get_filelike()
                batch = []
                while files:
                    batch.append(files.pop())
                    if len(batch) > 80 or not files:
                        docs = langchain_nextcloud_io.get_nc_docs_dc(batch, ui_state.NEXTCLOUD_LOGIN_DATA, verb=True, use_threading=True)
                        update_index(docs)
                        batch = []
            ret = helpers_ui.A2(A(f'Successfully indexed from Nextcloud!', color='success'))

    except Exception as err:
        ret = helpers_ui.A2(A('updatenextcloud_cb --> ERROR: {}'.format(err), color='danger'))
        # add_log_alert(ret)
        log.error(err, exc_info=True)

    return ret, valid, invalid




@callback(
    Output('showlog', 'data', allow_duplicate=True),
    Output("inp-foldercrawl", 'valid'),
    Output("inp-foldercrawl", 'invalid'),
    Input("btn-crawlnextcloud", "n_clicks"),
    State("inp-foldercrawl", "value"),
    prevent_initial_call=True,
)
def crawlnextcloud_cb(n3, value):
    valid, invalid = no_update, no_update

    try:
        ui_state.raise_on_readonly()
        if not ui_state.NEXTCLOUD_LOGIN_DATA:
            ret = A('updatenextcloud_cb --> ERROR: {}'.format('no Nextcloud login given!'), color='danger')
        elif not value:
            ret = A('crawlnextcloud_cb --> ERROR: {}'.format('no folder path given!'), color='danger')
        else:
            d = value.replace('\\', '/')
            if 'Shared/' in d: 
                d = '/Shared/' + d.split('Shared/')[-1]
            d = d + '/' if not d.endswith('/') else d

            with GetGraphHold() as graph:
                dicts = nextcloud_interface.crawl_directory(d, ui_state.NEXTCLOUD_LOGIN_DATA, verb=True, as_dict=True, ignore_errors=False)
                f = lambda d: node_factory.construct_doc_from_dc_data(d, graph)

                ids = [f(x) for x in dicts]
                ids = [x for x in ids if x]
                ret = A('Successfully crawled directory "{}" and found N={} files!'.format(d, len(ids)), color='success')

            valid = True
            invalid = False

    except Exception as err:
        ret = A('updatenextcloud_cb --> ERROR: {}'.format(err), color='danger')
        # add_log_alert(ret)
        log.error(err, exc_info=True)
        valid = False
        invalid = True

    return ret, valid, invalid

@callback(
    Output('showlog', 'data', allow_duplicate=True),
    Output("inp-remotepath", 'valid', allow_duplicate=True),
    Output("inp-remotepath", 'invalid', allow_duplicate=True),
    Input("confirm-btn-dellocal", "submit_n_clicks"),
    prevent_initial_call=True,
)
def dellocal_cb(n3):
    valid, invalid = no_update, no_update
    try:
        if os.environ.get('IS_READONLY', ''):
            raise PermissionError('This instance is a readonly client. The operation you are trying to carry out would change the database and is therefore not allowed.')

        state = ui_state.get()
        if state.db_path == ':memory:':
            ui_state._in_memory_graph = None
        else:
            os.remove(state.db_path)
        ret = A('Deleted the database: {}'.format(state.db_path), color='success')
        valid = True
        invalid = False
    except Exception as err:
        ret = A('updatenextcloud_cb --> ERROR: {}'.format(err), color='danger')
        # add_log_alert(ret)
        log.error(err, exc_info=True)
        valid = False
        invalid = True

    return ret, valid, invalid




@callback(
    Output('showlog', 'data', allow_duplicate=True),
    Output("inp-remotepath", 'valid', allow_duplicate=True),
    Output("inp-remotepath", 'invalid', allow_duplicate=True),
    Output('plotupdate', 'data', allow_duplicate=True),
    Output('btn-refresh', 'n_clicks', allow_duplicate=True),
    Output({'type': 'par', 'index': 'db_sinks'}, 'value', allow_duplicate=True),
    Input("confirm-btn-pullremote", "submit_n_clicks"),
    Input("confirm-btn-pullexample", "submit_n_clicks"),
    State("inp-remotepath", "value"),
    State('btn-refresh', 'n_clicks'),
    prevent_initial_call=True,
)
def pullremote_cb(n3, n4, value, n):
    valid, invalid, plotupdate = no_update, no_update, no_update
    db_sinks = no_update
    no = no_update

    try:
        state = ui_state.get()
        is_example = False
        if 'pullexample' in ctx.triggered_id:
            is_example = True
            value = ui_state.EXAMPLE_DB_FILE_PATH
            log.info(f'opening default db file: {value}')

        if not value:
            ret = A('pullremote_cb --> ERROR: {}'.format('no file path given!'), color='danger')
        else:
            remotegraph = graph_main.Graph(value)
            if state.db_path == ':memory:':
                ui_state._in_memory_graph = remotegraph.clone()
            else:
                remotegraph.save(state.db_path)
            
            # log.warning(f"{ui_state.IS_PLAYGROUND = }{ ui_state.link_after_pull =}, {is_example=}")
            if not ui_state.IS_PLAYGROUND and state.link_after_pull and not is_example:
                if not value in state.db_sinks:
                    state.db_sinks.append(value)
                    db_sinks = state.db_sinks

                log.info(f'Linked db "{state.db_path}" with additional sinks: {state.db_sinks}')


            ret = A(f'pullremote_cb --> SUCCESS: pulled  {remotegraph} with N={len(remotegraph)} nodes from "{value}" and saved in "{state.db_path}"', color='success')

            valid = True
            invalid = False
            plotupdate = ''
            no = n+1
            # value = ''

    except Exception as err:
        ret = A('updatenextcloud_cb --> ERROR: {}'.format(err), color='danger')
        # add_log_alert(ret)
        log.error(err, exc_info=True)
        valid = False
        invalid = True

    return ret, valid, invalid, plotupdate, no, db_sinks



