
import traceback
import time, datetime
import numpy as np
import logging

from flask import Flask, request, jsonify, url_for, render_template_string


# add to path for testing
import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, current_dir)
    sys.path.insert(0, parent_dir)


import ReqTracker

from ReqTracker.core.helpers import log

# from ReqTracker.core import node_factory, schema, graph_main, graph_base, helpers, nextcloud_interface

# from ReqTracker.ui import ui_state
# from ReqTracker.ui.helpers_ui import A, A2, idfromlink, get_log_sys_modal, get_log_changes_modal, get_sysinfo, mk_href

# from ReqTracker.ui_overlays import config_offcanvas, log_offcanvas, shownode_modal
# from ReqTracker.ui_vmc import vmc_docmaker, html_render

from ReqTracker.api import routes, uib, uim, uidf

t_started = datetime.datetime.utcnow()

template_dir = os.path.join(current_dir, 'templates')

print(template_dir)

app = Flask('ReqTrack_db_server', template_folder=template_dir, static_folder=os.path.join(current_dir, 'static'))

uib.add_routes(app)
uim.add_routes(app)
routes.add_routes(app)
uidf.add_routes(app)


@app.route("/")
@app.route('/#')
@app.route('/index')
def index():
    try:
        
        lines = [f'<p>{routes.m_ping()}</p>', "<hr>", "<h4>Available Routes:</h4>"]

        def has_no_empty_params(rule):
            defaults = rule.defaults if rule.defaults is not None else ()
            arguments = rule.arguments if rule.arguments is not None else ()
            return len(defaults) >= len(arguments)

        urls = []
        tmp = set()
        tpls = []
        urls = []
        for rule in app.url_map.iter_rules():
            # Filter out rules we can't navigate to in a browser
            # and rules that require parameters
            
            s = ''
            if rule.arguments and not rule.defaults:
                s += '/'.join([f'\<{k}\>' for k in rule.arguments])
            elif rule.defaults:
                s += '/'.join([f'\<{k}={v}\>' for k,v in rule.defaults.items()])

            if "GET" in rule.methods and has_no_empty_params(rule):
                if rule.defaults:
                    url = url_for(rule.endpoint, **(rule.defaults or {}))
                else:
                    url = url_for(rule.endpoint)
                tmp.add(url)
                tpls.append((url, s))
            else:
                url = rule.endpoint
                if not url in urls:
                    urls.append(url)

        content = '\n'.join(lines) + """<ul>{% for (url, s) in lst %}
            <li><a href={{ url }}> {{ url + s }}</a></li>
        {% endfor %}
        {% for u in urls %}
            <li>{{ u }}</li>
        {% endfor %}</ul>"""
        
        page = f'''<!DOCTYPE html>
    <html>
        <body>
            <div id="main-content">
{content}
            </div>
        </body>
    </html>'''
        return render_template_string(page, lst=tpls, urls=urls)
    except Exception as err:
        log.exception(err)
        raise


@app.route('/uim_graph', methods=['GET'])
def uim_graph():
    print('/uim/v1/graph/')
    try:    
        with uim.GetGraphHold() as graph:
            id = request.args.get('id', None)
            nn_max = request.args.get('nn', 25)
            TDLR = request.args.get('TDLR', 'TD')
            graphtype = request.args.get('graphtype', 'graph')
            if not id:
                id = next((n for n in graph if n.do_serialize))

            if not id in graph:
                return jsonify({"error": f"404 key '{id}' not in graph: '{graph}'"}), 404
            
            n = graph[id]
            lst = [(s.name, uim.graph_base.colors[s], ('selected' if n.status == s else '')) for s in uim.graph_base.STATUS]
            gv = n.nng(nn_max)
            content = uim.mmg_graph(gv, TDLR, graphtype)
            
        js = ""
        tmpl = uim.make_page(content, js)
        return render_template_string(tmpl, lst=lst, text=n.text, id_=id, status = n.graph_base.STATUS.name)
    except Exception as err:
        log.exception(err)
        return jsonify({"error": traceback.format_exc()}), 500

if __name__ == '__main__':
    log.setLevel('DEBUG')

    app.run('0.0.0.0', 8018, debug=True)
