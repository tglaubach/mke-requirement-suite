

class ViewContainer(graph_base.BaseNode):
    do_serialize=False
    def __init__(self, titletext, graph=None, kview=''):
        super().__init__(titletext, titletext, dc={})
        self.graph = graph
        self.kview = kview
    
    def get_row(self, parent_id='', color_col='color'):
        row = super().get_row(parent_id, color_col)
        nsub = len(self.graph.get_children(self)) if self.graph is not None else -1
        row['node_id'] += ' | N={} nodes'.format(nsub)
        return row

    def __str__(self) -> str:
        return str(self.id)
    


def with_me_as_graph(f):
    # return f
    def inner_func(calling_obj, *args, **kwargs):        
        t = time.time()
        log.debug('ENTERING "with_me_as_graph"')
        assert issubclass(type(calling_obj), GraphView), 'with_me_as_graph only works on objects inheriting from GraphView'
        if hasattr(calling_obj, 'nodes'):
            nodes, graphs = [], []
            try:
                nodes, graphs = calling_obj.nodes, [n.graph for n in calling_obj.nodes]
                for n in calling_obj.nodes:
                    n.graph = calling_obj
                ret = f(calling_obj, *args, **kwargs)  
            finally:
                for n, g in zip(nodes, graphs):
                    n.graph = g
        else:
            ret = f(calling_obj, *args, **kwargs)  
        dt = time.time() - t 
        log.debug(f'EXITING "with_me_as_graph" dt={dt * 1000:.2f}ms')

        return ret       
    
    return inner_func


"""

 ██████  ██████   █████  ██████  ██   ██       ██    ██ ██ ███████ ██     ██ 
██       ██   ██ ██   ██ ██   ██ ██   ██       ██    ██ ██ ██      ██     ██ 
██   ███ ██████  ███████ ██████  ███████ █████ ██    ██ ██ █████   ██  █  ██ 
██    ██ ██   ██ ██   ██ ██      ██   ██        ██  ██  ██ ██      ██ ███ ██ 
 ██████  ██   ██ ██   ██ ██      ██   ██         ████   ██ ███████  ███ ███  
                                                                             
                                                                             

"""

class GraphView(object):

    @classmethod
    def construct(self, *args, **kwargs):
        """wrapper for graph_main.construct function as classmethod"""
        return construct(*args, **kwargs)
    
    @classmethod
    def copy_node(self, n:graph_base.BaseNode, new_id, fields = None):
        dc = n.to_dict(add_parents=False)
        if not fields:
            fields = 'id _text data notes _status'.split()
        elif isinstance(fields, str) and fields.lower() == 'all':
            fields = list(dc.keys())

        dc = json.loads(json.dumps({k:v for k, v in dc.items() if k in fields}))
        if isinstance(new_id, dict):
            dc = {**dc, **new_id}
        else:
            dc['id'] = new_id
        return self.construct(dc)

    def to_plot_recs(self, color_col='color'):
        nodes = self.get_lvl0_nodes(include_root=True)
        data = []
        for n in nodes:
            data += n.get_rows('', color_col, graph=self)
        return data
    
    def get_node_as_dict(self, id, add_parents=True, ret_empty_on_fail = True):
        if id not in self and ret_empty_on_fail:
            return {}
        return self[id].to_dict(add_parents=add_parents, graph=self)
    

    ##################################################################################
    ##################################################################################
    ##################################################################################
    ##################################################################################

    def __init__(self, root_node='root', view=None, gather_after_init=True) -> None:

        self.view = View([]) if view is None else view
        
        if not issubclass(type(root_node), graph_base.BaseNode):
            root_node = ViewContainer(root_node, self, root_node)

        self._t_constructed = helpers.get_utcnow()
        if not hasattr(self.view.vs, 'name') or root_node.id not in self.view.vs['name']:
            self.view.add_vertex(root_node.id)

        self.root_node = root_node
        self._graph_parent = None

        if gather_after_init:
            self.gather_loose()
            self._strip()


    @property
    def name(self):
        return self.root_node.id
    
    @property
    def vn(self):
        return self.get_nodes_view()

    def get_info(self):
        s = """GRAPH: {}\n   - read-only?:{}\n   - N={} Nodes (Nv={} Nodes in View)\n   - M={} Edges\n   - t-constructed: {}\n"""
        s = s.format(self, self.is_readonly, len(self.nodes), self.get_len_nodes_view(), len(self.view.get_edgelist()), helpers.make_zulustr(self._t_constructed)) 
        return s
    
    def info(self):
        print(self.get_info())

    def _strip(self):
        self.view.simplify()
    
    def __len__(self):
        return len(self.view.vs)
    

    def pprint(self):
        for node in self.get_highest_nodes():
            node.pprint(graph=self)

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return 'GraphView-{}-{}'.format(self.name, id(self))
    
    ##################################################################################
    ##################################################################################

    def __contains__(self, key):
        id_ = getattr(key, 'id') if hasattr(key, 'id') else key
        self.get_nodes_view()


        return id_ in self._container_nodes or id_ in self.nodes
    
    def __getitem__(self, node_id) -> graph_base.BaseNode:
        if isinstance(node_id, list):
            return [self[id_] for id_ in node_id]
        if isinstance(node_id, tuple):
            return tuple([self[id_] for id_ in node_id])
        if isinstance(node_id, set):
            return set([self[id_] for id_ in node_id])
        
        id_ = getattr(node_id, 'id') if hasattr(node_id, 'id') else node_id

        if id_ not in self.nodes and id_ in self._container_nodes:
            return self._container_nodes[node_id]
        else:
            try:
                return self.nodes[node_id]
            except KeyError as err:
                closest_ids = simple_search_keys([n.id for n in self.nodes], str(node_id), fuzzy=True)
                closest_ids = ' '.join(closest_ids)
                raise KeyError(f'key="{node_id}" was not found in graph. Closest ids: to input: {closest_ids}')
            


    def __test_nodes_contained_sub(self, node):
        node = node.id if not isinstance(node, str) else node
        assert node in self, f'the node_id "{node}" is not in this graphs list of nodes'
        assert node in self.view.vs['name'], f'the node_id "{node}" is not in this graph_view list of node ids'


    def _test_nodes_contained(self, nodes_to_check):
        if issubclass(type(nodes_to_check), graph_base.BaseNode) or isinstance(nodes_to_check, str):
            nodes_to_check = [nodes_to_check]

        if nodes_to_check:
            nodes_to_check = el2id(nodes_to_check)
            assert 'name' in self.view.vs.attributes(), 'this view has no "name" attribute! maybe its empty?'
            for node in nodes_to_check:
                if hasattr(node, '__len__') and not isinstance(node, str):
                    for n in node:
                        self.__test_nodes_contained_sub(n)
                else:
                    self.__test_nodes_contained_sub(node)



    ##################################################################################
    ##################################################################################
    # views -> igraph API 
    ##################################################################################
    ##################################################################################
    
    def propagate_attributes(self):
        for id_ in self.view.vs['name']:
            i = self.view.vs['name'].index(id_)
            n = self[id_]
            attrs = n.get_row()
            for k, v in attrs.items():
                self.view.vs[i][k] = v

    def gather_loose(self):
        to_gather = [n for n in self.get_lvl0_nodes() if n != self.root_node]
        if to_gather:
            edges = [(self.root_node.id, n.id) for n in to_gather]
            # for performance reasons we can here direcly add the edges to the view, instead of calling self.add_edges
            # since this does a lot of testing if the nodes exist. Here we are just need to rewire existing ones
            self.view.add_edges(edges)
        return self
    

    def to_view(self, name_new='', view_or_edges=None, container_nodes = None, do_gather_loose=True):

        if not name_new and view_or_edges is None and container_nodes is None:
            name_new = self.name
            view_or_edges = self.view.copy()
            container_nodes = [n for n in self.vn if not n.do_serialize]

        if container_nodes is None:
            container_nodes = []

        for n in self._container_nodes:
            if n not in container_nodes and n != self.root_node:
                container_nodes.append(n)
    
        if issubclass(type(view_or_edges), ig.Graph):
            view = view_or_edges
        else:
            edges = el2id(view_or_edges, mapper={self.root_node.id: name_new})
            view = View(edges)
            
        nodes_excl = [self.root_node] if view.vs else []

        if nodes_excl:
            delete_vertices(view, nodes_excl)

        g = GraphView(name_new, view, gather_after_init=False)
        g._graph_parent = self

        if do_gather_loose:
            g.gather_loose()
            g._strip()

        return g
    

    def get_viewnodes(self, as_id=False):
        nodes = self.view.vs['name']
        return nodes if as_id else self[nodes]
    
    def get_nodes_view(self, as_id=False):
        return self.get_viewnodes(as_id=as_id)

    def get_len_nodes_view(self):
        return len(self)

    def get_edges(self, as_id=False, with_root=True):
        nn = self.view.vs['name']
        edges = [(nn[a], nn[b]) for a, b in self.view.get_edgelist()]
        if not with_root:
            edges = [tpl for tpl in edges if self.root_node.id not in tpl]
            
        return edges if as_id else id2el(edges, self)

    def get_children(self, node):
        return self.__get_connected(node, self.view.successors)

    def get_parents(self, node):
        return self.__get_connected(node, self.view.predecessors)
    
    def get_linked(self, node):
        return self.__get_connected(node, self.view.neighbors)
    
    def __get_connected(self, node, testfun):
        node = id2el(node, self)
        if 'name' not in self.view.vs.attributes() or node.id not in self.view.vs['name']:
            return tuple()

        self._test_nodes_contained([node])
        node_ids = tuple([self.view.vs['name'][i] for i in testfun(node.id)])
        return self[node_ids]
    

    ##################################################################################
    ##################################################################################
    # methods graph selection etc.
    ##################################################################################
    ##################################################################################
        
    def flatten(self):
        return list(self.get_nodes_view(as_id=False))

    def get_lowest_nodes(self):
        f = lambda vertex: not vertex.successors()
        ids = [v['name'] for v in self.view.vs.select(f) if v['name'] != self.root_node.id]
        return self[ids]
    
    def get_lvl0_nodes(self, include_root=False):
        f = lambda vertex: not vertex.predecessors()
        
        ids = [v['name'] for v in self.view.vs.select(f) if v['name'] != self.root_node.id]
        if include_root:
            ids = [self.root_node.id] + ids
        ids = [i for i in ids if i in self]
        return self[ids]
    
    def get_highest_nodes(self):
        f = lambda vertex: not vertex.predecessors()
        highest = [self[v['name']] for v in self.view.vs.select(f) if v['name'] != self.root_node.id]
        nn = self.view.vs['name']
        highest += [self[nn[n]] for n in self.view.successors(self.root_node.id)]
        highest = list(set(highest))
        highest.sort(key=lambda x: x.id)
        return highest
    
    def get_loose_nodes(self):
        f = lambda vertex: not vertex.neighbors()
        return [self[v['name']] for v in self.view.vs.select(f) if v['name'] != self.root_node.id]

    def get_flat_dict(self, copy=False):
        return {n.id:n for n in self.nodes} if copy else self.nodes
    
    def get_adjecency_matrix(self):
        return np.array(self.view.get_adjacency().data)
    
    def get_filelike(self):
        return [n for n in self.nodes if hasattr(n, 'path')]
    


    def get_xy(self, lyout='tree', invertY=True, normalize=False, view = None):
        return self.view.get_positions(lyout=lyout, invertY=invertY, normalize=normalize, view=view, as_numpy=True)

    
    ##################################################################################
    ##################################################################################
    # methods graph manipulation
    ##################################################################################
    ##################################################################################
    
    def set_me_as_graph(self, nodes=None):
        if nodes is None:
            nodes = self.get_viewnodes(with_containers=True)
        nodes = id2el(nodes, self.nodes)
        for n in nodes:
            n.graph = self
        return self

        
    def c_expand_single(self, node_to_expand):
        if isinstance(node_to_expand, str):
            node_to_expand = self[node_to_expand]

        my_name = node_to_expand.data['node_name']
        my_parent_graph = node_to_expand.data['parent_graph']
        clusters_dc = node_to_expand.data['command_input']
        updated_clusters = {k:v for k, v in clusters_dc.items() if k != my_name}
        return my_parent_graph.cluster_many(updated_clusters)
    
    def c_expand_all(self):
        g = self
        while len([id_ for id_ in g.view.vs['name'] if 'clust' in id_]) > 0:
            assert not g._graph_parent is None, f'graph {g} has no parent any more!'
            g = g._graph_parent
        return g
    
    def c_contract_containers(self, to_exclude=None):
        if to_exclude is None:
            to_exclude = []
        
        to_exclude = set(el2id(to_exclude))
        containers = [n for n in self.nodes if 'container' in n.classname.lower() and n.id not in to_exclude]
        clusters_dc = {c:[c]+c.get_offsprings() for c in containers}
        return self.c_contract_many(clusters_dc)

    def c_contract_single(self, node_to_contract):
        to_contract = [node_to_contract] + node_to_contract.get_offsprings()
        return self.c_contract_many({node_to_contract.id: el2id(to_contract)})
    
    def c_contract_many(self, clusters_dc):
                
        edges = self.get_edges(as_id=True)
        edges = np.array(edges, dtype=object)

        containers = []
        for new_name, to_contract in clusters_dc.items():
            if not isinstance(new_name, str): 
                new_name = new_name.id
            
            to_contract = el2id(to_contract)
            container = ViewContainer(f'{new_name}>clust_N{len(to_contract)}')

            s_to_contracts = set(to_contract)
            tester = np.vectorize(lambda x: x in s_to_contracts)
            is_match = tester(edges)
            matches = np.sum(is_match, axis=1)

            id_ = container.id
            edges[is_match] = id_
            edges = edges[matches != 2]
            container.data = {
                'node_name': new_name,
                'contracts': to_contract,
                'parent_graph': self,
                'command_input': clusters_dc,
                }
            
            s_tc = [self[nn].status for nn in s_to_contracts]
            container._status = graph_base.join_status_many([s_tc])
            containers.append(container)

        return self.to_view(self.root_node.id, edges.tolist(), container_nodes=containers)

    def reverse(self):
        nodes = self.view.vs['name']
        edge_ids = [(nodes[b], nodes[a]) for a, b in self.view.get_edgelist()]
        edge_ids = [tpl for tpl in edge_ids if self.root_node.id not in tpl]
        name = f'reverse({self.name})'
        return self.to_view(name, edge_ids)

        
    def pop(self):
        name = f'pop({self.name})'
        nodes_to_pop = el2id(self.get_highest_nodes())
        view_new = delete_vertices(self.view.copy(), nodes_to_pop)
        return self.to_view(name, view_new)


    ##################################################################################
    ##################################################################################
    # methods searching
    ##################################################################################
    ##################################################################################
    

    def hint(self, id_to_find, kmax=5):
        keys = self.get_viewnodes(as_id=True, with_containers=True)
        res = simple_search_keys(keys, id_to_find, k_max=kmax)
        if not res:
            print('nothing found... trying fuzzy search')
            return simple_search_keys(keys, id_to_find, k_max=kmax, fuzzy=True)
        else:
            if len(res) > kmax:
                print(f'found k={len(res)} matches... returning the first {kmax}')
                return res[:kmax]
            else:
                print(f'found k={len(res)} matches... ')
                return res


    def search_fuzzy(self, searchtext:str, k_max = 3, matchcase=False):
        return self.search(searchtext, k_max=k_max, only_found=False, matchcase=matchcase)
    

    @with_me_as_graph
    def search(self, searchtext:str, k_max = -1, only_found=True, matchcase=False, mode='keep', apply_on_view=True):

        name_new = 'search:' + searchtext

        nodes = self.get_nodes_view() if apply_on_view else self.nodes
        all_dcs = {el.id:el.to_dict(add_parents=False) for el in nodes}

        matches_ids = simple_search(all_dcs, searchtext, k_max=k_max, only_found=only_found, matchcase=matchcase)
        matches_objects = [self[mid] for mid in matches_ids]
        matches = matches_objects[:]

        edges_to_exclude = []
        if mode == 'lineage':
            for x in matches_objects:
                matches += x.get_lineage()
        elif mode == 'flat':
            edges_to_exclude = self.get_edges(as_id=True)
            # matches = set().union(*edges_to_exclude)
        else:
            pass
        
        nodes_to_exclude = [node.id for node in self.nodes if node not in matches or node.id not in self.view.vs['name']]
        new_view = delete_vertices(self.view.copy(), nodes_to_exclude)
        delete_edges(new_view, edges_to_exclude)
        return self.to_view(name_new, new_view)
    
    @with_me_as_graph
    def filt_by(self, name_new, filtfun, mode='keep', apply_on_view=True):
        
        nodes = self.get_nodes_view() if apply_on_view else self.nodes
        matches_objects = [x for x in nodes if filtfun(x)]
        matches = matches_objects[:]

        edges_to_exclude = []
        if mode == 'lineage':
            for x in matches_objects:
                matches += x.get_lineage()
        if mode.startswith('nn_'):
            n_max = int(mode.split('_')[-1])
            # log.debug(f'getting neighbors! n_max={n_max}, len(matches): {len(matches)}')
            for x in matches_objects:
                matches += x.get_nn(n_max=n_max)
            # log.debug(f'got neighbors! n_max={n_max}, len(matches): {len(matches)}')
        elif mode == 'flat':
            edges_to_exclude = self.get_edges(as_id=True)
        else:
            pass


        nodes_to_exclude = [node.id for node in self.nodes if node not in matches]
        new_view = delete_vertices(self.view.copy(), nodes_to_exclude)
        if edges_to_exclude:
            delete_edges(new_view, edges_to_exclude)
        return self.to_view(name_new, new_view)
    
    @with_me_as_graph
    def filt_by_nn(self, el:graph_base.BaseNode, name_new='', n_max=100_000, clip='auto'):
        if isinstance(el, str) and isinstance(name_new, int):
            n_max = name_new
            name_new = el
            el = self[el]

        nns = el.get_nn(n_max=n_max, clip=clip)
        nns_s = set([n.id for n in nns])
        containers = [n.id for n in nns if n in nns_s and not n.do_serialize]

        tmp = set(list(nns_s) + containers)
        
        edges = self.get_edges(as_id=True)
        edges = [(p, c) for p, c in edges if p in tmp and c in tmp]

        if not name_new:
            name_new = el.id + f'|nn_{len(nns)}'
            if len(nns) >= int(n_max):
                name_new += '_trunc'
        
        return self.to_view(name_new, edges, containers, do_gather_loose=False)


    @with_me_as_graph
    def filt_by_dish(self, dish_id, mode='keep', includes=None):
        if includes is None:
            includes = set(['VAS', 'VS', 'DS', 'VR', 'R'])
         
        if dish_id == '##':
            def filtfun(el):
                if '##' in el.dish_id:
                    return True
                elif dish_id == el.dish_id:
                    return True
                elif el.key in includes and [n.id for n in el.get_lineage() if n.dish_id == dish_id]:
                    return True
                else:
                    return False
        elif not dish_id:
            def filtfun(el):
                return True if el.dish_id else False
            dish_id = 'ALL'
        else:
            def filtfun(el):
                if '.D.{}'.format(dish_id) in el.id:
                    return True
                elif '#{}'.format(dish_id.replace('#', '').replace('SKA', '')) in el.dish_id:
                    return True
                elif 'P{}'.format(dish_id.replace('#', '').replace('SKA', '')) in el.dish_id:
                    return True
                elif dish_id == el.dish_id:
                    return True
                elif el.key in includes and [n.id for n in el.get_lineage() if n.dish_id == dish_id]:
                    return True
                else:
                    return False
        
        return self.filt_by('dish:'+ dish_id, filtfun, mode=mode)

    
    def filt_by_design(self, mode='keep'):
        def filtfun(el):
            if isinstance(el, ViewContainer):
                return False
            a = isinstance(el, schema.VerificationDesignAnalysis)
            b = 'DOCU' in el.tags
            c = isinstance(el, schema.Document) and not el.dish_id
            return a or b or c
        return self.filt_by('by_design', filtfun, mode=mode)
    
    
    def group_nodes(self, *args, **kwargs):
        assert self.nodes, 'this graph has no data to group!'

        if len(args) == 1 and isinstance(args[0], dict):
            kwargs = {**args[0], **kwargs}
        else:
            kwargs = {**{f'mygroup_{i}':nodes for i, nodes in enumerate(args, 1)}, **kwargs}
        
        dc = {k:set([n.id if not isinstance(n, str) else n for n in v]) for k, v in kwargs.items()}
            
        def santas_little_helper_fun(p, c):
            for gname, gnodes in dc.items():
                if c in gnodes:
                    return gname, c # found in group gname
            return p, c # not found... leave as is

        new_name = self.name + '|grouped'
        edges = []
        new_nodes = []
        for group_name, nodes_to_group in dc.items():
            assert np.all([n in self for n in nodes_to_group]), f'the given nodes for group {group_name} are not all contained in the graph!'
            new_nodes.append(ViewContainer(group_name, self, kview=self.name))

        edges += [santas_little_helper_fun(*tpl) for tpl in self.get_edges(as_id=True)]        
        return self.to_view(new_name, edges, container_nodes=new_nodes)
    
    @with_me_as_graph
    def group_loose(self):
        groups = {}
        for cls in classes:
            nodes = set([c.id for c in self.root_node.children if isinstance(c, cls) and not c.children])
            if hasattr(cls, 'key') and nodes:
                groups['loose_' + cls.key] = nodes
        log.debug('PLOTTING: groups of loose nodes: ' + str({k:len(v) for k, v in groups.items()}))
        if groups:
            return self.group_nodes(groups)
        else:
            return self


    def groupby(self, column):
        assert self.nodes, 'this graph has no data to group by!'
        assert np.all([hasattr(n, column) for n in self.nodes]), f'the given column: {column} is not in all nodes!'

        name_new = f'groupby({column})'

        is_listlike = isinstance(getattr(next(iter(self.nodes)), column), (list, tuple))
        f = lambda x: f'{column}:{x}'
        k = 'empty'


        edges = []
        if is_listlike:
            all_groups = set().union(*[getattr(n, column) for n in self.nodes])
            for g in all_groups:
                edges += [(f(g), n.id) for n in self.nodes if g in getattr(n, column)]
            
            
            empties = [(f(k), n.id) for n in self.nodes if not getattr(n, column)]
            if empties:
                all_groups.add(k)
                edges += empties

        else:
            all_groups = set([getattr(n, column) for n in self.nodes])
            for g in all_groups:
                edges += [(f(g), n.id) for n in self.nodes if g == getattr(n, column)]
        
        edges += [(name_new, f(c)) for c in all_groups]
        nodes_new = [ViewContainer(f(g), self, kview=self.name) for g in all_groups]
        return self.to_view(name_new, edges, container_nodes=nodes_new)
    
    def filtt(self, key):
        if isinstance(key, str):
            return tuple((n for n in self.vn if n.id.startswith(key)))
        else:
            return tuple((n for n in self.vn if isinstance(n, key)))
        
    def filti(self, key, ignore_case=True):
        if ignore_case:
            fun = lambda n: key in n.id
        elif not isinstance(key, str):
            fun = lambda n: isinstance(n, key)
        else:
            fun = lambda n: key.lower() in n.id.lower()

        return tuple((n for n in self.vn if fun(n)))


    def sep_reqs(self):
        edges_keep = [(p, c) for p,c in self.get_edges(as_id=True) if not p.startswith('VR.')]
        return self.to_view(self.name+'|sepreqs', edges_keep)
    
    @with_me_as_graph
    def plot(self, lyout='auto', ax=None, yhirachy=False, nodes_to_highlite=None, n_annotations_max=500, *arg, **kwargs):        
        if not 'plot_pyplot' in globals():
            try:
                
                from ReqTracker.plotting.plt_pyplot import plot_pyplot
            except Exception as err:
                log.exception(err)
                raise
            
        return plot_pyplot(self, lyout, ax, yhirachy, nodes_to_highlite, n_annotations_max, *arg, **kwargs)
    
    @with_me_as_graph
    def plot_ly(self, lyout='auto', yhirachy=False, nodes_to_highlite=None, n_annotations_max=500, figsize=1200, as_dict=False, **kwargs):
        if not 'plot_ly' in globals():
            try:
                from ReqTracker.plotting.plt_plotly import plot_ly
            except Exception as err:
                log.exception(err)
                raise
        
        r = plot_ly(self, lyout=lyout, figsize=figsize, yhirachy=yhirachy, nodes_to_highlite=nodes_to_highlite, 
             n_annotations_max=n_annotations_max, **kwargs)
        return r.to_json() if as_dict else r

    @with_me_as_graph
    def plot_sunburst(self, color_col_plt='status', color_col='color', maxdepth=10, figsize=1200, as_dict=False, **kwargs):
        if not 'make_sunburst_plot' in globals():
            try:
                from ReqTracker.plotting.plt_dash import make_sunburst_plot
            except Exception as err:
                log.exception(err)
                raise
        fig = make_sunburst_plot(self, color_col_plt=color_col_plt, color_col=color_col, maxdepth=maxdepth)
        fig.update_layout(
            height=figsize,
        )
        r = fig

        return r.to_json() if as_dict else r

    @with_me_as_graph
    def plot_treemap(self, color_col_plt='status', color_col='color', maxdepth=10, figsize=1200, as_dict=False, **kwargs):
        if not 'make_treemap_plot' in globals():
            try:
                from ReqTracker.plotting.plt_dash import make_treemap_plot
            except Exception as err:
                log.exception(err)
                raise
        fig = make_treemap_plot(self, color_col_plt=color_col_plt, color_col=color_col, maxdepth=maxdepth)
        fig.update_layout(
            height=figsize,
        )
        r = fig
        return r.to_json() if as_dict else r


"""

 ██████  ██████   █████  ██████  ██   ██ 
██       ██   ██ ██   ██ ██   ██ ██   ██ 
██   ███ ██████  ███████ ██████  ███████ 
██    ██ ██   ██ ██   ██ ██      ██   ██ 
 ██████  ██   ██ ██   ██ ██      ██   ██ 
"""          



class Graph(GraphView):

    @staticmethod
    def deserialize(dc_saved):
        
        nodes = dc_saved.get('nodes', None)
        views = dc_saved.get('views', None)

        assert nodes, 'this dict has no "nodes" field!'
        assert views, 'this dict has no "views" field!'

        if isinstance(nodes, dict):
            nodes = list(nodes.values())
        
        assert isinstance(nodes, list), '"nodes" must be of type list'
        assert isinstance(views, dict), '"views" must be of type dict'


        f = lambda v: v.id if hasattr(v, 'id') else v['id']
        all_nodes_dc = {f(v):v for v in nodes if v['_status'] != graph_base.STATUS.DELETED.name}
        edges = []
        for my_id, dc in all_nodes_dc.items():
            if 'parents' in dc:
                child_id = my_id
                for parent_id in dc['parents']:
                    edges.append((parent_id.strip(), child_id.strip()))
            elif 'children' in dc:
                parent_id = my_id
                for child_id in dc['children']:
                    edges.append((parent_id.strip(), child_id.strip()))
        
        all_nodes = [construct(v) for k, v in all_nodes_dc.items()]
        all_nodes = [n for n in all_nodes if n is not None]
        all_nodes.sort(key=lambda x: x.id)

        if edges:
            if not 'imported' in views:
                views['imported'] = {'edges': edges, 'positions': None}

        g = Graph()
        g.add(all_nodes, views)

        return g




    def __init__(self, root_node='root', 
                 view=None, 
                 nodes = None, 
                 gather_after_init=True, 
                 pull_on_init=True, 

                 **kwargs) -> None:
        
        if issubclass(type(root_node), graph_base.BaseNode):
           datasource = None
           data_to_import = None
        if isinstance(root_node, dict) and 'nodes' in root_node:
            data_to_import = root_node
            root_node = 'root'
        else:
            data_to_import = None

        super().__init__(root_node, view, nodes, gather_after_init)

        self.on_change = set()
        self._changelog = []
        
        self._updated_nodes_since_constructed = set()
        self.log_changes = graph_base.LOG_CHANGES

        self._t_constructed = helpers.get_utcnow()

        if self.source is not None and pull_on_init:
            self.pull()

        


    def _copy_internals(self, other):
        other.on_change = set([o for o in self.on_change])
        other._changelog = self._changelog[:]
        other._t_constructed = self._t_constructed
        other._updated_nodes_since_constructed = set([o for o in self._updated_nodes_since_constructed])
        other.log_changes = self.log_changes
        other.source = self.source

    @property
    def has_changes(self):
        return any((t > self._t_constructed for (t, s) in self._changelog))

    def __str__(self):
        return 'Graph-{}-{}{}'.format(self.name, id(self), '*' if self.has_changes else '')

    def get_info(self):
        s = '\n   - has_changes?: {}\n   - K={} Nodes updates since last save'
        s = s.format(self.has_changes, len(self._updated_nodes_since_constructed)) 
        return super().get_info() + s
    

    ##################################################################################
    ##################################################################################
    # access
    ##################################################################################
    ##################################################################################

    def __contains__(self, key):
        id_ = getattr(key, 'id') if hasattr(key, 'id') else key
        return id_ in self._container_nodes or id_ in self.nodes
    
    def __getitem__(self, node_id) -> graph_base.BaseNode:
        if isinstance(node_id, list):
            return [self[id_] for id_ in node_id]
        if isinstance(node_id, tuple):
            return tuple([self[id_] for id_ in node_id])
        if isinstance(node_id, set):
            return set([self[id_] for id_ in node_id])
        
        id_ = getattr(node_id, 'id') if hasattr(node_id, 'id') else node_id

        if id_ not in self.nodes and id_ in self._container_nodes:
            return self._container_nodes[node_id]
        else:
            try:
                return self.nodes[node_id]
            except KeyError as err:
                closest_ids = simple_search_keys([n.id for n in self.nodes], str(node_id), fuzzy=True)
                closest_ids = ' '.join(closest_ids)
                raise KeyError(f'key="{node_id}" was not found in graph. Closest ids: to input: {closest_ids}')
            


    ##################################################################################
    ##################################################################################
    # logging
    ##################################################################################
    ##################################################################################
    
    def mark_updated(self):
        self._t_constructed = helpers.get_utcnow()
        self._updated_nodes_since_constructed.clear()
    
    def get_changed_objects_since_constructed(self, as_dict=True):
        if as_dict:
            nodes = [e.to_dict(add_parents=True) for e in self._updated_nodes_since_constructed if e.do_serialize]
        else:
            nodes = [e for e in self._updated_nodes_since_constructed]
        changes = self.get_changes(since_construction=True, dt_as_string=True)
        return nodes, changes
    
    
    def get_changes(self, since_construction=True, dt_as_string=False):
        tmp = self._changelog[:]
        
        if since_construction:
            tmp  = [(t,s) for (t, s) in tmp if t >= self._t_constructed]

        if dt_as_string:
            tmp = [(helpers.make_zulustr(t), s) for (t, s) in tmp]
        return tmp
    
    def get_changess(self, n=0):
        return tuple((helpers.make_zulustr(t) + '| ' + s for (t, s) in self._changelog[n:]))



    def add_changenote(self, s, ts=None, updated_nodes=None):
        assert not self.is_readonly, 'something went wrong, since add_changenote was called on a read-only graph'
        
        if self.log_changes:
            

            if isinstance(s, datetime.datetime) and ts:
                ts, s = s, ts
            if ts is None:
                ts = helpers.get_utcnow()

            log.debug(s)
            
            self._changelog.append((ts, s))
            
            if updated_nodes is not None:
                
                if issubclass(type(updated_nodes), graph_base.BaseNode):
                    updated_nodes = set([updated_nodes])
                if isinstance(updated_nodes, str):
                    updated_nodes = set([updated_nodes])

                for node in updated_nodes:
                    if node in self:
                        n = self[node]
                        log.debug('_updated_nodes_since_constructed += ' + str(n.id))
                        self._updated_nodes_since_constructed.add(n)
                    elif issubclass(type(updated_nodes), graph_base.BaseNode):
                        log.debug('_updated_nodes_since_constructed += ' + str(n.id))
                        self._updated_nodes_since_constructed.add(node)
                        

            try:
                for cb in self.on_change:
                    cb(ts, s)
            except Exception as err:
                log.error('error on evaluating callback: ' + str(err))

    ##################################################################################
    ##################################################################################
    
    def del_node(self, node, remove=True):
        return self.del_nodes([node], remove=remove)

    def del_nodes(self, nodes, remove=True):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        i = 0
        for node in nodes:
            if node in self:
                node = self[node] if not issubclass(type(node), graph_base.BaseNode) else node
                self.del_edges(node.get_edges(), gather=False)
                node.graph = None # unlink from graph
                node._status = graph_base.STATUS.DELETED # flag deleted (no notifying changes here, since we will notify later below)
                if remove:
                    self.nodes.remove(node.id if hasattr(node, 'id') else node) # remove from node collection
                if not isinstance(node, ViewContainer):
                    self.add_changenote('graph.del_node({})'.format(node), updated_nodes=node)
                i += 1
        delete_vertices(self.view, el2id(nodes)) # this will also delete all edges refencing the nodes
        self.gather_loose()
        return i
    
    def replace_existing_node(self, node):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        assert node.id in self, f'the given node with id={node.id} and memory_id={id(node)} already exists as memory_id={id(self[node.id])}'
        i = 0
        self.nodes[node.id] = node
        node.graph = self
        if node.do_serialize:
            self.add_changenote('graph.replace_existing_node({})'.format(node), updated_nodes=node)
        i += 1

        if node.id not in self.view.vs['name']:
            self.view.add_vertex(node.id)

        return i
    
    def add_new_node_by_id(self, new_id, **kwargs):
        assert not new_id in self, f'{new_id=} already exists in graph!'
        node = construct({**dict(id=new_id), **kwargs})
        self.add_new_node(node)
        return node

    def add_new_node(self, node):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        i = 0
        if node not in self.nodes:
            # log.debug('ADDING NEW NODE: ' + node.id)
            self.nodes[node.id] = node
            node.graph = self
            if not isinstance(node, ViewContainer):
                self.add_changenote('graph.add_new_node({})'.format(node), updated_nodes=node)
            i += 1
        else:
            node = self.nodes[node.id]

        if node.id not in self.view.vs['name']:
            self.view.add_vertex(node.id)

        return i
    
    def set_me_as_graph(self, nodes=None):
        if nodes is None:
            nodes = self.get_viewnodes(as_id=False, with_containers=True)
        nodes = id2el(nodes, self.nodes)

        if issubclass(type(nodes), graph_base.BaseNode):
            self.set_me_as_graph([nodes]) # must be an array
        else:
            for node in nodes:
                self.add_new_node(node)

    def rename_cp(self, old_node, new_id):

        old_node = self[old_node]
        new_node = self.copy_node(self, new_id, 'all')
        self.add_new_node(new_node)
        cc, pp = self.children, self.parents

        edges_add = [(new_node.id, c.id) for c in cc] + [(p.id, new_node.id) for p in pp]
        edges_del = [(old_node, c.id) for c in cc] + [(p.id, old_node.id) for p in pp]
        self.add_edges(edges_add)
        self.del_edges(edges_del)
        self.del_node(self)
        return new_node
    


    def add(self, nodes_new, views, gather=True):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        self.add_nodes(nodes_new, gather=False)
        self.add_views(views)
        if gather:
            self.gather_loose()
            self._strip()

    def add_node(self, node, gather=True):
        self.add_nodes([node], gather=gather)

    def add_nodes(self, nodes, gather=True):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        self.set_me_as_graph(nodes)
        node_ids = []
        for node in nodes:
            assert issubclass(type(node), graph_base.BaseNode), 'the given object "{}" to add is of type: "{}", but expected was something that inherits from Node'.format(str(node), type(node))
            self.add_new_node(node)
            node_ids.append(node.id if not isinstance(node, str) else node)

        if 'name' in self.view.vs.attributes():
            fun = lambda node_id: node_id not in self.view.vs['name']
            node_ids = [node_id for node_id in node_ids if fun(node_id)]
        self.view.add_vertices(node_ids)

        if gather:
            self.gather_loose()
            self._strip()

    def add_edge_chain(self, nodes):
        self.add_edges(list(zip(nodes[:-1], nodes[1:])))


    def add_edge(self, edge):
        self.add_edges([edge])
    

    def add_edges(self, edges):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        if edges:
            
            tuples = el2id(edges)

            self._test_nodes_contained(tuples)
            edges = [tpl for tpl in tuples if not self.view.are_connected(*tpl)]
            if self.log_changes:
                log.debug('add_edges ' + str(edges))
            

            self.view.add_edges(edges)
                                
            # find all first level nodes which now have a parent and break the link to parent
            have_parents = [self.view.vs['name'][n] for n in self.view.successors(self.root_node.id) if len(self.view.predecessors(n)) > 1]
            edges_rem = [(self.root_node.id, id_) for id_ in have_parents]
            delete_edges(self.view, edges_rem)

            for e in edges:
                self.add_changenote('graph.edges += ({}, {})'.format(e[0], e[1]), updated_nodes=e)
            for e in edges_rem:
                self.add_changenote('graph.edges -= ({}, {})'.format(e[0], e[1]), updated_nodes=e)

    def del_edge(self, edge):
        self.del_edges([edge])

    def del_edges(self, edges=None, gather=True, check=True):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        edges = self.get_edges() if edges is None else edges
        tuples = el2id(edges)
        if check:
            self._test_nodes_contained(edges)
        edges = [tuple(tpl) for tpl in tuples if self.view.are_connected(*tpl)]
        self.view.delete_edges(edges)
        if gather:
            self.gather_loose()

        for e in edges:
            self.add_changenote('graph.edges -= ({}, {})'.format(e[0], e[1]), updated_nodes=e)
        

    def merge(self, other:GraphView):
        return self.merge_view(other.view, nodes_to_add=other.nodes)

    def merge_view(self, view_in:ig.Graph, nodes_to_add=[]):
        assert issubclass(type(view_in), ig.Graph), 'can only merge igraph.Graph objects!'
        self._test_nodes_contained(view_in.vs['name'])
        name = f'merged({self.name})'

        tmp = view_in.vs['name']
        f = lambda x: name if x == self.root_node.id else x
        
        edges = [(f(tmp[a]), f(tmp[b])) for a, b in view_in.get_edgelist()]

        edges_avail = self.get_edges(as_id=True)
        edges = [tpl for tpl in edges if tpl not in edges_avail]

        nodes = [id_ for id_ in set().union(*edges) if id_ not in self.view.vs['name']]
        if nodes_to_add:
            nodes_to_add_dc = {n.id: n for n in nodes_to_add}
            missing = [id_ for id_ in nodes if not id_ in nodes_to_add_dc]
            assert not missing, 'Not all nodes in the graph to merge in are available in the collection of nodes. missing is: {}'.format(', ')

        for n in nodes_to_add:
            self.nodes[n.id] = n

        self.view.add_vertices(nodes)    
        self.view.add_edges(edges)
        self.gather_loose()
        
        return self


    def clone_new_dish(self, dish_id_new, dish_id_from='##', only_dish_children=True):
        assert not self.is_readonly, 'the given graph is a read_only view of another graph and can not be changed!'
        sfrom = '.D.{}'.format(dish_id_from)
        sto = '.D.{}'.format(dish_id_new)

        def filtrfun(el):
            if sfrom in el.id or el.dish_id == dish_id_from:
                return True
            else:
                return False
            
        rep = lambda x: x.replace(sfrom, sto)



        def replace_dish_id(n):
            dc = n.to_dict(add_parents=True)
            dc['parents'] = [rep(x.id) for x in n.parents if not n.is_filelike]
            # log.debug(('REPLACE', n, n.parents, '->', dc['parents']))
            dc['id'] = rep(dc['id'])
            dc['tags'] = [rep(x) for x in dc['tags']]
            if dish_id_new not in dc['tags']:
                dc['tags'].append(dish_id_new)
            return dc
        
        new_nodes = []
        new_edges = []
        for x in self.nodes:
            if filtrfun(x):
                dish_new_dc = replace_dish_id(x)
                node_new = construct(dish_new_dc)
                
                if node_new is not None:
                    new_edges += [[p, node_new.id] for p in dish_new_dc['parents']]

                    children = self.get_children(x)
                    ne = [[node_new.id, rep(c.id)] for c in children if not c.is_filelike and (not only_dish_children or c.dish_id)]
                    # log.debug(('ADD', children, '->', ne))
                    new_edges += ne
                    node_new.add_note(f'Cloned from {x.id}->{node_new.id} on {helpers.nowiso()}')
                    new_nodes.append(node_new)

        self.add(new_nodes, new_edges)
        return self
    
    ##################################################################################
    ##################################################################################
    # compare
    ##################################################################################
    ##################################################################################
    def get_data_diff(self, other):
        import difflib, yaml

        dc1 = {d['id']:d for d in self.serialize()['nodes']}
        dc2 = {d['id']:d for d in other.serialize()['nodes']}

        s1 = yaml.dump(dc1)
        s2 = yaml.dump(dc2)

        return '\n'.join(difflib.unified_diff(s1.split('\n'), s2.split('\n')))

    
    ##################################################################################
    ##################################################################################
    # save/load
    ##################################################################################
    ##################################################################################


    def serialize(self, no_docs=False):    
        self.gather_loose()
        self._strip()
        log.debug('{} serializing N={} nodes'.format(self, len(self.nodes)))
        nodes = [el.to_dict(add_parents=True, graph=self, no_doc=no_docs) for el in self.nodes if el.do_serialize and el._status != graph_base.STATUS.DELETED]
        views = {k:v.to_dict() for k,v in self.views.items()}
        return {'nodes':nodes, 'views':views}
    
    def clone(self):
        return Graph.deserialize(self.serialize())
    
    

    def load(self, pth=None):
        with open(pth, 'r') as fp:
            dc = json.loads(fp)
        Graph.deserialize(dc, self)
        log.debug('Loaded N={} nodes and N={} views from DB'.format(len(self.nodes), len(self.views)))


    def save(self, pth=None):
        source, pth = get_io_interface(pth)
        dc = self.serialize()
        source.save(dc, pth)
        log.debug('Saved N={} nodes and N={} changes to DB'.format(len(dc['nodes']), len(dc['changes'])))
        self.mark_updated()

