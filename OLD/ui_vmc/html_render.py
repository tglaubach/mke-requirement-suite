from collections import namedtuple
import json
import random
import textwrap
import time
import urllib
import re
import uuid
import os
import base64
import markdown

from flask import render_template_string

import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

import ReqTracker
from ReqTracker.core import helpers, graph_main, graph_base, schema

try:
    from ReqTracker.ui_vmc import vmc_docmaker as vmc
except Exception as err:
    pass

DEFAULT_IMAGE_PATH = os.path.join(parent_dir, 'ReqTracker', 'assets', 'mpifr.png')
with open(DEFAULT_IMAGE_PATH, 'rb') as fp:
    DEFAULT_IMAGE_BLOB = base64.b64encode(fp.read()).decode('utf-8')


def mk_link(id_, label=None, pth='show', p0='uib', v='v1', **kwargs):
    return f'<a href="/{p0}/{v}/{pth}/{urllib.parse.quote_plus(id_)}" target="_self">{label if label else id_}</a>'

def mk_tpl(id_, label=None, pth='show', p0='uib', v='v1', **kwargs):
    return f"/{p0}/{v}/{pth}/{urllib.parse.quote_plus(id_)}", label if label else id_



class html_renderer:


    @staticmethod
    def vm_Text(label, content, **kwargs):
        if label:
            return f'<div style="min-width:100">{label}</div><div>{content}</div>'
        else:
            return f'<div>{content}</div>'
            

    
    @staticmethod
    def vm_Markdown(label, content, **kwargs):
        parts = []
        if label:
            parts += [
                f'<div style="min-width:100;">{label}</div>',
                '<hr/>'
            ]
        
        s = markdown.markdown(content)
        
        # s = f'<pre disabled=true style="width:90%; min-height:200px; overflow-x: scroll; overflow-y: none; margin:5px;display:block;font-family: Lucida Console, Courier New, monospace;font-size: 0.8em;">\n\n{content}\n\n</pre>'
        #s = f'<span style="display:block;" class="note">\n\n{content}\n\n</span>'
        parts += [s]

        return '\n\n'.join(parts)
    

    @staticmethod
    def vm_LargeText(label, content, **kwargs):
        nn = [len(s) for s in content.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{content}\n\n</textarea>'
    

    @staticmethod
    def vm_Iterator(content, **kwargs):
        children = []

        for i, c in enumerate(content, 1):
            children.append(c.to_html(add_header=False) if hasattr(c, 'get_VM') else c)

        return f'\n\n'.join([f'<div>{c}</div>' for c in children])
    


    @staticmethod
    def vm_BaseFallback(label, content, **kwargs):
        j = '#!/RAWJSON!\n' + json.dumps(content, indent=2)
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
    

    @staticmethod
    def vm_Verbatim(label, content, **kwargs):

        j = content
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        children = [
            f'<div style="min-width:100;">{label}</div>',
            f'<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
        ]
        return '\n\n'.join(children)

    @staticmethod
    def vm_Image(label='', imageblob=None, value='', width=0.8, caption="", **kwargs):       
        
        if imageblob is None:
            imageblob = DEFAULT_IMAGE_BLOB

        uid = (id(imageblob) + int(time.time()) + random.randint(1, 100))


        if not value:
            value = f'image_{uid}.png'

        s = imageblob.decode("utf-8") if isinstance(imageblob, bytes) else imageblob
        if not s.startswith('data:image'):
            s = 'data:image/png;base64,' + s
        

        children = [
            f'<div style="min-width:100;">{label}</div>',
            f'<div style="min-width:100;">image-name:</div>',
            f'<div>{value}</div>',
            f'<div style="min-width:100;">width</div>',
            f'<div>{width}</div>',
            f'<div style="min-width:100;">caption</div>',
            f'<div>{caption}</div>',
            f"<image src=\"{s}\", style=\"max-width:80%\"></image>",
            ]
        
        # children = dcc.Upload(id=self.mkid('helper_uploadfile'), children=children, multiple=False, disable_click=True)

        return '\n\n'.join(children)

    @staticmethod
    def vm_BaseFallback(label, content, **kwargs):
        j = '#!/RAWJSON!\n' + json.dumps(content, indent=2)
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
    
    @staticmethod
    def vm_Iterator(content, **kwargs):
        return f'\n\n'.join([f'<div>{c}</div>' for c in content])
    
    @staticmethod
    def vm_LargeText(label, content, **kwargs):
        nn = [len(s) for s in content.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{content}\n\n</textarea>'
    





def html_docdc2html(content):

    if isinstance(content, str):
        return html_renderer.vm_Text(None, content)
    elif isinstance(content, list) and content and isinstance(content[0], list):
        return html_renderer.vm_BaseFallback(**content) # BUG: not sure if this works! this needs to be implemented properly!
    elif isinstance(content, dict) and content.get('type', None) == 'iter' and isinstance(content.get('value', None), list):
        return html_renderer.vm_Iterator([html_docdc2html(c) for c in content.get('value')])
    elif isinstance(content, list):
        return html_renderer.vm_Iterator([html_docdc2html(c) for c in content])
    elif isinstance(content, dict) and content.get('type', None) == 'table':
        return html_renderer.vm_BaseFallback(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'image':
        return html_renderer.vm_Image(**content)
    elif isinstance(content, dict) and content.get('type', None) == 'input':
        return html_renderer.vm_BaseFallback(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'rawfile':
        return html_renderer.vm_BaseFallback(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'verbatim':
        return html_renderer.vm_Verbatim(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'markdown':
        return html_renderer.vm_Markdown(**content) 
    else:
        raise ValueError(f'the element of type {type(content)}, could not be parsed.', content)
    



def html_doc2html(doc):
    if not "vmc" in locals():
        tmp = doc.values() if isinstance(doc, dict) else doc
        return '\n\n'.join([html_docdc2html(dc) for dc in tmp])
        #return 'ReqTrack ERROR! NO HTML RENDER ENGINE AVAILABLE!'
    else:
        if issubclass(type(doc), schema.BaseNode):
            node = doc
            doc = node.doc_get()

        dashdoc = vmc.DocFactory.from_doc(doc)
        return dashdoc.to_html()



def html_node2html(node, keywords=None, no_text_header=False, with_doc=True, graph_plot='', **kwargs):
    keywords = [] if not keywords else keywords
    text_md = []


    text_md.append(f'<h2>NODE: {node.id}</h2>')
    text_md.append(f'<hr/>')

    if graph_plot:
        text_md.append(f'<h3>Plot:</h3>')
        text_md.append(graph_plot)

    status = node.status.name if not isinstance(node.status, str) else node.status
    tags = ', '.join((t for t in node.tags))

    text_md.append(f'<div><b>STATUS</b>: <span style="color:{graph_base.colorss[status]};">{status}</span></div>')
    text_md.append(f'<div><b>TAGS</b>: {tags}</div>')
    
    text_md.append(f'<hr/>')
    if issubclass(type(node), schema.BaseVersionedDocument):
        text = node.get_text(short=True)
    else:
        text = node.text

    
    if text:
        if not no_text_header:
            text_md.append('<h4>TEXT:</h4>')
        text_md.append(html_renderer.vm_Markdown('', text))
        text_md.append(f'<hr/>')

    if node.notes:
        if not no_text_header:
            text_md.append('<h4>NOTES:</h4>')
            
        text_md.append('<ul>')
        text_md += ['   <li><b>{}</b>: {}</li>'.format(k, markdown.markdown(v)) for k, v in node.notes.items()]
        text_md.append('</ul>')

    # for kw in keywords:
    #     text_md = re.sub(kw, f'** {kw} **', text_md, flags=re.IGNORECASE)


    text_md.append('<h4>PARENTS:</h4>')
    if node.has_graph:
        text_md += [' | '.join([mk_link(e.id, **kwargs) for e in node.parents])]
    else:
        text_md += ['<div style="color:red;">Can not show parents, since node is not connected to graph</div>']
    text_md.append('<h4>CHILDREN:</h4>')
    if node.has_graph:
        text_md += [' | '.join([mk_link(e.id, **kwargs) for e in node.children])]
    else:
        text_md += ['<div style="color:red;">Can not show children, since node is not connected to graph</div>']
    
    if with_doc:
        text_md.append('<hr/>')
        text_md.append('<h4>DOCUMENT PARTS:</h4>')
        text_md.append(html_doc2html(node.doc))

    s = '\n\n'.join(text_md)

    for kw in keywords:
        s = re.sub(kw, f'<mark>{kw}</mark>', s, flags=re.IGNORECASE)

    return s




def html_node2md(node, keywords=None, no_text_header=False):
    keywords = [] if not keywords else keywords
    text_md = []

    if issubclass(type(node), schema.BaseVersionedDocument):
        text = node.get_text(short=True)
    else:
        text = node.text

    if text:
        if not no_text_header:
            text_md.append('#### TEXT:\n\n')
        text_md += [text]

    if node.notes:
        if not no_text_header:
            text_md.append('#### NOTES:\n\n')
        text_md += ['**{}**: {}'.format(k, v) for k, v in node.notes.items()]

    for kw in keywords:
        text_md = re.sub(kw, f'** {kw} **', text_md, flags=re.IGNORECASE)

    text_md.append('#### PARENTS:\n\n')
    text_md += [' | '.join([e.id for e in node.parents])]
    text_md.append('\n\n#### CHILDREN:\n\n')
    text_md += [' | '.join([e.id for e in node.children])]



    return '\n'.join(text_md)


def sub_searchhits_tmplt():
    #     <span style="display:block;">
    #     <b style="min-width:45px;">ITEM:</b>
    #     <div class="mb-1"> 
    #         <a href="{{ n.url }}/{{ n.id }}" target="_self">{{ n.id }}</a>
    #         <div> {{ n.ttl }} </div>
    #     </div>
    # </span>
    return """
{% for n in node_vms %}
    <div style="margin-left:10px;margin-right:10px;">
    <div style="border:3px;border-style:solid;border-radius:3px;padding:1em;">




    {% if n.has_id %}
        <div class="mb-1"> 
            <b style="min-width:45px;">SNIPPET:</b>
            <div> {{ n.snippet }} </div>
            <alert color="warning">{{ n.alert }}</alert>
        </div>
    {% else %}
        {{ n.node_html|safe }}
    {% endif %}

    </div>
    <hr>
    </div>
{% endfor %}
"""

def sub_searchhits_kwargs(hits, g, url='/uim/v1/main/', **kwargs):
    if not isinstance(hits, list):
        hits = [hits]

    vms = []
    for hit in hits:
        id_ = hit['id']
        ttl = ''
        alert = f"the node with ID={id_} could not be found in the graph g{g}.."
        node_html = ''

        if hit['id'] in g:
            node = g[hit['id']]
            ttl = ' | {} ({})'.format(node.title, node.classname)
            node_html = html_node2html(node, keywords=hit['keywords'], add_header=False, no_text_header=True)

        dc = dict(
            id = id_,
            url = url,
            ttl = ttl,
            alert = alert,
            snippet = markdown.markdown(hit.get('snippet', '')),
            node_html = node_html
        )

        VMClass = namedtuple('node_vm', dc.keys())
        vm = VMClass(**dc)
        vms.append(vm)
    return dict(node_vms=vms)


def searchhits2html(hits, g, url='/uim/v1/main/', **kwargs):    
    tmplt = sub_searchhits_tmplt()
    params = sub_searchhits_kwargs(hits, g, url, **kwargs)
    return render_template_string(tmplt, **params)




if __name__ == '__main__':
    
    test_dict = {
        "id": "R.DS.G.11",
        "_text": "DS Components Electrical Safety\nThe DS components shall be electrically safe in accordance to the applicable sections of SANS 60950-1 (for guidance refer to [301-000000-018]).",
        "data": {},
        "tags": [
        "INSP"
        ],
        "notes": {
        "imported_note_00": "SANS Report"
        },
        '_status': 'NONE',
        'error': '',
        'type': 'BaseNode'
    }
    example_doc_dc = {
        'Mytext': 'some text...',
        'Other_text': 'some other text...',
        'Myiter': [],
        'MyImage': {'type': 'image', 'value': None}
    }

    el = graph_main.construct(test_dict)
    for v in example_doc_dc.values():
        el.doc_append_part(v)

    s = html_node2html(el)

    pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\temp.html'
    
    with open(pth, 'w+') as fp:
        fp.write(s)
