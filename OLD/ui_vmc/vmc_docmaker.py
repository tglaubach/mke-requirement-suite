from pathlib import Path

import re
import base64
import datetime
import io
import os
import shutil
import time
import traceback
import copy
import base64
from collections import UserDict

import json


from dash import Dash, dcc, html, dash_table, Input, Output, State, callback, no_update, clientside_callback, ALL, MATCH, ctx, ClientsideFunction
import dash_bootstrap_components as dbc

import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)


import ReqTracker
from ReqTracker.core import helpers, schema

from ReqTracker.ui import ui_state
from ReqTracker.ui.helpers_ui import A

VERYVERBOSE = True
DEFAULT_IMAGE_PATH = os.path.join(parent_dir, 'ReqTracker', 'assets', 'mpifr.png')
with open(DEFAULT_IMAGE_PATH, 'rb') as fp:
    DEFAULT_IMAGE_BLOB = base64.b64encode(fp.read()).decode('utf-8')


# print(DEFAULT_IMAGE_PATH)
# print(DEFAULT_IMAGE_BLOB)

log = helpers.log


UPLOAD_DIR = 'wip'

f = lambda k: dict(type='repmake', index=k)
fm = lambda t, k: dict(type=t, index=k)




"""
 ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          ▐░▌          
▐░▌          ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌          ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀█░▌
▐░▌          ▐░▌          ▐░▌       ▐░▌          ▐░▌          ▐░▌
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌ ▄▄▄▄▄▄▄▄▄█░▌ ▄▄▄▄▄▄▄▄▄█░▌
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 
"""

def _input_mapping(content, defs, othr_args):
    if content is None:
        content = {}

    assert isinstance(content, dict), f'need dict content for _input_mapping but got {type(content)=} with {content=}'
    c = content

    othr_args = {**defs, **othr_args}

    for k in defs:
        if k not in c or not c[k]:
            c[k] = othr_args[k]
    return c

class BaseViewModel():
    key = ''
    allows_dragdrop = ""
    allows_dragdrop_multi = False

    @staticmethod
    def test_dict_is_viewmodel(id_dict):
        parts = "type sub index token parent arg uid".split()
        return isinstance(id_dict, dict) and all((k in id_dict for k in parts))

    @staticmethod
    def test_dict_is_vmcontainer(id_dict):
        return isinstance(id_dict, dict) and id_dict['type'] == "part-container" and id_dict['sub'] == "element"

    @staticmethod
    def test_dict_is_ignore(id_dict):
        return isinstance(id_dict, dict) and id_dict['type'] == "part-container" and id_dict['sub'] == "typesel"
    

    def __init__(self, content=None, index = '', token='', parent=0, uid=None) -> None:
        self.content = content
        self.index = index
        self.token = token
        self.uid = (id(self) + int(time.time())) if uid is None else uid #uuid.uuid4().int
        self.parent = parent

    def mkid(self, k, arg="", index=None, type_=None):
        index = self.index if index is None else index
        type_ = self.__class__.__name__ if type_ is None else type_
        parent = self.parent.uid if issubclass(type(self.parent), BaseViewModel) else self.parent
        return {"sub":k,"index":index,"token":self.token, "parent": parent, "arg":arg, "uid": self.uid, "type":type_}
    
    def match_children(self, other, args_only=False):
        match_all = False if args_only else True

        testers = {
            'token': lambda x: self.token == x,
            'parent': lambda x: self.parent == x,
            'arg': lambda x: True if x or match_all else False
        }

        if issubclass(type(other), BaseViewModel):
            other = {k: getattr(self, k) for k in testers}
        return all((testers[k](other[k]) for k in other))
    
    @property
    def label(self):
        label = ''
        if issubclass(type(self.parent), BaseViewModel):
            label += self.parent.label + '.'

        else:
            label += self.token 
        

        if isinstance(self.index, int):
            if not label.endswith('.') :
                label += '-'
            label += f'{self.index}'
        
        return label
                         
    def to_html(self, disabled=False, add_header=True, can_select=False, **kwargs):
        if VERYVERBOSE:
            log.debug(('to_dash', self.token, self, self.key))
        if add_header:
            header = [                
                f'<div><b style="min-width:100;">Token: {self.token}</b></div>',
            ]
        else:
            header = []

        cc = []
        if header:
            cc += header
            
        if can_select:
            raise NotImplementedError()
        else:
            cc += [
                f'<div><b style="min-width:100;">Type: {self.key}</b><div>',
            ]
        
        c = cc + [f'<div>{self.get_html(disabled=disabled, **kwargs)}</div>']

        txt = '\n\n'.join(c)

        return f'<div style="margin:5; padding:5; border-width:1px; border-style:solid; border-color:gray; border-radius:10px">{txt}</div>'
    
    
    def to_dash(self, disabled=False, add_header=True, can_select=True, **kwargs):
        
        if VERYVERBOSE:
            log.debug(('to_dash', self.token, self, self.key))

        if add_header:
            header = [
                dbc.InputGroupText(html.B(f'Token: '), style={"minWidth":100}),
                dbc.InputGroupText(html.B(f'"{self.token}"'), style={"minWidth":200, 'backgroundColor': 'white'}),
                ]

        else:
            header = []


        cc = []
        if header:
            cc += header

        if can_select:
            cc += [
                dbc.InputGroupText('Type', style={"minWidth":100}),
                dbc.Select(options=VM_OPTIONS,value=self.key, id=self.mkid('typesel',type_='part-container'), disabled=disabled)
            ]
        
        if header and disabled:
            h = []
        elif cc:
            h = dbc.InputGroup(cc, size='sm')
        else:
            h = [] 
        
        if self.allows_dragdrop and not disabled:
            h = dcc.Upload(children=h, id=self.mkid(self.allows_dragdrop), multiple=self.allows_dragdrop_multi, disable_click=True)

        c = [
            h, 
            html.Div(
                children=self.get_VM(disabled=disabled, **kwargs), 
                id=self.mkid('element',type_='part-container'))
        ]
        stl ={'margin': 5, 'padding': 5, 'borderWidth': '1px',
                    'borderStyle': 'solid',
                    'borderColor': 'gray',
                    'borderRadius': '10px',}
        
        return dbc.ListGroupItem(c, style=stl) if add_header else html.Div(c, style=stl)
    
            
    def to_docdc(self):
        raise NotImplementedError('abstract method which needs to be overwritten')
    
    def __repr__(self) -> str:
        return str(self)
    
    def __str__(self) -> str:
        id_ = self.mkid("")
        del id_['sub']
        content = helpers.limit_len(str(self.content))
        return f'{self.__class__.__name__}[{id_}].{content=})'
        
class vm_Text(BaseViewModel):
    key = 'text'

    def to_docdc(self):
        return self.content
    
    def get_html(self, **kwargs):
        return f'<div style="min-width:100">{self.label}</div><div>{self.content}</div>'

    def get_VM(self, disabled=False):
        return html.Div([
            dbc.InputGroup([
                dbc.InputGroupText(self.label, style={"minWidth":100}),
                dbc.Input(value=self.content, id=self.mkid('content', arg="value"), disabled=disabled)
            ], size='sm')
        ], id=self.mkid('mdiv'))
    


class vm_Markdown(BaseViewModel):
    key = 'markdown'

    def to_docdc(self):
        return {'type': self.key, 'value': self.content}
            
    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        if isinstance(content, dict) and content.get('type', None) == self.key:
            content = content.get('value', '')
        super().__init__(content, index, token, parent, uid)

    def get_html(self, **kwargs):
        return f'<div style="min-width:100;">{self.label}</div><hr></hr>\n\n<span style="display:block;" class="note">\n\n{self.content}\n\n</span>'
    
    def get_VM(self, disabled=False):
        if disabled:
            tooltip = None
            cont = dcc.Markdown(self.content, id=self.mkid('content', arg="value"))
            c = html.Div([
                html.Label(html.B(self.label)),
                html.Hr(),
                cont
            ], style={'textAlign': 'left'})
        else:
            tooltip = dbc.Tooltip('click on the label to show as rendered markdown', target=self.mkid('helper_btn_show_md'))
            cont = dbc.Textarea(value=self.content, id=self.mkid('content', arg="value"), style={'minHeight': 200, 'fontFamily': "Lucida Console, Courier New, monospace"}, disabled=disabled)
            c = dbc.InputGroup([
                dbc.Button(self.label, style={"minWidth":100}, id=self.mkid('helper_btn_show_md'), color='secondary', disabled=disabled),
                tooltip,
                cont, 
            ], size='sm')

        return html.Div([
            dbc.Modal(children=[], id=self.mkid('helper_dialog'), is_open=False, size='xl', style={'minHeight': 300}),            
            c
        ], id=self.mkid('mdiv'))
    


class vm_LargeText(BaseViewModel):
    key = 'textbox'

    def get_html(self, **kwargs):
        nn = [len(s) for s in self.content.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{self.label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{self.content}\n\n</textarea>'
    
    def get_VM(self, disabled=False):
        return html.Div([
            dbc.InputGroup([
                dbc.InputGroupText(self.label, style={"minWidth":100}),
                dbc.Textarea(value=self.content, id=self.mkid('content', arg="value"), disabled=disabled)
            ], size='sm')
        ], id=self.mkid('mdiv'))
    
    def to_docdc(self) -> str:
        return self.content
    
class vm_Iterator(BaseViewModel):
    key = 'iter'
    allows_dragdrop = "helper_uploadfiles"
    allows_dragdrop_multi = True


    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        content = [] if content is None else content
        super().__init__(content, index, token, parent, uid)

    def to_docdc(self) -> list:
        f = lambda e: e.to_docdc() if not isinstance(e, str) else e
        return {'type': self.key, 'value': [f(c) for c in self.content]}
        
    def _mkbutton(self, i):
        return dbc.Button(
                    "+ MD",
                    id = self.mkid("helper_btn_add", index=i),
                    # className="position-absolute top-100 start-50 translate-middle",
                    size='sm',
                    color='secondary',
                    style={"minWidth":50}
                )
    def _mkbutton_add_i(self, i):
        return dbc.Button(
                    "+ IM",
                    id = self.mkid("helper_btn_add_i", index=i),
                    # className="position-absolute top-100 start-50 translate-middle",
                    size='sm',
                    color='info',
                    style={"minWidth":50}
                )
    
    def _mkbutton_del(self, i):
        return dbc.Button(
                    "- DEL",
                    id = self.mkid("helper_btn_rem", index=i),
                    # className="position-absolute top-100 start-50 translate-middle",
                    size='sm',
                    color='danger',
                    style={"minWidth":50}
                )
    def _mkbutton_switch(self, i):
        return dbc.Button(
                    '\u21F5',
                    id = self.mkid("helper_btn_switch", index=i),
                    # className="position-absolute top-100 start-50 translate-middle",
                    size='sm',
                    color='light',
                    style={"minWidth":50}
                )
        

        # return dcc.ConfirmDialogProvider(, message='Are you sure you want to delete the last component?')
    
    def get_html(self, **kwargs):
        children = []

        for i, c in enumerate(self.content, 1):
            children.append(c.to_html(add_header=False) if hasattr(c, 'get_VM') else c)

        return f'\n\n'.join([f'<div>{c}</div>' for c in children])
    
    def get_VM(self, raw=False, disabled=False, **kwargs):
        children = []
        if not disabled:
            children.append(dbc.ButtonGroup([self._mkbutton(0), self._mkbutton_add_i(0)], size='sm', style={'margin':1}))

        for i, c in enumerate(self.content, 1):
            children.append(c.to_dash(add_header=False, disabled=disabled) if hasattr(c, 'get_VM') else c)

            if not disabled:
                grp = [self._mkbutton(i), self._mkbutton_add_i(i), self._mkbutton_del(i)]
                if i < len(self.content):
                    grp = [self._mkbutton_switch(i)] + grp
                children.append(dbc.ButtonGroup(grp, size='sm', style={'margin':1}))
        
        return children if raw else html.Div(children, id=self.mkid('mdiv'))
    
    def append_element(self, el):
        self.content.append(el)
        for ii, e in enumerate(self.content):
            e.index = ii
            e.parent = self

        log.debug(('append_element @', el))

    def add_element(self, el):
        i = int(el.index) if el.index else 0
        self.content.insert(i, el)
        for ii, e in enumerate(self.content):
            e.index = ii
            e.parent = self

        log.debug(('add_element @', i, el))

    def switch_elements(self, i):
        i1 = int(i)
        i0 = i-1
        assert i1 > 0, f'can only swap with index > 0 but got{i=}'
        
        self.content[i0], self.content[i1] = self.content[i1], self.content[i0]

        for ii, e in enumerate(self.content):
            e.index = ii
            e.parent = self

        log.debug(('switch_element@', (i0, i1)))

    def rem_element(self, el):
        if isinstance(el, int) and el < len(self.content):
            i = el
        else:
            if hasattr(el, 'uid'):
                el = el.uid
            i = [i for i, e in enumerate(self.content) if str(e.uid) == el]
        
        el = self.content.pop(i)
        el.parent = ""
        el.index = ""
        for ii, e in enumerate(self.content):
            e.index = ii
            e.parent = self

        log.debug(('rem_element @', i, el))

class vm_BaseFallback(BaseViewModel):

    def get_html(self, **kwargs):
        j = '#!/RAWJSON!\n' + json.dumps(self.content, indent=2)
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{self.label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
    

    def get_VM(self, disabled=False, **kwargs):
        j = '#!/RAWJSON!\n' + json.dumps(self.content, indent=2)

        return html.Div([
            None if disabled else dbc.InputGroup([
                dbc.InputGroupText(f'The ViewModel for "{self.key}" is not implemented yet. Please edit the content as JSON.'),
            ], size='sm'),
            dbc.InputGroup([
                dbc.InputGroupText(self.label, style={"minWidth":100}),
                dbc.Textarea(value=j, id=self.mkid('content', arg="value"), style={'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '10px'}, disabled=disabled)
            ], size='sm')
        ], id=self.mkid('mdiv'))
    
    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        if isinstance(content, str):
            if content.startswith('#!/RAWJSON!'):
                content = content.replace('#!/RAWJSON!').strip()
            content = json.loads(content)
        super().__init__(content, index, token, parent, uid)


class vm_Tabular(vm_BaseFallback):
    key='tabular'

    def to_docdc(self):
        f = lambda e: e.to_docdc() if not isinstance(e, str) else e
        return [[f(e) for e in row] for row in self.content]

    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        content = [[]] if content is None else content
        super().__init__(content, index, token, parent, uid)

class vm_ResultTabular(vm_BaseFallback):
    key='rtable'

    def to_docdc(self):
        f = lambda e: e.to_docdc() if not isinstance(e, str) else e
        return [[f(e) for e in row] for row in self.content]
    
    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        content = [['']*4] if content is None else content
        super().__init__(content, index, token, parent, uid)
        
        
class vm_Table(vm_BaseFallback):
    key='table'
    # BUG: there is something wrong here.... we are not using this at the moment anyway!
    def to_docdc(self):
        kwargs = json.loads(self.content) if isinstance(self.content, str) and self.content else self.content
        
        return {
            "type": self.key, 
            'value': kwargs.get(['value'], [[]]),
            'layout': kwargs.get(['layout'], ''),
            'hlines': kwargs.get(['hlines'], 1),
            'caption': kwargs.get(['caption'], ''),
            }

class vm_Image(BaseViewModel):
    key='image'
    allows_dragdrop = "helper_uploadfile"
    allows_dragdrop_multi = False

    @classmethod
    def get_image_src(cls, imageblob):
        
        s = imageblob.decode("utf-8") if isinstance(imageblob, bytes) else imageblob
        if not s.startswith('data:image'):
            s = 'data:image/png;base64, ' + s
        
        log.debug(('get_image_src', s[:100] + ' (...) ' + s[-20:]))
        return s
        
    def to_docdc(self):
        return {**{"type": self.key}, **{k:self.content[k] for k in 'value width imageblob caption'.split()}}
    
    def __init__(self, content=None, index='', token='', parent=0, uid=None, **kwargs) -> None:
        
        defs = {
            'value': None,
            'imageblob': None,
            'width': 0.8,
            'caption': ''
        }


        content = _input_mapping(content, defs, kwargs)

        assert isinstance(content, dict), f'need dict for type={self.key} content'
        assert content['imageblob'] is None or isinstance(content['imageblob'], str), 'filecontent must be of type string'
        assert content['value'] is None or isinstance(content['value'], str), 'value must be of type string'


        super().__init__(content, index, token, parent, uid)

        if self.content['value'] is None:
            self.content['value'] = f'image_{self.uid}.png'
        else:
            self.content['value'] = os.path.basename(self.content['value'])

        if self.content['imageblob'] is None:
            self.content['imageblob'] = DEFAULT_IMAGE_BLOB

    @property
    def imageblob(self):
        return self.content['imageblob']
    @property
    def value(self):
        return self.content['value']
    @property
    def width(self):
        return self.content['width']
    @property
    def caption(self):
        return self.content['caption']
    

    def get_image_html(self):
        return html.Img(id=self.mkid('imageblob', arg='src'),className="image", 
                 src=self.get_image_src(self.imageblob), style={'maxWidth': '100%'})

    def get_html(self,**kwargs):       
        
        children = [
            f'<div style="min-width:100;">{self.label}</div>',
            f'<div style="min-width:100;">image-name:</div>',
            f'<div>{self.value}</div>',
            f'<div style="min-width:100;">width</div>',
            f'<div>{self.width}</div>',
            f'<div style="min-width:100;">caption</div>',
            f'<div>{self.caption}</div>',
            f"<image src=\"{self.get_image_src(self.imageblob)}\", style=\"max-width:80%\"></image>",
            ]
        
        # children = dcc.Upload(id=self.mkid('helper_uploadfile'), children=children, multiple=False, disable_click=True)

        return '\n\n'.join(children)
    
    

    def get_VM(self, disabled=False, **kwargs):       
        image = None if self.imageblob is None else self.get_image_html()

        children = [
                dbc.InputGroup([
                    dbc.InputGroupText(self.label, style={"minWidth":100}),
                    dbc.InputGroupText('image-name', style={"minWidth":100}),
                    dbc.Input(value=self.value, 
                                placeholder='filename.png', 
                                id=self.mkid('value', arg="value"), disabled=disabled),
                    # dcc.Upload(dbc.Button('Upload', size='sm')),
                    dbc.InputGroupText('width', style={"minWidth":100}),
                    dbc.Input(value=self.width, 
                                type='number',
                                min=0., max=1., step=0.05,
                                id=self.mkid('width', arg="value"), style={'maxWidth': 100}, disabled=disabled),
                    dbc.InputGroupText('caption', style={"minWidth":100}),
                    dbc.Input(value=self.caption, 
                                placeholder='', 
                                id=self.mkid('caption', arg="value"), disabled=disabled)
                ], size='sm'),

                html.Div(image, id=self.mkid('helper_imagecontainer')),

            ]
        
        # children = dcc.Upload(id=self.mkid('helper_uploadfile'), children=children, multiple=False, disable_click=True)

        return html.Div(children, id=self.mkid('mdiv'))


class vm_RawFile(BaseViewModel):
    key = 'rawfile'
    allows_dragdrop = "helper_uploadfile"
    allows_dragdrop_multi = False

    def to_docdc(self):
        return {**{"type": self.key}, **{k:self.content[k] for k in 'value filecontent'.split()}}

    def __init__(self, content=None, index='', token='', parent=0, uid=None, **kwargs) -> None:
        
        defs = {
            'value': '',
            'filecontent': None,
        }

        content = _input_mapping(content, defs, kwargs)
        assert isinstance(content, dict), f'need dict for type={self.key} content'
        assert content['filecontent'] is None or isinstance(content['filecontent'], str), 'filecontent must be of type string'
        assert content['value'] is None or isinstance(content['value'], str), 'value must be of type string'
        
        super().__init__(content, index, token, parent, uid)

        if self.content['value'] is None:
            self.content['value'] = f'rawfile_{self.uid}.tex'
        else:
            self.content['value'] = os.path.basename(self.content['value'])

    def get_html(self, **kwargs):

        j = self.content.get("filecontent")
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        children = [
            f'<div style="min-width:100;">{self.label}</div>',
            f'<div style="min-width:100;">rawfile-name: </div>',
            f'<div style="min-width:100;">{self.content.get("value")}</div>',
            f'<hr></hr>',
            f'\n\n',
            f'<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
        ]
        return '\n\n'.join(children)
    

    def get_VM(self, disabled=False, **kwargs):       

        return html.Div([
            
                dbc.InputGroup([
                    dbc.InputGroupText(self.label, style={"minWidth":100}),
                    dbc.InputGroupText('rawfile-name', style={"minWidth":100}),
                    dbc.Input(value=self.content.get("value"), 
                                placeholder='rawfile.tex', 
                                id=self.mkid('value', arg="value"), disabled=disabled),
                ], size='sm'),

                dbc.Textarea(value=self.content['filecontent'], 
                                placeholder='filecontent', 
                                id=self.mkid('filecontent', arg="value"), 
                                style={
                                    'fontFamily': "Lucida Console, Courier New, monospace", 
                                    'fontSize': '10px'
                                    },
                                disabled=disabled
                            )

            ], id=self.mkid('mdiv'))
    

class vm_InputText(vm_LargeText):
    key = 'finput'

    def to_docdc(self) -> str:
        return {
            "type": self.key, 
            'value': f'{self.key}_{self.uid}.tex', 
            'content': self.content
        }
    
    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        if isinstance(content, dict) and content.get('type', None) == self.key:
            content = content.get('value', '')
        super().__init__(content, index, token, parent, uid)

class vm_Verbatim(BaseViewModel):
    key = 'verbatim'

    def to_docdc(self):
        return {
            "type": self.key, 
            'value': self.content,
        }
    def __init__(self, content=None, index='', token='', parent=0, uid=None) -> None:
        if isinstance(content, dict) and content.get('type', None) == self.key:
            content = content.get('value', '')
        super().__init__(content, index, token, parent, uid)

    def get_html(self, **kwargs):

        j = self.content
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        children = [
            f'<div style="min-width:100;">{self.label}</div>',
            f'<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
        ]
        return '\n\n'.join(children)
    
    def get_VM(self, disabled=False, **kwargs):
        return html.Div([
            dbc.InputGroup([
                dbc.InputGroupText(self.label, style={"minWidth":100}),
                dbc.Textarea(value=self.content, id=self.mkid('content',arg='value'), style={'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '10px'}, disabled=disabled)
            ], size='sm')
        ], id=self.mkid('mdiv'))
    


"""

 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌          ▐░▌          ▐░▌     ▐░▌            ▐░▌
▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                            
"""

TOKEN_METADATA = 'AUTHOR LONGTITLE DOCID DISHID DOCTITLE VERSION DOCNAME NODE_ID NODE_GRAPH NODE_INFO ALLNODES'.split()

example_doc_dc = {
    'Mytext': 'some text...',
    'Other_text': 'some other text...',
    'Myiter': [],
    'MyImage': {'type': 'image', 'value': None}
}

VMTYPES = [vm_Text, vm_LargeText, vm_Image, vm_Iterator, vm_Markdown, vm_Tabular, vm_ResultTabular, vm_Table, vm_RawFile, vm_InputText, vm_Verbatim]

VM_CONSTRUCTORS = {cls.__name__:cls for cls in VMTYPES}


VMS_RDY = 'text markdown image iter verbatim textbox rawfile input'.split()

VM_SELMAP = {
    'text': vm_Text,
    'markdown': vm_Markdown,
    'image': vm_Image,
    'iter': vm_Iterator,
    'verbatim': vm_Verbatim,

    'textbox': vm_LargeText,
    'tabular': vm_Tabular,
    
    'table': vm_Table,
    
    'rtable':  vm_ResultTabular,
    'rawfile': vm_RawFile,
    'input': vm_InputText,
}

VM_SELMAP_INV = {v:k for k, v in VM_SELMAP.items()}
# VM_OPTIONS = [{"label": opt, "value": i} for i, opt in enumerate(VM_SELMAP.keys())]
VM_OPTIONS = [{'label': k, 'value': k, "disabled": not k in VMS_RDY} for k in VM_SELMAP.keys()]



def t_dashtree2tree(dash_dc, dc=None):
    if dc is None:
        dc = {}

    if isinstance(dash_dc, list):
        tmp = [t_dashtree2tree(e, dc) for e in dash_dc]
        return dc
    elif 'props' in dash_dc: # react element dict
        return t_dashtree2tree(dash_dc['props'], dc)
    elif 'children' in dash_dc and not 'id' in dash_dc and isinstance(dash_dc['children'], (list, dict)): # some other Dash/HTML component
        return t_dashtree2tree(dash_dc['children'], dc)
    elif 'id' in dash_dc and BaseViewModel.test_dict_is_vmcontainer(dash_dc['id']):
        log.debug(f"found part-container to parse with id={dash_dc['id']}")
        return t_dashtree2tree(dash_dc['children'], dc)
    elif 'id' in dash_dc and BaseViewModel.test_dict_is_ignore(dash_dc['id']):
        return dc
    elif 'id' in dash_dc and BaseViewModel.test_dict_is_viewmodel(dash_dc['id']) and dash_dc['id']['type'] in VM_CONSTRUCTORS: # some VM Dash/HTML component
                
        sub = dash_dc['id']['sub']
        uid = dash_dc['id']['uid']
        arg = dash_dc['id']['arg']

        log.debug((arg, dash_dc))
        
        content = {sub: dash_dc[arg]} if arg else {}
        if content:
            args = {k:helpers.limit_len(str(v)) for k, v in content.items()}
            id_ = str(dash_dc['id'])
            log.debug(f'found arguments for {id_=} -> {args=}')
        else:
            args = None

        if not uid in dc:
            dc[uid] = {'id': {}, 'args': {}, 'children': []}

        if sub == 'mdiv':
            dc[uid]['id'] = dash_dc['id']
        elif not dc[uid]['id']:
            dc[uid]['id']['type'] = dash_dc['id']['type']
            dc[uid]['id']['uid'] = dash_dc['id']['uid']
            dc[uid]['id']['token'] = dash_dc['id']['token']
            dc[uid]['id']['index'] = dash_dc['id']['index']


        if not sub.startswith("helper_"):
            for k, v in content.items():
                if isinstance(v, str) and v.startswith('#!/RAWJSON!\n'):
                    log.debug(f'parsing RAWJSON for {k=} with {len(v)=} chars')
                    v = json.loads(v[len('#!/RAWJSON!\n'):])
                dc[uid]['args'][k] = v
            
            dc[uid]['children'].append(dash_dc['id']['uid'])

            log.debug(f'matched element with {args=} and id=' + str(dash_dc['id']))
        else:
            log.debug(f'no match for element with {content=} and id=' + str(dash_dc['id']))

        if 'children' in dash_dc and isinstance(dash_dc['children'], (list, dict)):
            t_dashtree2tree(dash_dc['children'], dc)

        return dc
    elif 'id' not in dash_dc and 'children' in dash_dc and (isinstance(dash_dc['children'], str) or not dash_dc['children']):
        return dc
    else:
        log.error(f'dash2viewmodel: could not parse:')
        print(json.dumps(dash_dc, indent=2))
        return dc
    




def t_docdc2vm(content, token, index=''):
    

    kwargs = dict(content=content, index=index, token=token)

    if isinstance(content, str):
        return vm_Text(**kwargs)
    elif isinstance(content, list) and content and isinstance(content[0], list) and token == 'REQTABLE':
        return vm_ResultTabular(**kwargs) # BUG?
    elif isinstance(content, list) and token == 'REQTABLE':
        return vm_ResultTabular(**kwargs) # BUG?
    elif isinstance(content, list) and content and isinstance(content[0], list) and len(content[0]) == 0 and token == 'VERSIONHIST':
        kwargs['content'] = [['']*3]
        return vm_Tabular(**kwargs) # BUG?
    elif not content and token == 'VERSIONHIST':
        kwargs['content'] = [['']*3]
        return vm_Tabular(**kwargs) # BUG?
    elif isinstance(content, list) and content and isinstance(content[0], list):
        return vm_Tabular(**kwargs) # BUG: not sure if this works! this needs to be implemented properly!
    elif isinstance(content, dict) and content.get('type') == 'iter' and isinstance(content.get('value', None), list):
        el = vm_Iterator(index=index, token=token)
        for c in content['value']:
            el.append_element(t_docdc2vm(c, token))
        return el
    elif isinstance(content, list):
        el = vm_Iterator(index=index, token=token)
        for c in content:
            el.append_element(t_docdc2vm(c, token))
        return el
    elif isinstance(content, dict) and 'type' in content and content['type'] == 'table':
        return vm_Table(**kwargs)
    elif isinstance(content, dict) and 'type' in content and content['type'] == 'image':
        return vm_Image(**kwargs)
    elif isinstance(content, dict) and 'type' in content and content['type'] == 'input':
        return vm_InputText(**kwargs)
    elif isinstance(content, dict) and 'type' in content and content['type'] == 'rawfile':
        return vm_RawFile(**kwargs)
    elif isinstance(content, dict) and 'type' in content and content['type'] == 'verbatim':
        return vm_Verbatim(**kwargs)
    elif isinstance(content, dict) and 'type' in content and content['type'] == 'markdown':
        return vm_Markdown(**kwargs)
    else:
        raise ValueError(f'the element of type {type(content)}, could not be parsed.', content)
    

def t_tree2vmtree(tree, indices=None):
    if indices:
        if not isinstance(indices, dict):
            indices = {i['uid']:i for i in indices}
        for uid in indices:
            if uid in tree and not tree[uid]['id']:
                tree[uid]['id'] = indices[uid]
        
    elements = {}
    for uid, element_dc in tree.items():
        kwargs = {**element_dc['id'], **element_dc['args']}
        if VERYVERBOSE:
            log.debug('constructing with data={} id={}'.format(tuple(element_dc['args'].keys()), element_dc['id']))

        cls = VM_CONSTRUCTORS[kwargs['type']]
        for key in 'type sub arg'.split():
            kwargs.pop(key, None)
        
        element = cls(**kwargs)
        elements[element.uid] = element

    for v in elements.values():
        for v2 in elements.values():
            if v2.parent == v or v2.parent == v.uid:
                v.add_element(v2)

    return elements

class tt_FlatTree(UserDict):
    """Flattened UID - property dict intermediate step for parsing dash property dicts to other formats
    
    can be constrtucted by __class__.from_dashtree(dash_dc)
    """
    @staticmethod
    def from_dashtree(dash_dc:dict):
        return tt_FlatTree(t_dashtree2tree(dash_dc))
    
    @staticmethod
    def from_(other):
        if isinstance(other, tt_DocTree):
           raise NotImplementedError()
        elif isinstance(other, tt_TokenVMTree):
            raise NotImplementedError()
        elif isinstance(other, tt_FlatTree):
            return other
        else:
            raise TypeError(f'unknown type {other=} expected one of {(tt_TokenVMTree, tt_DocTree, tt_FlatTree)}')


    def to_(self, cls):
        assert cls in (tt_TokenVMTree, tt_DocTree, tt_FlatTree)
        return cls.from_(self)

    def to_uid_vmtree(self, indices=None) -> dict:
        return t_tree2vmtree(self.data, indices=indices)
    
    
    def to_token_dc(self) -> dict:
        return self.to_(tt_DocTree).to_dict()
    
    def to_dash(self, disabled=False, add_header=False, can_select=False) -> list:
        vm = self.to_(tt_TokenVMTree)
        return vm.to_dash(disabled=disabled, add_header=add_header, can_select=can_select)




class tt_DocTree(UserDict):
    """ JSON parsable dict format for generating documents using the docmaker or repmaker modules 
        tokens as keys - contents as values
    """
    # def __init__(self, tokendoc_dc:dict) -> None:
    #     self.data = tokendoc_dc
    
    @staticmethod
    def from_(other):
        if isinstance(other, tt_DocTree):
            return other
        elif isinstance(other, tt_TokenVMTree):
            return tt_DocTree({k:v.to_docdc() for k, v in other.data.items()})
        elif isinstance(other, tt_FlatTree):
            return tt_DocTree.from_(tt_TokenVMTree.from_(other))
        else:
            raise TypeError(f'unknown type {other=} expected one of {(tt_TokenVMTree, tt_DocTree, tt_FlatTree)}')

    def to_(self, cls):
        assert cls in (tt_TokenVMTree, tt_DocTree, tt_FlatTree)
        return cls.from_(self)

    def to_json(self) -> str:
        return json.dumps(self.data, indent=2)

    def to_dict(self) -> dict:
        return copy.deepcopy(self.data)
    
    def to_token_dc(self):
        return self.to_(tt_DocTree).to_dict()
    
    def to_dash(self, disabled=False, add_header=False, can_select=False) -> list:
        vm = self.to_(tt_TokenVMTree)
        return vm.to_dash(disabled=disabled, add_header=add_header, can_select=can_select)

    def to_html(self) -> str:
        vm = self.data.to_(tt_TokenVMTree)
        return vm.to_html()
    
class tt_TokenVMTree(UserDict):
    """ part tokens as key, ViewModels (inherit from BaseViewModel) of all root elements as values
            children ViewModels are in the corresponding "content" properties of the individual
            classes

        can be transformed to -> to_dash
    """
    @staticmethod
    def from_(other):
        if isinstance(other, tt_DocTree):
            return tt_TokenVMTree({token:t_docdc2vm(d, token) for token, d in other.data.items()})
        elif isinstance(other, tt_TokenVMTree):
            return other
        elif isinstance(other, tt_FlatTree):
            uid_vmtree = t_tree2vmtree(other.data)
            return tt_TokenVMTree({e.token:e for e in uid_vmtree.values() if not e.parent})
        else:
            raise TypeError(f'unknown type {other=} expected one of {(tt_TokenVMTree, tt_DocTree, tt_FlatTree)}')

    def to_(self, cls):
        assert cls in (tt_TokenVMTree, tt_DocTree, tt_FlatTree)
        return cls.from_(self)


    # def __init__(self, tokenvm_dc) -> None:
    #     self.data = tokenvm_dc

    def to_token_dc(self):
        return self.to_(tt_DocTree).to_dict()

    def to_dash(self, disabled=False, add_header=True, can_select=False) -> list:
        dc_vm = self.data
        tmp = dc_vm.values() if isinstance(dc_vm, dict) else dc_vm
        return [vm.to_dash(disabled=disabled, add_header=add_header, can_select=can_select) for vm in tmp]

    def to_html(self) -> str:
        dc_vm = self.data
        tmp = dc_vm.values() if isinstance(dc_vm, dict) else dc_vm
        return '\n\n'.join([vm.to_html() for vm in tmp])


class DocFactory():
    """morphing high level object which takes care of all different tree representations under the hood.
    used to transform between:
        - dash trees
        - document (token) trees
        - uid style parsed dash trees of view models
    """
    @staticmethod
    def from_dash(dash_dc:dict):
        docf = DocFactory()
        docf.data = tt_FlatTree.from_dashtree(dash_dc)
        return docf
    
    @staticmethod
    def from_doc(tocken_dc):
        docf = DocFactory()
        docf.data = tt_DocTree(tocken_dc)
        return docf


    def __init__(self) -> None:
        # disallow direct construction with data, so that only the from_ static methods are allowed to construct
        self.data = {}
    
    
    def to_uid_vmtree(self, indices=None) -> dict:
        return self.data.to_(tt_FlatTree).to_uid_vmtree(indices=indices)
    
    def to_token_dc(self) -> dict:
        return self.data.to_(tt_DocTree).to_dict()
    
    def to_dash(self, disabled=False, add_header=True, can_select=True) -> list:
        vm = self.data.to_(tt_TokenVMTree)
        return vm.to_dash(disabled=disabled, add_header=add_header, can_select=can_select)

    def to_html(self) -> str:
        vm = self.data.to_(tt_TokenVMTree)
        return vm.to_html()




"""

 ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌          ▐░▌       ▐░▌▐░▌               ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌ ▄▄▄▄▄▄▄▄      ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌▐░░░░░░░░▌     ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌ ▀▀▀▀▀▀█░▌     ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌ ▄▄▄▄█░█▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 

 """     




@callback(Output({"arg":"","type":'part-container',"index":MATCH,"parent":MATCH,"sub":"element","token":MATCH,"uid":MATCH}, 'children'),
          Input({"arg":"","type":'part-container',"index":MATCH,"parent":MATCH,"sub":"typesel","token":MATCH,"uid":MATCH}, 'value'),
          prevent_initial_call=True)
def type_update_cb(inp):
    try:
            
        log.debug(('type_update_cb', ctx.triggered_id, inp))
        id_ = ctx.triggered_id
        cls = VM_SELMAP[inp]
        viewmodel = cls(token=id_['token'], index=id_['index'], uid=id_['uid'], parent=id_["parent"])
        log.debug(('type_update_cb', ' --> ', viewmodel))
        return viewmodel.get_VM()
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

        return no_update
    
clientside_callback(
    """
    function attach_image_paste_callback_sub (id_) { 
        console.log("attach_image_paste_callback_sub");
        console.log(id_);
        document.addEventListener("paste", handlePaste);
    }

    function handlePaste(event) {
        console.log(["handlePaste", document.activeElement.id]);
        const id_ = id_;
        if (event.clipboardData && event.clipboardData.items && id_.includes('"type":vm_Image.__name__')) {
            
            for (var i = 0; i < event.clipboardData.items.length; i++) {
                var item = event.clipboardData.items[i];
                if (item.type.indexOf("image") !== -1) {
                    var blob = item.getAsFile();
                    var image_src = URL.createObjectURL(blob);
                    var path = document.getElementById(id_);
                    var imgid = id_.replace(/"sub":"[a-zA-Z0-9_\-]*"/, '"sub":"imageblob"');
                    imgid.replace(/"arg":"[a-zA-Z0-9_\-]*"/, '"arg":"src"');
                    var img = document.getElementById(id_path2image(imgid));
                    
                    if (path.value.length == 0) {
                        path.value = image_name;
                    }
                    img.src = image_src;
                }
            }
        }
    }
    """,
    Output("on_render2", "style"),
    Input("on_render2", "children"),
)


@callback(Output({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"mdiv","token":MATCH,"uid":MATCH}, 'children', allow_duplicate=True),
          Input({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"helper_btn_add","token":MATCH,"uid":MATCH}, 'n_clicks'),
          Input({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"helper_btn_add_i","token":MATCH,"uid":MATCH}, 'n_clicks'),
          Input({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"helper_btn_rem","token":MATCH,"uid":MATCH}, 'n_clicks'),
          Input({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"helper_btn_switch","token":MATCH,"uid":MATCH}, 'n_clicks'),
          State({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"mdiv","token":MATCH,"uid":MATCH}, 'children'),
          State({"arg":ALL,"type":vm_Iterator.__name__,"index":ALL,"parent":ALL,"sub":"mdiv","token":MATCH,"uid":MATCH}, 'id'),
          prevent_initial_call=True)
def btn_addrem_cb(dummy, dummy2, dummy3, dummy4, childrn, indices):


    try:
        log.debug(('btn_addrem_cb', ctx.triggered_id))

        childrn = childrn[0]

        elements = DocFactory.from_dash(childrn).to_uid_vmtree()
        element = elements[ctx.triggered_id['uid']]

        if ctx.triggered_id['sub'] == 'helper_btn_add':
            element.add_element(vm_Markdown(f"## Chapter {ctx.triggered_id['index']+1}", token=ctx.triggered_id['token'], index=ctx.triggered_id['index'], parent=element.uid))
        elif ctx.triggered_id['sub'] == 'helper_btn_add_i':
            element.add_element(vm_Image(token=ctx.triggered_id['token'], index=ctx.triggered_id['index'], parent=element.uid))            
        elif ctx.triggered_id['sub'] == 'helper_btn_rem':
            element.rem_element(int(ctx.triggered_id['index'])-1)
        elif ctx.triggered_id['sub'] == 'helper_btn_switch':
            element.switch_elements(int(ctx.triggered_id['index']))
        else:
            log.error(f'unknown case! {ctx.triggered_id["sub"]=}')

        if VERYVERBOSE:
            print('\nelement=')
            print(element)

        return [element.get_VM(raw=True)]
    
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

        return no_update






@callback(Output({"arg":ALL,"type":vm_Iterator.__name__,"index":MATCH,"parent":MATCH,"sub":"mdiv","token":MATCH,"uid":MATCH}, 'children', allow_duplicate=True),
          Input({"arg":"","type":vm_Iterator.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_uploadfiles","token":MATCH,"uid":MATCH}, 'contents'),
          State({"arg":"","type":vm_Iterator.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_uploadfiles","token":MATCH,"uid":MATCH}, 'filename'),
          State({"arg":ALL,"type":vm_Iterator.__name__,"index":MATCH,"parent":MATCH,"sub":"mdiv","token":MATCH,"uid":MATCH}, 'children'),
          State({"arg":ALL,"type":vm_Iterator.__name__,"index":MATCH,"parent":MATCH,"sub":"mdiv","token":MATCH,"uid":MATCH}, 'id'),
          prevent_initial_call=True)
def upload_imagefiles_list(list_of_contents, list_of_names, childrn, indices):
    info = (type(list_of_contents), len(list_of_contents), [len(c) for c in list_of_contents]) if not list_of_contents is None else None
    
    log.debug(('upload_imagefiles_list', ctx.triggered_id, list_of_names, info))
    try:

        uploaddata = []
        if list_of_contents and list_of_names:
            uploaddata = [(filecontent, filename) for filecontent, filename in zip(list_of_contents, list_of_names) if filecontent and filename]

        if uploaddata:
            if VERYVERBOSE:
                for ind in indices:
                    print(ind)

            elements = DocFactory.from_dash(childrn).to_uid_vmtree()
            element = elements[ctx.triggered_id['uid']]

            kwargsb = dict(token=ctx.triggered_id['token'], index=ctx.triggered_id['index'], parent=element.uid)

            for fileblob, path in uploaddata:
                
                content_type, filecontent = fileblob.split(',')
                
                if any(('image/'+c in content_type for c in 'png bmp avif bmp gif jpeg jpg tiff tif webp'.split())):
                    kwargs = {**kwargsb, **dict(path=path, imageblob=fileblob)}
                    log.debug(f'{path=}! {content_type=} {kwargsb=}')
                    element.append_element(vm_Image(**kwargs))
                elif 'data:text/plain' in content_type:
                    filecontent = base64.b64decode(filecontent).decode('utf-8')

                    kwargs = {**kwargsb, **dict(path=path, filecontent=filecontent)}
                    log.debug(f'{path=}! {content_type=} {kwargsb=}')
                    element.append_element(vm_RawFile(**kwargs))
                else:
                    log.error(f'unknown mimetype for {path=} {content_type=}')

            return [element.get_VM(raw=True)]
        else:
            return no_update
        
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)
        return no_update

@callback(
          Output({"arg":"src","type":vm_Image.__name__,"index":MATCH,"parent":MATCH,"sub":"imageblob","token":MATCH,"uid":MATCH}, 'src'),
          Output({"arg":"value","type":vm_Image.__name__,"index":MATCH,"parent":MATCH,"sub":"value","token":MATCH,"uid":MATCH}, 'value'),
          Input({"arg":"","type":vm_Image.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_uploadfile","token":MATCH,"uid":MATCH}, 'contents'),
          State({"arg":"","type":vm_Image.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_uploadfile","token":MATCH,"uid":MATCH}, 'filename'),
          prevent_initial_call=True)
def upload_imagefile(filecontent, filename):
    try:
        info = (len(filecontent), type(filecontent), filecontent[:30]) if not filecontent is None else 'None'
        log.debug(('upload_imagefile', ctx.triggered_id, info, filename))
        if not filecontent is None:
            # imageblob = base64.b64decode(filecontent)
            src = vm_Image.get_image_src(filecontent)
            return src, filename
        else:
            return no_update, no_update
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)
        return no_update, no_update


@callback(Output({"arg":"",    "type":vm_Markdown.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_dialog","token":MATCH,"uid":MATCH}, 'is_open', allow_duplicate=True),
          Output({"arg":"",    "type":vm_Markdown.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_dialog","token":MATCH,"uid":MATCH}, 'children', allow_duplicate=True),
          Input({"arg":"",     "type":vm_Markdown.__name__,"index":MATCH,"parent":MATCH,"sub":"helper_btn_show_md","token":MATCH,"uid":MATCH}, 'n_clicks'),
          State({"arg":"value","type":vm_Markdown.__name__,"index":MATCH,"parent":MATCH,"sub":"content",           "token":MATCH,"uid":MATCH}, 'value'),
          prevent_initial_call=True)
def btn_show_md(dummy, value):
    try:
        log.debug(('btn_show_md', ctx.triggered_id, helpers.limit_len(value)))
        return True, dcc.Markdown(value, style={'margin': 10, 'minHeight': 300})
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)
        return no_update, no_update
