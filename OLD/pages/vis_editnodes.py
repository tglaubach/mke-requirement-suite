import collections, urllib
import dash

from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update, get_asset_url

import dash_bootstrap_components as dbc
import plotly.graph_objects as go


from ReqTracker.core import helpers, graph_base, graph_main, schema, node_factory

from ReqTracker.ui import ui_state, edit_node, helpers_ui
from ReqTracker.ui.vis.annotation_vis import LAYOUTS_POSSIBLE
from ReqTracker.ui.visualisation_main import update_figure
from ReqTracker.io_files import elasticsearch_api, langchain_nextcloud_io
from ReqTracker.ui_vmc import vmc_docmaker as vmc

f = lambda k: {"type": 'dlge', "index": k}

A = helpers_ui.A
A2 =helpers_ui.A2

log = helpers.log
GetGraphHold = ui_state.GetGraphHold
add_log_alert = ui_state.add_log_alert

def title(graph_name='all', node_id=None):
    return f"Edit Node Dialog: g={graph_name}, node={node_id}"


def description(graph_name='all', node_id=None):
    return f"Edit Node Dialog: g={graph_name}, node={node_id}"


dash.register_page(
    __name__,
    title=title,
    description=description,
)

def mk_plot(graph:graph_main.GraphView, el, state:ui_state.UiState):

    
    
    
    log.debug(f'mk_plot: {graph} --> {el}')
    if graph.name.startswith('search:'):
        subg = graph.to_view()
        subg.view.delete_edges([(subg.root_node.id, c.id) for c in subg.root_node.children])
    else:
        # log.debug('NN -----> {} | nn: parents: {}, children: {}'.format(el, el.parents, el.children))
        # fun = lambda n: (n.classname in ui_state.cls_to_show) or (not n.do_serialize)
        # g = graph.filt_by(graph.name, fun)
        # print(len(graph), g, len(g), len([n for n in graph.vn if fun(n)]), ui_state.cls_to_show)
        subg = graph.filt_by_nn(el, el.id, n_max=state.nn_nmax, clip=state.nn_clip)

    fig = update_figure(subg, state.display_style_splot, maxdepth=state.maxdepth, figsize=200, network_plot_style=state.network_style_splot)
    fig.update_layout(
                showlegend=False,
                margin=dict(b=1,l=1,r=1,t=1),
                xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)
            )
    return fig


def make_inp_dlg(el, graph, tags_possible=[], node_ids = None, state:ui_state.UiState=None):

    if state is None:
        state = ui_state.get()
    
    if not node_ids:
        node_ids = graph.get_viewnodes(as_id=True, with_containers=True)
    
    node_ids = [n for n in node_ids if graph[n].classname in state.cls_to_show or not graph[n].do_serialize]

    f = lambda k: {'type': 'dlge', 'index': k}

    dis = (not el.do_serialize) or ui_state.IS_READONLY
    if dis:
        stl = {'display': 'none'}
    else:
        stl = {}
    
    confirm = 'Are you sure you want to delete the node with id={} (It has N={} children and M={} parents)'.format(el.id, len(el.children), len(el.parents))

    if el.id not in node_ids:
        node_ids += el.id

    nav = dcc.Loading(dbc.Row([
            dbc.Label("Navigate: "),
            dbc.InputGroup([
                dbc.Button(
                        "settings",
                        id=f("show-sel-node-types"),
                    ),
                dbc.Popover(
                    dbc.PopoverBody([
                            dbc.Label("Classes to Show"),
                            dbc.Checklist(
                                options=[{"label": c.__name__, "value": c.__name__} for c in graph_main.classes],
                                value=state.cls_to_show,
                                id=f('cls_to_show'),
                                inline=False,
                                switch=True,
                            ),
                    ]),
                    target=f("show-sel-node-types"),
                    trigger="click",
                ),
                dbc.InputGroupText("Select a Node:", style={'minWidth': 150}),
                dbc.Button("Last ", color='primary', id = f('last'),),
                dcc.Dropdown(
                    id = f('selectnode'),
                    options=node_ids, #[id2i(id_) for id_ in node_ids],
                    value=el.id,
                    placeholder="Select Node",
                    # size='sm',
                    style={'minWidth': '33%'},
                ),
                dbc.Button("Next", color='primary', id = f('next'),),
                dbc.Button("Show Similar Files", color='info', id=f('morelikethis')),

            ], style={'margin': '2px'}, size="sm"),

            # dbc.Pagination(id=f('dlge_pagination'), fully_expanded=False, max_value=len(node_ids), previous_next=True, style={'margin': '2px', 'marginLeft':'15px', 'minWidth': '85%', 'width': '100%'}),

        ], style=edit_node.sb))

    inps = edit_node.mk_input_grp(el, graph, tags_possible=tags_possible, f=f)

    if ui_state.SHOW_LOADING:
        inps = dcc.Loading(inps)

    fig = mk_plot(graph, el, state)

    
    grph = dbc.Row([
            dbc.Label("Detail-Plot: "),
                    dbc.InputGroup([
                        dbc.InputGroupText('N nodes to show max:', style={'minWidth': 180}),
                        dbc.Input(type="number", min=0, value=state.nn_nmax, id={'type': 'par', 'index':'nn_nmax'}),
                        dbc.InputGroupText('Select Plotstyle: ', style={'minWidth': 180}),
                        dbc.Select(options=LAYOUTS_POSSIBLE, value=state.network_style_splot, id={"type": "par", "index": "network_style_splot"}),
                ], style={'marginLeft': '10px', 'minWidth': 250, 'width': '100%'}, size="sm"),
                dcc.Graph(figure=fig, id=f('graph_fig'), style={'zIndex': 1, 'margin': '5px', 'width': '100%'})
    ], style=edit_node.sb)


    if el.do_serialize:
        dashdoc = vmc.DocFactory.from_doc(el.doc_get()).to_dash()
        dashdoc_row = html.Div([
                dbc.Label("Doc Content:"),
                html.Center(dashdoc),
        ], id=f('dashdoc'), style=edit_node.sb)
    else:
        dashdoc_row = html.Div([], id=f('dashdoc'), style=edit_node.sb)

    dlg = dbc.Card([
            dbc.CardHeader(nav),
            dbc.CardBody([
                dcc.Store(id=f('mem_id'), data=el.id),
                html.H4(edit_node.mk_header(el), id=f('header')),
                grph,
                html.Div(inps, id=f('content')),
                dashdoc_row,
            ]),
            dbc.CardFooter([
                dbc.Row([
                    dbc.Col(dcc.ConfirmDialogProvider(
                        dbc.Button("Delete Node", color='danger', disabled=dis, id=f('delete_b')),
                        id=f('delete'),
                        message=confirm, 
                    ), style=stl),
                    
                    
                    dbc.Col(dbc.Button("Reload (no save)", color='secondary', id=f('refresh'))),
                    dbc.Col(dbc.Button("Apply", color="primary", id=f('apply'), disabled = dis)),
                ])
            ]),
        ], id=f('cont'))
    return dlg
    



def layout(g='all', node_id=None, **kwargs):

    try:
        log.debug('layout | g={} | node_id={}'.format(g, node_id))

        g = urllib.parse.unquote_plus(g)
        if node_id:
            node_id = urllib.parse.unquote_plus(node_id)

        state = ui_state.get()

        with ui_state.GetGraphHold() as graph:
            graph_view = ui_state.get_graph_view(graph, g, reverse_graph=False, apply_contraction=False, st=state)   
            
            if not node_id:
                node_id = graph_view.root_node

            if not node_id or not node_id in graph_view:
                return html.Div([
                    'ERROR: The given node_id="{}" could not be found in the graph="{}".'.format(node_id, g),
                    'Return to: ', 
                    dcc.Link(href=f"/vis-editnodes", target="_self")
                    ])
            
            ret = make_inp_dlg(graph_view[node_id], graph_view, tags_possible=state.tags_possible)
            g = graph_view.name

        ttl = html.H4('{}: {}'.format('EDITNODES', g))
        ret = [ttl, ret, dcc.Store(id=f('mem_gname'), data=g)]
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))
        
    return ret


@callback(
        
    Output(f("selectnode"), 'value'),
    Input(f("last"), 'n_clicks'),
    Input(f("next"), 'n_clicks'),
    State(f("selectnode"), 'options'),
    State(f("selectnode"), 'value'),
    prevent_initial_call = True,
)
def dlgupdate_nextlast_cb(nnext, nlast, node_ids, node_id):
    button_clicked = ctx.triggered_id
    id_ = no_update
    log.debug('dlgupdate_nextlast_cb | ' + str(button_clicked))
    if button_clicked and 'index' in button_clicked:
        if button_clicked['index'] == 'last':
            i = node_ids.index(node_id)
            i = (i-1) % len(node_ids)
            id_ = node_ids[i]
            log.info('dlgupdate_nextlast_cb (last) -> Selecting id={}'.format(id_))
            
        elif button_clicked['index'] == 'next':
            i = node_ids.index(node_id)
            i = (i+1) % len(node_ids)
            id_ = node_ids[i]
            log.info('dlgupdate_nextlast_cb (next) -> Selecting id={}'.format(id_))

    return id_

@callback(
    Output(f('content'), 'children', allow_duplicate=True),
    Output(f('selectnode'), 'options', allow_duplicate=True),
    Output(f("selectnode"), 'value', allow_duplicate=True),

    Input(f("delete"), 'submit_n_clicks'),
    State(f("selectnode"), 'value'),
    prevent_initial_call = True,
)
def del_node_cb(dlge_del, id_):

    try:
        ui_state.raise_on_readonly()
        state = ui_state.get()
        with ui_state.GetGraphHold() as graph:
            node = graph[id_]
            i_sel = graph.get_viewnodes(as_id=True).index(id_)
            graph.del_node(node)
            i_sel = i_sel % len(graph)
            
            keys = graph.get_viewnodes(as_id=True)
            id_ = keys[i_sel]
            ret = edit_node.mk_input_grp(graph[id_], graph, tags_possible=state.tags_possible, f=f)

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR update: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))
    return ret


@callback(
    Output(f('c_links'), 'children', allow_duplicate=True),
    Output(f('dchildren'), 'value', allow_duplicate=True),
    Output(f('selectnode'), 'options', allow_duplicate=True),

    Output(f('iaddchild'), 'value'),
    Output(f('iaddchild'), 'valid'),
    Output(f('iaddchild'), 'invalid'),
    Input(f('iaddchild'), 'value'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def iaddchild_cb(new_child_id, id_):
    try:
        c_links = no_update
        dchildren = no_update
        options = no_update

        invalid = False
        valid = False
        changed_id = [p['prop_id'] for p in callback_context.triggered][0]
        log.debug('iaddchild_cb -> {} {} {}'.format(changed_id, id_, new_child_id))

        ui_state.raise_on_readonly()
        if isinstance(id_, dict) and 'index' in id_:
            id_ = id_['index'] 

        if new_child_id:
            with ui_state.GetGraphHold() as graph:
                el = graph[id_]
                node_new_child = ui_state.make_node(graph, new_child_id)
                log.debug(('iaddchild_cb', el, node_new_child, el.linked))

                if el == graph.root_node:
                    valid = True
                    invalid = False
                    new_child_id = ''
                else:
                    if node_new_child.id != el.id and not node_new_child in el.linked:
                        graph.add_edge((el.id, node_new_child.id))
                        add_log_alert(A('{} | sucessfully added node as child: {}'.format(el.id, node_new_child.id), color="success"))
                        valid = True
                        invalid = False
                        new_child_id = ''
                    else:
                        valid = False
                        invalid = True
                        add_log_alert(A('{} | could not add child because already linked or same: {}'.format(el.id, node_new_child.id), color="warning"))
                        
                dc = edit_node.get_update_dc(el, graph)
                c_links = dc['c_links']
                dchildren = dc['dchildren']
                options = graph.get_viewnodes(as_id=True, with_containers=True)
    except Exception as err:
        log.error(err, exc_info=True)
        add_log_alert(A("{} | ERROR inewchildnode_cb: {}".format(id_, err), color="danger"))
        invalid = True

    return c_links, dchildren, options, new_child_id, valid, invalid


@callback(
    Output(f('c_links'), 'children', allow_duplicate=True),
    Output(f('dchildren'), 'value', allow_duplicate=True),

    Output(f('inewfile'), 'value'),
    Output(f('inewfile'), 'valid'),
    Output(f('inewfile'), 'invalid'),
    Input(f('inewfile'), 'value'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def inewfile_cb(inp, id_):
    try:
        ui_state.raise_on_readonly()
        c_links = no_update
        dchildren = no_update
        invalid = False
        valid = False
        changed_id = [p['prop_id'] for p in callback_context.triggered][0]
        log.debug('inewfile_cb -> {} {} {}'.format(changed_id, id_, inp))
        state = ui_state.get()
        if inp:
            with ui_state.GetGraphHold() as graph:
                el = graph[id_]
                node = node_factory.construct_doc(inp, ui_state.NEXTCLOUD_LOGIN_DATA, graph, on_exist='get', lookups=state.db_path)
                el.add_child(node)
                dc = edit_node.get_update_dc(el, graph)

                c_links = dc['c_links']
                dchildren = dc['dchildren']
                inp = ''
                valid = True

    except Exception as err:
        log.error(err, exc_info=True)
        add_log_alert(A("{} | ERROR inpg_inewfile_cb: {}".format(id_, err), color="danger"))
        invalid = True

    return c_links, dchildren, inp, valid, invalid


@callback(
    Output(f('notes_md'), 'children', allow_duplicate=True),
    Output(f('innote'), 'value'),
    Output(f('innote'), 'valid'),
    Output(f('innote'), 'invalid'),
    Input(f('innote'), 'value'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def innote_cb(inp, id_):
    notes_md = no_update
    invalid = False
    valid = False
    try:
        ui_state.raise_on_readonly()
        changed_id = [p['prop_id'] for p in callback_context.triggered][0]
        log.debug('innote_cb -> {} {} {}'.format(changed_id, id_, inp))

        if inp:
            with ui_state.GetGraphHold() as graph:
                el = graph[id_]
                el.add_note(inp)
                valid = True
                inp = ''
                dc = edit_node.get_update_dc(el, graph)
                # log.debug(dc)
                notes_md = dc['notes_md']


    except Exception as err:
        log.error(err, exc_info=True)
        add_log_alert(A("{} | ERROR inpg_innote_cb: {}".format(id_, err), color="danger"))
        invalid = True

    return notes_md, inp, valid, invalid



@callback(
    Output(f('mem_id'), 'data', allow_duplicate=True),

    Output(f('graph_fig'), 'figure', allow_duplicate=True),
    Output(f('header'), 'children', allow_duplicate=True),
    Output(f('dstatus'), 'value', allow_duplicate=True),
    Output(f('itags'), 'value', allow_duplicate=True),
    Output(f('itxt'), 'value', allow_duplicate=True),
    Output(f('mdtxt'), 'children', allow_duplicate=True),
    Output(f('notes_md'), 'children', allow_duplicate=True),
    Output(f('p_links'), 'children', allow_duplicate=True),
    Output(f('dparents'), 'options', allow_duplicate=True),
    Output(f('dparents'), 'value', allow_duplicate=True),
    Output(f('c_links'), 'children', allow_duplicate=True),
    Output(f('dchildren'), 'options', allow_duplicate=True),
    Output(f('dchildren'), 'value', allow_duplicate=True),
    Output(f('dashdoc'), 'children', allow_duplicate=True),

    Output(f('dstatus'), 'disabled', allow_duplicate=True),
    Output(f('itags'), 'disabled', allow_duplicate=True),
    Output(f('itxt'), 'disabled', allow_duplicate=True),
    Output(f('innote'), 'disabled', allow_duplicate=True),
    Output(f('delete_b'), 'disabled', allow_duplicate=True),
    
    Output('sdlg', 'is_open', allow_duplicate=True),

    Input(f("selectnode"), 'value'),
    State(f("mem_gname"), 'data'),

    prevent_initial_call = True,
)
def open_modal_cb(id_, gname):
    ks = 'header dstatus itags itxt mdtxt notes_md p_links keys dparents c_links keys dchildren'.split()
    dis = [no_update] * 5
    ret = tuple([no_update, no_update] + [no_update for _ in ks] + [no_update] + dis + [no_update])

    try:
        log.debug('open_modal_cb' + str(id_))
        with ui_state.GetGraphHold() as g:
            gv = ui_state.get_graph_view(g, kview=gname, apply_contraction=False)
            if id_ not in gv:
                id_ = gv.root_node.id
                add_log_alert(A('open_modal_cb --> id:{} not found in {}. Falling back to: {}'.format(id_, gv, gv.root_node.id), color='warning'))
            dc = edit_node.get_update_dc(gv[id_], gv) 
            fig = mk_plot(gv, gv[id_], ui_state.get())
            disabl = ui_state.IS_READONLY or (not gv[id_].do_serialize)
            val = True if not id_ in g else not g[id_].do_serialize
            dis = [val] * 5
            if gv[id_].do_serialize:
                dashtree = vmc.DocFactory.from_doc(gv[id_].doc_get()).to_dash(disabled=disabl, add_header=False, can_select=disabl)
                dashdoc_row = [ 
                    dbc.Label("Doc Content:"), 
                    html.Center(dashtree)
                ]
            else:
                dashdoc_row = []

        ret = tuple([id_, fig] + [dc[k] for k in ks] + [dashdoc_row] + dis + [False])

    except Exception as err:
        add_log_alert(A('{} open_modal_cb --> ERROR: {}'.format(id_, err), color='danger'))
        log.error(err, exc_info=True)

    return ret

@callback(
    Output(f('selectnode'), 'value', allow_duplicate=True),
    Input({'type': 'par', 'index': 'nn_nmax'}, 'value'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def nn_nmax_cb(n1, id_):
    return id_

@callback(
    Output(f('selectnode'), 'value', allow_duplicate=True),
    Input({'type': 'par', 'index': 'network_style_splot'}, 'value'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def network_style_splot_cb(n1, id_):
    return id_


@callback(
    Output(f('selectnode'), 'value', allow_duplicate=True),
    Input(f('refresh'), 'n_clicks'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def refresh_node_cb(n1, id_):
    return id_



    
@callback(
    Output('dialogs', 'children', allow_duplicate=True),
    Output('dialogs', 'is_open', allow_duplicate=True),
    Input(f('morelikethis'), 'n_clicks'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def morelikethis_cb(n1, id_):
    ret = no_update
    is_open = no_update
    try:
        state = ui_state.get()
        log.debug('morelikethis_cb ' + str(id_))
        if n1 and id_:
            with GetGraphHold() as graph:
                tempstore = set()
                txt = helpers_ui.node2md(graph[id_])
                filename_filt = 'MKE*' if state.only_search_mke_docs else ''
                if elasticsearch_api.es is None:
                    ret = [dbc.Alert(f'can not search similar to "{id_}" because there is no connection connection to Elasticsearch', color='warning')]
                else:
                    hits = elasticsearch_api.es_more_like_this(txt, filename_filt=filename_filt, n_max=state.n_search_results)

                    def helpr(hit):
                        pi = hit['source']
                        uname = ui_state.NEXTCLOUD_LOGIN_DATA[0] if ui_state.NEXTCLOUD_LOGIN_DATA else ''
                        lnk = f'{langchain_nextcloud_io.DEFAULT_NC_URL}/remote.php/dav/files/{uname}/{pi}'
                        return helpers_ui.doc2item(hit, tempstore, lnk)
                    
                    els = [helpr(hit) for hit in hits]

                    st = {'margin': '15px', "minHeight": '100px', "maxHeight": '500px', 'overflow':'scroll', 'width': '100%'}

                    ret = [
                        dbc.ModalHeader(helpers_ui.node2title(graph[id_], prefix='Similar Files to ')),
                        dbc.ModalBody(html.Div(els, style=st)),
                    ]
                is_open = True

    except Exception as err:
        add_log_alert(A('morelikethis_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

    return ret, is_open

@callback(
    Output(f('selectnode'), 'value', allow_duplicate=True),

    Input(f('apply'), 'n_clicks'),

    State(f('dstatus'), 'value'),
    State(f('itxt'), 'value'),
    State(f('itags'), 'value'),
    State(f('dparents'), 'value'),
    State(f('dchildren'), 'value'),
    State(f('dashdoc'), 'children'),
    State(f('mem_id'), 'data'),
    prevent_initial_call = True,
)
def save_node_cb(n1, dstatus, itxt, itags, dparents, dchildren, dashdoc_dc, id_):
    id_to_edit = no_update

    try:
        button_clicked = ctx.triggered_id
        log.debug('save_node_cb' + str(dstatus) + str(itxt))
        if dstatus:
            ui_state.raise_on_readonly()

            dc = {
                'status': dstatus,
                'text': itxt,
                'itags': itags,
                'newfiles_from_index': None,
                'children': dchildren,
                'parents': dparents,
            }

            doc = vmc.DocFactory.from_dash(dashdoc_dc).to_token_dc()     

            log.info(f'saving {id_}...')
            log.debug(itxt)
            with GetGraphHold() as graph:
                edit_node.save_node_changes(graph[id_], dc, graph, ui_state.NEXTCLOUD_LOGIN_DATA)
                graph[id_].doc_set(doc)
            id_to_edit = id_

    except Exception as err:
        add_log_alert(A('toggle_modal_editnode_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

    return id_to_edit    







@callback(
    Output(f('selectnode'), 'value', allow_duplicate=True),
    Output(f('selectnode'), 'options', allow_duplicate=True),
    Input({'type': 'par', 'index': 'cls_to_show'}, 'value'),
    State(f('mem_id'), 'data'),
    State(f('mem_gname'), 'data'),
    prevent_initial_call = True,
)
def cls_to_show_cb(classes_to_show, id_, gname):
    id_to_edit = no_update
    selectnode_options = no_update

    try:
        with GetGraphHold() as g:
            gv = ui_state.get_graph_view(g, gname)
            tmp = gv.get_viewnodes(as_id=False, with_containers=True)
            selectnode_options = [el.id for el in tmp if el.classname in classes_to_show]
            if id_ not in selectnode_options:
                id_to_edit = g.root_node.id
        # ui_state.push_projvars(cls_to_show=classes_to_show)
    except Exception as err:
        add_log_alert(A('cls_to_show_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

    return id_to_edit, selectnode_options



@callback(
    Output(f('selectnode'), 'value', allow_duplicate=True),
    Input(f('graph_fig'), 'clickData'), 
    prevent_initial_call=True
)
def graph_click_cb(clickData):
    try:
        node_id = no_update
        if clickData and 'points' in clickData and len(clickData) > 0:
            log.debug(clickData)
            full_id = clickData['points'][0]['id']
            node_id = full_id.split('|')[-1].strip()
            log.debug('id_ --> {}'.format(node_id))
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)
    return node_id
