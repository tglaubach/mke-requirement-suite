import urllib

import dash
from dash import html, dcc, no_update, Output, Input, State, callback, ctx, MATCH, ALL

# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import dash_cytoscape as cyto

from ReqTracker.core import helpers, graph_main, graph_base, schema

from ReqTracker.ui import helpers_ui, ui_state
from ReqTracker.ui.vis.annotation_vis import LAYOUTS_POSSIBLE
from ReqTracker.ui.vis.cyto_vis import get_fig, SIZEFUNS_ALLOWED


log = helpers.log

A = helpers_ui.A


key = 'vis_cyto'
f = lambda s: { 'type': key, 'index': s }


dash.register_page(__name__)#, path_template=f"/{__name__}/<g>",)

inps = dict(g='all', reverse=False, gname='all', fixed=0)
ids_expand = []


# def set_titles(graph_view):
#     global titles_dc
#     titles_dc = {n.id:n.hovertext for n in graph_view.vn}

def layout(g='all', reverse=False, **other_unknown_query_strings):
    global inps, ids_expand
    
    
    ids_expand = []

    gname = ''
    ret = None
    ttl = no_update

    fixed = 0

    try:
        g = urllib.parse.unquote_plus(g)

        ttl = None
        state = ui_state.get()

        log.debug('vis_cyto layout | g={} | reverse={}'.format(g, reverse))
        with ui_state.GetGraphHold() as graph:        
            graph_view = ui_state.get_graph_view(graph, g, reverse, st=state)    
            if state.start_contracted:
                graph_view = graph_view.c_contract_containers()
            
            fig = get_fig(graph_view, fixed, state=state)

            
        ret = html.Div(fig, id=f('container'))
            

        ttl = dbc.InputGroup([
            dbc.InputGroupText('{}: {}'.format('Cyto', gname), id={"type": key, "index": "ttl"}),
            
            dbc.Tooltip("Plotting Style: Select a style to use for plotting", target={"type": "par", "index": "display_style_plt"}, placement='bottom'),
            dbc.Button('\u27F3', id={"type": key, "index": "refreshgraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Refresh the Plot below", target={"type": key, "index": "refreshgraph"}, placement='bottom'),
            dbc.Button('\u21B9', id={"type": key, "index": "reversegraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Flip the graph upside down", target={"type": key, "index": "reversegraph"}, placement='bottom'),
            
            dbc.InputGroupText("figsize", style={'width': 220}),
            dbc.Input(type="number", value=state.figsize, id={"type": "par", "index": "figsize"}, min=0, step=100),
            
            dbc.InputGroupText("network_plot_style", style={'width': 220}),
            dbc.Select(options=LAYOUTS_POSSIBLE, value=state.network_plot_style, id={"type": "par", "index": "network_plot_style"}),

            dbc.InputGroupText("size_fun", style={'width': 140}),
            dbc.Select(options=SIZEFUNS_ALLOWED, value=state.size_fun, id={"type": "par", "index": "size_fun"}),
            dbc.InputGroupText("fixed", style={'width': 140}),
            dbc.Select(
                    id=f("fixed-switch"),
                    options = "loose fixed".split(),
                    value="fixed",
                ),

        ], size="m")

        

    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        s = "ERROR vis_cyto: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = dbc.Alert(s, color="danger")

    mem = dcc.Store(f('mem-reload'), data='')


    if ui_state.SHOW_LOADING:
        ret = dcc.Loading(ret)

    inps=dict(g=g, reverse=reverse, gname=gname, fixed=fixed)

    return [ttl, mem, ret]


@callback(
    Output("id_toshow", "data", allow_duplicate=True),
    Output(f("mem-reload"), 'data', allow_duplicate=True),
    Input({'type': 'cytoscape-events', 'index': ALL}, 'tapNodeData'),
    prevent_initial_call=True)
def displayTapNodeData(data):
    node_id = no_update
    needs_reload = no_update

    global inps, ids_expand
    
    try:
        log.debug(f'displayTapNodeData {data} | {inps}')

                  
        if data:
            d = next((i for i in data if i), None)
            if d:
                gname = inps['gname']

                if '>clust' in d['label']:
                    to_expand = d['label'].split('>clust')[0]
                    if not to_expand in ids_expand:
                        ids_expand.append(to_expand)
                        log.debug('reloading and uncollapsing: ' + to_expand)
                        needs_reload = helpers.nowiso()
                else:
                    node_id = d['label']
                    log.debug('displayTapNodeData with id_ --> {}'.format(node_id))
                    node_id = {'id': node_id, 'gname': gname}
                
    except Exception as err:
        ui_state.add_log_alert(A('displayTapNodeData --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)
    return node_id, needs_reload


@callback(
        
    Output(f('container'), 'children', allow_duplicate=True),
    Input({"type": key, "index": "refreshgraph"}, 'n_clicks'),
    Input({"type": key, "index": "reversegraph"}, 'n_clicks'),
    Input({"type": 'par', "index": ALL}, 'value'),
    Input(f("fixed-switch"), 'value'),
    Input(f("mem-reload"), 'data'),
    prevent_initial_call=True
)
def update_page(dummy, dummy2, dummy3, fixed_switch, needs_reload):
    global inps, ids_expand
    fig = no_update
    # elements=no_update
    try:
        log.debug(f'update_page {ctx.triggered_id}')

        reverse = 'index' in ctx.triggered_id and ctx.triggered_id['index'] == 'reversegraph'
        
        fixed = fixed_switch != 'fixed'
        g = inps['g']
        state = ui_state.get()

        if not state.start_contracted:
            ids_expand = []
        
        with ui_state.GetGraphHold() as graph:        
            graph_view = ui_state.get_graph_view(graph, g, reverse, containers_to_expand=ids_expand, st=state)    
            fig = get_fig(graph_view, fixed)

            # elements = fig.elements

        log.debug('layout | g={} | reverse={}'.format(g, reverse))



    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        s = "ERROR vis_cyto: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        fig = dbc.Alert(s, color="danger")

    return fig








# @callback(Output('cytoscape-tapEdgeData-output', 'children'),
#               Input('cytoscape-event-callbacks-2', 'tapEdgeData'))
# def displayTapEdgeData(data):
#     if data:
#         return "You recently clicked/tapped the edge between " + \
#                data['source'].upper() + " and " + data['target'].upper()


# @callback(Output('cytoscape-mouseoverNodeData-output', 'children'),
#               Input('cytoscape-event-callbacks-2', 'mouseoverNodeData'))
# def displayTapNodeData(data):
#     if data:
#         return "You recently hovered over the city: " + data['label']


# @callback(Output('cytoscape-mouseoverEdgeData-output', 'children'),
#               Input('cytoscape-event-callbacks-2', 'mouseoverEdgeData'))
# def displayTapEdgeData(data):
#     if data:
#         return "You recently hovered over the edge between " + \
#                data['source'].upper() + " and " + data['target'].upper()
