from pathlib import Path

import re
import base64
import datetime
import io
import os
import shutil
import time
import traceback
import copy
import base64
from collections import UserDict

import json

import subprocess
from typing import Any
import uuid

import dash
from dash import Dash, dcc, html, dash_table, Input, Output, State, callback, no_update, clientside_callback, ALL, MATCH, ctx, ClientsideFunction
import dash_bootstrap_components as dbc


import sys, inspect, os


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))

if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    app = Dash('', 
    external_stylesheets=[dbc.themes.FLATLY ])#, dbc.themes.LITERA SUPERHERO ])
else:
    def title(docid='', **kwargs):
        return f"Automatic Document Generator: docid={docid}"


    def description(docid='', **kwargs):
        return f"Automatic Document Generator: docid={docid}"

    dash.register_page(
        __name__,
        title=title,
        description=description,
    )


# app = Dash(__name__, external_stylesheets=[dbc.themes.FLATLY ])#, dbc.themes.LITERA SUPERHERO ])

import ReqTracker
from ReqTracker.core import helpers, schema
from ReqTracker.exporters import docmaker_tex, repmaker
from ReqTracker.ui_vmc import vmc_docmaker as vm
from ReqTracker.ui.helpers_ui import A
from ReqTracker.ui import ui_state

# print(DEFAULT_IMAGE_PATH)
# print(DEFAULT_IMAGE_BLOB)

log = helpers.log


UPLOAD_DIR = 'wip'

f = lambda k: dict(type='repmake', index=k)
fm = lambda t, k: dict(type=t, index=k)

mine_pattern = re.compile(r"(?P<COMMAND>[A-Z]+)?_?(?P<tp>[A-Z]+)?_?(?P<ncols>[0-9]+)?:?(?P<KEY>[a-zA-Z0-9\-_]+)?")






"""

 ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌ ▀▀▀▀█░█▀▀▀▀ 
▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░▌          ▐░▌       ▐░▌     ▐░▌     ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     
▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     
▐░░░░░░░░░░░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     
 ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀      
                    
"""

TEMPLATE_OPTIONS = [dict(label='', value='')] + [dict(label=os.path.basename(t), value=t) for t in repmaker.get_possible_templates()]


width = {
    'PBS': 130,
    'doc_type': 100,
    'doc_type': 60,
    'doc_no': 80,
    'dish_no': 100,
    'title': None
}



def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]

def get_docinfo_dc(docname):
    parts = schema.Document.fun_match(docname)
    defs = {
        'PBS': '316',
        'org': 'MPI',
        'doc_type': 'REP',
        'version': '01',
        'title': 'Some_Title'
    }

    defs = {**defs, **parts}

    words = defs['title'].replace('-', ' ').replace('_', '').split(' ')

    w = []
    for ww in words:
        w += camel_case_split(ww)

    longtitle = ' '.join(w)
    author = 'ReqTracker.ui.v' + ReqTracker.__version__ if hasattr(ReqTracker, '__version__') else '<dirty-wip>'
    defs['longtitle'] = longtitle
    defs['author'] = author

    return defs

def layout(docid='', **kwargs):
    if docid:
        docname = docid + '-TITLE'
    elif __name__ == '__main__':
        docname = 'MKE-316-000000-MPI-XXX-9999-99-Some_Name'
    else: 
        docname = 'MKE-316-000000-MPI-XXX-9999-99-Some_Name'

    defs = get_docinfo_dc(docname)

    # childrn = []
    # for k in 'PBS - org - doc_type - doc_no - dish_no - version - title'.split():
    #     if k == '-':
    #         childrn.append(dbc.InputGroupText('-'))
    #     else:
    #         w = width.get(k, 60)
    #         v = defs.get(k, None)
    #         # if w:
    #         childrn.append(dbc.Input(value=v, id=f(k), placeholder=k, style={'maxWidth': w}, debounce=False))
    #         # else:
    #         #     childrn.append(dbc.Input(v, id=f(k), placeholder=k))


    longtitle = defs['longtitle']
    author = defs['author']

    listitems = vm.DocFactory.from_doc(vm.example_doc_dc).to_dash()


    layout = html.Center([
        dcc.Download(id=f('download')),
        dbc.Modal(id=f('dialogs'), is_open=False, fullscreen=False, scrollable=True, size='xl'),
        html.Center([
            html.H3('MKE - Automatic Document Generator', id=f('vis-docmaker-page-title')),

            html.H5('Document Metadata'),
            # dbc.InputGroup([
            #     dbc.InputGroupText('Document:', style={'width': 140}),
            #     *childrn,
            #     dbc.InputGroupText('.pdf'),
                
            # ], size='sm'),
            

            dbc.InputGroup([
                dbc.InputGroupText('Filename:', style={'width': 140}),
                dbc.Input(value = docname, id=f('DOCNAME'), placeholder='DOCNAME'),

            ], size='sm'),

            dbc.InputGroup([
                dbc.InputGroupText('Long Title:', style={'width': 140}),
                dbc.Input(id=f('LONGTITLE'), value = longtitle, placeholder='LONGTITLE'),
            ], size='sm'),

            dbc.InputGroup([
                dbc.InputGroupText('Template:', style={'width': 140}),
                dbc.Select(value=repmaker.TEMPLATE_DEFAULT, options=TEMPLATE_OPTIONS, id=f('template')),
                dbc.InputGroupText('Author:'),
                dbc.Input(id=f('AUTHOR'), value = author, placeholder='AUTHOR'),
                dbc.InputGroupText('Main Node:'),
                dbc.Select(value='', options=[], id=f('basenode')),
                dbc.Button('\u27F3', id=f('basenode_refresh')),
            ], size='sm'),

            dbc.InputGroup([
                dbc.InputGroupText('Changenote:', style={'width': 140}),
                dbc.Textarea(id=f('changenote'), value = "auto generated", placeholder='changenote'),
            ], size='sm'),

            html.H5('Document Content'),
            dbc.ListGroup(listitems, id=f('docelements')),
            html.Hr(),
            html.H5('Export'),
            dbc.InputGroup([
                dbc.InputGroupText('DOWNLOAD:'),
                dbc.Button('ZIP', id=f('compile_zip')),
                dbc.Button('JSON', id=f('compile_json')),
                dbc.InputGroupText('SHOW:'),
                dbc.Button('DOC', id=f('compile_json_show')),
                dbc.Button('TREE', id=f('compile_tree_show'), color='info'),
                dbc.Button('RENDER', id=f('render')),
            ], size='sm'),


        ], style={'margin':20, 'maxWidth': 1200})
    ])
    
    return layout


# {
#     "sub": "typesel",
#     "index": 0,
#     "token": "Myiter",
#     "parent": 2371176885456,
#     "arg": "",
#     "uid": 2371212016464,
#     "type": "part-container"
#   },
# {"arg":"","index":"","parent":0,"sub":"typesel","token":"Mytext","type":"part-container","uid":2690927786896}
# Output({"arg":"src","type":"vm_Image","index":MATCH,"parent":MATCH,"sub":"imageblob","token":MATCH,"uid":MATCH}, 'src'),
#           Output({"arg":"filename","type":"vm_Image","index":MATCH,"parent":MATCH,"sub":"filename","token":MATCH,"uid":MATCH}, 'value'),


# {"arg":"value","index":"","parent":0,"sub":"value","token":"MyImage","type":"vm_Image","uid":1821132691883}


"""

 ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌          ▐░▌       ▐░▌▐░▌               ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌ ▄▄▄▄▄▄▄▄      ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌▐░░░░░░░░▌     ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌ ▀▀▀▀▀▀█░▌     ▐░▌     ▐░▌          
▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ▐░▌          
▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌ ▄▄▄▄█░█▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀ 

 """     





@callback(Output(f('download'), 'data'),
          Output(f('DOCNAME'), 'valid'),
          Output(f('DOCNAME'), 'invalid'),
          Output(f('dialogs'), 'is_open'),
          Output(f('dialogs'), 'children'),
          Input(f('compile_zip'), 'n_clicks'),
          Input(f('compile_json'), 'n_clicks'),
          Input(f('compile_json_show'), 'n_clicks'),
          Input(f('compile_tree_show'), 'n_clicks'),
          Input(f('render'), 'n_clicks'),
          State(f('docelements'), 'children'),
          State(f('DOCNAME'), 'value'),
          State(f('AUTHOR'), 'value'),
          State(f('LONGTITLE'), 'value'),
          State(f('changenote'), 'value'),
          State(f('template'), 'value'),
          State(f('basenode'), 'value'),
          prevent_initial_call=True)
def compile_cb(dummy1, dummy2, dummy3, dummy4, dummy5, childrn, docname, author, longtitle, changenote, template, basenode):
    try:
        
        log.debug(('compile_cb', len(childrn), docname, author, longtitle, helpers.limit_len(changenote, 25), template, basenode))
        
        assert docname, 'Need a document name to generate a report!'

        docf = vm.DocFactory.from_dash(childrn)

        



        if docname.endswith('.pdf'):
            docname = docname[:-4]

        rep = repmaker.ReportMaker(docname, author, longtitle, changenote, template_dir=template)

        node = None
        if basenode:
            with ui_state.GetGraphHold() as g:
                node = g[basenode]            

        rep.init(node=node)
        rep.set_data(docf.to_token_dc())
                
        if ctx.triggered_id == f('render'):
            chldrn = docf.to_dash(disabled=True)
            return no_update, no_update, no_update, True, chldrn

        elif ctx.triggered_id == f('compile_json'):
            j = json.dumps(rep.data, indent=2)
            filename = rep.docname + '.json'
            log.debug(f'sending: {filename=}')
            return dcc.send_string(src=j, filename=filename, type="application/json"), True, False, no_update, no_update
        
        elif ctx.triggered_id == f('compile_zip'):
            filename, filecontent_bio = rep.run(node=node)
            filecontent = filecontent_bio.read()
            return dcc.send_bytes(src=filecontent, filename=filename, type='application/zip'), True, False, no_update, no_update
        
        elif ctx.triggered_id == f('compile_json_show') or ctx.triggered_id == f('compile_tree_show'):
            if ctx.triggered_id == f('compile_tree_show'):
                j = json.dumps(dict(tree = docf.to_uid_vmtree(), childrn=childrn, token_dc=docf.to_token_dc()), indent=2)
                # j = json.dumps(dict(tree = tree, token_dc=token_dc
                filename = 'tree'
            else:
                j = json.dumps(rep.data, indent=2)
                filename = rep.docname + '.json'

            chldrn = [
                dbc.Label('filename: ' + filename),
                dbc.Textarea(value=j, 
                                placeholder='filecontent', 
                                style={
                                    'fontFamily': "Lucida Console, Courier New, monospace", 
                                    'fontSize': '10px',
                                    "minHeight": "800px"
                                    }
                            )
            ]

            return no_update, no_update, no_update, True, chldrn
        else:
            raise ValueError(f'unknown trigger id! {ctx.triggered_id=}')
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)
    
    return no_update, no_update, no_update, no_update, no_update



@callback(Output(f('docelements'), 'children'),
          Input(f('template'), 'value'),
          State(f('DOCNAME'), 'value'),
          State(f('AUTHOR'), 'value'),
          State(f('LONGTITLE'), 'value'),
          State(f('changenote'), 'value'),
          State(f('basenode'), 'value'),
          prevent_initial_call=False)
def template_update_cb(template, docname, author, longtitle, changenote, basenode):

    try:
        if not template:
            return vm.DocFactory.from_doc(vm.example_doc_dc).to_dash()

        if basenode:
            with ui_state.GetGraphHold() as g:
                node = g[basenode]
        else:
            node = None

        rep = repmaker.ReportMaker(docname, author, longtitle, changenote, template_dir=template)

        rep.init(node=node)
        dc_doc = rep.data
        
        if vm.VERYVERBOSE:
            log.debug('\n\ndc_doc=\n' + json.dumps(dc_doc, indent=2))

        dc_doc = {k:v for k, v in dc_doc.items() if k not in vm.TOKEN_METADATA}

        dash_tree = vm.DocFactory.from_doc(dc_doc).to_dash()

        return dash_tree
    
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

        return no_update



@callback(Output(f('basenode'), 'value'),
          Output(f('basenode'), 'options'),
          Input(f('basenode_refresh'), 'n_clicks'),
          Input(f('basenode'), 'value'), 
          prevent_initial_call = __name__ == '__main__')
def basenode_update_cb(dummy, basenode):
    try:
        
        with ui_state.GetGraphHold() as g:
            nodes = [dict(label=str(n), value=n.id) for n in g.get_viewnodes(as_id=False, with_containers=False)]
            nodes.insert(0, dict(label='', value=''))
            if basenode not in g:
                basenode = ''

        return basenode, nodes

    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

        return no_update, no_update


if __name__ == '__main__':

    log.setLevel(helpers.logging.DEBUG)
    # dc = parse_dashtree(a)
    # print(json.dumps(dc, indent=2))

    app.layout = layout()
    log.info('__main__ --> app.run_server(...)')
    app.run_server(debug=True, use_reloader=True)  # Turn off reloader if inside Jupyter

    
