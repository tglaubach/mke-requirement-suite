import json
import re
import time
import urllib
import textwrap

from OLD.io import woosh_api
import dash
import numpy as np
from whoosh import index, highlight, writing

from dash import dash_table, dcc, html, no_update, Output, Input, State, callback, clientside_callback
import dash_bootstrap_components as dbc
import yaml

from ReqTracker.core import graph_base, helpers, schema

from ReqTracker.io_files import langchain_nextcloud_io, elasticsearch_api
from ReqTracker.ui import ui_state, helpers_ui, crawl
from ReqTracker.io.graphdb_io import fs_search, fs_setup, fs_info

A = helpers_ui.A

log = helpers.log

# ix = None

# def get_ix():
#     global ix
#     if ix is None:
#         ix = woosh_api.get_indexer(ui_state.DEFAULT_FINDEX_PATH, force_clean=ui_state.WOOSH_CLEAN_INDEX_ON_START)
        
#     return ix


opts = ['FILE-contents', 'DB-Nodes', 'DB-IDs']


def handle_search(search_options, search_text):
    
    state = ui_state.get()

    if search_options == 'ALL':
        search_options = opts
    elif isinstance(search_options, str):
        search_options = [search_options]

    if not search_text or not search_options:
        return None, html.Div('')
    ret = []
    t = time.time()
    for search_where in search_options:
        log.debug('searching ' + search_where)
        if search_where.startswith('DB-Nodes'):
            
            assert state.db_path.endswith('.sqlite'), 'can only search on a sqlite database source!'
            sres = fs_search(state.db_path, search_text, limit=state.n_search_results)
            log.debug('searching {}.json: -> adding N={} results'.format(search_where, len(sres)))

            with ui_state.GetGraphHold() as g:
                ret += [helpers_ui.hitnode2item(hit, g) for hit in sres.values()]
        elif search_where.startswith('DB-IDs'):

            assert state.db_path.endswith('.sqlite'), 'can only search on a sqlite database source!'
            sres = fs_search(state.db_path, search_text, search_column='id', limit=state.n_search_results)
            log.debug('searching {}.id: -> adding N={} results'.format(search_where, len(sres)))
            with ui_state.GetGraphHold() as g:
                ret += [helpers_ui.hitnode2item(hit, g) for hit in sres.values()]
        elif search_where.startswith('FILE-IDs'):
            tempstore = set()
            # hits, runtime = woosh_api.wsearch(search_text, ix=get_ix())
            if elasticsearch_api.es is None:
                ret += [dbc.Alert(f'can not search "{search_where}" because there is no connection connection to Elasticsearch', color='warning')]
            else:
                hits = elasticsearch_api.es_search(search_text, field='node_id', n_max=state.n_search_results)
                log.debug('searching {}.id: -> adding N={} results'.format(search_where, len(hits)))

                def helpr(hit):
                    pi = hit['source']
                    uname = ui_state.NEXTCLOUD_LOGIN_DATA[0] if ui_state.NEXTCLOUD_LOGIN_DATA else ''
                    lnk = f'{langchain_nextcloud_io.DEFAULT_NC_URL}/remote.php/dav/files/{uname}/{pi}'
                    return helpers_ui.doc2item(hit, tempstore, lnk)
                ret += [helpr(hit) for hit in hits]

        elif search_where.startswith('FILE'):
            tempstore = set()
            if elasticsearch_api.es is None:
                ret += [dbc.Alert(f'can not search "{search_where}" because there is no connection connection to Elasticsearch', color='warning')]
            else:
                # hits, runtime = woosh_api.wsearch(search_text, ix=get_ix())
                hits = elasticsearch_api.es_search(search_text, n_max=state.n_search_results)
                log.debug('searching {}.id: -> adding N={} results'.format(search_where, len(hits)))

                def helpr(hit):
                    pi = hit['source']
                    uname = ui_state.NEXTCLOUD_LOGIN_DATA[0] if ui_state.NEXTCLOUD_LOGIN_DATA else ''
                    lnk = f'{langchain_nextcloud_io.DEFAULT_NC_URL}/remote.php/dav/files/{uname}/{pi}'
                    return helpers_ui.doc2item(hit, tempstore, lnk)
                ret += [helpr(hit) for hit in hits]
        else:
            s = 'ERROR: unknown search field! "{}"'.format(search_where)
            ui_state.add_log_alert(A(s, color="danger"))
            ret = helpers_ui.A2(A(s, color="danger"))   
            status = ''
            return status, ret
        
    runtime = time.time() - t
    s = ', '.join(search_options)
    n = len([n for n in ret if not isinstance(n, dbc.Alert)])

    status = html.Div(f'{s} | Searchtext: "{search_text}" | -> {n} Docs in {runtime*1000:.1f}ms') 

    return status, ret



dash.register_page(__name__)#, path_template=f"/{__name__}/<g>",)

# dash.register_page('')

key = 'vis_search'
f = lambda k: {"type": key, "index": k}


def layout(search_where='FILE-contents', search_text= '', **kwargs):
    ret = None
    ttl = None

    try:
        search_where = urllib.parse.unquote_plus(search_where)
        search_text = urllib.parse.unquote_plus(search_text)
        top_pos = '15px' if search_text else '10%'
        sz = 'md' if search_text else 'lg'
        mw = 250 if sz == 'lg' else 150

        dis = ui_state.IS_PLAYGROUND or ui_state.IS_READONLY
        stl = {'display': 'none'} if dis else {}

        ttl = html.Div([
            # html.Center(html.H1("Search Engine")),

            html.Center(dbc.InputGroup([
                    dbc.Select(options=['ALL'] + opts, value=search_where, id=f('search-where'), style={'maxWidth': mw}),
                    dbc.Input(
                        id=f('input-1-submit'),
                        value=search_text,
                        debounce=True,
                        placeholder='SEARCH: type any search request and press enter ...',
                        style={'minWidth': 350},
                    ),
                    dbc.Button('\u2315', id=f('confirm')), #'&#x1F50E;&#xFE0E;'
                    dbc.Button('\u27F3', id=f('crawl'), color='secondary', disabled=dis, style=stl),
                    dbc.Button(u"\U0001F6C8", id=f('info'), color='info'),
                    

                    dbc.Tooltip('Run a nextcloud crawler for A.) Updates in the Nextcloud filesystem. B.) All files in the database. NOTE: This might take a while!', target=f('crawl')),
                    
                ], id=f('s_inp_g'), style={'margin': 10, 'maxWidth': '75%', 'marginTop': top_pos}, size=sz))
            ])
        
        if search_text:
            status, els = handle_search(search_where, search_text)
            log.debug('search_text "{}"'.format(search_text))
            log.debug('status "{}"'.format(status))
        else:
            status, els = None, None
        ret = html.Div([
                    html.Br(),
                    html.Div(status, id=f('output-status')),
                    html.Hr() if search_text else html.Br(),
                    html.Div(els, id=f('output-keypress'))
                ]),                


    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        s = "ERROR vis_search-layout: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))

    if ui_state.SHOW_LOADING:
        ret = dcc.Loading(ret)


    return [ttl, ret]


# @callback(
#     Output('url', 'search', allow_duplicate=True),
#     Input(f('input-1-submit'), 'value'),
#     Input(f('confirm'), 'n_clicks'),
#     Input(f('search-where'), 'value'),
#     prevent_initial_call = True
# )
# def update_output_qry(search_text, n, search_where):
#     qm = no_update
#     try:
#         if search_text:
#             qm = helpers_ui.mk_href(search_text=search_text, search_where=search_where)

#     except Exception as err:
#         log.error(err, exc_info=True)
#         s = "ERROR update_output_qry: {}".format(err)
#         ui_state.add_log_alert(A(s, color="danger"))

#     return qm


clientside_callback(
    """
    function(st, n, sw) {
        return `?search_text=${encodeURIComponent(st)}&search_where=${encodeURIComponent(sw)}`;
    }
    """,
    Output('url', 'search', allow_duplicate=True),
    Input(f('input-1-submit'), 'value'),
    Input(f('confirm'), 'n_clicks'),
    Input(f('search-where'), 'value'),
    prevent_initial_call = True,
)


@callback(
    Output(f("output-keypress"), 'children', allow_duplicate=True),
    Output(f("output-status"), 'children', allow_duplicate=True),
    Input(f('crawl'), 'n_clicks'),
    State(f('search-where'), 'value'),
    prevent_initial_call = True
)
def click_crawl_cb(n, search_where):
    res = no_update
    status = no_update

    try:
        ui_state.raise_on_readonly()

        state = ui_state.get()

        if search_where.startswith('DB'):
            t = time.time()
            assert state.db_path.endswith('.sqlite'), 'can only search on a sqlite database source!'
            fs_setup(state.db_path, do_update=True)
            elapsed = time.time() - t
            status = html.Div(f'Crawled: "{state.db_path}" | -> DONE in {elapsed:.3f}s') 

        elif search_where.startswith('FILE'):
            gname, missing, elapsed, docs, dc, ltc = crawl.crawl(force_clean=False, verb=True)
            status = html.Div(f'Crawled: "{gname}" since {ltc} | -> N={len(docs)} Docs -> N={len(dc)} files in {elapsed:.1f}s') 

            s = []

            s += ['#### LTC = LAST TIME CHECKED = LAST TIME CHANGED:\n', str(ltc)]

            if missing:
                s += ['#### Documents in DB but missing in Nextcloud:', '']
                s += [f'1. {m} | {pth}' for m, pth in missing.items()]
                s += ['', '']

            s += ['#### Updated Documents since LTC:', '']
            s += [f'1. {n} | {n.title}' for id_, (n, _)  in dc.items()]

            res = dcc.Markdown('\n'.join(s), style={'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '10px'})

        else:
            s = f'ERROR: unknown crawl field! {search_where=}'
            ui_state.add_log_alert(A(s, color="danger"))
            res = helpers_ui.A2(A(s, color="danger"))   



    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR click_crawl_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        res = helpers_ui.A2(A(s, color="danger"))

    return res, status




@callback(
    Output(f("output-keypress"), 'children', allow_duplicate=True),
    Output(f("output-status"), 'children', allow_duplicate=True),
    Input(f('info'), 'n_clicks'),
    State(f('search-where'), 'value'),
    prevent_initial_call = True
)
def click_info_cb(n, search_where):
    res = no_update
    status = no_update

    try:
        state = ui_state.get()
        if search_where.startswith('DB'):
            t = time.time()
            assert state.db_path.endswith('.sqlite'), 'can only search on a sqlite database source!'
            res = html.Pre(yaml.dump(fs_info(state.db_path)))
            elapsed = time.time() - t
            status = html.Div(f'Info for: "{state.db_path}" | -> DONE in {elapsed:.3f}s') 

        elif search_where.startswith('FILE'):
            r = elasticsearch_api.es_info()
            res = html.Pre(yaml.dump(r))
        else:
            s = 'ERROR: unknown info field!'
            ui_state.add_log_alert(A(s, color="danger"))
            res = helpers_ui.A2(A(s, color="danger"))   



    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR click_info_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        res = helpers_ui.A2(A(s, color="danger"))

    return res, status



