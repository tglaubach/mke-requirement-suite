
import math
import pandas as pd
import textwrap, urllib

import dash
from dash import html, dcc, no_update, Output, Input, State, callback, ALL, MATCH, callback_context, ctx
import dash_bootstrap_components as dbc

# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

from ReqTracker.core import helpers, graph_main, graph_base, schema

from ReqTracker.ui import helpers_ui, ui_state
from ReqTracker.ui.vis.link_list_vis import mk_link_lst


from ReqTracker.ui.vis.list_vis import mk_listvis

log = helpers.log
A = helpers_ui.A

def title(graph_name='all', reverse_graph=False):
    return f"Dash List Vis: g={graph_name}" + '|reverse' if reverse_graph else ''


def description(graph_name='all', reverse_graph=False):
    return f"Dash List Graph Style Vis: g={graph_name}" + '|reverse' if reverse_graph else ''


dash.register_page(__name__)

key = 'vis-list'

def get_list(g, reverse, i_page=None, state:ui_state.UiState=None):

    if state is None:
        state = ui_state.get()

    with ui_state.GetGraphHold() as graph:            
        graph_view = ui_state.get_graph_view(graph, g, reverse)   
        
        filtfun = lambda x: x.classname in state.classes_to_plot

        # log.debug('cls' + str(ui_state.classes_to_plot))
        if state.display_style_lst.startswith('link'):
            ret = mk_link_lst(graph_view, state.display_style_lst, filtfun=filtfun)
        else:
            ret = mk_listvis(graph_view, state.display_style_lst, filtfun=filtfun)
        gname = graph_view.name

        
        if not i_page is None:
            il = i_page % len(ret)
            iu = min(il+state.list_items_per_page, len(ret))
            ret = ret[il:iu]
            

    return ret, gname

def layout(g='all', reverse=False):


    ret = no_update
    ttl = no_update
    try:
        state = ui_state.get()
        log.debug('layout | g={} | reverse={}'.format(g, reverse))
        
        ret_lst, gname = get_list(g, reverse, state=state)

        
        ttl = dbc.InputGroup([
            dbc.InputGroupText('{}: {}'.format('LIST-VIEW', gname), id={"type": key, "index": "ttl"}),
            
            dbc.Tooltip("List Style: Select a List style for visualisation", target={"type": "par", "index": "display_style_lst"}, placement='bottom'),
            dbc.Button('\u27F3', id={"type": key, "index": "refreshgraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Refresh the Plot below", target={"type": key, "index": "refreshgraph"}, placement='bottom'),
            dbc.Button('\u21B9', id={"type": key, "index": "reversegraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Flip the graph upside down", target={"type": key, "index": "reversegraph"}, placement='bottom'),
            dbc.InputGroupText("display_style_lst:", style={'width': 140}),
            dbc.Select(options=ui_state.DISPLAY_STYLES_LST_POSSIBLE, value=state.display_style_lst, id={"type": "par", "index": "display_style_lst"}),
        ], size="sm")

        n_items = len(ret_lst)
        n_pages = n_items / state.list_items_per_page
        n_pages = math.ceil(n_pages)

        fully_expanded = n_pages < 30
        
        log.debug(f'Pagination: {n_items}, {n_pages}, {fully_expanded}')

        pg  = html.Div(dbc.Pagination(active_page=1, 
                             max_value=n_pages, 
                             first_last=True, 
                             previous_next=True, 
                             fully_expanded=fully_expanded, id={"type": key, "index": "page"},
                             style={'margin': 2, 'textAlign': 'center'}, size='sm'),
                             style={'display':'flex', 'justifyContent': 'center', 'width': '100%'})
        
        iu = min(state.list_items_per_page, len(ret_lst))
        ret_lst = ret_lst[:iu]
        ret = html.Div(ret_lst, id={'type': key, 'index': 'content'})

        log.debug(('layout --> ', gname))

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_list: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))


    mem = dcc.Store({"type": key, "index": "mem-inp"}, data=dict(g=g, reverse=reverse, gname=gname))  
    if ui_state.SHOW_LOADING:
        ret = dcc.Loading(ret)
        
    return [mem, ttl, pg, ret]

"""
 ██████ ██████      ██      ██ ███████ ████████      █████   ██████ ████████ ██  ██████  ███    ██ ███████ 
██      ██   ██     ██      ██ ██         ██        ██   ██ ██         ██    ██ ██    ██ ████   ██ ██      
██      ██████      ██      ██ ███████    ██        ███████ ██         ██    ██ ██    ██ ██ ██  ██ ███████ 
██      ██   ██     ██      ██      ██    ██        ██   ██ ██         ██    ██ ██    ██ ██  ██ ██      ██ 
 ██████ ██████      ███████ ██ ███████    ██        ██   ██  ██████    ██    ██  ██████  ██   ████ ███████ 
                                                                                                           
"""



@callback(
    Output({'type': 'bstat', 'index': MATCH}, 'value'),
    Input({'type': 'bstat', 'index': MATCH}, 'value'),
    State({'type': 'bstat', 'index': MATCH}, 'id'),
    prevent_initial_call = True,
)
def lst_update_state_cb(new_status, node_id):
    try:
        changed_id = [p['prop_id'] for p in callback_context.triggered][0]
        log.debug('modal_update_node_close_cb -> {}'.format(changed_id))

        node_id = node_id['index'].split('|')[0]
        with ui_state.GetGraphHold() as graph:
            el = graph[node_id]        
            old_status = el.status
            
            success = el.set_status(new_status)
            if success:
                ui_state.add_log_alert(A('{} | sucessfully changed status {}->{}'.format(node_id, old_status, el.status.name), color="success"))


    except Exception as err:
        log.error(err, exc_info=True)
        ui_state.add_log_alert(A("{} | ERROR update_node: {}".format(node_id, err), color="danger"))


    return el.status.name




@callback(
    Output({'type': 'baddnote', 'index': MATCH}, 'value'),
    Output({'type': 'baddnote', 'index': MATCH}, 'valid'),
    Output({'type': 'baddnote', 'index': MATCH}, 'invalid'),
    Input({'type': 'baddnote', 'index': MATCH}, 'value'),
    State({'type': 'baddnote', 'index': MATCH}, 'id'),
    prevent_initial_call = True,
)
def baddnote_cb(note_txt, node_id):
    try:
        txt = note_txt
        invalid = False
        valid = False
        changed_id = [p['prop_id'] for p in callback_context.triggered][0]
        log.debug('baddnote_cb -> {}'.format(changed_id))
        
        node_id = node_id['index'].split('|')[0]
        if note_txt:
            with ui_state.GetGraphHold() as graph:
                el = graph[node_id]
                log.debug(el)
                if (note_txt.startswith('id:') or note_txt.startswith('file:')) and schema.Document.fun_match(note_txt):
                    # not a note but a file
                    node = ui_state.make_node(graph, note_txt)
                    log.debug((el, node))
                    if node.id != el.id and not node in el.linked:
                        graph.add_edge((el.id, node.id))
                    else:
                        ui_state.add_log_alert(A('{} | could not add child because already linked or same: {}'.format(el.id, node.id), color="warning"))
                    note_txt = node.id
                    ui_state.add_log_alert(A('{} | sucessfully added file as child: {}'.format(el.id, node.id), color="success"))
                else:
                    el.add_note(note_txt)
                    ui_state.add_log_alert(A('{} | sucessfully added new note: {}'.format(node_id, graph_base.limit_len(note_txt)), color="success"))
                    note_txt = ''
                valid = True
        
    except Exception as err:
        log.error(err, exc_info=True)
        ui_state.add_log_alert(A("{} | ERROR update_node: {}".format(node_id, err), color="danger"))
        invalid = True

    return note_txt, valid, invalid




@callback(
    Output({"type": key, "index": "content"}, 'children', allow_duplicate=True),
    Output({"type": key, "index": "ttl"}, 'children', allow_duplicate=True),
    Input({"type": key, "index": "page"}, "active_page"),
    Input({"type": key, "index": "refreshgraph"}, 'n_clicks'),
    Input({"type": key, "index": "reversegraph"}, 'n_clicks'),
    Input({"type": 'par', "index": ALL}, 'value'),
    State({"type": key, "index": "mem-inp"}, 'data'),
    prevent_initial_call=True
)
def update_page(i_page, dummy, dummy2, dummy3, inps):

    fig = no_update
    ttl = no_update
    try:
        state = ui_state.get()
        reverse = 'index' in ctx.triggered_id and ctx.triggered_id['index'] == 'reversegraph'

        g = inps['g']

        log.debug('layout | g={} | reverse={}'.format(g, reverse))

        fig, gname = get_list(g, reverse, i_page=i_page, state=state)
        ttl = '{}: {}'.format('LIST-VIEW', gname)


    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        s = "ERROR vis_list-update_page: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))


    return fig, ttl

