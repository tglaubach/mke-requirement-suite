from collections import defaultdict
import math

import re
from OLD.io import woosh_api
import dash
from dash import callback, no_update, Input, Output, State, ctx, html, dcc

import dash_bootstrap_components as dbc

# add to path for testing
import sys, inspect, os

import numpy as np
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)



from ReqTracker.core import graph_base, helpers, schema, graph_main

from ReqTracker.io_files import langchain_nextcloud_io
from ReqTracker.ui import ui_state, helpers_ui, crawl

A = helpers_ui.A

log = helpers.log


import dash_treeview_antd

if __name__ == '__main__':
    app = dash.Dash('', external_stylesheets=[dbc.themes.FLATLY])

    app.scripts.config.serve_locally = True
    app.css.config.serve_locally = True


key = 'vis_tree'
f = lambda k: {"type": key, "index": k}

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "sticky",
    "top": 0,
    # "left": 0,
    "bottom": 0,
    "width": "24rem",
    "padding": "1rem",
    "backgroundColor": "#f8f9fa",
    "overflow": "scroll",
    "maxHeight": "100%"
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    # "marginLeft": "24rem",
    # "marginRight": "2rem",
    "padding": "1rem 1rem",
    "overflow": "scroll",
    "maxHeight": "100%"
}


def get_ids_from_selected(selected, mapper):
    # do it the hard way since we should perserve order
    ids = []
    for s in selected:
        if s in mapper and mapper[s] and not mapper[s] in ids: # a node
            ids.append(mapper[s])
        elif s in mapper:
            mapper[s] # not a node - must be container... find all contained
            ii = [id_ for ss, id_ in mapper.items() if s in ss and id_]
            for i in ii:
                if not i in ids:
                    ids.append(i)
    return ids

def get_items(ids, g, suffixes=None):
    with ui_state.GetGraphHold() as graph:
        gv = ui_state.get_graph_view(graph, g)
        if not suffixes:
            suffixes = ['']* len(ids)
        fun = lambda x, sx: dbc.ListGroupItem(helpers_ui.node2item(x, add_header=True, n_links_max=-1, item_suffix=sx))
        return [fun(gv[id_], suffix) for id_, suffix in zip(ids, suffixes) if id_ in gv]
    
def get_tree_from_graph(graph_view:graph_main.GraphView, disp, group_loose=True, sep_reqs=False, no_root=False, hide_reqs=False,fully_expand=False):


    fig = None
    
    try:
        state = ui_state.get()

        hide_loose_docs = state.hide_loose_docs
        classes_to_plot = state.classes_to_plot

        g = graph_view
        log.debug(f'plotting graph: {g.name}')


        if hide_loose_docs and (g.name.startswith('groupby') or g.name.startswith('reversed')):
            log.debug('PLOTTING: not hiding any loose docs because the view is a grouped view')
        elif hide_loose_docs:
            f = lambda node: not (isinstance(node, schema.Document) and not any((p.do_serialize for p in node.linked)))
            g2 = g.filt_by(g.name + '|noldocs', f, mode='keep')
            g = g2 if len(g2) > 1 else g
            log.debug('PLOTTING: Before hiding loose docs: N={} after M={}'.format(len(graph_view), len(g)))     
        else:
            log.debug('PLOTTING: Not Excluding Anything but grouping loose ones instead')


        if classes_to_plot and (g.name.startswith('groupby') or g.name.startswith('reversed')):
            log.debug('PLOTTING: not filtering by classes because the view is a grouped view')
        elif classes_to_plot and not set(classes_to_plot) == set((cls.__name__ for cls in graph_main.classes)):
            classes_to_plot = set(classes_to_plot)
            filtfun = lambda node: type(node).__name__ in classes_to_plot or type(node).__name__ == graph_main.ViewContainer.__name__
            n = len(g)
            g = g.filt_by(g.name + '|classfilt', filtfun, mode='keep')
            log.debug('PLOTTING: Before excluding by classtype: N={} after M={}'.format(n, len(g)))
        else:
            log.debug('PLOTTING: not filtering by classes because all are ON')


        if not g.name.startswith('groupby') and not g.name.startswith('reversed'):
            if group_loose:
                g = g.group_loose()
            
            if sep_reqs:
                g = g.sep_reqs()
            
            if hide_reqs:
                f = lambda node: not type(node).__name__ in (schema.VerificationRequirement.__name__, schema.Requirement.__name__)
                g = g.filt_by(g.name + '|hidereq', f, mode='keep')


        if no_root:
            g.view.delete_edges([(g.root_node.id, c.id) for c in g.root_node.children])

        fig = get_tree(g, disp, fully_expand)

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR while plotting! {}".format(err)
        alert = A(s, color='danger')
        fig = helpers_ui.A2(alert)
        ui_state.add_log_alert(alert)

    return fig


def get_tree(g:graph_main.GraphView, disp, fully_expand=False):

    maxdepth = 10000 if fully_expand else 3

    mapper = {}
    titles = {}

    expanded = []
    selected = []
    rows = []
    if disp == 'nodes':
        
        def transform_nodes(node, graph, full_id=''):
            id_ = re.sub('[\.\-,|]', '_', node.id)
            fid = f'{full_id}-{id_}'
            children = [transform_nodes(c, graph, fid) for c in graph.get_children(node)]
            
            title = '[{}] {}'.format(len(children), node.id)
            ttl = node.title
            if ttl and ttl != node.id:
                title += '  ' + helpers.limit_len(ttl, 120)

            mapper[fid] = node.id

            if len(fid.split('-')) < maxdepth:
                expanded.append(fid)
            
            titles[fid] = title
            return {
                'title': title,
                'key': fid,
                'children': children,
            }
        rows = transform_nodes(g.root_node, g, f'{g.name}->n={len(g)} items')
        # g.groupby('dish_id')

    else:
        
        docs = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]

        def helper(n, dc):
            p = n.path
            dc2 = dc
            prts = p.split('/')
            k = ''
            for i, sub in enumerate(prts, 1):
                # print(i, len(prts), k, sub, p)
                if i < len(prts):
                    if not sub in dc2:
                        dc2[sub] = {}
                    dc2 = dc2[sub]
                    k = sub
                else:
                    dc2[k] = n

        


        def transform_nodes(key, node, full_id=''):
            if isinstance(node, dict):
                id_ = re.sub('[\.\-,|]', '_', key)
                title = key
            
                fid = f'{full_id}-{id_}'
                children = [transform_nodes(k, c, fid) for k, c in node.items()]
                title = '[{}] {}'.format(len(children), title)
                if len(fid.split('-')) < maxdepth:
                    expanded.append(fid)

                mapper[fid] = None

            else:    
                id_ = re.sub('[\.\-,|]', '_', node.id)
                fid = f'{full_id}-{id_}'
            
                docs = node.get_version_dcs()
                docs.sort(key=lambda n: n['path'])
                def get_ttl(d):
                    dd = node.fun_match(d['path'])
                    v = '[VERSION "{}"]  '.format(dd['version']) if 'version' in dd else ''
                    return v + d['path']
                
                children = [dict(title=get_ttl(d), key=f'{fid}-{i}') for i, d in enumerate(docs)]
                vers = node.get_version_hist()
                v = vers[-1] if vers else ''
                title = 'V:[{}] << {} >> '.format(v, node.id)
                ttl = node.title
                if ttl and ttl != node.id:
                    title += '  ' + helpers.limit_len(ttl, 120)

                mapper[fid] = node.id
                titles[fid] = title
            return {
                'title': title,
                'key': fid,
                'children': children
            }
        
        tree = {}
        for p in docs:
            helper(p, tree)

        rows = transform_nodes(f'{g.name}->n={len(g)} items', tree)

    layout = dash_treeview_antd.TreeView(
            id=f('main'),
            multiple=True,
            checkable=False,
            selected=selected,
            expanded=expanded,
            data=rows
        )
    layout.mapper = mapper
    layout.titles = titles
    return layout


def get_content(g, disp, expand, qsearch='', select_all=False):
    with ui_state.GetGraphHold() as graph:
        gv = ui_state.get_graph_view(graph, g)

        tree = get_tree_from_graph(gv, disp, fully_expand=expand)

        if qsearch:
            pat = re.compile('[\.\-,|]', flags=re.IGNORECASE)
            sub = lambda s: pat.sub('', s)
            qs = sub(qsearch).lower()
            qsearchl = qsearch.lower()

            fun = lambda n, ttl: (qsearchl in ttl.lower()) # (qs in sub(n).lower()) 
            matches = [n for n, ttl in tree.titles.items() if fun(n, ttl)]

            tree.selected = matches
            tree.expanded += tree.selected
            tree.expanded = list(set(tree.expanded))

        elif select_all:
            allids = [n for n in tree.mapper.keys()]
            tree.selected = allids
            tree.expanded = allids

        del tree.titles

        return tree
    


def title(graph_name='all', disp='filesys',):
    return f"TreeView Style Vis: g={graph_name} | {disp}"


def description(graph_name='all', disp=False):
    return f"TreeView Style Vis: g={graph_name} | {disp}"

if not __name__ == '__main__':
    dash.register_page(__name__)


def layout(g='all', disp='filesys', **kwargs):

    ret = no_update
    ttl = no_update
    mem = no_update

    try:
        log.debug('layout | g={} | disp={}'.format(g, disp))
        kwargs = dict(g=g, disp=disp, expand=False)
        tree = get_content(**kwargs)
        if hasattr(tree, 'mapper'):
            mapper = tree.mapper
            del tree.mapper
        else:
            mapper = {}

        treev = dbc.Row(tree, id=f('content'))
        out = html.Div(id=f('output-selected'))

        if ui_state.SHOW_LOADING:
            out = dcc.Loading(out)
            treev = dcc.Loading(treev)

        sidebar = html.Span(
            [
                html.H5(f"Graph: g={g}"),
                dbc.InputGroup([
                    dbc.Button('\u27F3', id=f("refreshgraph"), n_clicks=0, size='sm'),
                    dbc.Tooltip("Refresh the tree below", target=f("refreshgraph"), placement='bottom'),
                    # '\u21F9', '\uD83D'
                    dbc.Button('E', id=f("expandall"), n_clicks=0, size='sm'),
                    dbc.Tooltip("Expand the whole tree below", target=f("expandall"), placement='bottom'),

                    dbc.Button('S', id=f("select_all"), n_clicks=0, size='sm'),
                    dbc.Tooltip("Select all", target=f("tograph"), placement='bottom'),
                    dbc.InputGroupText('Show:'),
                    dbc.Select(
                        id=f("disp_style"),
                        options="filesys nodes".split(),
                        value='filesys',
                    ),
                    dbc.Tooltip("Display Style: Select a style to use for displaying", target=f("display_style_plt"), placement='bottom'),
                ], size="sm"),
                dbc.InputGroup([
                    dbc.Input(id=f("quicksearch"), placeholder="quicksearch... (ENTER)", size='sm', debounce=True, type="text"),
                    dbc.Tooltip("Type something to search for and submit by pressing ente.", target=f("quicksearch"), placement='bottom'),
                ], size="sm"),
                treev
                # dbc.Nav(,
                #     vertical=True,
                #     pills=True,
                # ),
            ],
            style={'minWidth': '10%','maxWidth': '33%'},
        )

        ret = [dbc.Row([
                dbc.Col(sidebar, style=SIDEBAR_STYLE, width='auto', class_name="h-100  border",), #xs=4, sm=4, md=2, lg=2, xl=2, xxl=2), 
                dbc.Col(out, style=CONTENT_STYLE), #xs=8, sm=8, md=10, lg=10, xl=10, xxl=10),
            ], style={'flexGrow': '1'}),
            dbc.Row([           
                dcc.Store(f("mem-inp"), data=dict(g=g, disp=disp)), 
                dcc.Store(data=mapper, id=f('mapper')),
                dcc.Store(f("mem-get_content"), data=kwargs)
            ])
        ]


    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))

    # die ganze app steckt in einem flex container; 
    # flex richtung ist column (standard); die row mit dem hauptinhalt bekommt flew grow
    style = {'height': '85vh', 'display': 'flex', 'flexDirection': 'column'}  
    return dbc.Container(ret, style=style, fluid=True)



@callback(Output(f('content'), 'children', allow_duplicate=True),
          Output(f('mapper'), 'data', allow_duplicate=True),
          Input(f('mem-get_content'), 'data'),
          prevent_initial_call=True)
def get_content_cb(kwargs):
    try:
        log.debug(f'get_content_cb: {kwargs}')
        content = get_content(**kwargs)
        mapper = content.mapper
        del content.mapper
        return content, mapper
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR expanding_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger")), None
        return helpers_ui.A2(A(s, color="danger")), None

@callback(Output(f('content'), 'children', allow_duplicate=True),
          Output(f('mapper'), 'data', allow_duplicate=True),
          Output(f('mem-get_content'), 'data', allow_duplicate=True),
          Input(f('expandall'), 'n_clicks'),
          State(f('disp_style'), 'value'),
          State(f('mem-inp'), 'data'),
          prevent_initial_call=True)
def expandall_cb(n, disp, inps):
    try:
        kwargs = dict(g=inps['g'], disp=disp, expand=True)
        return None, None, kwargs
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR expandall_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        return helpers_ui.A2(A(s, color="danger")), None, no_update

@callback(Output(f('content'), 'children', allow_duplicate=True),
          Output(f('mapper'), 'data', allow_duplicate=True),
          Output(f('mem-get_content'), 'data', allow_duplicate=True),
          Input(f('select_all'), 'n_clicks'),
          State(f('disp_style'), 'value'),
          State(f('mem-inp'), 'data'),
          prevent_initial_call=True)
def select_all_cb(n, disp, inps):
    try:
        kwargs = dict(g=inps['g'], disp=disp, expand=True, select_all=True)
        return None, None, kwargs
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR select_all_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        return helpers_ui.A2(A(s, color="danger")), None, no_update



@callback(Output(f('content'), 'children', allow_duplicate=True),
          Output(f('mapper'), 'data', allow_duplicate=True),
          Output(f('mem-get_content'), 'data', allow_duplicate=True),
          Input(f('disp_style'), 'value'),
          State(f('mem-inp'), 'data'),
          prevent_initial_call=True)
def disp_style_cb(disp, inps):
    try:
        log.debug('disp_style_cb!')
        kwargs = dict(g=inps['g'], disp=disp, expand=False)
        return None, None, kwargs
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        return helpers_ui.A2(A(s, color="danger")), None

@callback(Output(f('content'), 'children', allow_duplicate=True),
          Output(f('mapper'), 'data', allow_duplicate=True),
          Output(f('mem-get_content'), 'data', allow_duplicate=True),

            Output(f('quicksearch'), 'valid'),
            Output(f('quicksearch'), 'invalid'),
            Input(f('quicksearch'), 'value'),
            State(f('disp_style'), 'value'),
            State(f('mem-inp'), 'data'),
            prevent_initial_call=True)
def qsearch_cb(qsearch, disp, inps):
    try:

        log.debug('qsearch_cb!')
        if qsearch:
            content = dict(g=inps['g'], disp=disp, expand=False, qsearch=qsearch)
            return None, None, content, True, False
        else:
            return no_update, no_update, no_update, False, False
        
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        return helpers_ui.A2(A(s, color="danger")), None, no_update, False, True








@callback(Output(f("pagei"), "children"),
            Input(f("page"), 'active_page'),
            State(f('main'), 'selected'),
            State(f('mapper'), 'data'),
            State(f('mem-inp'), 'data'))
def display_selected_page_cb(i, selected, mapper, inps):

    try:
        ids = get_ids_from_selected(selected, mapper)
        il = (i-1) * ui_state.TREE_SHOW_ITEMS_PERPAGE
        ih = i * ui_state.TREE_SHOW_ITEMS_PERPAGE
        ih = np.clip(ih, 0, len(ids))
        il = np.clip(il, 0, len(ids))
        suffixes = [f' {i}/{len(ids)}' for i in range(il+1, ih)]
        return get_items(ids[il:ih], inps['g'], suffixes)

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR display_selected_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = dbc.Alert(s, color="danger")
    return ret



@callback(Output(f('output-selected'), 'children'),
            Input(f('main'), 'selected'),
            State(f('mapper'), 'data'),
            State(f('mem-inp'), 'data'))
def display_selected_cb(selected, mapper, inps):

    try:
        if ui_state.TREE_SHOW_SEL_MODE == 'items':
            
            ids = get_ids_from_selected(selected, mapper)
            n_items = len(ids)

            ids = ids[:ui_state.TREE_SHOW_ITEMS_PERPAGE]
            suffixes = [f' {i}/{len(ids)}' for i, _ in enumerate(ids, 1)]

            ret_lst = get_items(ids, inps['g'], suffixes)
            
            n_pages = n_items / ui_state.TREE_SHOW_ITEMS_PERPAGE
            n_pages = math.ceil(n_pages)

            fully_expanded = n_pages < 30
            
            stl = 'none' if (not ids or n_pages <= 1 ) else 'flex'
            pg  = html.Div(dbc.Pagination(active_page=1, 
                    max_value=n_pages, 
                    first_last=True, 
                    previous_next=True, 
                    fully_expanded=fully_expanded, id=f("page"),
                    style={'margin': 2, 'textAlign': 'center'}, size='sm'),
                    style={'display':stl, 'justifyContent': 'center', 'width': '100%'})

            ret = [
                dbc.Row(html.H3(f'Selected: N = {n_items} items')),
                dbc.Row(pg),
                dbc.Row(dbc.ListGroup(ret_lst, id=f('pagei')))
                ]

        else:
            ret_lst = [helpers_ui.id2lnk_dcc(id_) for id_ in ids]
            ret = html.Div(['You have selected:'] + helpers.intersperse(ret_lst, '/'))
    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR display_selected_cb: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = dbc.Alert(s, color="danger")
    return ret


if __name__ == '__main__':
    # log.setLevel(helpers.logging.DEBUG)
    app.layout = layout()

    app.run_server(debug=True, use_reloader=True)
