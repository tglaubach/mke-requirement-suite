
import pandas as pd
import textwrap
import urllib

import dash
from dash import html, dcc, no_update, Output, Input, State, callback, ctx, MATCH, ALL, clientside_callback

# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc

from ReqTracker.core import helpers, graph_main, graph_base, schema

from ReqTracker.ui import helpers_ui, ui_state

from ReqTracker.ui.visualisation_main import update_figure
from ReqTracker.ui.vis.annotation_vis import LAYOUTS_POSSIBLE, SIZEFUNS_POSSIBLE


log = helpers.log

A = helpers_ui.A

SIZEFUNS_ALLOWED = SIZEFUNS_POSSIBLE + ['25', '15', '5']

DISPLAY_STYLES_PLOT_POSSIBLE = ['graph', 'sunburst', 'treemap', 'pie']


no_data_template = go.layout.Template()
no_data_template.layout.annotations = [
    dict(
        name="do_data watermark",
        text="NO DATA TO PLOT!",
        textangle=-30,
        opacity=0.1,
        font=dict(color="black", size=100),
        xref="paper",
        yref="paper",
        x=0.5,
        y=0.5,
        showarrow=False,
    )
]
def mk_error_template(s):
    tmp = go.layout.Template()
    tmp.layout.annotations = [
        dict(
            name=s,
            text='\n'.join(textwrap.wrap(s)),
            textangle=0,
            opacity=0.8,
            font=dict(color="red", size=20),
            xref="paper",
            yref="paper",
            x=0.5,
            y=0.5,
            showarrow=False,
        )
    ]
    return tmp






# def update_figure(g:graph_main.Graph, plotstyle, maxdepth=10, figsize=1600, network_plot_style='tree', size_fun='25', hovermode='str'):

#     log.debug('update -> {} {} {}'.format(plotstyle, g.get_len_nodes_view(), maxdepth))

#     fig = None

#     color_col = 'status'

#     # print('HNS: ', hns)
#     if g.name.startswith('search:'):
#         color_col = 'is_match'

#     if not g.get_len_nodes_view():
#         fig = px.pie(pd.DataFrame([]))
#         fig.update_layout(template=no_data_template)
#     elif plotstyle == 'graph':
#         fig = make_annotation_plot(g, lyout=network_plot_style, size_fun=size_fun, hovermode=hovermode)
#     elif plotstyle == 'sunburst':
#         fig = make_sunburst_plot(g, color_col, maxdepth=maxdepth)
#     elif plotstyle == 'treemap':
#         fig = make_treemap_plot(g, color_col, maxdepth=maxdepth)
#     elif plotstyle == 'pie':
#         data = pd.DataFrame([n.to_dict(add_parents=False) for n in g.get_nodes_view()])
#         states = data['_status'].unique()
#         dc_colors = {s.name:c for s, c in graph_base.colors.items()}
#         df = []
#         for state in states:
#             df.append({
#                 'status': state,
#                 'count': (data['_status'] == state).sum(),
#                 'color': dc_colors[state]
#             })
#         #title='Item counts by status',

#         fig = px.pie(pd.DataFrame(df), values='count', names='status', color='color')
#         fig.update_traces(textposition='inside', textinfo='label+percent')
#         fig.update_layout(height=400)
#     else:
#         raise KeyError(plotstyle)
    
#     if fig and not fig['data']:
#         fig.update_layout(template=no_data_template)
        
#     if fig and plotstyle != 'pie':
#         fig.update_layout(clickmode='event') # +select

#     if not fig:
#         return {'layout': {}, 'data': []}
#     else:
#         fig.update_layout(
#             height=figsize,
#         )
            
#         return fig


def get_plot_vis(graph_view:graph_main.GraphView, display_style_plt=None, group_loose=True, sep_reqs=True, no_root=False, hide_reqs=True, add_title=True, state=None):

    if state is None:
        state = ui_state.get()

    if display_style_plt is None or display_style_plt == 'None':
        if state.display_style_plt is None or state.display_style_plt == 'None':
            state.update(display_style_plt=ui_state.UiState().display_style_plt)
            state.push()

            # ui_state.pull_projvars('display_style')
        display_style_plt = state.display_style_plt

    maxdepth = state.maxdepth
    network_plot_style = state.network_plot_style
    figsize = state.figsize
    size_fun = state.size_fun
    hide_loose_docs = state.hide_loose_docs
    classes_to_plot = state.classes_to_plot

    fig = {'layout': {}, 'data': []}
    
    try:

        g = graph_view
        log.debug(f'plotting graph: {g.name}, {display_style_plt}')


        if hide_loose_docs and (g.name.startswith('groupby') or g.name.startswith('reversed')):
            log.debug('PLOTTING: not hiding any loose docs because the view is a grouped view')
        elif hide_loose_docs:
            f = lambda node: not (isinstance(node, schema.Document) and not any((p.do_serialize for p in node.linked)))
            g2 = g.filt_by(g.name + '|noldocs', f, mode='keep')
            g = g2 if len(g2) > 1 else g
            log.debug('PLOTTING: Before hiding loose docs: N={} after M={}'.format(len(graph_view), len(g)))     
        else:
            log.debug('PLOTTING: Not Excluding Anything but grouping loose ones instead')


        if classes_to_plot and (g.name.startswith('groupby') or g.name.startswith('reversed')):
            log.debug('PLOTTING: not filtering by classes because the view is a grouped view')
        elif classes_to_plot and not set(classes_to_plot) == set((cls.__name__ for cls in graph_main.classes)):
            classes_to_plot = set(classes_to_plot)
            filtfun = lambda node: type(node).__name__ in classes_to_plot or type(node).__name__ == graph_main.ViewContainer.__name__
            n = len(g)
            g = g.filt_by(g.name + '|classfilt', filtfun, mode='keep')
            log.debug('PLOTTING: Before excluding by classtype: N={} after M={}'.format(n, len(g)))
        else:
            log.debug('PLOTTING: not filtering by classes because all are ON')


        if not g.name.startswith('groupby') and not g.name.startswith('reversed'):

            
            if sep_reqs:
                g = g.sep_reqs()
            
            if hide_reqs:
                f = lambda node: not type(node).__name__ in (schema.VerificationRequirement.__name__, schema.Requirement.__name__)
                g = g.filt_by(g.name + '|hidereq', f, mode='keep')
            
            if group_loose and not [nn for nn in g._container_nodes if '>clust_' in nn.id]:
                g = g.group_loose()

        if no_root:
            g.view.delete_edges([(g.root_node.id, c.id) for c in g.root_node.children])

        fig = update_figure(g, display_style_plt, 
                            maxdepth=maxdepth, 
                            figsize=figsize, 
                            network_plot_style=network_plot_style, 
                            size_fun=size_fun)
        if add_title:
            fig.update_layout(title=g.name)

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR while plotting! {}".format(err)
        alert = A(s, color='danger')
        fig = px.pie(pd.DataFrame([]))
        fig.update_layout(height=200,template=mk_error_template(s))
        ui_state.add_log_alert(alert)

    return fig


def get_fig(g, reverse, display_style_plt, ids_expand=[], state:ui_state.UiState=None):

    if state is None:
        state = ui_state.get()

    with ui_state.GetGraphHold() as graph:            
        graph_view = ui_state.get_graph_view(graph, g, reverse, containers_to_expand=ids_expand, st=state)
        log.debug(f'get_fig {g}, {graph_view.name}!')

        no_root = graph_view.name.startswith('search:')
        
        gname = graph_view.name

        fig = get_plot_vis(graph_view, display_style_plt, no_root=no_root, state=state) if len(graph_view) else no_data_template
    
        fig.update_layout(
                showlegend=False,
                margin=dict(b=1,l=1,r=1,t=30),
                xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)
            )
        
    return fig, gname 


dash.register_page(__name__)#, path_template=f"/{__name__}/<g>",)

# dash.register_page('')

key = 'vis_plot'


def layout(g='all', reverse=False, display_style_plt=None, to_expand=None, **other_unknown_query_strings):
    gname = ''
    ret = no_update
    ttl = no_update

    try:
        state = ui_state.get()
        g = urllib.parse.unquote_plus(g)

        log.debug('vis_plot layout | g={} | reverse={}'.format(g, reverse))
        fig, gname = get_fig(g, reverse, display_style_plt)
        

        ttl = dbc.InputGroup([
            dbc.InputGroupText('{}: {}'.format('PLOT', gname), id={"type": key, "index": "ttl"}),
            
            dbc.Tooltip("Plotting Style: Select a style to use for plotting", target={"type": "par", "index": "display_style_plt"}, placement='bottom'),
            dbc.Button('\u27F3', id={"type": key, "index": "refreshgraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Refresh the Plot below", target={"type": key, "index": "refreshgraph"}, placement='bottom'),
            dbc.Button('\u21B9', id={"type": key, "index": "reversegraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Flip the graph upside down", target={"type": key, "index": "reversegraph"}, placement='bottom'),
            
            dbc.InputGroupText('Select Plotstyle: '),
            dbc.Select(options=DISPLAY_STYLES_PLOT_POSSIBLE, value=state.display_style_plt, id={"type": "par", "index": "display_style_plt"}, style={'width': '110px'}, size='sm'),


            dbc.InputGroupText("maxdepth", style={'width': 140}),
            dbc.Input(type="number", id={"type": "par", "index": "maxdepth"}, value=state.maxdepth),

            dbc.InputGroupText("figsize", style={'width': 140}),
            dbc.Input(type="number", value=state.figsize, id={"type": "par", "index": "figsize"}, min=0, step=100),

            dbc.InputGroupText("network_plot_style", style={'width': 140}),
            dbc.Select(options=LAYOUTS_POSSIBLE, value=state.network_plot_style, id={"type": "par", "index": "network_plot_style"}),
    
            dbc.InputGroupText("size_fun", style={'width': 140}),
            dbc.Select(options=SIZEFUNS_ALLOWED, value=state.size_fun, id={"type": "par", "index": "size_fun"}),

        ], size="m")


        
            

    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        s = "ERROR graph-layout: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        fig = mk_error_template(s)


    mem = html.Div([
        dcc.Store({"type": key, "index": "mem-inp"}, data=dict(g=g, reverse=reverse, display_style_plt=display_style_plt, gname=gname, to_expand=to_expand)),  
        dcc.Store({"type": key, "index": "mem-reload"}, data=to_expand if to_expand else []) 
        ])

    ret = dcc.Graph(figure=fig, id={"type": key, "index": "graph"}, style={'margin': '5px', 'width': '100%'})
    
    if ui_state.SHOW_LOADING:
        ret = dcc.Loading(ret)


    return [mem, ttl, ret]




@callback(
    Output("id_toshow", "data", allow_duplicate=True),
    Output({"type": key, "index": "mem-reload"}, 'data', allow_duplicate=True),
    Input({"type": key, "index": "graph"}, 'clickData'), 
    State({"type": key, "index": "mem-inp"}, 'data'),
    State({"type": key, "index": "mem-reload"}, 'data'),
    prevent_initial_call=True
)
def graph_click_cb(clickData, inps, ids_expand_in):
    try:
        node_id = no_update
        reload = no_update

        if clickData and 'points' in clickData and len(clickData) > 0:
            log.debug(('clickdata!', clickData))
            full_id = clickData['points'][0]['id']
            id_ = full_id.split('|')[-1].strip()
            
            if '>clust' in id_:
                to_expand = id_.split('>clust')[0]
                ids_expand = ids_expand_in if ids_expand_in else []
                if not to_expand in ids_expand:
                    ids_expand.append(to_expand)
                    log.debug('reloading and uncollapsing: ' + to_expand)
                    reload = ids_expand
            else:
                log.debug('id_ --> {}'.format(id_))
                node_id = {'id': id_, 'gname': inps['g']}
            
    except Exception as err:
        ui_state.add_log_alert(A('graph_click_cb --> ERROR: {}'.format(err), color='danger'))
        log.error(err, exc_info=True)

    return node_id, reload

clientside_callback(
    """
    function(ids_expand, other) {
        return [];
    }
    """,
    Output({"type": key, "index": "mem-reload"}, 'data', allow_duplicate=True),
    Input({"type": key, "index": "refreshgraph"}, 'n_clicks'),
    Input({"type": "par", "index": ALL}, 'value'),
    prevent_initial_call = True,
)




@callback(
    Output({"type": key, "index": "graph"}, 'figure', allow_duplicate=True),
    Output({"type": key, "index": "ttl"}, 'children', allow_duplicate=True),
    Input({"type": key, "index": "reversegraph"}, 'n_clicks'),
    Input({"type": 'par', "index": ALL}, 'value'),
    Input({"type": key, "index": 'mem-reload'}, 'data'),
    State({"type": key, "index": "mem-inp"}, 'data'),
    prevent_initial_call=True
)
def update_page(dummy, dummy2, ids_expand, inps):

    fig = no_update
    ttl = no_update
    try:
        state = ui_state.get()
        reverse = 'index' in ctx.triggered_id and ctx.triggered_id['index'] == 'reversegraph'
        
        if 'index' in ctx.triggered_id and ctx.triggered_id['index'] == 'refreshgraph':         
            ids_expand = []

        display_style_plt = inps['display_style_plt']
        g = inps['g']

        log.debug('layout | g={} | reverse={}'.format(g, reverse))

        fig, gname = get_fig(g, reverse, display_style_plt, ids_expand, state)
        ttl = '{}: {}'.format('PLOT', gname)


    except Exception as err:
        log.exception(err)
        log.error(err, exc_info=True)
        s = "ERROR vis_plot-update_page: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        fig = mk_error_template(s)

    return fig, ttl

