import dash
from dash import html, dcc, no_update, Output, Input, callback, ALL, State, ctx
import dash_bootstrap_components as dbc

# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

from ReqTracker.core import helpers, graph_main, graph_base, schema

from ReqTracker.ui import helpers_ui, ui_state


from ReqTracker.ui.vis.table_vis import mk_datatable_lst


log = helpers.log

A = helpers_ui.A

def title(graph_name='all', reverse_graph=False):
    return f"DashTable Style Vis: g={graph_name}" + '|reverse' if reverse_graph else ''


def description(graph_name='all', reverse_graph=False):
    return f"DashTable Graph Style Vis: g={graph_name}" + '|reverse' if reverse_graph else ''

key = 'vis_table'

def get_tbl(g, reverse):
    with ui_state.GetGraphHold() as graph:            
        graph_view = ui_state.get_graph_view(graph, g, reverse)   
        ret = mk_datatable_lst(graph_view, id_={'type': key, 'index': 'table'})
        gname = graph_view.name
    ret = html.Div(ret, id={'type': key, 'index': 'table-container'})
    return ret, gname


dash.register_page(__name__)


def layout(g='all', reverse=False, **kwargs):
    gname = ''
    
    ret = no_update
    try:
        log.debug('layout | g={} | reverse={}'.format(g, reverse))

        ret, gname = get_tbl(g, reverse)


        ttl = dbc.InputGroup([
            dbc.InputGroupText('{}: {}'.format('TABLE', gname), id={"type": key, "index": "ttl"}),
            
            dbc.Tooltip("Plotting Style: Select a style to use for plotting", target={"type": "par", "index": "display_style_plt"}, placement='bottom'),
            dbc.Button('\u27F3', id={"type": key, "index": "refreshgraph"}, n_clicks=0, size='sm'),
            dbc.Tooltip("Refresh the Plot below", target={"type": key, "index": "refreshgraph"}, placement='bottom'),


        ], size="m")


    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))

    mem = dcc.Store({"type": key, "index": "mem-inp"}, data=dict(g=g, reverse=reverse, gname=gname))  
    
    if ui_state.SHOW_LOADING:
        ret = dcc.Loading(ret)

    return [mem, ttl, ret]





@callback(Output('id_toshow', 'data', allow_duplicate=True), 
          Input({'type': 'tbl-docs', 'index': ALL}, 'active_cell'),          
          Input({'type': 'tbl-docs', 'index': ALL}, 'derived_virtual_data'),
          prevent_initial_call=True)
def table_click_cb(active_cell, data):

    try:
        id_ = ''
        log.info('table callback for :' + str(active_cell))
        
        if active_cell and active_cell[0]:
            # log.debug(data)
            active_cell = active_cell[0]
            log.debug(active_cell)
            iid = active_cell['row']
            if active_cell['column_id'] != 'version':
                row = data[0][iid]
                if row['node_id'].startswith('<a'):
                    id_ = helpers.idfromlink(row['node_id'])
                else:
                    id_ = row['node_id']
            log.debug(id_)

        if id_:
            return id_
    except Exception as err:
        
        log.error(err, exc_info=True)
        ui_state.add_log_alert(A('table_click_cb --> ERROR: {}'.format(err), color='danger'))

    return no_update



@callback(
    Output({"type": key, "index": "table-container"}, 'children', allow_duplicate=True),
    Output({"type": key, "index": "ttl"}, 'children', allow_duplicate=True),
    Input({"type": key, "index": "refreshgraph"}, 'n_clicks'),
    Input({"type": 'par', "index": ALL}, 'value'),
    State({"type": key, "index": "mem-inp"}, 'data'),
    prevent_initial_call=True
)
def update_page(dummy, dummy2, inps):

    ret = no_update
    ttl = no_update
    try:

        reverse = 'index' in ctx.triggered_id and ctx.triggered_id['index'] == 'reversegraph'

        g = inps['g']

        log.debug('layout | g={} | reverse={}'.format(g, reverse))

        ret, gname = get_tbl(g, reverse)
        ttl = '{}: {}'.format('TABLE: ', gname)


    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(A(s, color="danger"))
        ret = helpers_ui.A2(A(s, color="danger"))

    return ret, ttl


