import pandas as pd
import numpy as np

import dash
from dash import html, no_update, Output, State, Input, callback, dcc, dash_table
import yaml

from ReqTracker.core import graph_base, graph_main, helpers, schema

import dash_bootstrap_components as dbc
from ReqTracker.ui import helpers_ui, ui_state


log = helpers.log

def title(*args, **kwargs):
    return f"Req-Tracker Default Page"


def description(*args, **kwargs):
    return f"Req-Tracker Default Page (or 404)"



dash.register_page(__name__)


def layout(*args, **kwargs):

    ret = html.Div([
        dcc.Store(id={'type': 'def', 'index': 'mem_tlast_render'}, data=helpers.nowiso()),
        html.H2("DEFAULT PAGE (OR 404 - NotFound)", id={'type': 'def', 'index': 'onrender'}),
        html.P('You are either on the start page, or arrived at a page which could not be loaded.'),
        html.H3("INFO"),
        html.Div(id={'type': 'def', 'index': 'infoc'}),
    ], id={'type': 'def', 'index': 'main'}, style = {'margin':'5px'})
    return ret


@callback(
    Output({'type': 'def', 'index': 'infoc'}, "children"),
    Input({'type': 'def', 'index': 'onrender'}, "children"),
)
def on_render(dummy):
    ret = no_update

    try:
        log.info('default_view: ON_RENDER triggered. Reloading project vars!')

        with ui_state.GetGraphHold() as g:
            ret = get_log_changes_card(g)

        ui_state.setv(t_render_last=helpers.nowiso())

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR show_table: {}".format(err)
        ui_state.add_log_alert(helpers_ui.A(s, color="danger"))
        ret = helpers_ui.A2(helpers_ui.A(s, color="danger"))
        
    return ret

def get_log_changes_card(g:graph_main.Graph):
    st = {'margin': '15px', 'width': '90%', 'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '12px'}
    ret = []
    state = ui_state.get()

    tnow = helpers.make_zulustr(helpers.get_utcnow())
    ll = [tpl for tpl in g._changelog[-200:] if len(tpl) == 2 and helpers.make_zulustr(tpl[0]) > state.t_render_last]
    is_truncated = len(ll) >= 200

    ret.append(html.P(['LAST TIME THIS PAGE WAS RENDERED: ', html.B(state.t_render_last)]))



    ret.append(html.H4("New Documents since last render"))
    lines = []
    fun = lambda x: helpers.parse_zulutime(x) if isinstance(x, str) else x
    docs = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]
    ltcs = np.array([fun(n.last_time_changed) for n in docs])

    now = helpers.get_utcnow()

    def fun(n:schema.BaseVersionedDocument):
        delta = now - helpers.parse_zulutime(n.last_time_changed)
        delta = '{0} ago'.format(delta)
        row = [n.id, n.title, delta, n.version]
        width = [40, 55, 30, 15]
        return ' | '.join([r.ljust(w) for r, w in zip(row, width)])
    
    tmp = []
    tl = helpers.parse_zulutime(state.t_render_last)
    if not np.any(ltcs[ltcs != None] > tl):
        lines += ['<< NO NEW DOCUMENTS >> (Here are the last 10 documents instead):', '']
        idx = np.argsort(ltcs)[::-1][:10]
        tmp += [fun(docs[i]) for i in idx]
    else:
        idx = np.argsort(ltcs)[::-1]
        tmp += [fun(docs[i]) for i in idx if ltcs[i] > state.t_render_last]

    lines += [' | '.join(['ID'.ljust(40), 'TITLE'.ljust(55), 'LAST_TIME_CHANGED'.ljust(30), 'VERSION'.ljust(15)])]
    lines += [' | '.join(['-'*40, '-'*55, '-'*30, '-'*15])]
    lines += tmp
    ret.append(html.Pre('\n'.join(lines), style=st))
    ret.append(html.Hr())


    ret.append(html.H4("Changes since last render for Graph: "))
    lines = []
    lines += ["Graph: {}, last N={} changes shown (truncated?: {})".format(g, len(lines), is_truncated)]
    if not ll:
        lines += ['<< NO CHANGES FOUND >>']
    else:
        lines += [('LAST RENDER = '.rjust(15, '-') + state.t_render_last)]
        for t, txt in ll:
            t = helpers.make_zulustr(t)
            txt = helpers.limit_len(txt.replace('\n', '').replace('\r', ''), 100)
            lines.append(t + ' | ' + txt)
        lines += [('NOW = '.rjust(15, '-') + tnow)]
    ret.append(html.Pre('\n'.join(lines), style=st))
    ret.append(html.Hr())


    ret.append(html.H4('Graph Value Counts:'))
    lines = []

    ser = pd.Series({n.id:(f'{n.classname}' + (f'({n.key})' if hasattr(n, 'key') else '')) for n in g.nodes})
    vc = ser.value_counts()
    vcr = (vc / len(ser) * 100).round(1)
    vct = (vc *0 + len(ser))
    df = pd.DataFrame({'count': vc, 'count-total': vct, '% of total': vcr})
    lines += str(df).split('\n')

    ret.append(html.Pre('\n'.join(lines), style=st))
    ret.append(html.Hr())


    ret.append(html.H4('Global Params:'))
    lines = []

    lines += yaml.dump(state.to_dict()).split('\n')

    ret.append(html.Pre('\n'.join(lines), style=st))
    ret.append(html.Hr())

    return ret
    # children = [
    #     dbc.CardHeader(),
    #     dbc.CardBody(),
    # ]

    # return children

