import textwrap
import urllib
import re
import uuid
import os

from dash import Dash, dcc, html, no_update
import dash_bootstrap_components as dbc
import plotly.graph_objects as go

import ReqTracker
from ReqTracker.core import helpers, graph_main, graph_base, schema


getuuid = lambda : str(uuid.uuid4())[:8]


# id2lnk = lambda id_: html.A(id_, href="#", id={"type": "dlg_open", "index": id_ + '|' + getuuid()})
# id2lnk_B = lambda id_: html.A(html.B(id_), href="#", id={"type": "dlg_open", "index": id_ + '|' + getuuid()}, style={"color": "black"})
# id2lnk_hB = lambda id_: html.A(html.B(id_), href=f"/getid/{urllib.parse.quote_plus(id_)}", target="_self", style={"color": "black"}, disable_n_clicks=True)
# id2lnk_h = lambda id_: html.A(id_, href=f"/getid/{urllib.parse.quote_plus(id_)}", target="_self", disable_n_clicks=True)
# id2lnk_dcc = lambda id_: dcc.Link(id_, href=f"/getid/{urllib.parse.quote_plus(id_)}", target="_self")

id2lnk_dcc = lambda id_: dcc.Link(id_, href=f"/vis-editnodes?node_id={urllib.parse.quote_plus(id_)}", target="_self")
id2lnk_Bdcc = lambda id_: dcc.Link(html.B(id_), href=f"/vis-editnodes?node_id={urllib.parse.quote_plus(id_)}", target="_self")

# id2lnk_md = lambda id_: f'[{id_}](/getid/{id_})'
# id2lnk_html = lambda id_: f'<a href="/getid/{urllib.parse.quote_plus(id_)}" target="_self">{id_}</a>'

def mk_href(*args, **kwargs):
    s = '/'.join((urllib.parse.quote_plus(arg) for arg in args))
    if kwargs:
        s += '?' + urllib.parse.urlencode(kwargs)
    return s

    

def lnkhtml2id(url):
    id_ = ''
    url = urllib.parse.unquote_plus(url)
    if url.startswith('/getid/'):
        id_ = url[len('/getid/'):]
    return id_

def idfromlink(s):
    id_ = ''
    for m in re.finditer(r">([^<]*)<\/a>", s):
        id_ = m.group()[1:-4]
        break
    return id_


def A(txt, **kwargs):
    ts = helpers.make_zulustr(helpers.get_utcnow()).replace('T', ' ')
    if txt and txt.strip():
        err_txt = f'{ts} : {txt.strip()}'
    else:
        err_txt = ''

    if 'dismissable' not in kwargs:
        kwargs['dismissable'] = True
    return (err_txt, kwargs)


def A2(tpl):
    err_txt, kwargs = tpl
    return dbc.Alert(err_txt, **kwargs)


def on_log_change(logdata:list, oldlog:list):

    trigger_on = ['danger', 'warning']

    if oldlog is None:
        oldlog = []
    
    alert_txt = no_update
    alert_count = {}
    trigger_popup = False

    while logdata:
        el = logdata.pop()
        if not el is None and len(el) == 2:
            txt, kwargs = el
            if 'color' in kwargs and kwargs['color'] in trigger_on:
                trigger_popup = True
                
        else:
            txt = str(el)
            kwargs = {'color': 'secondary'}

        if txt and txt.strip() and not helpers.parse_zulutime(txt.strip()) is not None:
            kwargs['dismissable'] = True
            if 'color' in kwargs:
                if not kwargs['color'] in alert_count:
                    alert_count[kwargs['color']] = 0
                alert_count[kwargs['color']] += 1
            else:
                if not 'info' in alert_count:
                    alert_count[kwargs['color']] = 0
                alert_count[kwargs['info']] += 1
            oldlog.insert(0, dbc.Alert(txt.strip(), **kwargs))

    while len(oldlog) > 50:
        oldlog.pop()


    

    if alert_count:
        n = 0
        col = 'info'
        for k, v in alert_count.items():
            if k == 'warning' and col != 'danger':
                col = 'warning'
            elif k == 'danger':
                col = 'danger'
            n += v

        n = "99+" if n > 99 else str(n)
        alert_txt = html.Div([
                'log',
                dbc.Badge(
                    n,
                    color=col,
                    pill=True,
                    text_color="white",
                    className="position-absolute top-0 start-100 translate-middle",
                    style={'display': 'none' if n == 0 else 'block'}
                )
            ])
    else:
        alert_txt = 'log'

    is_open = True if trigger_popup else no_update

    return oldlog, is_open, alert_txt


def get_log_sys_modal():
    tnow = helpers.make_zulustr(helpers.get_utcnow())
    lines = helpers.fifo_log.lines[:]
    lines.insert(0, '----- NOW =' + tnow + '-'*90)

    is_truncated = helpers.fifo_log.is_truncated

    sti = {'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '10px', 'marginTop': 0, 'marginBottom': 0, 'lineHeight': '12px'}
    els = []
    fun = lambda x: '\n   '.join(textwrap.wrap(x, 150))
    

    for el in lines:
        fel = fun(el)
        if 'error' in el.lower():
            n = html.Pre(fel, style={**sti, **{'color': "#fc0303"}})
        elif 'warning' in el.lower():
            n = html.Pre(fel, style={**sti, **{'color': '#FF5F15'}})
        elif '[ DEBUG ' in el:
            n = html.Pre(fel, style={**sti, **{'color': '#808080'}})
        else:
            n = html.Pre(fel, style={**sti, **{'color': '#000000'}})
        els.append(n)

    st = {'margin': '15px', "minHeight": '100px', "maxHeight": '500px', 'overflow':'scroll', 'width': '100%'}

    children = [
        dbc.ModalHeader("Syslog: N={} lines (truncated?: {})".format(len(lines), is_truncated)),
        dbc.ModalBody(html.Div(els, style=st)),
    ]

    return children

def get_log_changes_modal(g:graph_main.Graph):
    tsave = helpers.make_zulustr(g._t_constructed)
    tnow = helpers.make_zulustr(helpers.get_utcnow())
    ll = [tpl for tpl in g._changelog[-200:] if len(tpl) == 2]
    is_truncated = len(ll) >= 200

    lines = []
    found = False
    for t, txt in ll:
        t = helpers.make_zulustr(t)
        if t > tsave and not found:
            lines += ['----- LAST SAVE =' + tsave + '-'*90]
            found = True
        txt = helpers.limit_len(txt.replace('\n', '').replace('\r', ''), 100)
        lines.append(t + ' | ' + txt)
        
    if not found:
        lines += ['----- LAST SAVE =' + tsave + '-'*90]
    lines += ['-----       NOW =' + tnow + '-'*90]
    s = '\n'.join(lines)
    st = {'margin': '15px', "minHeight": '100px', "maxHeight": '500px', 'overflow':'scroll', 'width': '100%', 'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '10px'}

    children = [
        dbc.ModalHeader("Changes for: {}, N={} lines (truncated?: {})".format(g, len(lines), is_truncated)),
        dbc.ModalBody(html.Pre(s, style=st)),
    ]

    return children


def get_sys_info():
    try:
        import platform
        from psutil import virtual_memory
        import getpass
        import datetime
        import time

        username = getpass.getuser()
        now = datetime.datetime.now()
        mem = virtual_memory()
        uname = platform.uname()

        txt = ''
        txt += '\n' + ("="*40 + "System Information" + "="*40)
        txt += '\n' + ("")
        txt += '\n' + (f"User:      {username}")
        txt += '\n' + (f"System:    {uname.system}")
        txt += '\n' + (f"Node Name: {uname.node}")
        txt += '\n' + (f"Release:   {uname.release}")
        txt += '\n' + (f"Version:   {uname.version}")
        txt += '\n' + (f"Machine:   {uname.machine}")
        txt += '\n' + (f"Processor: {uname.processor}")
        txt += '\n' + ("RAM:       {:.1f} GB".format(mem.total / (1024 * 1024 * 1024)))
        txt += '\n\n'
        txt += '\n' + ("Current date and time : " + now.strftime("%Y-%m-%d %H:%M:%S"))
        txt += '\n' + ("                  ISO : " + datetime.datetime.now().astimezone().replace(microsecond=0).isoformat())
        txt += '\n' + ('')
        txt += '\n' + ("="*100)
        return txt
    
    except Exception as err:
        return 'Error while trying to print system info. Error: "{}"'.format(err)



def get_sysinfo(g, t_started, nc_username, t_last_nc_sync, n_woosh):

    if not isinstance(t_started, str):
        t_started = helpers.make_zulustr(t_started)
    t_last_nc_sync
    if not t_last_nc_sync is None and not isinstance(t_last_nc_sync, str):
        t_last_nc_sync = helpers.make_zulustr(t_last_nc_sync)
    
    version = ReqTracker.__version__ if hasattr(ReqTracker, '__version__') else '<dirty-wip>'

    txt = ''
    
    txt += ("="*40 + "MeerKAT+ Requirement Tracking Suite" + "="*40)
    txt += '\n   t-now:                    {}'.format(helpers.make_zulustr(helpers.get_utcnow()))
    txt += '\n   Running as Flask/Dash Server'
    txt += '\n   Version:                  {} v{}'.format(ReqTracker.__name__, version)
    txt += '\n   System Running Since:     {}'.format(t_started)
    txt += '\n   Nextcloud User Connected: {}'.format(nc_username)
    txt += '\n   Last Sync to Nextcloud:   {}'.format(t_last_nc_sync)
    txt += '\n' + ("="*(40 + len("MeerKAT+ Requirement Tracking Suite")) + "="*40)


    ldict = 'IS_REQTRACK_SERVER IS_PRODUCTIVE IS_SUBSCRIBER IS_PLAYGROUND NC_PUSH_PATH IS_READONLY'.split()
    txt += '\n\nENVIRONMENTAL VARIABLES:'
    for k in ldict:
        v = os.environ.get(k, '<<< NOT FOUND IN ENVS >>>')
        txt += f'\n   {k}="{v}"'
    txt += '\n'

    txt += '\n'
    if n_woosh >= 0:
        txt += '\n---- WOOSH Search Engine: ' + ' ' + '-'*90
        txt += '\n' + ('N-Entries in index Table: {:_.0f}'.format(n_woosh)) +  '\n\n'
        txt += '\n' + '=' * 100

    k = g.name
    txt += '\n---- graph: ' + str(k) + ' ' + '-'*90
    txt += '\n' + g.get_info()

    
    txt += '\n' + '=' * 100
    txt += '\n\n'

    txt += get_sys_info()


    st = {'margin': '15px', "minHeight": '100px', "maxHeight": '500px', 'overflow':'scroll', 'width': '100%', 'fontFamily': "Lucida Console, Courier New, monospace", 'fontSize': '11px'}

    children = [
        dbc.ModalHeader("System Info: "),
        dbc.ModalBody(html.Pre(txt, style=st)),
    ]

    return children

def node2md(node, keywords=None, no_text_header=False):
    keywords = [] if not keywords else keywords
    text_md = []
    if issubclass(type(node), schema.BaseVersionedDocument):
        text = node.get_text(short=True)
    else:
        text = node.text

    if text:
        if not no_text_header:
            text_md.append('#### TEXT:\n\n')
        text_md += [text]

    if node.notes:
        if not no_text_header:
            text_md.append('#### NOTES:\n\n')
        text_md += ['**{}**: {}'.format(k, v) for k, v in node.notes.items()]

    for kw in keywords:
        text_md = re.sub(kw, f'** {kw} **', text_md, flags=re.IGNORECASE)

    text_md.append('#### PARENTS:\n\n')
    text_md += [' | '.join([e.id for e in node.parents])]
    text_md.append('\n\n#### CHILDREN:\n\n')
    text_md += [' | '.join([e.id for e in node.children])]



    return '\n'.join(text_md)


# id2lnk_dcc = lambda id_: dcc.Link(id_, href=f"/vis-editnodes?node_id={urllib.parse.quote_plus(id_)}", target="_self")
def node2title(node, item_suffix='', prefix=None):
    
    ttl = [prefix] if prefix else []
    
    ttl += [html.B('ITEM{}: '.format(item_suffix), style={'minWidth': 45}), id2lnk_dcc(node.id) ]
    ttl.append(' | {} ({})'.format(node.title, node.classname))
    return html.H5(ttl)

def node2item(node, keywords=None, add_header=False, n_links_max = 20, item_suffix='', no_text_header=False):
    keywords = [] if not keywords else keywords

    comps = []

    if add_header:
        comps.append(node2title(node, item_suffix=item_suffix))

    
    links_parents = []
    links_parents = ["Parents: "] + helpers.intersperse([id2lnk_dcc(e.id) for e in node.parents], '/')
    if n_links_max > 0 and len(links_parents) > n_links_max:
        links_parents, links_parents_raw = [], links_parents
        for sublist in helpers.splitw(links_parents_raw, n_links_max):
            links_parents.append(dbc.Label(html.Div(sublist)))
    else:
        links_parents = [dbc.Label(html.Div(links_parents))]

    links_children = []
    links_children = ["Children: "] + helpers.intersperse([id2lnk_dcc(e.id) for e in node.children], '/')
    if n_links_max > 0 and len(links_children) > n_links_max:
        links_children, links_children_raw = [], links_children
        for sublist in helpers.splitw(links_children_raw, n_links_max):
            links_children.append(dbc.Label(html.Div(sublist)))
    else:
        links_children = [dbc.Label(html.Div(links_children))]

    tags = [dbc.Badge(t, color='Secondary', pill=True, className="me-1") for t in node.tags]
    status = dbc.Badge(node.status.name, color=graph_base.colors[node.status])

    comps += [
        html.Div([html.Div([
                html.B('STATUS: ', style={'minWidth': 45}),
                status,  
            ], className="mb-1"),
            html.Div([
                html.B('TAGS: ', style={'minWidth': 45}),
                *tags,  
            ], className="mb-1"),
        ], className="d-flex w-100 justify-content-between")]
            


    text_md = []
    if issubclass(type(node), schema.BaseVersionedDocument):
        text = node.get_text(short=True)
    else:
        text = node.text

    if text:
        if not no_text_header:
            text_md.append('#### TEXT:\n\n')
        text_md += [text]

    if node.notes:
        if not no_text_header:
            text_md.append('#### NOTES:\n\n')
        text_md += ['**{}**: {}'.format(k, v) for k, v in node.notes.items()]
    

    text_md = '\n'.join(text_md)

    for kw in keywords:
        text_md = re.sub(kw, f'<mark>{kw}</mark>', text_md, flags=re.IGNORECASE)

    comps += [html.Div(dcc.Markdown(text_md, dangerously_allow_html=True, style={"border":"1px", "borderStyle":"solid", "borderRadius": "5px", "padding": "1em"})), 
            html.Div(links_parents),
            html.Div(links_children)
            ]

    return comps


def hitnode2item(hit, g):

    ttl = [ id2lnk_dcc(hit['id'])]

    if hit['id'] in g:
        node = g[hit['id']]
        ttl.append(' | {} ({})'.format(node.title, node.classname))
    else:
        node = None

    header = html.Div(
                [
                    html.Div([
                        html.B('ITEM: ', style={'minWidth': 45}),
                        *ttl
                    ], className="mb-1"),

                    html.Small(html.Div([
                        html.B('SCORE: ', style={'minWidth': 45}),
                        "{:.3f}".format(hit['score'])                     
                    ])),
                ],
                className="d-flex w-100 justify-content-between",
            )
    
    comps = []
    
    if node is None:
        id_ = hit['id']
        comps += [
            html.Div([
                html.B('SNIPPET: ', style={'minWidth': 45}),
                    dcc.Markdown(hit['snippet']),  
                ], className="mb-1"),
                dbc.Alert(f"the node with ID={id_} could not be found in the graph g{g}..", color="warning"),
        ]        
    else:
        #, style={            "maxHeight": "300px",             "overflow": "auto"}
        comps += [html.Div(node2item(node, keywords=hit['keywords'], add_header=False, no_text_header=True))]
        
        

    body = [header, html.Div(comps, style={"border":"3px", "borderStyle":"solid", "borderRadius":"3px", "padding": "1em"}), html.Hr()]
    return dbc.ListGroupItem(body, style={"marginLeft": 10, "marginRight": 10})






def doc2item(hit, tempstore, lnk):
    
    # log.debug(hit)

    if 'link' in hit and hit['link']:
        ttl = html.A(hit['id'] + '-' + hit['title'], target='_blank', href=hit['link'],  rel="noopener noreferrer")
    else:
        ttl = hit['id'] + '-' + hit['title']
    
    pi = hit['source']
    lnk = html.A(pi, target='_blank', href=lnk,  rel="noopener noreferrer")


    comps = [
            html.Div(
                [
                    html.Div([
                        html.B('ITEM: ', style={'minWidth': 45}),
                        ttl,  
                    ], className="mb-1"),

                    html.Small(html.Div([
                        html.B('SCORE: ', style={'minWidth': 45}),
                        "{:.3f}".format(hit['score'])                     
                    ])),
                ],
                className="d-flex w-100 justify-content-between",
            ),


            html.Small([
                html.Div([
                    html.B('SOURCE: ', style={'minWidth': 45}),
                    lnk,
                ]),
                html.Div([
                    html.B('PAGE: ', style={'minWidth': 45}),
                    '{}/{}'.format(hit['page'], hit['n_pages']),  
                ], className="mb-1"),
            ], className="d-flex w-100 justify-content-between"),
        ]


    if hit['highlights'] not in tempstore:
        tempstore.add(hit['highlights'])

        el = decode_text(hit['highlights'])
        # el = html.Pre(html.Code(hit['highlights']), style={"border":"1px", "borderStyle":"solid", "borderRadius": "5px", "padding": "1em"})

        comps += [
            html.Div(el), #style={
            #     "maxHeight": "300px",
            #     "overflow": "auto"}), 
            html.Hr()]
    else:
        comps += [html.Div(f"content already shown in search results")]
    return dbc.ListGroupItem(comps, style={"marginLeft": 10, "marginRight": 10})



def intersperse(lst, item):
    result = [item] * (len(lst) * 2 - 1)
    result[0::2] = lst
    return result

def split_mark(s, el):
    tmp = re.split(f"<mark>{el}</mark>", s, flags=re.IGNORECASE)
    return intersperse(tmp, html.Mark(el))

def decode_text(txt):

    def helpr(l):
        return '\n'.join(textwrap.wrap(re.sub(r"  ", " ", l.strip()), 200))
    txt = re.sub(r'[^\x00-\x7f]',r'', txt) # exclude non ascii
           
    lines = [l.strip() for l in txt.split('\n')]
    if len([l for l in lines if not l]) > 3 or len(lines) > 20:
        lines = [l if l else '\n' for l in lines]
        tmp = ' '.join(lines)
        tmp = '\n'.join([helpr(l) for l in tmp.split('\n')])
        while '\n\n\n' in tmp:
            tmp = tmp.replace('\n\n\n', '\n')
        txt = tmp

    els = set(list(re.findall(r"<mark>(.*?)</mark>", txt)))
    parts = [txt]

    for el in els:
        parts = [split_mark(s, el) if isinstance(s, str) else s for s in parts]
        parts = [item for sublist in parts for item in sublist]

    el = [html.Code(parts)]

    return html.Pre(el, style={"border":"3px", "borderStyle":"solid", "borderRadius":"3px", "margin":2, "padding": "1em"})