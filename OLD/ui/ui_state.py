import json, os, shutil, datetime, time, hashlib
from dataclasses import dataclass, field, asdict
from typing import List, Dict, Set
import numpy as np

from ReqTracker.core import helpers, graph_base, graph_main, schema, node_factory
from ReqTracker.io import graphdb_io

from ReqTracker.ui.helpers_ui import A, A2, html, dcc, mk_href, dbc
from ReqTracker.io_files import elasticsearch_api, nextcloud_interface
log = helpers.log


import sys, inspect, os


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))

print(parent_dir + '\n\n')

if __name__ == '__main__':
    sys.path.insert(0, parent_dir)


"""
██████  ███████ ███████ ██ ███    ██ ███████ ███████ 
██   ██ ██      ██      ██ ████   ██ ██      ██      
██   ██ █████   █████   ██ ██ ██  ██ █████   ███████ 
██   ██ ██      ██      ██ ██  ██ ██ ██           ██ 
██████  ███████ ██      ██ ██   ████ ███████ ███████ 
                         """                            

map_str2bool = lambda s: True if s and s != "0" else False


IS_REQTRACK_SERVER = os.path.basename(sys.argv[0]) in ['ui_server.py', 'ui_reqserver.py'] or sys.argv[0].endswith('flask\\__main__.py') or  sys.argv[0].endswith('flask/__main__.py')
IS_PRODUCTIVE = map_str2bool(os.environ.get('IS_PRODUCTIVE', ''))
IS_SUBSCRIBER = map_str2bool(os.environ.get('IS_SUBSCRIBER', ''))
IS_PLAYGROUND = map_str2bool(os.environ.get('IS_PLAYGROUND', ''))
NC_PUSH_PATH = os.environ.get('NC_PUSH_PATH', '').strip('/')
NC_PUSH_ASYNC = os.environ.get('NC_PUSH_ASYNC', '')
IS_READONLY = os.environ.get('IS_READONLY', '')

DISABLE_DANGERZONE = map_str2bool(IS_PRODUCTIVE)

EXAMPLE_DB_FILE_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'assets', 'example_db.sqlite')

MAKE_ADDITIONAL_SINKS_ASYNC = True if IS_SUBSCRIBER else False

DEFAULT_FIGSIZE = 1600
DEFAULT_TABID = 'All'
DEFAULT_SEARCHMODE = 'contains'
DEFAULT_FILTMODE = 'lineage'

TABIDS_DEFAULT = ['bydesign', 'all', 'bytags', 'byclassname']
SEARCHMODES_POSSIBLE = ['contains', 'fuzzy']
FILTMODES_POSSIBLE = ['auto', 'keep', 'lineage', 'flat', 'nn_5', 'nn_10']
TAGS_DEFAULT = ['MONITOR', 'LMC', 'FUN', 'RF', 'ODC_SAT', 'INSP', 'SARAO', 'cam', 'TEST', 'StarTracker', 'DOCU', 'site', 'AIV', 'crane', 'CHECK']

SHOW_LOADING = True
PAGE_SIZE = 30
DT_CHECK_FILEUPDATES_SEC = 60 * 30
DT_CHECK_FILEUPDATES_SEC_RND = 15
DISPLAY_STYLES_POSSIBLE = ['search', 'editnodes', 'plot', 'cyto', 'table', 'tree', 'docmaker']

DISPLAY_STYLES_LST_POSSIBLE = ['link_list-hirachy', 'list-hirachy', 'link_list-flat', 'list-flat']
MAKE_BACKUPS_ON_PUSH = True

WOOSH_CLEAN_INDEX_ON_START = False

ALL_CLASSNAMES = [x.__name__ for x in graph_main.classes]

NODE_SHOW_FIGSIZE = 200
NODE_SHOW_NNMAX = 4294967296
NODE_SHOW_PLOTSTYLE = 'tree'
NODE_SHOW_SCALE_XY = (0.5, 2)
NODE_SHOW_SIZEFUN = '25'

TREE_SHOW_SEL_MODE = 'items'
TREE_SHOW_ITEMS_PERPAGE = 20



from sys import platform
if IS_PLAYGROUND:
    # linux playground
    DEFAULT_DB_PATH = 'req_db.sqlite'
    DEFAULT_DB_SINKS = []
    DEFAULT_PULL_PATH = ""
    DEFAULT_CONFIG_PATH = 'req_config.json'
    DEFAULTUPLOAD_PATH = 'req_uploads/'
    DEFAULT_FINDEX_PATH = 'wooosh_index/'
    NC_INFO_PATH = 'info.hash'
    ES_INFO_PATH = 'es.txt'
    LOGIN_SOURCE = 'logins_auth.json'
    USE_ASYNC_IOS = False # not necessary
    REQUIRE_AUTHENTIFICATION = False
    ES_DEFAULT_HOST = os.environ.get('ES_DEFAULT_HOST', "http://localhost:9200")
    ES_DEFAULT_INDEX = "reqtrack"
elif platform == "linux" or platform == "linux2":
    # linux
    if IS_READONLY:
        DEFAULT_DB_PATH = '/home/jovyan/db/req_db.sqlite'
        DEFAULT_DB_SINKS = []
        DEFAULT_PULL_PATH = "http://10.98.76.45:8020/"

        DEFAULT_CONFIG_PATH = '/tmp/reqtrack/req_config.json'
        DEFAULTUPLOAD_PATH = '/tmp/reqtrack/req_uploads/'

        DEFAULT_FINDEX_PATH = '/tmp/reqtrack/wooosh_index/'
        NC_INFO_PATH = '/tmp/reqtrack/info.hash'
        ES_INFO_PATH = '/tmp/reqtrack/es.txt'
        LOGIN_SOURCE = '/tmp/reqtrack/logins_auth.json'

    elif IS_SUBSCRIBER:
        DEFAULT_DB_PATH = ':memory:'
        DEFAULT_DB_SINKS = []
        DEFAULT_PULL_PATH = "http://10.98.76.45:8020/"

        DEFAULT_CONFIG_PATH = '/home/jovyan/db/req_config.json'
        DEFAULTUPLOAD_PATH = '/home/jovyan/db/req_uploads/'

        DEFAULT_FINDEX_PATH = '/home/jovyan/db/wooosh_index/'
        NC_INFO_PATH = '/home/jovyan/db/info.hash'
        ES_INFO_PATH = '/home/jovyan/db/es.txt'
        LOGIN_SOURCE = '/home/jovyan/db/logins_auth.json'

    else:
        DEFAULT_DB_PATH = '/home/jovyan/db/req_db.sqlite'
        DEFAULT_DB_SINKS = ['/home/jovyan/db/req_db.sqlite']
        DEFAULT_PULL_PATH = ""

        DEFAULT_CONFIG_PATH = '/home/jovyan/db/req_config.json'
        DEFAULTUPLOAD_PATH = '/home/jovyan/db/req_uploads/'

        DEFAULT_FINDEX_PATH = '/home/jovyan/db/wooosh_index/'
        NC_INFO_PATH = '/home/jovyan/db/info.hash'
        ES_INFO_PATH = '/home/jovyan/db/es.txt'
        LOGIN_SOURCE = '/home/jovyan/db/logins_auth.json'

    USE_ASYNC_IOS = False # not necessary
    REQUIRE_AUTHENTIFICATION = False
    ES_DEFAULT_HOST = os.environ.get('ES_DEFAULT_HOST', "http://localhost:9200")
    ES_DEFAULT_INDEX = "reqtrack"
else:

    # pdir = r'c:\Users\tglaubach\repos\mke-requirement-suite'
    pdir = r'C:\Users\tobia\source\repos\mke-requirement-suite'
    # Windows...
    IS_PLAYGROUND = "1"
    # DEFAULT_DB_PATH = parent_dir + r'\scripts\temp\{}_temp.sqlite'.format(int(time.time()))
    # DEFAULT_DB_PATH = "http://10.98.76.45:8020/"
    DEFAULT_DB_PATH = ":memory:"
    DEFAULT_DB_PATH = pdir + r'\scripts\req_db.sqlite'
    DEFAULT_DB_SINKS = []
    DEFAULT_PULL_PATH = "http://10.98.76.45:8020/"
    


    DEFAULT_CONFIG_PATH = pdir + r'\scripts\req_config.json'
    # DEFAULT_CONFIG_PATH = pdir + r'\scripts\temp\{}req_config.json'.format(int(time.time()))
    DEFAULTUPLOAD_PATH = pdir + r'\scripts\upload'
    DEFAULT_FINDEX_PATH = pdir + r'\scripts\wooosh_index'
    NC_INFO_PATH = pdir + r'\scripts\info.hash'
    ES_INFO_PATH = pdir + r'\scripts\es.txt'
    ES_DEFAULT_HOST = "http://localhost:9200"
    ES_DEFAULT_INDEX = "reqtrack"
    LOGIN_SOURCE = pdir + r'\scripts\logins_auth.json'
    USE_ASYNC_IOS = False # might work with http source
    REQUIRE_AUTHENTIFICATION = False

AUTH = None
NEXTCLOUD_LOGIN_DATA = ()
NCFS = None
USERNAME_PW_PAIRS = dict()


ldict = 'IS_REQTRACK_SERVER IS_PRODUCTIVE IS_SUBSCRIBER IS_PLAYGROUND NC_PUSH_PATH IS_READONLY'.split()
log.info(f'SHOWING STATIC SETTINGS:')
for k in ldict:
    log.info(f'   {k}={locals()[k]}')
log.info(f'SHOWING STATIC SETTINGS: --> done')

@dataclass()
class UiState():
    sql_filter: str = ''
    figsize: int = DEFAULT_FIGSIZE
    db_path: str = DEFAULT_DB_PATH
    filtmode: str = DEFAULT_FILTMODE
    default_tabid: str = DEFAULT_TABID
    network_plot_style: str = 'fr'
    size_fun: str = '25'
    display_style: str = 'plot'
    display_style_plt: str = 'treemap'
    display_style_lst: str = 'hirachy'
    maxdepth: int = 10
    searchmode: str = DEFAULT_SEARCHMODE
    tags_possible: List[str] = field(default_factory=lambda : list(set(TAGS_DEFAULT)))
    hide_loose_docs: bool = True
    classes_to_plot: List[str] =  field(default_factory=lambda : list(ALL_CLASSNAMES))
    nn_nmax: int =  5
    nn_clip: bool =  True
    display_style_splot: str = 'graph'
    network_style_splot: str = 'tree'
    cls_to_show: List[str] =  field(default_factory=lambda : list(ALL_CLASSNAMES))
    list_items_per_page: int =  20
    start_contracted: bool =  False
    scale_xy: float =  1.
    only_search_mke_docs: bool =  True
    n_search_results: int =  20
    link_after_pull: bool =  False
    t_render_last: str = helpers.nowiso()
    t_last: str = helpers.nowiso()
    db_sinks: List[str] = field(default_factory=lambda : list(DEFAULT_DB_SINKS))
    logstate: List[str] = field(default_factory=lambda : [])

    def __contains__(self, key:str):
        return hasattr(self, key)
    
    # def test_is_diff(self, key, value):
    #     return key in self and getattr(self, key) != value
    
    # def set_if_diff(self, key, value):
    #     assert key in self, f'field {key} does not exist in class! Set value was: {value}'
    #     if getattr(self, key) != value:
    #         setattr(self, key, value)
    #         return True
    #     else:
    #         return False
        
    # def sync(self, raise_on_not_found=True, **kwargs):
    #     n_changes = self.update(raise_on_not_found=raise_on_not_found, **kwargs)
        
    #     # if n_changes and IS_REQTRACK_SERVER:
    #     #     log.debug(f'writing {n_changes=} to {DEFAULT_CONFIG_PATH}')
    #         # self.push(DEFAULT_CONFIG_PATH)
    #     return n_changes
    
    # def update(self, raise_on_not_found=True, **kwargs):
    #     n_changes=0
    #     for key, value in kwargs.items():
    #         if raise_on_not_found and key not in self:
    #             raise KeyError(str(key) + ' does not exist!')
    #         n_changes += 1 if self.set_if_diff(key, value) else 0

    #     return n_changes
    
    # def push(self, pth):
    #     pass
        # d = os.path.dirname(pth)
        # if d:
        #     os.makedirs(d, exist_ok=True)

        # with open(pth, 'w') as fp:
        #     json.dump(asdict(self), fp, indent=2)

    def to_dict(self):
        return asdict(self)


    # @classmethod
    # def pull(cls, pth):
    #     try:
    #         if not os.path.exists(pth):
    #             log.info(f'file "{pth}" does not exist. Generating default config file!')
    #             s = UiState()
    #             s.push(pth)
    #             dc = s.to_dict()
    #         else:
    #             with open(pth, 'r') as fp:
    #                 dc = json.load(fp)
    #     except json.decoder.JSONDecodeError as err:
    #         log.exception(err)
    #         log.error('ERROR while trying to load config file. Deverting to default and deleting file')
    #         try:
    #             os.remove(pth)
    #         except OSError:
    #             pass
    #         dc = {}
    #     except FileNotFoundError as err:
    #         dc = {}


    #     return UiState(**dc)
    

    # @classmethod
    # def pull_projvars(*args, **kwargs):
    #     # log.debug('pull_projvar({})'.format(kwargs))
    #     state = UiState.pull(DEFAULT_CONFIG_PATH)
    #     if kwargs:
    #         state.sync(**kwargs)
    #     # log.debug('pull_projvar(...) -> {}'.format(state))
    #     return state

    # @classmethod
    # def push_projvars(*args, **kwargs) -> int:
    #     log.debug('push_projvar({})'.format(kwargs))
    #     state = UiState.pull(DEFAULT_CONFIG_PATH)
    #     return state.sync(**kwargs)
    

DEFAULT_STATE = UiState()

def get() -> UiState:
    s = os.environ.get('reqtracker_cache', '')
    if s:
        # log.debug(s)
        state = UiState(**json.loads(s))
    else:
        log.debug('reqtracker_cache NOT EXISTS --> making new')
        state = UiState()
        os.environ['reqtracker_cache'] = json.dumps(state.to_dict())

    # log.debug('\n\n' + os.environ['reqtracker_cache'])

    return state

def get_dc() -> dict:
    return asdict(get())
    
def setv(**kwargs) -> UiState:
    state = get()
    for k, v in kwargs.items():
        assert k in state, f'{k} is not a setting parameter'
        log.debug(f'setting {k=} from {getattr(state, k)} to {v}')
        setattr(state, k, v)
    
    os.environ['reqtracker_cache'] = json.dumps(state.to_dict())
    # log.debug('\n\n' + os.environ['reqtracker_cache'])

    return get()

def reset_default() -> UiState:
    state = UiState()
    os.environ['reqtracker_cache'] = json.dumps(state.to_dict())
    return state

logdata = []

_in_memory_graph = None

# # needed to make the editor recognize global variables :-/
# figsize, db_path,filtmode,default_tabid,network_plot_style,size_fun,display_style,display_style_plt,display_style_lst,maxdepth,searchmode,tags_possible,hide_loose_docs,classes_to_plot, nn_nmax, nn_clip, display_style_splot, network_style_splot, cls_to_show, list_items_per_page, start_contracted, scale_xy, only_search_mke_docs, n_search_results, link_after_pull, t_render_last, t_last, db_sinks = (state[k] for k in PROJ_VARS_DEFAULT.keys())

T_STARTED = helpers.get_utcnow()
t_render_last = helpers.nowiso()

CHNG_CONFIG_CB_KEYS = 'searchmode default_tabid filtmode maxdepth network_plot_style figsize size_fun display_style_lst hide_loose_docs classes_to_plot'.split()

if IS_PLAYGROUND:
    link_after_pull = False
    db_path = ':memory:'
    _in_memory_graph = graph_main.Graph(EXAMPLE_DB_FILE_PATH).clone()






"""

 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄            ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀  ▀▀▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌          ▐░▌          ▐░▌     ▐░▌            ▐░▌
▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌      ▐░▌  ▄▄▄▄▄▄▄▄▄█░▌
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                            
"""

def raise_on_readonly():
    if os.environ.get('IS_READONLY', ''):
        raise PermissionError('This instance is a readonly client. The operation you are trying to carry out would change the database and is therefore not allowed.')

def update_tags(newtags=None):
    
    global tags_possible
    if newtags is None:
        newtags = []

    with GetGraphHold() as graph:
        tmp = set().union(*[n.tags for n in graph.nodes])
        newtags += list(set([t for t in tmp if t not in tags_possible]))

        if newtags:
            tags_possible = list(set(tags_possible).union(set(newtags)))
            UiState.push_projvars(tags_possible=tags_possible)

    return tags_possible, newtags



def add_log_alert(x):
    global logdata
    (txt, kwargs) = x
    if 'color' in kwargs and kwargs['color'] == 'danger':
        log.error(txt)
    elif 'color' in kwargs and kwargs['color'] == 'warning':
        log.warn(txt)
    else:
        log.info(txt)

    logdata.append(x)




def log_changes(ts, txt):
    ts = helpers.make_zulustr(ts).replace('T', ' ')
    s_txt = f'{ts} CHANGE: {txt}'
    if 'error' in s_txt.lower():
        color = 'danger'
    else:
        color = 'primary'

    add_log_alert((s_txt, {'color': color}))



class GetGraphHold(graph_main.HoldChangesBase):
    def __init__(self, source=None, sinks=None):
        
        state = get()
        global _in_memory_graph

        self.path = db_path = (source if source else state.db_path)

        
        if sinks is None:
            if IS_READONLY or (IS_PRODUCTIVE and IS_SUBSCRIBER):
                sinks = []
            else:
                sinks = state.db_sinks if not IS_PLAYGROUND and state.db_sinks else []
        
        log.debug(f'INIT GetGraphHold: {db_path=} | {sinks=}')

        if db_path == ':memory:' and _in_memory_graph is None:
            _in_memory_graph = graph_main.Graph(db_path, do_autosave=False, make_sinks_async=False, auth=AUTH, nc_auth=NEXTCLOUD_LOGIN_DATA)

        if db_path == ':memory:':
            g = _in_memory_graph
            state = get()
            g.add_sinks(sinks, make_async=MAKE_ADDITIONAL_SINKS_ASYNC) # will only add if not already available
            g._hold_changes = None
        else:
            if state.sql_filter:
                graphdb_io.test_no_kws(state.sql_filter)
                wheres = [f"id LIKE '%{state.sql_filter}%' OR id LIKE 'DS.%' OR id LIKE 'R.DS%' OR id LIKE 'VR.%'"]
            else:
                wheres = None
            

            g = graph_main.Graph(db_path, do_autosave=True, make_sinks_async=MAKE_ADDITIONAL_SINKS_ASYNC, auth=AUTH, nc_auth=NEXTCLOUD_LOGIN_DATA, sinks=sinks, wheres=wheres)
            g._hold_changes = None
            g.on_change.add(log_changes)

        if not IS_PLAYGROUND and g._sinks:
            g.do_autosave = True

        
        super().__init__(g)

        log.debug(f'INIT DONE GetGraphHold: {g=} | {len(g)=}')

    def __enter__(self) -> graph_main.Graph: 
        self.enter()
        return self.obj
    
    def __exit__(self, exception_type, exception_value, exception_traceback):
        log.debug('EXITING "GetGraphHold" for object "{}"'.format(self.obj))
        r = self.exit()
        log.debug('(r, NC_PUSH_PATH, ncfs) = ' + str((r, NC_PUSH_PATH, NCFS)))
        if not IS_READONLY and r and NC_PUSH_PATH and not NCFS is None:
            for sink in self.obj.get_filesinks():
                if sink.path:
                    path_local = sink.path
                    path_remote = f'{NC_PUSH_PATH}/{os.path.basename(sink.path)}'
                    log.info(f'syncing: {path_local} -> {path_remote} on {NCFS.nc.url}')

                    NCFS.push_file_if_newer(path_local, path_remote, push_in_thread=NC_PUSH_ASYNC)

        g = self.obj

        log.debug('DONE EXITING "GetGraphHold" for object "{}"'.format(self.obj))

        self.obj = None
        global _in_memory_graph
        if _in_memory_graph is None or g != _in_memory_graph:
            del g

        return r
    

def save_backup():
    db_path = DEFAULT_STATE.db_path

    if os.path.exists(db_path):
        pre = helpers.get_utcnow().strftime("BU_%y%m%d_%H%M%S_")
        pth = os.path.join(os.path.dirname(db_path), pre + os.path.basename(db_path))
        shutil.copyfile(db_path, pth)
        return A("{} | Saved data to {}".format(datetime.datetime.now().isoformat(), pth), color="success")
    else:
        return A('{} | "{}" does not exist.'.format(datetime.datetime.now().isoformat(), db_path), color="warning")
    
        
    

def make_node(graph, node_id, ret_info=False):

    if ':' in node_id:
        node_id = node_id.split(':')[-1]

    if node_id in graph:
        node = graph[node_id]
        info = f'make_node({node_id}) -> {node} (<{node.classname}> existed getting)'
    if schema.Document.fun_match(node_id):
        
        node = node_factory.construct_doc(node_id, NEXTCLOUD_LOGIN_DATA, graph, on_exist='get')
        node_id = node.id
        info = f'make_node({node_id}) -> {node} (<{node.classname}> from file_factory)'
    else:
        node = graph.construct(dc_node={'id': node_id}, allow_fallback=False)
        assert node is not None, f'the node with id {node_id} could not be constructed from dish'
        graph.add_node(node)
        info = f'make_node({node_id}) -> {node} (<{node.classname}> from construct)'

    log.debug(info)
    if ret_info:
        return node, info
    else:
        return node

def update_nextcloud(dt_elapsed):

    log.debug('TICK nextcloud update')
    dt_get = dt_elapsed + datetime.timedelta(seconds=60)
    files = nextcloud_interface.query_changed_since(dt_get, NEXTCLOUD_LOGIN_DATA)

    with GetGraphHold() as graph:
        ids = [(d, node_factory.construct_doc_from_dc_data(d, [graph])) for d in files]
        updated_nodes = [graph[id_].add_version(dc_data, on_exist='ignore') for dc_data, id_ in ids if id_ in graph]
        return [n.id for n in updated_nodes]


def update_nextcloud_tick(t_last, do_tick=None):
    t_last_in = t_last # datetime is value type so this works
    t_now = helpers.get_utcnow()
    updated_ids = []



    if NEXTCLOUD_LOGIN_DATA:
        
        if t_last is None:
            do_tick = True
            t_fallback = t_now - datetime.timedelta(DT_CHECK_FILEUPDATES_SEC_RND)

            t_last = helpers.parse_zulutime(setv(t_last=helpers.make_zulustr(t_fallback)))
        elif isinstance(t_last, str):
            t_last = helpers.parse_zulutime(t_last)

        if t_last is None or not isinstance(t_last, datetime.datetime):
            t_last = t_fallback
        if t_now is None or not isinstance(t_now, datetime.datetime):
            t_now = helpers.get_utcnow()
        
        dt_elapsed = t_now - t_last

        if do_tick is None:
            dt_fuzzy = np.random.default_rng().normal(0, DT_CHECK_FILEUPDATES_SEC_RND)
            dt_fuzzy = np.clip(dt_fuzzy, -DT_CHECK_FILEUPDATES_SEC_RND*3, DT_CHECK_FILEUPDATES_SEC_RND*3)
            dt = datetime.timedelta(seconds=DT_CHECK_FILEUPDATES_SEC + dt_fuzzy)
            do_tick = (dt_elapsed > dt)
        # log.debug((dt_elapsed, dt))
        
        if do_tick:
            updated_ids = update_nextcloud(dt_elapsed)
            t_last = t_now

        if t_last != t_last_in:
            setv(t_last=helpers.make_zulustr(t_last))


    return t_last, updated_ids


def get_graph_view(graph, kview, reverse_graph=False, apply_contraction=True, containers_to_expand=None, st=None, set_to_nodes=True) -> graph_main.GraphView:
    
    containers_to_expand = containers_to_expand if not containers_to_expand is None else []

    if st is None:
        st = DEFAULT_STATE

    graph_view = graph
    log.debug('active_tabid: {}'.format(kview))
    if kview == 'all' or kview == 'graph':
        log.debug('all')
        graph_view = graph
    elif kview == 'bytags':
        k = 'groupby(tags)'
        log.debug(k)
        graph_view = graph.groupby('tags')
    elif kview == 'byclassname':
        k = 'groupby(classname)'
        log.debug(k)
        graph_view = graph.groupby('classname')
    elif kview == 'bydesign':
        k = 'by_design'
        log.debug(k)
        f = 'keep' if st.filtmode == 'auto' else st.filtmode
        graph_view = graph.filt_by_design(mode=f)
    elif kview == 'bytest':
        k = 'by_test'
        log.debug(k)
        f = 'keep' if st.filtmode == 'auto' else st.filtmode
        graph_view = graph.filt_by_dish(None, mode=f)
    elif kview.startswith('dish:'):
        dish_id = kview.split(':')[-1]
        k=kview
        log.debug(k)
        f = 'keep' if st.filtmode == 'auto' else st.filtmode
        graph_view = graph.filt_by_dish(dish_id, mode=f)        
    elif kview.startswith('reverse: '):
        graph_view = graph.reverse()
    elif kview.startswith('eval: '):
        inp = kview[len('eval: '):]
        log.info('evaluating ' + inp)
        graph_view = eval(inp, {'graph': graph}, {'g': graph})
        
    elif kview.startswith('search:'):
        searchtext = kview[len('search:'):]
        log.info('searching ' + searchtext)
        if st.searchmode == 'fuzzy':
            graph_view = graph.search_fuzzy(searchtext, k_max=10)
        else:
            mode = 'flat' if st.filtmode == 'auto' else st.filtmode
            graph_view = graph.search(searchtext, k_max=len(graph.nodes), mode=mode)
            log.debug('SEARCHRESULT: found {} matches ({}) (mode={})'.format(len(graph_view.get_viewnodes()), graph_view, mode))
      
    elif kview.startswith('contr:'):
        graph_view = graph.c_contract_containers()
    else:
        graph_view = graph

    if reverse_graph:
        graph_view = graph_view.reverse()

    log.debug(graph_view.name)
    
    if apply_contraction and st.start_contracted:
        if containers_to_expand and 'DS.REQS' not in containers_to_expand:
            tmp = containers_to_expand + ['DS.REQS']
        else:
            tmp = containers_to_expand

        graph_view = graph_view.c_contract_containers(tmp)

    if set_to_nodes:
        graph_view.set_me_as_graph()

    return graph_view



def make_tabs(g:graph_main.Graph, tabid_in=None):


    log.debug('make_tabs: {}'.format(g))

    faulty_ids = [n for n in g.nodes if n.dish_id == '#']
    if faulty_ids:
        log.warning('WARNING! faulty_dish_ids: {}'.format(faulty_ids))

    all_tab_ids = [('dish:' + n.dish_id) for n in g.nodes if n.dish_id]

    # all_tab_ids += list(graphviews.keys())
    all_tab_ids += [g.name]
    if tabid_in is not None:
        all_tab_ids += [tabid_in]

    all_tab_ids = sorted(set(all_tab_ids))

    for tabid in TABIDS_DEFAULT: 
        if tabid in all_tab_ids:
            all_tab_ids.remove(tabid)
    
    
    tabids = list(set(TABIDS_DEFAULT + all_tab_ids))
    tabids.sort()
    
    all_hrefs = []
    # r = get_display_style_route(disp_style)
    
    # if r == 'editnodes':
    # f = lambda n, k: dcc.Link(n, href=mk_href(g=k, node_id=g.root_node.id), target="_self")
    # else:
    f = lambda n, k: dcc.Link(n, href=mk_href(g=k), target="_self")

    dc = {'bydesign': 'By_Design', 'bytest': 'By_Test', 'bytags':'Groupby_Tags', 'byclassname': 'Groupby_Classname' }
    all_hrefs += [dcc.Link('All', href=mk_href(g='all'), target="_self")]
    all_hrefs += [f(name, k) for k, name in dc.items()]
    all_hrefs += [f(e, e) for e in tabids if e != 'all' and e!= g.root_node.id and not e in dc]

    all_hrefs = [html.B('VIEWS:', style={'margin': '2px'})] + helpers.intersperse(all_hrefs, ' | ')
    all_hrefs = [dbc.Label(all_hrefs)]

    log.debug('tabs: {}'.format(all_tab_ids))

    return all_hrefs



def get_nc_info():
    from cryptography.fernet import Fernet

    with open(NC_INFO_PATH, 'r') as fp:
        k, a, b = fp.readlines()
        FERNET = Fernet(bytes(k, 'ASCII'))
        a = FERNET.decrypt(a).decode()
        b = FERNET.decrypt(b).decode()
    return (a,b)

def get_es_info():
    with open(ES_INFO_PATH, 'r') as fp:
        k = fp.readlines()[0]
    return k


def elastic_init(do_test=False):
    if os.path.exists(ES_INFO_PATH) and ES_DEFAULT_HOST:
        try:
            elasticsearch_api.es_host = ES_DEFAULT_HOST
            elasticsearch_api.es_index = ES_DEFAULT_INDEX
            elasticsearch_api.init(('elastic', get_es_info()), ES_DEFAULT_HOST, do_test=do_test)
            log.info('Sucessfully connected to Elasticsearch')
        except Exception as err:
            log.error(f'ERROR while connecting to Elasticsearch | {type(err)}: {err}')
    else:
        log.info(f'No Nextcloud login info given -- not logging in')



""" 
 █████  ██    ██ ████████ ██   ██ 
██   ██ ██    ██    ██    ██   ██ 
███████ ██    ██    ██    ███████ 
██   ██ ██    ██    ██    ██   ██ 
██   ██  ██████     ██    ██   ██ 
    """                              


if IS_REQTRACK_SERVER:

    if REQUIRE_AUTHENTIFICATION:
        try:
            with open(LOGIN_SOURCE, 'r') as fp:
                USERNAME_PW_PAIRS = json.load(fp)
            log.info('loading users success.. loaded N={} login users from db'.format(len(USERNAME_PW_PAIRS)))
        except Exception as err:
            log.info('could not find any username info... keeping the app open')
            log.exception(err, exc_info=True)

    helpers.mkdir(DEFAULTUPLOAD_PATH)

    if not os.path.exists(DEFAULTUPLOAD_PATH):
        log.info('creating direcory for uploads: "{}"'.format(DEFAULTUPLOAD_PATH))
        os.mkdir(DEFAULTUPLOAD_PATH)

    try:
        import getpass
        usr = getpass.getuser()
        pw = USERNAME_PW_PAIRS[usr]
        AUTH = (usr, pw)
        log.info(f'running wit user:"{usr} (for API access only!)')
    except Exception as err:
        AUTH = ()
        log.info(f'running without user because: {err} was not found')





    try:
        NEXTCLOUD_LOGIN_DATA = get_nc_info()
        NCFS = nextcloud_interface.NextcloudFileSyncer(NEXTCLOUD_LOGIN_DATA)
        log.info('Sucessfully connected to Nextcloud')
    except Exception as err:
        log.error(err)

    elastic_init()
    
