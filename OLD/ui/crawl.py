import re
import time

import numpy as np



from OLD.io import woosh_api
from ReqTracker.core import helpers, schema

from ReqTracker.io_files import langchain_nextcloud_io, elasticsearch_api
from ReqTracker.ui import ui_state
from ReqTracker.ui import helpers_ui
from ReqTracker.ui.helpers_ui import A


log = helpers.log


def crawl(force_clean=False, verb=True, limit=-1):
    
    t = time.time()

    with ui_state.GetGraphHold() as g:
        nodes_docs = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]
        gname = g.name

    ltcs = [helpers.parse_zulutime(n.last_time_changed) for n in nodes_docs]
    ltc = np.max(ltcs)
    if verb:
        helpers.log.info('STARTING AT: ' + helpers.make_zulustr(ltc))

    updated_ids = ui_state.update_nextcloud(ltc)
    helpers.log.info('FOUND N={} updated documents since : {}'.format(len(updated_ids), helpers.make_zulustr(ltc)))

    with ui_state.GetGraphHold() as g:
        if not force_clean:
            nodes_docs = [g[id_] for id_ in updated_ids]
        else:
            nodes_docs = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]

    if limit and limit > 0:
        nodes_docs = nodes_docs[:limit]
    
    nodes_docs = [n for n in nodes_docs if n.extension in '.pdf .docx']

    # docs = langchain_nextcloud_io.get_nc_docs(nodes_docs, ui_state.get_nc_info(), verb=True)
    docs = langchain_nextcloud_io.get_nc_docs_dc(nodes_docs, ui_state.get_nc_info(), verb=True)

    # with open('docs.pickle', 'wb') as fp:
    #     pickle.dump(docs, fp)
        
    # with open('docs.pickle', 'rb') as fp:
    #     docs = pickle.load(fp)

    missing = {n.id:n.get_latest_ver()['path'] for n, d in zip(nodes_docs, docs) if n is None or d is None}
    if verb and missing:
        log.info('START OF MISSING DOCS'.rjust(50, '-').ljust(100, '-'))

        for m, pth in missing.items():
            log.info(m.ljust(50), pth)
        
        log.info('END OF MISSING DOCS'.rjust(50, '-').ljust(100, '-'))

    dc = {n.id:(n, d) for n, d in zip(nodes_docs, docs) if n is not None and d is not None}
    # woosh_api.incremental_index(ui_state.DEFAULT_FINDEX_PATH, dc, force_clean=force_clean, verb=verb)
    if docs:
        elasticsearch_api.update_index(docs)
    elapsed = time.time()-t
    if verb:
        log.info((elapsed, len(docs), len(dc)))

    return gname, missing, elapsed, docs, dc, ltc