
from ReqTracker.io_files import nextcloud_interface
from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

import uuid
import string

import urllib

from ReqTracker.core import node_factory

printable = set(string.printable)

from ReqTracker.core import schema, graph_main, graph_base, helpers
from ReqTracker.ui.helpers_ui import id2lnk_Bdcc

log = helpers.log

        

def mk_node_list_entry(node, show_children=True, vm = '2px'):

    try:
        s = {
            "marginLeft": f"10px", 
            "marginRight": vm, 
            "marginTop": vm,  
            "marginBottom": vm, 
            "border":"1px black solid", 
            "borderRadius": "5px", 
            "height": "100%",
            # "width": "100%",
        }

        # s = {"marginLeft": f"{node.depth*5}px"}
        #
        # "width": "100%", 
        if hasattr(node, 'color'):
            s['backgroundColor'] = node.color

        if isinstance(node, schema.Document):
            desc_text = node.title
        else:
            desc_text = node.text.strip()
            desc_text = ''.join(filter(lambda x: x in printable, desc_text))

        my_id = node.id + '|row:' + str(uuid.uuid4().hex)

        txt = desc_text.strip().split('\n')[0]

        bid = {"type": 'beditnode', "index": my_id}
        badd = {"type": 'baddnote', "index": my_id}
        bstat = {"type": 'bstat', "index": my_id}

        ttid = 'rowkey|' + my_id

        status = '' if not hasattr(node, 'status') else node._status.name
        
        my_entry = dbc.Row([
            
            dbc.Col(id2lnk_Bdcc(node.id), width='auto', style={'marginLeft': '5px', 'marginTop': '2px'}, id = ttid),
            dbc.Col(txt),
    
            
            # dbc.Col(node.status.name if hasattr(node, 'status') else '', width='auto'),
            dbc.Col(dbc.Select([el.name for el in graph_base.STATUS], value=status, id=bstat, style={'width': 120}, size='sm'), width='auto'),
            
            dbc.Col(
                dbc.Input(type="text", placeholder="type a new note or file path and press enter", debounce=True, id=badd, size='sm', style={'minWidth': 300}), 
                style={'margin': '2px'}, 
                width='auto'),
            # dbc.FormFloating(
            # [
                
            #     dbc.Label("Add Note"),
            # ], style={'width': 250,'margin': '2px'}), width='auto'),
            # dbc.Col(dbc.Button('Edit', id=bid, style={'margin': '2px'}), width='auto')
        ], align='center')

        desc_text_md = desc_text.replace('\n', '\n\n')

        lines_des = desc_text_md.split('\n\n')
        header = '**{}**:'.format(node.id)

        lines = []
        lines += [header]
        lines += lines_des
        lines += ['___']
        

        if hasattr(node, 'status'):
            status = 'STATUS: ' + str(node.status.name)
        elif hasattr(node, '_status'):
            status = 'STATUS: ' + str(node._status.name)
        else:
            status = ''

        tags = 'TAGS: ' + ' | '.join(node.tags)

        dkeys = [helpers.limit_len(k) for k in node.data.keys()]
        dkeys = f'DATA-KEYS: {dkeys}' 

        lines += [status]
        lines += [tags]
        lines += [dkeys]
        lines += ['___']

        notes = [f'-*{note_ts}: * {note_txt}' for note_ts, note_txt in node.notes.items()]
        lines += ['**NOTES: **']
        lines += notes

        md = '\n\n'.join(lines)

        tt = html.Div(dcc.Markdown(md))

        entries = [
            html.Div(my_entry),
            dbc.Tooltip(tt,
                target=ttid,
                placement='bottom',
                delay={"show": 0, "hide": 10},
            )
        ]

        if show_children:
            entries += [mk_node_list_entry(el) for el in node.children]
        
        return html.Div(entries, style=s)
    
    except Exception as err:        
        log.error(err, exc_info=True, stack_info=True)
        log.error('ERROR: {} {}'.format(node.id, err))
        return dbc.Container(str(err))
    


def mk_listvis(graph:graph_main.Graph, style_lst, maxdepth=None, filtfun=None):

    if style_lst.endswith('flat'):
        nodes = graph.get_nodes_view()
        show_children = False
    else:
        nodes = graph.get_highest_nodes()
        show_children = True

    if not filtfun is None:
        log.debug(f'mk_listvis before filtering: {len(nodes)}')
        nodes = [x for x in nodes if filtfun(x)]

    log.debug('mk_listvis -> {} {} {}'.format(style_lst, len(nodes), maxdepth))
    children = [mk_node_list_entry(el, show_children=show_children, vm='5px') for el in nodes]
    return children

