import numpy as np
import pandas as pd

import plotly.express as px

from ReqTracker.core import graph_base


import traceback

colors = {k.name:v for k,v in graph_base.colors.items()}


def make_sunburst_plot(graph, color_col_plt='status', color_col='color', maxdepth=10):
    
    recs = graph.to_plot_recs(color_col)
    data = pd.DataFrame(recs)

    # print(data['color'])

    hoverdata = None
    if recs:
        hoverdata = graph_base.BaseNode.hoverdata
        hoverdata = {c:c in hoverdata for c in data.columns}

    fig = px.sunburst(
        data,
        names='node_id',
        parents='parent',
        # values='perc',
        color=color_col_plt,
        hover_data=hoverdata,
        # hover_template=hovertemplate,
        hover_name='node_id',
        ids='id',
        maxdepth=maxdepth,
        color_discrete_map=colors if color_col_plt == 'status' else None
    )


    return fig


def make_treemap_plot(graph, color_col_plt='status', color_col='color', maxdepth=10):
    
    recs = graph.to_plot_recs(color_col)
    data = pd.DataFrame(recs)

    # print(data['color'])

    hoverdata = None
    if recs:
        hoverdata = graph_base.BaseNode.hoverdata
        hoverdata = {c:c in hoverdata for c in data.columns}
    
    fig = px.treemap(
        data,
        names='node_id',
        parents='parent',
        # values='perc',
        color=color_col_plt,
        hover_data=hoverdata,
        # hover_template=hovertemplate,
        hover_name='node_id',
        ids='id',
        maxdepth=maxdepth,
        color_discrete_map=colors if color_col_plt == 'status' else None,
        
    )


    return fig


