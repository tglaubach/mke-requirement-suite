import numpy as np


import plotly.graph_objects as go


# from ReqTracker.core import graph_base

# add to path for testing
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)


from ReqTracker.core import helpers, graph_main, schema

log = helpers.log

ALL_SYMBOLS = ['circle', 'square', 'diamond', 'cross', 'x', 'pentagon', 'hexagon', 'star-triangle-up', 'star-square', 'square-cross', 'diamond-wide', 'star-diamond', 'hourglass', 'diamond-tall',  'bowtie', 'circle-cross', 'square-x', 'diamond-cross', 'diamond-x', 'cross-thin', 'x-thin', 'asterisk', 'hash', 'y-up', 'y-down', 'y-left', 'y-right', 'line-ew', 'line-ns', 'line-ne', 'line-nw', 'arrow-up', 'arrow-down', 'arrow-left', 'arrow-right', 'arrow-bar-up', 'arrow-bar-down', 'arrow-bar-left', 'arrow-bar-right']
CLASSSYMBOLS = {c.__name__:ALL_SYMBOLS[i] for i, c in enumerate(graph_main.classes)}
CLASSSYMBOLS[schema.RequirementContainer.__name__] = 'circle'
CLASSSYMBOLS[graph_main.ViewContainer.__name__] = 'circle-x'
                
MAX_SIZE = 40
# apply_log = True

# https://igraph.org/python/doc/api/igraph.Graph.html#layout

LAYOUTS_POSSIBLE = """auto
circle
davidson_harel
drl
fr
grid
graphopt
kk
large
mds
random
tree
rt_circular
star
sugiyama
point""".split('\n')



SIZEFUNS_POSSIBLE = """pagerank
degree
indegree
outdegree
eccentricity
log(pagerank)
log(degree)
log(indegree)
log(outdegree)
log(eccentricity)""".split('\n')


def make_annotation_plot(graph, pos_fun, lyout='tree', size_fun='25', hovermode='str', flip_xy=False, scale_xy=None):

    fig = go.Figure()
    
    Xn, Yn, Xe, Ye, labels, sizes = pos_fun(graph, lyout=lyout, size_fun=size_fun, need_edges=True, MAX_SIZE=MAX_SIZE)
    if flip_xy:
        Xn, Yn, Xe, Ye = Yn, Xn, Ye, Xe

    if sizes is None:
        sizes = 25

    if scale_xy is None:
        s = 1 + (0.2 * ((800 / len(graph))-1))
        scale_xy = (s,s)

    if not isinstance(scale_xy, (tuple, list)):
        scale_xy = (scale_xy, scale_xy)


    nodes = graph.get_nodes_view()

    colors = [n.color for n in nodes]
    ids = [n.id for n in nodes]
    symbols = [CLASSSYMBOLS[n.classname] for n in nodes]

    if hovermode == 'info':
        labels = [str(graph[n.id].info()) for n in nodes]
    elif hovermode == 'as_str':
        labels = [graph[n.id].as_str(100) for n in nodes]

    fig.add_trace(go.Scatter(x=Xe,
                    y=Ye,
                    mode='lines',
                    line=dict(color='rgb(80,80,80)', width=0.5),
                    hoverinfo='none',
                    ))

    fig.add_trace(go.Scatter(x=Xn,
                    y=Yn,
                    mode='markers+text',
                    textposition='middle right', # 'top right', # 'middle center',
                    marker=dict(symbol=symbols,
                                    size=sizes,
                                    # color='#6175c1',    #'#DB4551',
                                    line=dict(color='rgb(50,50,50)', width=1)
                                    ),
                    text=ids,
                    # hoverinfo='hovertext',
                    hovertext=labels,
                    marker_color = colors,
                    ids=ids,
                    # opacity=0.8
                    ))

    fig.update_traces(textfont_size=8)

    return fig


