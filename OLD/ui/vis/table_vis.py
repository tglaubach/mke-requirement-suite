import pandas as pd

from ReqTracker.io_files import nextcloud_interface
from dash import html, dash_table
import dash_bootstrap_components as dbc

from ReqTracker.core import schema, graph_main, graph_base, helpers
from ReqTracker.core import node_factory
# from ReqTracker.io import graphdb_io

log = helpers.log


cols_table = 'node_id title status tags n-notes tickets version'.split()
cols_markdown = ['node_id', 'version']
f = lambda i: {"name": i, "id": i, 'editable': False, 'presentation': None if i not in cols_markdown else 'markdown'}
column_scheme = [f(i) for i in cols_table]

df_default = pd.DataFrame([], columns='node_id title status tags n-notes tickets version'.split())
df_tooltips_default = pd.DataFrame([], columns='version n-notes'.split())

# def fun(s, col):
#     return {
#             'if': {
#                 'filter_query': '{stat}="' + s.name + '"',
#             },
#             'color': col,
#             # 'fontWeight': 'bold'
#         }

# style_data_conditional = [] # [fun(k, v) for k, v in graph_base.colors.items()][:2]
# log.debug(style_data_conditional)


def helper(n):
    s = schema.Document.fun_get_md_link_s(n['path'], n['link']) 
    if n['checksum']:
        s += ' ({})'.format(n['checksum']) 
    return s


def get_df_docs(all_docs):

    if not all_docs:
        log.debug('no data given! Falling back to default!')
        return df_default, df_tooltips_default

    rows = {}
    rows_tooltips = {}


    for f0 in all_docs:

        notes = '\n\n---\n\n'.join([f'*{note_ts}: * \n{note_txt}' for note_ts, note_txt in f0.notes.items()])
        row = {
            'node_id': f0.id, #id2lnk_html(f0.id),
            'title': f0.title,
            'status': f0.status.name,
            'tags': f0.tagss,
            'n-notes': len(f0.notes),
            'tickets': ', '.join(f0.tickets_md),
            'version': '',
        }

        row_tt = {
                # 'node_id': {'value': f0.text, 'type': 'markdown'},
                'version': {'value': '', 'type': 'markdown'}, 
                'title': {'value': f0.text, 'type': 'markdown'}, 
                'n-notes': {'value': notes, 'type': 'markdown'},
            }
        
        if isinstance(f0, schema.Document):
            
            link = f0.get_md_link()
            dc = f0.to_doc_dict()
            if 'version' in dc:
                row['version'] = '[{}]({})'.format(dc['version'], dc['link'])
            else:
                row['version'] = 'error -> no version found'
            
            if 'title' in dc:
                row['title'] = dc['title']
        
            docs = f0.get_version_dcs()
            if docs:
                docs.sort(key=lambda n: n['path'])
                link += '---\n\n**versions**\n\n'
                link += '\n-'.join([helper(n) for n in docs])
                row_tt['version'] = {'value': link, 'type': 'markdown'}

        rows[f0.id] = row
        rows_tooltips[f0.id] = row_tt

    df = pd.DataFrame(rows).T
    df_tooltips = pd.DataFrame(rows_tooltips).T
    return df, df_tooltips


def mk_datatable_lst(graph:graph_main.Graph, id_=None, **kwargs):
    # if style_lst == 'table-nodes':
    # TODO: implement style switching?
    df_table, table_tooltips = get_df_docs(graph.get_viewnodes())
    
    log.debug('Dataframes for table: {} and {}'.format(df_table.shape, table_tooltips.shape))

    if not id_:
        id_ = 'main_table'

    return dbc.Row(dash_table.DataTable(
            id=id_,
            columns=column_scheme,
            markdown_options={"html": False, 'link_target': '_blank'},
            data=df_table.to_dict('records'),
            editable=False,
            # style_cell_conditional=style_data_conditional,
            filter_action="native",
            sort_action="native",
            sort_mode="multi",
            row_deletable=False,
            page_action="native",
            page_current= 0,
            page_size= 20,

            # row_deletable=False,
            # page_current= 0,
            # page_size= 20,
            # page_action='custom',
            # filter_action='custom',
            # filter_query='',
            # sort_action='custom',
            # sort_mode='multi',
            # sort_by=[],

            tooltip_data=table_tooltips.to_dict('records'),
            # style_cell={'textAlign': 'left'},
            tooltip_delay=200,
            tooltip_duration=None,
            # css=[{
            #     'selector': '.dash-table-tooltip',
            #     'rule': 'fontFamily: monospace; min-width: 300px !important; max-width: 1000px !important'
            # }],
        ))
    
    # return dash_table.DataTable(id="node_table", 
    #     filter_action="native",
    #     sort_action="native",
    #     sort_mode="multi",
    #     data = df_table.to_dict('records'),
    #     tooltip_data = table_tooltips, 
    #     columns = column_scheme,
    #     tooltip_delay=0,
    #     tooltip_duration=None,
    #     css=[{ 'selector': '.dash-table-tooltip',
    #         'rule': 'background-color: grey; fontFamily: monospace; color: white; max-width: 1000px',
    #     }, {
    #         "selector": ".Select-menu-outer",
    #         "rule": 'display : block'
    #     }],
    #     editable=False,
    #     style_cell={'padding-right': '10px', 'textAlign': 'left'},
    #     style_header={
    #         'fontWeight': 'bold'
    #     },       
    # ),