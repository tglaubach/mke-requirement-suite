import numpy as np
import time
#
import dash_bootstrap_components as dbc
import dash_cytoscape as cyto


# from ReqTracker.core import graph_base

# add to path for testing
import sys, inspect, os
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)


from ReqTracker.core import helpers, graph_main, graph_base, schema

from ReqTracker.ui import helpers_ui, ui_state
from ReqTracker.ui.vis.annotation_vis import LAYOUTS_POSSIBLE, SIZEFUNS_POSSIBLE
from ReqTracker.ui.visualisation_main import graph_to_xy_size

log = helpers.log

A = helpers_ui.A


SIZEFUNS_ALLOWED = SIZEFUNS_POSSIBLE + ['25', '15', '5']

ALL_SYMBOLS = 'ellipse rectangle diamond triangle round-triangle star pentagon heptagon hexagon round-rectangle barrel round-diamond bottom-round-rectangle cut-rectangle rhomboid right-rhomboid round-pentagon round-hexagon concave-hexagon round-heptagon octagon round-octagon tag round-tag vee '.split()

# ALL_SYMBOLS = ['circle', 'square', 'diamond', 'cross', 'x', 'pentagon', 'hexagon', 'star-triangle-up', 'star-square', 'circle-x', 'square-cross', 'diamond-wide', 'star-diamond', 'hourglass', 'diamond-tall',  'bowtie', 'circle-cross', 'square-x', 'diamond-cross', 'diamond-x', 'cross-thin', 'x-thin', 'asterisk', 'hash', 'y-up', 'y-down', 'y-left', 'y-right', 'line-ew', 'line-ns', 'line-ne', 'line-nw', 'arrow-up', 'arrow-down', 'arrow-left', 'arrow-right', 'arrow-bar-up', 'arrow-bar-down', 'arrow-bar-left', 'arrow-bar-right']
CLASSSYMBOLS = {c.__name__:ALL_SYMBOLS[i] for i, c in enumerate(graph_main.classes)}
CLASSSYMBOLS[schema.RequirementContainer.__name__] = 'ellipse'
CLASSSYMBOLS[graph_main.ViewContainer.__name__] = 'round-pentagon'


default_stylesheet = [
    {
        'selector': 'node',
        'style': {
            'background-color': 'data(color)', #'#BFD7B5',
            'color': 'data(is_match)', 
            'label': 'data(label)',
            'width': 'data(size)',
            'height': 'data(size)',
            'shape': 'data(symbol)',
        }
    },
    # {
    #     'selector': 'node[is_match]',
    #     'style': {
    #         'color': '#FC0303', #'#BFD7B5',
    #     }
    # },
    {
        'selector': 'edge',
        'style': {
            'line-color': 'data(color)', #'#A3C4BC',
            'targetArrowShape': 'triangle',
            'curveStyle': 'bezier'
        }
    }
]


styles = {
    'container': {
        'position': 'fixed',
        'display': 'flex',
        'flex-direction': 'column',
        'height': '100%',
        'width': '100%'
    },
    'cy-container': {
        'flex': '1',
        'position': 'relative'
    },
    'cytoscape': {
        'position': 'absolute',
        'width': '100%',
        'height': '100%',
        'z-index': 999
    }
}


lyt = {
        'name': 'cose',
        'idealEdgeLength': 100,
        'nodeOverlap': 20,
        'refresh': 20,
        'fit': True,
        'padding': 30,
        'randomize': False,
        'componentSpacing': 100,
        'nodeRepulsion': 400000,
        'edgeElasticity': 100,
        'nestingFactor': 5,
        'gravity': 80,
        'numIter': 1000,
        'initialTemp': 200,
        'coolingFactor': 0.95,
        'minTemp': 1.0
    }


def edge2cyto(trg, src, color=None):
    if color:
        return {'data': {'source': src, 'target': trg, 'color': color}}
    else:
        return {'data': {'source': src, 'target': trg}}


def node2cyto(n, x, y, s=10):
    x, y = n.data['position'] if 'position' in n.data else x, y
    is_match = '#0000FF' if n.is_match else '#000000'
    return {'data': {'id': n.id, 'label': n.id, 'color':n.color, 'size':s, 'symbol': CLASSSYMBOLS[n.classname], 'is_match': is_match}, 'position': {'x': x, 'y': y}}


def get_cytodata(graph, network_plot_style, size_fun, scale_xy=None):

    Xn, Yn, labels, sizes = graph_to_xy_size(graph, network_plot_style, size_fun, need_edges=False, scaler=50., MAX_SIZE=50)

    if isinstance(sizes, (float, int)):
        sizes = [sizes] * len(graph)

    if network_plot_style == 'tree':
        Xn /= 2
        Yn *= 30
        Xn, Yn = Yn, Xn

    if scale_xy is None:
        s = 1 + (0.2 * ((800 / len(graph))-1))
        scale_xy = (s,s)

    if not isinstance(scale_xy, (tuple, list)):
        scale_xy = (scale_xy, scale_xy)

    scale_x, scale_y = scale_xy
    Xn *= scale_x
    Yn *= scale_y
        
    vn = graph.vn

    assert len(Xn) == len(Yn) == len(vn) == len(sizes), f'vectors have different lengthes! {Xn}!={Yn}!={vn}!={sizes}'
    

    nodes = [node2cyto(n, x, y, s) for n, x, y, s, in zip(vn, Xn, Yn, sizes)]
    edges = [edge2cyto(trgt, src, graph[src].color) for trgt, src in graph.get_edges(as_id=True)]
    
    return nodes, edges


def get_cyto_vis(graph_view:graph_main.GraphView, 
                    group_loose=True, 
                    sep_reqs=True, 
                    no_root=False, 
                    hide_reqs=True,  
                    state=None):
    
    if state is None:
        state = ui_state.get()

    hide_loose_docs = state.hide_loose_docs
    classes_to_plot = state.classes_to_plot
    network_plot_style = state.network_plot_style
    size_fun = state.size_fun
    scale_xy = None if state.scale_xy == 0 else state.scale_xy

    ret = (None, None)

    try:

        g = graph_view
        log.debug(f'plotting graph: {g.name}')


        if hide_loose_docs and (g.name.startswith('groupby') or g.name.startswith('reversed')):
            log.debug('PLOTTING: not hiding any loose docs because the view is a grouped view')
        elif hide_loose_docs:
            f = lambda node: not (isinstance(node, schema.Document) and not any((p.do_serialize for p in node.linked)))
            g2 = g.filt_by(g.name + '|noldocs', f, mode='keep')
            g = g2 if len(g2) > 1 else g
            log.debug('PLOTTING: Before hiding loose docs: N={} after M={}'.format(len(graph_view), len(g)))     
        else:
            log.debug('PLOTTING: Not Excluding Anything but grouping loose ones instead')


        if classes_to_plot and (g.name.startswith('groupby') or g.name.startswith('reversed')):
            log.debug('PLOTTING: not filtering by classes because the view is a grouped view')
        elif classes_to_plot and not set(classes_to_plot) == set((cls.__name__ for cls in graph_main.classes)):
            classes_to_plot = set(classes_to_plot)
            filtfun = lambda node: type(node).__name__ in classes_to_plot or type(node).__name__ == graph_main.ViewContainer.__name__
            n = len(g)
            g = g.filt_by(g.name + '|classfilt', filtfun, mode='keep')
            log.debug('PLOTTING: Before excluding by classtype: N={} after M={}'.format(n, len(g)))
        else:
            log.debug('PLOTTING: not filtering by classes because all are ON')


        if not g.name.startswith('groupby') and not g.name.startswith('reversed'):
            if group_loose:
                g = g.group_loose()
            
            if sep_reqs:
                g = g.sep_reqs()
            
            if hide_reqs:
                f = lambda node: not type(node).__name__ in (schema.VerificationRequirement.__name__, schema.Requirement.__name__)
                g = g.filt_by(g.name + '|hidereq', f, mode='keep')

        if no_root:
            g.view.delete_edges([(g.root_node.id, c.id) for c in g.root_node.children])

        ret = get_cytodata(g, network_plot_style, size_fun, scale_xy=scale_xy)
        

    except Exception as err:
        log.error(err, exc_info=True)
        s = "ERROR while plotting! {}".format(err)
        alert = A(s, color='danger')
        ret = (alert, None)
        ui_state.add_log_alert(alert)

    return ret




# dash.register_page('')

def get_fig(graph_view, 
            fixed=False, 
            figsize=None, 
            no_root=None,
            group_loose=True, 
            sep_reqs=True, 
            hide_reqs=True,  
            state=None):
    
    if state is None:
        state = ui_state.get()

    figsize = state.figsize if figsize is None else figsize

    stl = {'width': '100%', 'height': figsize}
    if fixed:
        stl = {**styles['cytoscape'], **stl}
    
    log.debug(f'get_fig, {graph_view.name}!')

    if no_root is None:
        no_root = graph_view.name.startswith('search:')

    l = lyt if fixed else {'name': 'preset', 'fit':True}

    

    if len(graph_view):
        edges, nodes = get_cyto_vis(graph_view, no_root=no_root, hide_reqs=hide_reqs, sep_reqs=sep_reqs, group_loose=group_loose, state=state)

        fig = cyto.Cytoscape(
            responsive = True if fixed else False,
            style= stl,
            id={'type': 'cytoscape-events', 'index': time.time()},
            layout=l,
            elements=edges + nodes,
            stylesheet=default_stylesheet,
            boxSelectionEnabled=True if fixed else False,
        )
        
    else:
        fig = dbc.Alert(f'NO DATA IN GRAPH: {graph_view}', color="warning")

    return fig
