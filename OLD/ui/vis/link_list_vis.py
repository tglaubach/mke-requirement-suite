
from ReqTracker.io_files import nextcloud_interface
from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

import uuid
import string

import urllib

from ReqTracker.core import node_factory

printable = set(string.printable)

from ReqTracker.core import schema, graph_main, graph_base, helpers

log = helpers.log

def mkel(id_, *args):
    txt = ' | '.join(args)
    s = {'marginLeft': '10px', 'marginTop': '1px', "color": "black", 'marginLeft': '10px', 'marginTop': '1px', "width": "80%"}
    return dcc.Link(id_ + ' | ' + txt, 
                  href=f"/getid/{urllib.parse.quote_plus(id_)}", 
                  target="_self", 
                  style=s)
        

def mk_node_list_entry(node, show_children=True, vm = '2px'):

    try:
        s = {
            "marginLeft": f"10px", 
            "marginRight": vm, 
            "marginTop": vm,  
            "marginBottom": vm, 
            "border":"1px black solid", 
            "borderRadius": "5px", 
            "height": "100%",
        }


        if hasattr(node, 'color'):
            s['backgroundColor'] = node.color

        if issubclass(type(node), schema.BaseVersionedDocument):
            desc_text = node.title
        else:
            desc_text = node.text.strip()
            desc_text = ''.join(filter(lambda x: x in printable, desc_text))

        txt = desc_text.strip().split('\n')[0]


        entries = [mkel(node.id, node.status.name, txt)] 
        if show_children:
            entries += [mk_node_list_entry(el) for el in node.children]
        
        return html.Div(entries, style=s)
    
    except Exception as err:        
        log.error(err, exc_info=True, stack_info=True)
        log.error('ERROR: {} {}'.format(node.id, err))
        return dbc.Container(str(err))
    


def mk_link_lst(graph:graph_main.Graph, style_lst, maxdepth=None, filtfun=None):

    if style_lst == 'flat':
        nodes = graph.get_nodes_view()
        show_children = False
    else:
        nodes = graph.get_highest_nodes()
        show_children = True

    if not filtfun is None:
        log.debug(f'mk_link_lst before filtering: {len(nodes)}')
        nodes = [x for x in nodes if filtfun(x)]

    log.debug('mk_link_lst -> {} {}'.format(style_lst, len(nodes)))
    
    children = [mk_node_list_entry(el, show_children=show_children, vm='5px') for el in nodes]
    
    # if n_columns  > 1:
    #     cols = split(children, n_columns)
    #     children = dbc.Row([dbc.Col(c) for c in cols])
        
    return children

