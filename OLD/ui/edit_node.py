import collections

import os

from dash import Dash, dcc, html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go


from ReqTracker.core import helpers, graph_base, schema, node_factory
from ReqTracker.ui.helpers_ui import id2lnk_dcc, A, mk_href
from ReqTracker.ui import ui_state

log = helpers.log


sb = {"border":"1px black solid", "borderRadius": "5px", "marginLeft": "15px", "marginRight": "35px", "margin": "5px"}


def mk_header(el):
    ttl = el.title
    if ttl:
        ttl = ' | ' + ttl
    return html.Div([ html.B("Edit: "), id2lnk_dcc(el.id), '{} ({})'.format(ttl, el.classname)])



def get_update_dc(el, graph):

    

    keys = graph.get_viewnodes(as_id=False)
    keys = [{"label": str(n), "value": n.id} for n in keys]
    status = el._status.name


    dc = el.get_row()

    keys = graph.get_viewnodes(as_id=False)
    keys = [{"label": str(n), "value": n.id} for n in keys]



    n_max = 20
    links_parents = []
    links_parents = ["Parents: "] + helpers.intersperse([id2lnk_dcc(e.id) for e in el.parents], '/')
    if len(links_parents) > n_max:
        links_parents, links_parents_raw = [], links_parents
        for sublist in helpers.splitw(links_parents_raw, n_max):
            links_parents.append(dbc.Label(html.Div(sublist)))
    else:
        links_parents = [dbc.Label(html.Div(links_parents))]

    links_children = []
    links_children = ["Children: "] + helpers.intersperse([id2lnk_dcc(e.id) for e in el.children], '/')
    if len(links_children) > n_max:
        links_children, links_children_raw = [], links_children
        for sublist in helpers.splitw(links_children_raw, n_max):
            links_children.append(dbc.Label(html.Div(sublist)))
    else:
        links_children = [dbc.Label(html.Div(links_children))]
    
    notes_md = [dcc.Markdown('**{}**:\n{}'.format(k, v)) for k, v in el.notes.items()]

    txt = el.text

    ret = {
        'id': el.id,
        'header': mk_header(el),
        'mdtxt': txt,
        'itxt': txt,
        'dstatus': status,
        'itags': dc['tags'].split(),
        'notes_md': notes_md,
        'keys': keys,
        'c_links': links_children,
        'dchildren': [e.id for e in el.children],
        'p_links': links_parents,
        'dparents': [e.id for e in el.parents],
    }
    return ret


def mk_input_grp(el, graph, tags_possible=[], f=None):

    if f is None:
        f = lambda k: {"type": 'inpg_' + k, "index": el.id}

    if ui_state.IS_READONLY:
        stl = {'display': 'none'}
    else:
        stl = {}
    dis = (not el.do_serialize) or ui_state.IS_READONLY

    # log.info(('\n'*3) + f'disabled={dis} ((not {el.do_serialize}) or {ui_state.IS_READONLY})' + '\n'*3)

    dc = get_update_dc(el, graph)
    
    log.debug(f'DISABLED: {(el, dis)}')

    md = dcc.Markdown(dc['mdtxt'], id=f('mdtxt'), style={'minHeight': 100})

    if dis:
        tags = [dbc.Badge(t, color='Secondary', pill=True, className="me-1") for t in el.tags]
    else:
        tags = []

    els = [      
        dbc.Row([
            dbc.Label("General: "),
            dbc.InputGroup([
                dbc.InputGroupText("status:", style={'minWidth': 150}),
                dbc.Badge(el.status.name, color=graph_base.colors[el.status]) if dis else None,
                dbc.Select(options=[x.name for x in graph_base.STATUS], value=dc['dstatus'], id=f('dstatus'), disabled=dis, style=stl),
            ], style={'margin': '2px'}, size="sm"),
            dbc.InputGroup([
                dbc.InputGroupText("tags:", style={'minWidth': 150}),
                *tags,
                dcc.Dropdown(options=list(tags_possible), value=dc['itags'], id=f('itags'), multi=True, placeholder="tags...", style={**{'minWidth': 300}, **stl}, disabled=dis),
            ], style={'margin': '2px', 'width': '100%'}, size="sm"),
        ], style=sb),
        dbc.Row([
            md if dis else None,
            dbc.Tabs(
            [
                dbc.Tab([
                    md if not dis else None
                ], label="Text", id=f('tmdtxt'), tab_id='text_show'),
                dbc.Tab([
                    dbc.Textarea(value=dc['itxt'], id=f('itxt'), style={'minHeight': 100}, disabled=dis),
                ], label="Edit Text", id=f('titxt'), tab_id='text_edit'),
            ], active_tab='text_show', style=stl
        ),   
        ], style=sb),

        dbc.Row([
                dbc.Label("Notes"),
                html.Div(dc['notes_md'], id=f('notes_md')),
                dbc.InputGroup([
                    dbc.InputGroupText("new note:", style={'minWidth': 150}),
                    dbc.Input(type="text", placeholder="(ENTER) type a text to add as note", debounce=True, value='', id=f('innote'), size='sm', style={'minWidth': 300}, disabled=dis), 
                ], style={**{'margin': '2px'}, **stl}, size="sm"),

        ], style=sb),

        dbc.Row([
            dbc.Label("Add Files: "),
            dbc.InputGroup([
                    dbc.InputGroupText("add file from nc-path:", style={'minWidth': 150}),
                    dbc.Input(type="text", placeholder="(ENTER) insert filepath to add document as node. e.G: /Shared/.../mydoc.txt", debounce=True, id=f('inewfile'), size='sm', style={'minWidth': 300}), 
                ], style={'margin': '2px'}, size="sm"),
        ], style={**sb, **stl}),
        dbc.Row([
            html.Div(dc['c_links'], id=f('c_links')),
            dbc.InputGroup([
                    dbc.InputGroupText("add new child node:", style={'minWidth': 150}),
                    dbc.Input(type="text", placeholder="(ENTER) insert new node ID to add ... e.G: VAS.TEST", debounce=True, id=f('iaddchild'), size='sm', style={'minWidth': 300}, disabled=dis), 
                ], style={**{'margin': '2px'}, **stl}, size="sm"),
            dcc.Dropdown(options=dc['keys'], value=dc['dchildren'], id=f('dchildren'), multi=True, style={**{'minWidth': 300, 'margin': '2px'}, **stl})
        ], style=sb),
        dbc.Row([
            html.Div(dc['p_links'], id=f('p_links')),
            dcc.Dropdown(options=dc['keys'], value=dc['dparents'], id=f('dparents'), multi=True, style={**{'minWidth': 300, 'margin': '2px'}, **stl})
        ], style=sb),
        dcc.Store(id=f('dstore'))
    ]

    return els




def make_inp_modal(el, graph, tags_possible=[], f=None):
    
    if f is None or f == 'None':
        f = lambda k: {"type": 'inpm_' + k, "index": el.id}

    dis = (not el.do_serialize) or ui_state.IS_READONLY
    
    confirm = 'Are you sure you want to delete the node with id={} (It has N={} children and M={} parents)'.format(el.id, len(el.children), len(el.parents))

    children = [
            dbc.ModalHeader(mk_header(el), id=f('header')),
            dbc.ModalBody(
                dcc.Store(f('mem_id'), data=el.id),
                mk_input_grp(el, graph, tags_possible=tags_possible)
            ),
            dbc.ModalFooter([
                    dcc.ConfirmDialogProvider(
                        dbc.Button("Delete Node", color='danger'),
                        id=f('delete'),
                        message=confirm
                    ),
                    
                    dbc.Button("Reload (no save)", color='secondary', id=f('refresh')),
                    dbc.Button("Cancle (Close no save)", color='secondary', id=f('close')),
                    dbc.Button("Apply", color="primary", id=f('apply'), disabled=dis),
                    dbc.Button("OK (Apply and Close)", color="success", id=f('okclose'), disabled=dis)
            ])]
        
    
    return children
    




def save_node_changes(el, change_dc, graph_nodes, nextcloud_login_data):
        
    try:
        log.debug('save_node_changes -> {}'.format(el.id))
        # log.debug(change_dc)

        dstatus = change_dc['status']
        itxt = change_dc['text']
        itags = change_dc['itags']
        inpg_dfiles = change_dc['newfiles_from_index']
        dchildren = change_dc['children']
        dparents = change_dc['parents']
        
        tags = [e.strip() for e in itags]
        tags = [x for x in tags if x]

        el.set_status(dstatus)

        if el.text != itxt:
            el.set_text(itxt)

        if collections.Counter(el.tags) != collections.Counter(tags):
            el.tags = tags

        if inpg_dfiles:
            for nid in inpg_dfiles:
                node = node_factory.construct_doc(nid, nextcloud_login_data, graph_nodes, on_exist='get')
                dchildren += [node.id]

        ochildren = [e.id for e in el.children]
        if collections.Counter(ochildren) != collections.Counter(dchildren):
            for e in ochildren:
                el.del_child(graph_nodes[e])
            for e in dchildren:
                el.add_child(graph_nodes[e])
                
        oparents = [e.id for e in el.parents]
        if collections.Counter(oparents) != collections.Counter(dparents):
            for e in oparents:
                el.del_parent(graph_nodes[e])
            for e in dparents:
                el.add_parent(graph_nodes[e])

        return A('{} | sucessfully updated!'.format(el.id), color="success")
    except Exception as err:
        log.error(err, exc_info=True, stack_info=True)
        return A("{} | ERROR update_node: {}".format(el.id, err), color="danger")

