
import numpy as np
import pandas as pd
import textwrap


# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

from ReqTracker.core import helpers, graph_main, graph_base, schema

from ReqTracker.ui.vis.annotation_vis import make_annotation_plot, LAYOUTS_POSSIBLE, SIZEFUNS_POSSIBLE
from ReqTracker.ui.vis.hirachy_vis import make_sunburst_plot, make_treemap_plot


log = helpers.log


SIZEFUNS_ALLOWED = SIZEFUNS_POSSIBLE + ['25', '15', '5']





no_data_template = go.layout.Template()
no_data_template.layout.annotations = [
    dict(
        name="do_data watermark",
        text="NO DATA TO POT!",
        textangle=-30,
        opacity=0.1,
        font=dict(color="black", size=100),
        xref="paper",
        yref="paper",
        x=0.5,
        y=0.5,
        showarrow=False,
    )
]
def mk_error_emplate(s):
    tmp = go.layout.Template()
    tmp.layout.annotations = [
        dict(
            name=s,
            text='\n'.join(textwrap.wrap(s)),
            textangle=0,
            opacity=0.8,
            font=dict(color="red", size=20),
            xref="paper",
            yref="paper",
            x=0.5,
            y=0.5,
            showarrow=False,
        )
    ]
    return tmp


def graph_to_xy_size(graph, lyout='tree', size_fun='pagerank', need_edges=True, scaler=1., MAX_SIZE= 25.):
        
    # lyout = 'tree' # 'kk', 'auto'
    log.debug((lyout, size_fun))
    g = graph.view

    if size_fun not in SIZEFUNS_POSSIBLE and size_fun.isdigit():
        sizes = float(size_fun) # scalar number
    elif size_fun in SIZEFUNS_POSSIBLE:
        if size_fun.startswith('log(') and size_fun.endswith(')'):
            size_fun = size_fun[4:-1]
            apply_log=True
        else:
            apply_log = False
        fun = getattr(g, size_fun)
        sizes = np.array(fun())


        sizes += np.min(sizes)
        sizes /= np.max(sizes)
        sizes *= MAX_SIZE

        i_root = g.vs['name'].index(graph.root_node.id)
        sizes[i_root] = 60
        nids = [n.id for n in graph.nodes if isinstance(n, schema.RequirementContainer) or isinstance(n, graph_main.ViewContainer)]
        sizes[np.isin(g.vs['name'], nids)] = 35

        if apply_log:
            sizes = MAX_SIZE * np.log10(np.abs(sizes)+1 / 1)
        
    else:
        raise KeyError(f'the given sizefun {size_fun} is not recognized. Allowed are: ' + ', '.join(SIZEFUNS_POSSIBLE))
    
    # print(sizes)
    # v_label = g.vs['name']
    v_label = [str(graph[id_]) for id_ in g.vs['name']]
    
    nr_vertices = len(v_label)
    if lyout == 'point':
        lay = [(0.,0.)] * len(v_label)
    else:
            
        try:
            lay = g.layout(lyout)
        except Exception as err:
            # log.error(f'ERROR while trying to set layout "{lyout}" falling back to "auto" (err: {err})')
            raise ValueError(f'ERROR while trying to set layout "{lyout}"')


    if need_edges:

        E = g.get_edgelist() # list of edges

        position = {k: lay[k] for k in range(nr_vertices)}
        Y = [lay[k][1] for k in range(nr_vertices)]
        M = max(Y)    

        L = len(position)
        Xn = [position[k][0] for k in range(L)]
        Yn = [2*M-position[k][1] for k in range(L)]
        Xe = []
        Ye = []
        for edge in E:
            Xe+=[position[edge[0]][0],position[edge[1]][0], None]
            Ye+=[2*M-position[edge[0]][1],2*M-position[edge[1]][1], None]


        return Xn, Yn, Xe, Ye, v_label, sizes
    else:
        position = np.array([lay[k] for k in range(nr_vertices)])
        M = np.max(position[:,1])
        position[:,1] = 2*M - position[:,1]
        position *= scaler
        Xn = position[:,0]
        Yn = position[:,1]
        return Xn, Yn, v_label, sizes

def update_figure(g:graph_main.Graph, plotstyle, maxdepth=10, figsize=1600, network_plot_style='tree', size_fun='25', hovermode='str', scale_xy=None):

    log.debug('update -> {} {} {}'.format(plotstyle, g.get_len_nodes_view(), maxdepth))

    fig = None

    color_col = 'status'

    # print('HNS: ', hns)
    if g.name.startswith('search:'):
        color_col = 'is_match'

    if not g.get_len_nodes_view():
        fig = px.pie(pd.DataFrame([]))
        fig.update_layout(template=no_data_template)
    elif plotstyle == 'graph':
        if scale_xy is None: 
            scale_xy = 1.
            
        fig = make_annotation_plot(g, graph_to_xy_size, lyout=network_plot_style, size_fun=size_fun, hovermode=hovermode, flip_xy=network_plot_style == 'tree', scale_xy=scale_xy)
    elif plotstyle == 'sunburst':
        fig = make_sunburst_plot(g, color_col, maxdepth=maxdepth)
    elif plotstyle == 'treemap':
        fig = make_treemap_plot(g, color_col, maxdepth=maxdepth)
    elif plotstyle == 'pie':
        data = pd.DataFrame([n.to_dict(add_parents=False) for n in g.get_nodes_view()])
        states = data['_status'].unique()
        dc_colors = {s.name:c for s, c in graph_base.colors.items()}
        df = []
        for state in states:
            df.append({
                'status': state,
                'count': (data['_status'] == state).sum(),
                'color': dc_colors[state]
            })
        #title='Item counts by status',

        fig = px.pie(pd.DataFrame(df), values='count', names='status', color='color')
        fig.update_traces(textposition='inside', textinfo='label+percent')
        fig.update_layout(height=400)
    else:
        raise KeyError(plotstyle)
    
    if fig and not fig['data']:
        fig.update_layout(template=no_data_template)
        
    if fig and plotstyle != 'pie':
        fig.update_layout(clickmode='event') # +select

    if not fig:
        return {'layout': {}, 'data': []}
    else:
        fig.update_layout(
            height=figsize,
        )
            
        return fig

# def get_output_vis(graph_view:graph_main.GraphView, display_style, display_style_lst, maxdepth=None, network_plot_style='tree', 
#                    figsize=1600, size_fun='25', hide_loose_docs=True, classes_to_plot=None, group_loose=True, sep_reqs=True, no_root=False, hide_reqs=True):



# def get_output_vis(graph_view:graph_main.GraphView, group_loose=True, sep_reqs=True, no_root=False, hide_reqs=True):

    
#     display_style = ui_state.display_style
#     display_style_lst = ui_state.display_style_lst
#     maxdepth = ui_state.maxdepth
#     network_plot_style = ui_state.network_plot_style
#     figsize = ui_state.figsize
#     size_fun = ui_state.size_fun
#     hide_loose_docs = ui_state.hide_loose_docs
#     classes_to_plot = ui_state.classes_to_plot
    


#     alerts = []

#     childs = []
#     fig = {'layout': {}, 'data': []}
    
#     graph_display = {'display':'block'}
#     try:
#         if display_style == 'list':
#             childs = mk_listvis(graph_view, display_style_lst, maxdepth=maxdepth)
#             graph_display = {'display':'none'}
#         elif display_style == 'link_list':
#             childs = mk_link_lst(graph_view, display_style_lst, maxdepth=maxdepth)
#             graph_display = {'display':'none'}
#         elif display_style.startswith('table'):
#             childs = mk_datatable_lst(graph_view, display_style_lst, maxdepth=maxdepth)
#             graph_display = {'display':'none'}
#         else:
#             g = graph_view
#             log.debug(f'plotting graph: {g.name}')


#             if hide_loose_docs and (g.name.startswith('groupby') or g.name.startswith('reversed')):
#                 log.debug('PLOTTING: not hiding any loose docs because the view is a grouped view')
#             elif hide_loose_docs:
#                 f = lambda node: not (isinstance(node, schema.Document) and not any((p.do_serialize for p in node.linked)))
#                 g2 = g.filt_by(g.name + '|noldocs', f, mode='keep')
#                 g = g2 if len(g2) > 1 else g
#                 log.debug('PLOTTING: Before hiding loose docs: N={} after M={}'.format(len(graph_view), len(g)))     
#             else:
#                 log.debug('PLOTTING: Not Excluding Anything but grouping loose ones instead')


#             if classes_to_plot and (g.name.startswith('groupby') or g.name.startswith('reversed')):
#                 log.debug('PLOTTING: not filtering by classes because the view is a grouped view')
#             elif classes_to_plot and not set(classes_to_plot) == set((cls.__name__ for cls in graph_main.classes)):
#                 classes_to_plot = set(classes_to_plot)
#                 filtfun = lambda node: type(node).__name__ in classes_to_plot or type(node).__name__ == graph_main.ViewContainer.__name__
#                 n = len(g)
#                 g = g.filt_by(g.name + '|classfilt', filtfun, mode='keep')
#                 log.debug('PLOTTING: Before excluding by classtype: N={} after M={}'.format(n, len(g)))
#             else:
#                 log.debug('PLOTTING: not filtering by classes because all are ON')


#             if not g.name.startswith('groupby') and not g.name.startswith('reversed'):
#                 if group_loose:
#                     g = g.group_loose()
                
#                 if sep_reqs:
#                     g = g.sep_reqs()
                
#                 if hide_reqs:
#                     f = lambda node: not type(node).__name__ in (schema.VerificationRequirement.__name__, schema.Requirement.__name__)
#                     g = g.filt_by(g.name + '|hidereq', f, mode='keep')


#             if no_root:
#                 g = g.to_view()
#                 g.view.delete_edges([(g.root_node.id, c.id) for c in g.root_node.children])

#             fig = update_figure(g, display_style, 
#                                 maxdepth=maxdepth, 
#                                 figsize=figsize, 
#                                 network_plot_style=network_plot_style, 
#                                 size_fun=size_fun)
            
#             graph_display = {'display':'block'}

#     except Exception as err:
#         log.error(err, exc_info=True)
#         s = "ERROR while plotting! {}".format(err)
#         alerts.append(A(s, color='danger'))
#         fig = px.pie(pd.DataFrame([]))
#         fig.update_layout(height=200,template=mk_error_emplate(s))
        

#     return fig, childs, graph_display, alerts