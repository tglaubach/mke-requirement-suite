import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
sys.path.insert(0, parent_dir)


from ReqTracker.core import graph_main, graph_base, schema
from ReqTracker.io import graphdb_io


if __name__ == '__main__':   
    pth_in = {
        'nodes': r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite',
        'fileindex': r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\fileindex.sqlite'
    }

    # pth_out = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db_test3.sqlite'


    target = 'http://10.98.76.45:8020/'
    # target = 'http://localhost:8050/'

    for tablename, pth in pth_in.items():
        g = graph_main.Graph(pth, do_autosave=False)
        g.set_new_datasource(target, auth=("tglaubach", "withhave41"), tablename=tablename)
        # g.set_new_datasource(pth_out, make_source_async=True)
        g.push()
        dc = g.serialize()
        graph_main.Graph.deserialize(dc, g, on_has_changes='overwrite')
        dc = g.serialize()




# if __name__ == '__main__':    
#     extractor = main()
#     extractor.runInParallel(numProcesses=2, numThreads=4)



# g = graph_main.Graph.load(pth_in)

# files = []
# for n in g.nodes:
#     versions = n.data['versions']
#     doc_ids = set([schema.Document.fun_path2id(v['path']) for v in versions])
#     doc_id = next(iter(doc_ids))
#     print(doc_id, '-->', len(doc_ids))
#     assert len(doc_ids) == 1, 'ambigious filename found!' + str(doc_ids)
#     dc = n.to_dict(add_parents=False)
#     dc['id'] = doc_id
#     files.append(dc)



# graphdb_io.save_loose(pth_sqlite, files)


#  
# print(files)