
import json

import unittest
import json
import igraph as ig
import numpy as np
import pandas as pd

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
sys.path.insert(0, parent_dir)


from ReqTracker.core import graph_main, graph_base, node_factory

# from ReqTracker.io import api_io

#%%
pth = r"C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.json"

# with open(pth, 'r') as fp:
#     dc = json.load(fp)
#     g = main.Graph.deserialize(dc)

# g.filt_by_dish('##', mode='lineage')

# print(len(g.get_highest_nodes()))

# for i, n in enumerate(g.get_highest_nodes()):
#     print(i, n.id)
import difflib

from ReqTracker.io import api_io, graphdb_io
from ReqTracker.core import schema, helpers

helpers.log.setLevel('DEBUG')
import time



# os.path.dirname(DEFAULT_CONFIG_PATH)

from ReqTracker.ui import ui_state

state = ui_state.get()


# ui_state.db_path = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db_temp.sqlite'

# ui_state.NC_PUSH_PATH = '/temp/databases'



# with ui_state.GetGraphHold() as g:
#     g.info()
#     id_ = 'VAS.TEST_' + str(round(time.time()))
#     g.add_node(g.construct(id_))
#     print(g[id_].to_dict(True))
#     print(g.get_changes(dt_as_string=True))
#     print(g.get_filesinks())


# print(ui_state.ncfs.cache)

# print('-'*100 + '\nDONE' + '-'*100)

# # k='VS.TEST4'
# url = 'http://127.0.0.1:8050'

# # api_io.node.add_node(url, id=k, text='SOME TEST SCRIPT')
# # api_io.node.add_note(url, k, 'This is only for testing!')
# # api_io.node.set_status(url, k, base.STATUS.IN_PREPARATION.name)

# pth = 'fileindex.txt'
# with open(pth, 'r') as fp:
#     lines = [line.strip() for line in fp.readlines() if not line.endswith('/')]

# matches = [line for line in lines if schema.Document.fun_match(line) or os.path.basename(line).startswith('316-') or os.path.basename(line).startswith('SKA-TEL')]

# # matches = matches[:10]

# import getpass

# pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'
# pth = 'http://10.98.76.45:8020/'
# graph = graph_main.Graph(pth)


# g = graph.view
# import getpass

# pth = 'http://10.98.76.45:8020/'
# auth = ('tglaubach', getpass.getpass('PW: '))
# graph_nodes = graph_main.Graph(pth, auth=auth, tablename='nodes')
# graph_fileindex = graph_main.Graph(pth, auth=auth, tablename='fileindex')


# dc1 = graph_nodes.serialize()
# dc2 = graph_fileindex.serialize()

# print(len(dc2['nodes']), len(dc1['nodes']))
# print(len(dc2['changes']), len(dc1['changes']))

# dc1['nodes'] += dc2['nodes']
# dc1['changes'] += dc2['changes']
# dc1['changes'].sort(key=lambda x: x[0])

# print(len(dc1['nodes']), len(dc1['changes']))


# pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'
# g = graph_main.Graph.deserialize(dc1)
# g.set_new_datasource(pth)

# g.push()


g = graph_main.Graph(r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite')

a = g['MKE-316-060000-ODC-TP-0026'].get_text()
print(a)

txts = [n.get_text() for n in g.filtt('MKE') if issubclass(type(n), schema.BaseVersionedDocument)]
print(txts[10])

g['MKE-316-060000-ODC-TP-0026'].get_text()
# g.set_new_datasource('http://10.98.76.45:8020/')
# g.push()

# g.group_loose()

# g = graph.view


# import openpyxl

# pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\requirements_raw.xlsx'
# wb = openpyxl.load_workbook(pth, read_only=True, keep_vba=False, data_only=True)
# dc = dict(wb[wb.sheetnames[0]].values)


# pth = 'http://10.98.76.45:8020/'
# pth = 'http://localhost:8050/'
# pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'


# g = graph_main.Graph(pth, auth='login')

# g.search("POC", k_max=len(g.nodes), mode='flat')

# print(set().union(*[n.tags for n in graph.nodes]))


# for k, v in dc.items():
#     if k:
#         k = k.strip()
#         f = 'FOUND' if k in graph else ''
        
#         print(f, k.ljust(20), helpers.limit_len(v.split('\n')[0], 20))
#         if k not in graph:
#             n = graph_main.construct(dc_node={'id': k, 'text': v.strip()})
#             print(n.to_dict(add_parents=False))

#             graph.add_node(n)

# graph.push_changes()

# g_loose = graph.deserialize(graphdb_io.get_loose(pth))


# # nodes = [n for n in graph.filt_by_dish('SKA19', mode='flat').get_nodes_view() if n.do_serialize]

# def filt_docmatch(n):
#     if not isinstance(n, schema.Document):
#         s = json.dumps(n.to_dict(add_parents=False))
#         matches = schema.Document.fun_match(s)
#         if matches:
#             return True
#     return False
    
# nodes = graph.filt_by('docmatch',filt_docmatch, mode='flat').get_viewnodes()

# for n in nodes:
#     ndc = n.to_dict(add_parents=False)
#     matchs = schema.Document.fun_path2id(json.dumps(ndc), ret_all_matches=True)
#     print(ndc)
    
#     for match in matchs:
#         if match in g_loose:
#             mdc = g_loose[match].to_dict(add_parents=False)
#             s = ' |     FOUND! |' + str(g_loose[match])
#             if match not in graph:
#                 graph.add_node(graph_main.construct(mdc))
#                 print('ADDING!', mdc['id'])
#             if match not in [x.id for x in graph.get_children(n.id)]:
#                 graph.add_edge((n.id, mdc['id']))
#                 print('ADDING!', (n.id, mdc['id']))
#         else:
#             closest = difflib.get_close_matches(match, g_loose.get_viewnodes(as_id=True), 3, 0.0)
#             s = ' | NOT FOUND! | closest_matches: -> ' + ' '.join(closest)

#         print(match + s)
#     print('-'*100)
# # graph.del_nodes(nodes, remove=False)
# graph.push_changes(pth)

# # usr = input('username: ')
# # pw_nc = getpass.getpass('password (Nextcloud): ')
# # pw_rs = getpass.getpass('password (Req Suite): ')


# # graph = graph_main.Graph()
# # for i, line in enumerate(matches, 1):
# #     doc = file_upload_factory.construct(line, (usr, pw_nc), graph=graph, on_exist='get', db_path=r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite')

# # for node in graph.nodes:
# #     print(i, node)
# #     print(node.to_dict(add_parents=False))
# #     print('-'*20)




# url = 'http://10.98.76.45:8020'
# # nodes = [node.to_dict(add_parents=False) for node in graph.nodes]
# nodes = graphdb_io.get_loose(r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite')
# interface = api_io.api(url, (usr, pw_rs))
# interface.add_nodes_loose(nodes)
# graph.save('loose_nodes.json')

# api_io.save(pth, g.serialize())

# #%%
# xlsx_io.save(current_dir + '/req_db.xlsx', g)

# g2 = xlsx_io.load(current_dir + '/req_db.xlsx')

# g2.pprint()

# print(g2.get_changes())

#%%



    
# g.clear_search()
# g.filt_by_dish('##')

    
# #%%
# g.set_view_main()
# g.filt_by_design()
# nodes = g.get_nodes_view(as_id=True)

# #%%
# # g.set_view_main()
# # g.pprint()
# pth = current_dir + '/export_req_db.xlsx'

# with open(pth, 'wb') as fp:
#     xlsx_io.save_xlsx(fp, g)
# # xlsx_io.save_xlsx(current_dir + '/export_req_db.xlsx', g)


# import io

# x = io.BytesIO()
# xlsx_io.save_xlsx(x, g)
# x.seek(0)
# print(x.read())
# xlsx_io.save_xlsx(current_dir + '/req_db.xlsx', g)

# g2 = xlsx_io.load_xlsx(current_dir + '/req_db.xlsx')
# #%%

# # with open(pth, 'rb') as fp:
# #     g2 = xlsx_io.load_xlsx(fp)

# # g2.pprint()

# g2.to_plot_recs()


