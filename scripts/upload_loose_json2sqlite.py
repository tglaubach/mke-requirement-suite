import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
sys.path.insert(0, parent_dir)


from ReqTracker.core import graph_main, graph_base, schema
from ReqTracker.io import graphdb_io

pth_in = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'
pth_out = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\fileindex.sqlite'



g = graph_main.Graph.deserialize(graphdb_io.get_loose(pth_in))
g.save(pth_out)



# g = graph_main.Graph.load(pth_in)

# files = []
# for n in g.nodes:
#     versions = n.data['versions']
#     doc_ids = set([schema.Document.fun_path2id(v['path']) for v in versions])
#     doc_id = next(iter(doc_ids))
#     print(doc_id, '-->', len(doc_ids))
#     assert len(doc_ids) == 1, 'ambigious filename found!' + str(doc_ids)
#     dc = n.to_dict(add_parents=False)
#     dc['id'] = doc_id
#     files.append(dc)



# graphdb_io.save_loose(pth_sqlite, files)


#  
# print(files)