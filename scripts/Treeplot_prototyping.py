#!/usr/bin/env python
# coding: utf-8

# In[1]:


import re
import textwrap

import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt


import openpyxl

import plotly.graph_objects as go



# In[2]:


class TreeNode():
    spacing_x = 1
    spacing_y = 0.25
    
    def __init__(self, node_id, text='', hovertext='', parent=None, font_size=10, font_color='rgb(0,0,0)', color='#6175c1'):
        self.id = node_id
        self.text = text if text else node_id
        self.hovertext = hovertext
        self.details = None
        self.children = []
        self.parent = parent

        # for annotations
        self.font_color = font_color
        self.font_size = font_size
        self.color = color
    
    
    @property
    def n_sub(self):
        return len(self.children)
    
    @property
    def is_onlychild(self):
        return False if self.parent is None or len(self.parent.children) != 1 else True
    
    @property
    def depth(self):
        if self.parent is None:
            return 0
        else:
            return self.parent.depth+1
    
    @property
    def width(self):
        return TreeNode.spacing_x
    
    @property
    def height(self):
        if self.n_sub:
            return np.sum([c.height for c in self.children]) + TreeNode.spacing_y
        else:
            return TreeNode.spacing_y
    
    def get_older_sibling(parent, child):
        assert child in parent.children, '{} is not a child of {}'.format(parent, child)
        i_child = parent.children.index(child)
        # print(i_child)
        return parent.children[i_child-1] if i_child > 0 else None
        
    @property
    def high_n(self):
        if self.parent is None:
            return None
        else:
            return self.parent.get_older_sibling(self)

            
    @property
    def x0(self):
        return TreeNode.spacing_x * self.depth

    @property
    def y0(self):
        if self.parent is None:
            return 0
        hn = self.high_n
        if hn is None:
            return self.parent.y0 + TreeNode.spacing_y*0.5
        else:
            return hn.y0 + hn.height
    
    @property
    def wh(self):
        return np.r_[self.width, self.height]
    
    @property
    def xy0(self):
        return np.r_[self.px, self.py]
    
    @property
    def xy1(self):
        return self.xy0 + self.wh
    
    @property
    def px(self):
        return self.x0 + self.width * 0.5
    
    @property
    def py(self):
        return self.y0 + self.height * 0.5
    
    @property
    def pos(self):
        return np.r_[self.px, self.py]
    
    def get_child_index(self, child):
        if child in self.children:
            return self.children.index(child)
        else:
            return None
        
    def get_max_depth(self):
        if self.children:
            return max(self.depth, np.max([c.get_max_depth() for c in self.children]))
        else:
            return self.depth
        

    def add_child(self, *args, **kwargs):
        self.children.append(TreeNode(*args,**kwargs, parent=self))
        return self.children[-1]
        
    def flatten(self):
        ret = [self]
        for c in self.children:
            ret += c.flatten()
        return ret
    
    def to_annotation(self):
        pos_x, pos_y = self.pos
        return dict(
                text=self.text,
                x=pos_x, y=pos_y,
                xref='x1', yref='y1',
                font=dict(color=self.font_color, size=self.font_size),
                bordercolor="rgb(0,0,0)",
                borderwidth=1,
                borderpad=2,
                bgcolor=self.color,
                opacity=0.8,
                showarrow=False)
    
    def to_lines(self):
        lx = []
        ly = []
        x, y = self.pos
        for c in self.children:
            xc, yc = c.pos
            lx += [x, xc, None]
            ly += [y, yc, None]
            lxc, lyc = c.to_lines()
            lx += lxc
            ly += lyc
        return lx, ly
    
    def __repr__(self):
        px, py = self.pos
        txt = (' | ' + self.hovertext) if self.hovertext and self.hovertext != self.id else ''
        txt = txt.split('\n')[0]
        if len(txt) > 40:
            txt = txt[:40] + '...'
        return str('{} @{}{}'.format(self.id, (px, py), txt))
    
    
    
    
    
    
    def info(self):
        print((self.depth * ' '*3) + '{} h,w={}, xy0={}, xy={}, n_sub={}'.format(self.id, (self.height, self.width), (self.x0, self.y0), self.pos, self.n_sub))
        for c in self.children:
            c.info()
        
        
    def plot(self):
        xy = self.pos
        xy0 = (self.x0, self.y0)
        bbox = dict(boxstyle="round", fc="0.8")
        arrowprops = dict(
            arrowstyle="->",
            connectionstyle="angle,angleA=0,angleB=90,rad=10")
        
        x = xy0[0] + np.r_[0, 0, 1, 1, 0]*self.width
        y = xy0[1] + np.r_[0, 1, 1, 0, 0]*self.height
        plt.plot(x, y)
        plt.plot(xy0[0], xy0[1], ' +k', markersize=10)
        
        plt.annotate(self.text, xy0, xy, 'data', bbox=bbox, arrowprops=arrowprops)
        for c in self.children:
            c.plot()
        
        
        
        
    def draw(self, p_parent=None):
        xy = self.pos
        xy0 = self.xy0
        xy1 = self.xy1
        bbox = dict(boxstyle="round", fc="0.8")

        if p_parent is None:
            an = plt.annotate(self.text, xy0, xy, 'data', bbox=bbox)
        else:
            (xyp, an) = p_parent
            s = "arc,angleA=-90,angleB=0,armA=0,armB=50,rad=0.5"
             # s = "arc3,rad=0.2"
            arrowprops = dict(
                patchB=an.get_bbox_patch(),
                arrowstyle="<-",
                connectionstyle=s)
            
            an = plt.annotate(self.text, xyp, xy, 'data', bbox=bbox, arrowprops=arrowprops)
            
        pos_vec = [xy]
        for c in self.children:
            print(c)
            xyc = c.draw(p_parent=(xy, an))
            pos_vec.append(xyc)
            # plt.annotate('', xyc, xy, 'data', arrowprops=dict(connectionstyle="arc3,rad=0.2",
            #                        **arrow_args))
        
        child_poss = np.array(pos_vec)
        
        xl, yl = np.min(child_poss, axis=0)
        xh, yh = np.max(child_poss, axis=0)
        
        if p_parent is not None:
            return xy
        else:
            lim_x = (xl - .5*TreeNode.spacing_x, xh + 3*TreeNode.spacing_x)
            lim_y = (yl - 3*TreeNode.spacing_y, yh + 0.5*TreeNode.spacing_y)
            print('setting limits', lim_x, lim_y)
            
            # plt.gca().axis('equal')
            plt.xlim(lim_x)
            plt.ylim(lim_y)
            plt.gca().invert_yaxis()
            


# In[3]:


graph = TreeNode('root')
c1 = graph.add_child('child_1')
c2 = graph.add_child('child_2')

c11 = c1.add_child('child_11')
c12 = c1.add_child('child_12')
c13 = c1.add_child('child_13')

c131 = c13.add_child('child_131')
c132 = c13.add_child('child_131')

c21 = c2.add_child('child_21')

plt.figure(figsize=(12,4))
graph.draw()
# graph.plot()


# In[4]:


graph.plot()


# In[ ]:





# In[5]:


# %pip install python-igraph


# In[6]:


# Define variable to load the dataframe
dataframe = openpyxl.load_workbook("requirements_raw.xlsx")
 
# Define variable to read sheet
dataframe1 = dataframe.active

data = []
# Iterate the loop to read the cell values
for i_row in range(0, dataframe1.max_row):
    data.append([None]*2)
    for i_col, col in enumerate(dataframe1.iter_cols(1, dataframe1.max_column)):
        data[i_row][i_col] = col[i_row].value
    
dc = {k:v for (k, v) in data if k and v}


# In[7]:


categories = [k.split('.')[2] for k in dc.keys()]
set(categories)


# In[8]:


import numpy as np
import re


# In[9]:


data = {
    'R.DS': {k:{kk:dc[kk] for kk in dc.keys() if f'.{k}.' in kk} for k in set(categories)}
}


# In[10]:


s = 'R.DS.P.12'


# In[11]:


sorter = lambda s: int(re.findall('[0-9]+', s)[0])

kk = list(data['R.DS']['P'].keys())
kk.sort(key=sorter)

data['R.DS']['P'] = {k:data['R.DS']['P'][k] for k in kk}
data['R.DS']['P'].keys()


# In[12]:


graph = TreeNode('R.DS', 'R.DS', 'Dish Structure Requirements')
print(graph)
for k, v in data['R.DS'].items():
    n = 'R.DS.' + k
    child = graph.add_child(n, n, n)
    print(child)
    
    kk = list(data['R.DS'][k].keys())
    kk.sort(key=sorter)
    dc_sub = {kk2:data['R.DS'][k][kk2] for kk2 in kk}
    
    for k2, v2 in dc_sub.items():
        child2 = child.add_child(k2, k2, v2)
        print(child2)
    
    


# In[ ]:





# In[13]:


fig = go.Figure()
edges = graph.flatten()

def make_label(text, hovertext):
    lines = hovertext.split('\n')
    header = '<b>{}</b>: {}'.format(text, lines[0])
    lnew = [header]
    for l in lines[1:]:
        lnew += textwrap.wrap(l)
    return '<br>'.join(lnew)
    
text = [edge.text for edge in edges]
var_names = [make_label(edge.id, edge.hovertext) for edge in edges]
positions = np.array([edge.pos for edge in edges])
annotations = [edge.to_annotation() for edge in edges]

Xe, Ye = graph.to_lines()

x_position = positions[:,0]
y_position = positions[:,1]

fig.add_trace(go.Scatter(x=Xe,
                   y=Ye,
                   mode='lines',
                   line=dict(color='rgb(210,210,210)', width=1),
                   hoverinfo='none'
                   ))

fig.add_trace(go.Scatter(x=x_position,
                  y=y_position,
                  mode='markers',
                  textposition='middle center',
                  marker=dict(symbol='circle',
                                size=1,
                                color='#6175c1',    #'#DB4551',
                                line=dict(color='rgb(50,50,50)', width=1)
                                ),
                  text=var_names,
                  hoverinfo='text',
                  opacity=0.3
                  ))


fig.update_layout(
    annotations=annotations,
    autosize=False,
    width=1000,
    height=5000,
    margin=dict(
        l=50,
        r=50,
        b=100,
        t=100,
        pad=4
    ),
)


# In[ ]:





# In[ ]:




