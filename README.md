# MKE - Requirement - Suite V2

## Description

This is the MeerKAT extension requirement-Tracker Software. It is designed to allow tracking and working with a huge set of requirements, tests, workpackages etc. for multiple devices under test. 

The main file is ReqTracker/server.py a plotly dash dashboard for visualization. 

    `pip install --index-url http://10.98.76.45:30021/simple/ ReqTracker --trusted-host 10.98.76.45`
    `pip install --index-url http://10.98.76.45:30021/simple/ ReqTracker --trusted-host 10.98.76.45 --upgrade`

## Root on VM through ssh

When in terminal that can reach the VM via ssh:

1. Check if there is still a valid connection by typing 

    `klist`

2. If not (e.G. >>>Expired<<<) init new one by typing:

    `kinit`

3. Connect to VM as root by: 

     `ssh <machine_name> -l root`



## Install / Run

Recommended Python Version: 3.11.7

Can run on any linux machine where docker and git is installed.

1. make sure the IPs and passwords in the parameters.yml file all point torwards the right servers etc.


2. The project can then be downloaded:

    `git clone https://gitlab.mpcdf.mpg.de/tglaubach/mke-requirement-suite`

    `cd mke-requirement-suite/`


3. The Jupyter user "jovyan" needs access to the meertest_data repository to write. For this connect to the VM as root (see last chapter) and type: 
    
    `chgrp +100 /var/meertest/`
    `chgrp +100 -R /var/meertest/`

4. If previously connected as root user make sure you disconnect again and reconnect as standard user in your VM. 

5. Install software by:

    `git pull && docker-compose -f docker-compose.yml build`

6. Start software (detached) by (for non detached omit the -d option): 

    `docker-compose -f docker-compose.yml up -d`

## Setting up a dev environment

On windows: Install anaconda from https://www.anaconda.com/

Make sure you install VS code as well. 
Make sure you have git installed.

checkout this reposity by typing 

    git clone <link_to_repo>

in Git bash.

Generate conda environment by typing 

    conda create -n meerreq python=3.11.7

in anaconda CMD prompt. Then move your CMD to the folder that contains your repository. Install project requirements in the venv through: 

    conda activate meerreq
    pip install -r requirements.txt
    

Open VS code in cloned folders. Select the newly created conda venv 

## Run FastAPI Dev Server

open an anaconda promt with the venv created in the last chapter

type: 

    fastapi dev main.py

