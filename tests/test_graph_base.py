import unittest
import json
import datetime

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

import ReqTracker.core.graph_base as base


test_dict = {
    "id": "R.DS.G.11",
    "_text": "DS Components Electrical Safety\nThe DS components shall be electrically safe in accordance to the applicable sections of SANS 60950-1 (for guidance refer to [301-000000-018]).",
    "data": {},
    "tags": [
    "INSP"
    ],
    "notes": {
    "imported_note_00": "SANS Report"
    },
    '_status': 'NONE',
    'error': '',
    'type': 'BaseNode'
}


class TestBaseNode(unittest.TestCase):

    def _comp_sets(self, lst, expected):
        # print(lst)
        # print(expected) 
        
        lst, expected = set(lst), set(expected)
        self.assertCountEqual(lst, expected)
    
        # self.assertEqual(len(lst), len(expected))   
        # for el in expected:
        #     self.assertIn(el, lst)

        # for el in lst:
        #     self.assertIn(el, expected)

    def test_clone(self):
        b = base.BaseNode('root')
        


    def test_construct(self):
        b = base.BaseNode(dc=test_dict)
        
        for k, v in test_dict.items():
            if k != 'type':
                self.assertTrue(hasattr(b, k), k)
                v2 = getattr(b, k)

                if isinstance(v, list):
                    self.assertListEqual(v, v2)
                elif isinstance(v, dict):
                    self.assertDictEqual(v, v2)
                elif k == '_status':
                    self.assertEqual(v, str(v2.name))
                else:
                    self.assertEqual(v, v2)

    def test_to_dict(self):
        b = base.BaseNode(dc=test_dict)
        self.assertDictEqual(test_dict, b.to_dict())

    def test_clone(self):
        b = base.BaseNode(dc=test_dict)
        bc = b.clone()
        self.assertDictEqual(bc.to_dict(), b.to_dict())
        self.assertNotEqual(id(b), id(bc))

    def test_has_graph(self):
        b = base.BaseNode(dc=test_dict)
        self.assertFalse(b.has_graph)


    def test_dish_id(self):
        b = base.BaseNode(dc=test_dict)
        self.assertEqual(b.dish_id, '')

    def test_get_row(self):
        b = base.BaseNode(dc=test_dict)
        
        row = b.get_row()
        def test_has_and_equal(obj, dc, k):
            self.assertTrue(hasattr(obj, k))
            self.assertIn(k, dc)
            self.assertEqual(getattr(obj, k), dc[k])

        test_has_and_equal(b, row, 'id')

        # just test, that it can be serialized with json
        json.dumps(row)

    def test_add_note(self):
        b = base.BaseNode(dc=test_dict)
        txt1 = 'some test note!'
        txt2 = 'some other test note!'
        txt3 = '2 some test note!'
        txt4 = '2 some other test note!'

        b.notes = {} # start fresh

        b.add_note(txt1)
        self.assertEqual(len(b.notes), 1)

        b.add_note(txt2)
        self.assertEqual(len(b.notes), 2)
        notes = list(b.notes.values())

        self.assertIn(txt1, notes)
        self.assertIn(txt2, notes)

        # try adding two with the same timetamp
        ts = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

        b.add_note(txt3, ts)
        b.add_note(txt4, ts)
        notes = list(b.notes.values())
        self.assertIn(txt3, notes)
        self.assertIn(txt4, notes)

        self.assertEqual(len(b.notes), 4)
        
if __name__ == '__main__':
    unittest.main()