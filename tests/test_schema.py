import unittest
import json
import datetime

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

from ReqTracker.core import schema, graph_main




class TestDocument(unittest.TestCase):

        
    def get_testdoc(self):
        return {
    "type": "Document",
    "id": "MKE-316-000000-MPI-SIP-3063",
    "_text": "",
    "data": {
        "versions": [
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT01-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298431",
            "fileid": "19298431",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        },
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT01-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298431",
            "fileid": "19298431",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        },
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT01-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298431",
            "fileid": "19298431",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        },
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT01-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298431",
            "fileid": "19298431",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        },
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT01-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298431",
            "fileid": "19298431",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        },
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT01-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298431",
            "fileid": "19298431",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        },
        {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT02-StarTrackerInstallation_WPChecklist.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298432",
            "fileid": "19298432",
            "checksum": "",
            "ltc": "2024-02-01T09:17:54Z"
        }

        ],
        "i_ver_last": 0,
        "tickets": []
    },
    "tags": [],
    "notes": {},
    "_status": "IN_CREATION",
    "error": "",
    }

    def test_match(self):
        inp = '/Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SOW-3061-02-StarTrackerInstallation_SOW.pdf'
        exp = {
            'pre': 'MKE',
            'PBS': '316-000000',
            'org': 'MPI',
            'doc_type': 'SOW',
            'doc_no': '3061', 
            'sub': '',
            'dish_no': '',
            'version': '02',
            'title': 'StarTrackerInstallation_SOW',
            'ext': '.pdf'
        }
        res = schema.Document.fun_match(inp)
        self.assertDictEqual(exp, res)

        inp = 'Shared/MeerKAT Extension/07_internal/2023_06_foundation_inspection/MKE-316-000000-MPI-SIR-3057-P116-01-FoundationInspectionReport.docx'
        exp = {
            'pre': 'MKE',
            'PBS': '316-000000',
            'org': 'MPI',
            'doc_type': 'SIR',
            'doc_no': '3057', 
            'sub': '',
            'dish_no': 'P116',
            'version': '01',
            'title': 'FoundationInspectionReport',
            'ext': '.docx'
        }
        res = schema.Document.fun_match(inp)
        self.assertDictEqual(exp, res)

       
        
        inp = ' MKE-316-000000-ODC-TR-0144_#03_02_Factory_Test_Report_Telescope_Structure.pdf'
        exp = {
            'pre': 'MKE',
            'PBS': '316-000000',
            'org': 'ODC',
            'doc_type': 'TR',
            'doc_no': '0144', 
            'sub': '',
            'dish_no': '#03',
            'version': '02',
            'title': 'Factory_Test_Report_Telescope_Structure',
            'ext': '.pdf'
        }
        res = schema.Document.fun_match(inp)
        self.assertDictEqual(exp, res)

        
    def test_trim_versions(self):
        n = schema.Document(dc = self.get_testdoc(), trim_versions=False)
        self.assertEquals(len(n.get_version_dcs()), 7)

        n = schema.Document(dc = self.get_testdoc(), trim_versions=True)
        self.assertEquals(len(n.get_version_dcs()), 2)

    def test_add_version(self):
        new_ver = {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-DRAFT05-StarTrackerInstallation_WPChecklist2.docx",
            "link": "https://cloud.mpifr-bonn.mpg.de/index.php/f/19298435",
            "fileid": "19298435",
            "checksum": "",
            "ltc": "2024-02-01T09:18:54Z"
        }
        n = schema.Document(dc = self.get_testdoc())
        self.assertEquals(len(n.get_version_dcs()), 2)
        n.add_version(new_ver)
        vers = n.get_version_dcs()
        self.assertEquals(len(n.get_version_dcs()), 3)
        vers_dc = {d['path']:d for d in vers}
        self.assertIn(new_ver['path'], vers_dc)
        
        self.assertEquals(n.get_fields()['version'], 'DRAFT05')


    def test_sort_versions(self):
        new_ver = {
            "path": "Shared/MeerKAT Extension/07_internal/Star_Tracker_OCS/Documentation/MKE-316-000000-MPI-SIP-3063-REPLACEME-StarTrackerInstallation_WPChecklist2.docx",
            "link": "",
            "fileid": "",
            "checksum": "",
            "ltc": ""
        }
        versions = 'DRAFT01p1 DRAFT03 RevA RevB v01 01 02 03 03p1'.split()
        n = schema.Document(dc = self.get_testdoc())
        
        for v in versions:
            dc = json.loads(json.dumps(new_ver))
            dc['path'] = dc['path'].replace('REPLACEME', v)
            n.add_version(dc)


        self.assertEquals(len(n.get_version_dcs()), len(versions)+2)
        vers = n.get_sorted_versions()

        # vers = [schema.Document.fun_match(v['path'])['version'] for v in vers]

        self.assertListEqual(vers, 'DRAFT01 DRAFT02'.split() + versions)
           

    def test_str(self):
        n = schema.Document(dc = self.get_testdoc())
        
        self.assertIsInstance(str(n), str)
        self.assertTrue(str(n))

            
 

if __name__ == '__main__':

    
    unittest.main()