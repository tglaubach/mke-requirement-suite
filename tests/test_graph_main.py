import copy
import unittest
import json
import igraph as ig
import numpy as np

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
    
sys.path.insert(0, os.path.join(parent_dir, 'src'))


import ReqTracker.core.graph_base as base
import ReqTracker.core.graph_main as main



test_edges = [
                ("A1", "B1"),
                ("A1", "B2"),
                ("B1", "C1"), 
                ("B2", "C2"),
                ("B2", "C3"),
                ("C1", "D1"), 
                ("C3", "D1"),
                ("A1", "B3"),
                ("B3", "C4"),
                ("C4", "D2")
]

test_nodes = set().union(*test_edges)
      
test_graph = {
    'nodes': [base.BaseNode(x).to_dict() for x in test_nodes],
    'views': {
        'imported': {
            'edges': test_edges,
            'positions': {}
        }
    }
}

test_edges2 = [
                ("A1", "B1"),
                ("A1", "B2.D.##"),
                ("B1", "C1"), 
                ("B2.D.##", "C2.D.##"),
                ("B2.D.##", "C3.D.##"),
                ("C1", "D1"), 
                ("C3.D.##", "D1"),
                ("A1", "B3"),
                ("B3", "C4"),
                ("C4", "D2")
]

test_nodes2 = set().union(*test_edges2)
            
test_graph2 = {
    'nodes': [base.BaseNode(x).to_dict() for x in test_nodes2],
    'views': {
        'imported': {
            'edges': test_edges2,
            'positions': {}
        }
    }
}

class TestGraph(unittest.TestCase):

    @staticmethod
    def get_test_graph():
        return main.Graph(copy.deepcopy(test_graph))

    
    def test_el2id(self):
        
        A1 = base.BaseNode('A1')
        B1 = base.BaseNode('B1')

        self.assertEqual('A1', main.el2id(A1))
        self.assertListEqual(['A1', 'B1'], main.el2id(A1, B1))

        edges = test_edges[:]
        self.assertEqual(json.dumps(edges), json.dumps(main.el2id(edges)))

        dc = {k:base.BaseNode(k) for k in test_nodes}
        f = lambda x: [dc[x[0]], dc[x[1]]]
        edges_objs = [f(x) for x in edges]

        self.assertEqual(json.dumps(edges), json.dumps(main.el2id(edges_objs)))
    

    def test_id2el(self):
        dc = {k:base.BaseNode(k) for k in test_nodes}
        edges = [list(x) for x in test_edges]

        f = lambda x: [dc[x[0]], dc[x[1]]]
        edges_objs = [f(x) for x in edges]

        # select a random one and make it an object to test if still works
        edges[1][1] = edges_objs[1][1]
        for tpl1, tpl2  in zip(edges_objs, main.id2el(edges, dc)):
            self.assertListEqual(tpl1, tpl2)
    

    
    def test_construct(self):
        g = main.Graph()
        self.assertEqual(g.has_changes, False)
        self.assertEqual(len(g.views), 0)
        self.assertTrue(len(g.nodes) == 0)

    def test_getitem(self):
        g = main.Graph()
        A1 = base.BaseNode('A1')
        g.add_node(A1)
        self.assertEqual(id(g['A1']), id(A1))
        self.assertEqual(id(g[A1]), id(A1))

    def test_contains(self):
        g = main.Graph()
        A1 = base.BaseNode('A1')
        g.add_node(A1)
        self.assertIn('A1', g)
        self.assertIn(A1, g)

    ###########################################################
    
    def test_set_me_as_graph(self):
        g = main.Graph()
        k = list(test_nodes)[0]
        n = base.BaseNode(k)

        g.set_me_as_graph(n)
        self.assertEqual(n.graph, g)
        
        nodes = [base.BaseNode(k) for k in test_nodes]
        g.set_me_as_graph(nodes)
        self.assertEqual(n.graph, g)

    
    def test_add_nodes(self):
        g = main.Graph()
        k = list(test_nodes)[0]
        n = base.BaseNode(k)
        g.add_node(n)
        self.assertIn(n, g)
        

    def test_add_edges(self):
        g = self.get_test_graph()
        g.add_edges('new_import', test_edges)
        edges = g.get_edges('new_import')
        for e1, e2 in edges:
            self.assertIn(e1, g)
            self.assertIn(e2, g)

    def test_del_edges(self):
        g = self.get_test_graph()
        
        try:
            g.del_edges('imported', test_edges)
        except Exception as err:
            self.assertTrue(False, str(test_edges) + ' | should have worked: ' + str(err))

        g.add_edges('imported', test_edges)
        g.del_edges('imported', test_edges)

        edges = g.get_edges('imported')
        self.assertEqual(len(edges), 0)
        
    def test_get_parents(self):
        g = self.get_test_graph()

        A1 = g['A1']
        parents = g.get_parents(A1, 'imported')
        self.assertEqual(len(parents), 0)

        for x in [g[x] for x in 'B1 B2 B3'.split()]:
            parents = g.get_parents(x, 'imported')
            self.assertEqual(len(parents), 1)
            self.assertIn(A1.id, parents)

        
        pD1 = g.get_parents(g['D1'], 'imported')
        self.assertEqual(len(pD1), 2)

        self.assertIn(g['C1'].id, pD1)
        self.assertIn(g['C3'].id, pD1)
        
        
    def test_get_children(self):
        g = self.get_test_graph()        
        cA1 = g.get_children(g['A1'], 'imported')
        self.assertEqual(len(cA1), 3)

        self.assertIn(g['B1'].id, cA1)
        self.assertIn(g['B2'].id, cA1)
        self.assertIn(g['B3'].id, cA1)
    
    #############################################################
        
    def test_serialize(self):
        g = self.get_test_graph()

        lst_ser = g.serialize()['nodes']
        dc_sern = {v['id']:v for v in lst_ser}
        dc_tstn = {v['id']:v for v in test_graph['nodes']}
        
        for id_, n in g.nodes.items():
            if n.do_serialize:
                self.assertIn(id_, dc_sern)
                self.assertIn(id_, dc_tstn)
                ntst = dc_tstn[id_]
                nser = dc_sern[id_]
                self.assertDictEqual(ntst, nser)


        #igraph identical_graphs
        # self.assertDictEqual(dc_ser, test_graph)
        
    def test_deserialize(self):
        g = self.get_test_graph()
        gdser = main.Graph.deserialize(g.serialize())
        dc_dsern = {x.id:x.to_dict() for x in gdser.nodes}
        dc_tstn = {v['id']:v for v in test_graph['nodes']}
        
        for id_, n in g.nodes.items():
            if n.do_serialize:
                self.assertIn(id_, dc_dsern)
                self.assertIn(id_, dc_tstn)
                ntst = dc_tstn[id_]
                nser = dc_dsern[id_]
                self.assertDictEqual(ntst, nser)

        

    def test_clone(self):
        g = self.get_test_graph()

        gdser = g.clone()
        self.assertNotEqual(id(gdser), id(g))

        dc_dsern = {x.id:x.to_dict() for x in gdser.nodes}
        dc_tstn = {x.id:x.to_dict() for x in g.nodes}

        for id_, n in g.nodes.items():
            self.assertIn(id_, dc_dsern)
            self.assertIn(id_, dc_tstn)
            ntst = dc_tstn[id_]
            nser = dc_dsern[id_]
            self.assertDictEqual(ntst, nser)

    
    def test_to_plot_recs(self):
        g = main.Graph().deserialize(test_graph)
        recs = g.to_plot_recs()

        self.assertIsInstance(recs, list)
        self.assertTrue(len(recs) > 0)
        for x in recs:
            self.assertTrue(x)
            self.assertIn('id', x)
        
    
    ###########################################################
    
    def test_search(self):
        g = main.Graph().deserialize(test_graph2)
        g.clone_new_dish('SKA119')
        nodes = g.search('SKA119')
        ids = [n.id for n in nodes]

        self.assertEqual(len(nodes), 3)
        self.assertIn('B2.D.SKA119', ids)
        self.assertIn('C2.D.SKA119', ids)
        self.assertIn('C3.D.SKA119', ids)

        g = main.Graph().deserialize(test_graph2)
        g.clone_new_dish('SKA119')
        nodes = g.search('SKA119')
        ids = [n.id for n in nodes]

        # self.assertEqual(len(g.get_nodes_view()), 4)
        self.assertIn('B2.D.SKA119', ids)
        self.assertIn('C2.D.SKA119', ids)
        self.assertIn('C3.D.SKA119', ids)

if __name__ == '__main__':
    unittest.main()