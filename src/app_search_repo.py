import base64
import datetime, os, json, time

from typing import List, Dict

import numpy as np
from ReqTracker.io_files import langchain_nextcloud_io, local_json_file_api, redmine_api

from ReqTracker.core import helpers, node_factory, schema
from app_helpers import check_new_docs, get_graph, GetGraphHold
from app_helpers import load_data as _load_data
from app_helpers import save_data as _save_data

log = helpers.log

obj = None

def load_index(config):
    return load_data(config, key='search_data')

def save_index(config, update_version=True):
    global obj

    assert not obj is None, 'need to load a model first'
    assert obj.has_data, 'no data to save'

def get_repo(config):
    try:
        index_dc, data_version_id = load_data(config)
    except Exception as err:
        index_dc = {}
        data_version_id = ''
        log.error(err)
    

    o = SearchRepo.deserialize(index_dc)
    o.data_version_id = data_version_id
    o.threaded = config.get('search_data', {}).get('threaded', o.threaded)
    return o


def setup(config):
    assert not config is None, 'can not load a model from source if no config is given!'
    global obj
    obj = get_repo(config)


def load(config=None, force_reload=False):
    global obj
    if obj is None or force_reload:
        setup(config)
    return obj

def save(config, obj, comment='', description='', force_overwrite=False):
    return save_data(config, obj.serialize(), comment=comment, description=description, data_version_id=None if force_overwrite else obj.data_version_id)




def load_data(config, key='search_data', with_data_version_id=True):
    log.info('loading Search Index ...')
    o = _load_data(config, key=key, with_data_version_id=with_data_version_id)
    log.info('loading Search Index... DONE')
    return o

def save_data(config, content, comment='', description='', key='search_data', data_version_id = None):
    log.info('saving Search Index...')
    o = _save_data(config, content=content, comment=comment, description=description, key=key, data_version_id=data_version_id)
    log.info('saving Search Index... DONE')
    return o

class SearchRepo(object):

    def deserialize(dc):
        log.info(f'deserialize with {len(dc)} and {dc.keys()=}')
        kwargs = {}

        if 'content_ids' in dc and 'content_values' in dc:
            content = dict(zip(dc['content_ids'], dc['content_values']))
            kwargs['content'] = content
        if 'content' in dc:
            content = dc['content']
            if isinstance(content, list):
                content = {p.get('pid'):p for p in content}
            kwargs['content'] = content

        return SearchRepo(**kwargs)

    def __init__(self, verb=True, data_version_id='', **kwargs) -> None:
        self.content = kwargs.get('content', {})
        
        self.data_version_id = data_version_id
        self.t_started = datetime.datetime.utcnow()
        self.verb = verb
        self.threaded = kwargs.get('threaded', 0)

    def info(self):
        return {
            't_started': helpers.make_zulustr(self.t_started),
            'n_indices': len(self),
            'data_version_id': self.data_version_id,
        }

    def __len__(self):
        return len(self.content)
    
    @property
    def has_data(self):
        return True if self.content else False
    
    def serialize(self):
        dc = {
            'content': list(self.content.values()),
        }
        return dc
    
    def get_ltc_newest_doc(self):
        if not self.content:
            return helpers.get_utcnow()
        m = np.array([helpers.parse_zulutime(n.get('ltc') if isinstance(n, dict) else n) for n in self.content.values()])
        m = m[m != None]
        m = np.max(m) if len(m) else helpers.get_utcnow()
        return m
    
    def do_index(self, dc_index:Dict[str, dict], version = None):
        self.content = dc_index

    def get_index_dc(self):
        return self.content

    # def info(self):
    #     return {'version': 'v1', 'n_indices': len(self.content_ids), 'running_since': self.t_started.isoformat()}
    
    def del_doc(self, doc_id):
        if isinstance(doc_id, str):
            fun = lambda x: x['node_id'] == doc_id
        elif isinstance(doc_id, (set, list)):
            doc_id = set(doc_id)
            fun = lambda x: (x['node_id'] in doc_id)
        else:
            raise ValueError(f'doc_id must be a string or list/set of strings, but given was {type(doc_id)=}')
        
        to_del = [k for k, n in self.content.items() if fun(n)]
        for k in to_del:
            self.content.pop(k)
            
        return len(to_del)

    def sync_docs(self, nodes_docs:List[schema.BaseVersionedDocument]):
        t = time.time()
        api = langchain_nextcloud_io.NextcloudFileLoader(verb=True, use_threading=self.threaded)
        docs = api.get_nc_docs_dc(nodes_docs)
        new_docs_dc = {n['pid']:n for n in docs if n is not None}
        n = self.insert_or_update_docs(new_docs_dc)
        
        elapsed = time.time()-t
        if self.verb:
            log.info((elapsed, len(docs), len(new_docs_dc)))
        return n


    def reindex_new_docs(self, config, t_last=None, force_clean=False, n_max=0, update_graph = False):
        
        with GetGraphHold(config) as g:

            if t_last is None:
                a = g.get_ltc_newest_doc()
                b = self.get_ltc_newest_doc()
                a = helpers.parse_zulutime(a) if isinstance(a, str) else a
                b = helpers.parse_zulutime(b) if isinstance(b, str) else b
                t_last = max(a,b)

            if isinstance(t_last, str):
                t_last = helpers.parse_zulutime(t_last)
            
            if force_clean:
                to_update = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]
            else:
                t_last, all_new_docs = check_new_docs(config, t_last)
                fun = lambda nd: nd and 'id' in nd and nd['id'] in g
                to_update = [g[nd.get('id')] for nd in all_new_docs if fun(nd)]

            if n_max > 0:
                to_update = to_update[:n_max]

            if to_update:
                
                if update_graph:
                    f = lambda d: node_factory.construct_doc_from_dc_data(d, g)
                    ids = [f(x) for x in to_update]
                    ids = [x for x in ids if x]
                    if ids:
                        g.add_changenote(f'Successfully updated N={len(ids)} docs!')

                if force_clean:
                    return self.do_index_from_nextcloud(to_update)
                else:
                    return self.do_update_from_nextcloud(to_update)

            else:
                return 0, []
    
    def do_index_from_nextcloud(self, nodes_docs:List[schema.BaseVersionedDocument]):
        log.info(f'reindex: doing complete reindex with N={len(nodes_docs)} pages...')
        pages_dc = self.download_nodedocs_as_pages(nodes_docs)
        log.info(f'reindex: got M={len(pages_dc)} pages...')
        self.do_index(pages_dc)
        log.info(f'reindex ... DONE')

        return len(pages_dc), list(set([p.get('node_id', 'N/A') + '-' + p.get('node.version', 'N/A') for p in pages_dc.values()]))
    
    def do_update_from_nextcloud(self, nodes_docs:List[schema.BaseVersionedDocument]):
        log.info(f'update: doing complete reindex with N={len(nodes_docs)} pages...')
        pages_dc = self.download_nodedocs_as_pages(nodes_docs)
        log.info(f'update: got M={len(pages_dc)} pages...')
        ret = self.insert_or_update_docs(pages_dc)
        log.info(f'update ... DONE')
        return ret
    
    def download_nodedocs_as_pages(self, nodes_docs:List[schema.BaseVersionedDocument]) -> Dict[str, dict]:
        api = langchain_nextcloud_io.NextcloudFileLoader(verb=True, use_threading=self.threaded)
        docs = api.get_nc_docs_dc(nodes_docs)

        missing = {n.id:n.get_latest_ver()['path'] for n, d in zip(nodes_docs, docs) if n is None or d is None}
        if self.verb and missing:
            log.info('START OF MISSING DOCS'.rjust(50, '-').ljust(100, '-'))

            for m, pth in missing.items():
                log.info(m.ljust(50) + str(pth))
            
            log.info('END OF MISSING DOCS'.rjust(50, '-').ljust(100, '-'))

        all_pages = []
        for node, pages in zip(nodes_docs, docs):
            dc = {f'node.{k}':helpers.make_zulustr(v) if isinstance(v, datetime.datetime) else v for k, v in node.get_latest_ver().items()}
            dc['node.version'] = node.version
            if pages:
                all_pages += [{**page, **dc} for page in pages if page and dc]

        return {p['pid']:p for p in all_pages}

        
    def insert_or_update_docs(self, new_pages_dc):
        if not isinstance(new_pages_dc, dict):
            new_pages_dc = {n.get('pid'):n for n in new_pages_dc}

        old_content = {k:v for k, v in self.content.items()}

        try:
            
            doc_ids = set([p.get('node_id') for p in new_pages_dc.values()])
            self.del_doc(doc_ids)

            for k, v in new_pages_dc.items():
                assert not k in self.content, f'the id {k=} already exist in this search repo'
                self.content[k] = v

            return len(new_pages_dc), list(set([p.get('node_id', 'N/A') + '-' + p.get('node.version', 'N/A') for p in new_pages_dc.values()]))
        
        except Exception as err:
            log.error(f'encountered error: {err=}')
            log.info('rolling back to original data')
            self.content = old_content
            raise

if __name__ == '__main__':

    import yaml
    from ReqTracker import login

    pth = r"C:\Users\tglaubach\repos\mke-requirement-suite\src\config.yaml"
    # pth = r'C:\Users\tobia\source\repos\mke-requirement-suite\src\config.yaml'
    with open(pth) as fp:
        config = yaml.safe_load(fp)
    
    
    login.setup(config)
    data = load_data(config)
    docs = [n for n in data['nodes'] if 'versions' in n.get('data', {})]
    docs = {n.get('id'):json.dumps(n) for n in docs[:5]}
    mdl = SearchRepo()
    mdl.do_index(docs)

    res = mdl.search('VAS')
    
