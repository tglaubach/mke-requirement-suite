from collections import namedtuple
import io
import json
import random
import textwrap
import time
import urllib
import re
import uuid
import os
import base64
import markdown

from jinja2 import Environment, Template

import sys, inspect, os

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

import ReqTracker
from ReqTracker.core import helpers, graph_main, graph_base, schema

def render_template_string(template_string, **kwargs):
    template = Environment().from_string(template_string)
    data = {'title': 'My Web Page', 'name': 'John Doe', 'message': 'Welcome!'}
    return template.render(data)
    

# DEFAULT_IMAGE_PATH = os.path.join(parent_dir, 'ReqTracker', 'assets', 'mpifr.png')
# with open(DEFAULT_IMAGE_PATH, 'rb') as fp:
#     DEFAULT_IMAGE_BLOB = '' # base64.b64encode(fp.read()).decode('utf-8')
# DEFAULT_IMAGE_BLOB = ''

def mk_link(id_, label=None, pth='show', p0='uib', v='v1', **kwargs):
    return f'<a href="/{p0}/{v}/{pth}/{urllib.parse.quote_plus(id_)}" target="_self">{label if label else id_}</a>'

def mk_tpl(id_, label=None, pth='show', p0='uib', v='v1', **kwargs):
    return f"/{p0}/{v}/{pth}/{urllib.parse.quote_plus(id_)}", label if label else id_



class html_renderer:

    @staticmethod
    def vm_Text(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))
    
        if label:
            return f'<div style="min-width:100">{label}</div><div>{content}</div>'
        else:
            return f'<div>{content}</div>'
            
    @staticmethod
    def vm_Markdown(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))

        parts = []
        if label:
            parts += [
                f'<div style="min-width:100;">{label}</div>',
                '<hr/>'
            ]
        
        s = markdown.markdown(content)
        
        # s = f'<pre disabled=true style="width:90%; min-height:200px; overflow-x: scroll; overflow-y: none; margin:5px;display:block;font-family: Lucida Console, Courier New, monospace;font-size: 0.8em;">\n\n{content}\n\n</pre>'
        #s = f'<span style="display:block;" class="note">\n\n{content}\n\n</span>'
        parts += [s]

        return '\n\n'.join(parts)
    

    @staticmethod
    def vm_LargeText(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))
    
        nn = [len(s) for s in content.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{content}\n\n</textarea>'
    

    @staticmethod
    def vm_Iterator(**kwargs):
        children = []
        content = kwargs.get('content', kwargs.get('value'))
        for i, c in enumerate(content, 1):
            children.append(c.to_html(add_header=False) if hasattr(c, 'get_VM') else c)

        return f'\n\n'.join([f'<div>{c}</div>' for c in children])
    


    @staticmethod
    def vm_BaseFallback(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))
    
        j = '#!/RAWJSON!\n' + json.dumps(content, indent=2)
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
    

    @staticmethod
    def vm_Verbatim(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))

        j = content
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        children = [
            f'<div style="min-width:100;">{label}</div>',
            f'<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
        ]
        return '\n\n'.join(children)

    @staticmethod
    def vm_Image(label='', imageblob=None, value='', width=0.8, caption="", **kwargs):       
        
        if imageblob is None:
            imageblob = ''

        uid = (id(imageblob) + int(time.time()) + random.randint(1, 100))


        if not value:
            value = f'image_{uid}.png'

        s = imageblob.decode("utf-8") if isinstance(imageblob, bytes) else imageblob
        if not s.startswith('data:image'):
            s = 'data:image/png;base64,' + s
        

        children = [
            f'<div style="min-width:100;">{label}</div>',
            f'<div style="min-width:100;">image-name:</div>',
            f'<div>{value}</div>',
            f'<div style="min-width:100;">width</div>',
            f'<div>{width}</div>',
            f'<div style="min-width:100;">caption</div>',
            f'<div>{caption}</div>',
            f"<image src=\"{s}\", style=\"max-width:80%\"></image>",
            ]
        
        # children = dcc.Upload(id=self.mkid('helper_uploadfile'), children=children, multiple=False, disable_click=True)

        return '\n\n'.join(children)

    @staticmethod
    def vm_BaseFallback(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))
    
        j = '#!/RAWJSON!\n' + json.dumps(content, indent=2)
        nn = [len(s) for s in j.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{j}\n\n</textarea>'
    
    @staticmethod
    def vm_Iterator( **kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))
        return f'\n\n'.join([f'<div>{c}</div>' for c in content])
    
    @staticmethod
    def vm_LargeText(**kwargs):
        label = kwargs.get('label', '')
        content = kwargs.get('content', kwargs.get('value'))
        nn = [len(s) for s in content.split('\n')]
        n = len(n)
        w = max(nn)
        return f'<div style="min-width:100;">{label}</div><hr></hr>\n\n<textarea cols="{w}" rows="{n}" disabled=True>\n\n{content}\n\n</textarea>'
    





def html_docdc2html(content):

    if isinstance(content, str):
        return html_renderer.vm_Text(content=content)
    elif isinstance(content, list) and content and isinstance(content[0], list):
        return html_renderer.vm_BaseFallback(**content) # BUG: not sure if this works! this needs to be implemented properly!
    elif isinstance(content, dict) and content.get('type', None) == 'iter' and isinstance(content.get('value', None), list):
        return html_renderer.vm_Iterator(content=[html_docdc2html(c) for c in content.get('value')])
    elif isinstance(content, list):
        return html_renderer.vm_Iterator(content=[html_docdc2html(c) for c in content])
    elif isinstance(content, dict) and content.get('type', None) == 'table':
        return html_renderer.vm_BaseFallback(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'image':
        return html_renderer.vm_Image(**content)
    elif isinstance(content, dict) and content.get('type', None) == 'input':
        return html_renderer.vm_BaseFallback(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'rawfile':
        return html_renderer.vm_BaseFallback(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'verbatim':
        return html_renderer.vm_Verbatim(**content) 
    elif isinstance(content, dict) and content.get('type', None) == 'markdown':
        return html_renderer.vm_Markdown(**content) 
    else:
        raise ValueError(f'the element of type {type(content)}, could not be parsed.', content)
    

def node_get_pyplot_b64(node, margin=(2,2), caption='', maxlevel=6):
    
    if not 'plt' in locals():
        import matplotlib.pyplot as plt

    edges = node.get_lower(as_edges=True, maxlevel=maxlevel) # + node.get_upper(maxlevel=1)
    assert node.has_graph, 'need to supply a node which is bound to a graph'
    g = node.graph    
    v = g.to_view(node.id, edges)

    ax = v.plot(lyout='tree',  nodes_to_highlite=[node.id])
    plt.tight_layout()

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    base64_string = base64.b64encode(buf.getvalue()).decode('utf-8')
    return base64_string


def html_doc2html(doc):
    tmp = doc.values() if isinstance(doc, dict) else doc
    return '\n\n'.join([html_docdc2html(dc) for dc in tmp])
    #return 'ReqTrack ERROR! NO HTML RENDER ENGINE AVAILABLE!'


def html_node2html(node, keywords=None, no_text_header=False, with_doc=True, graph_plot='', **kwargs):
    keywords = [] if not keywords else keywords
    text_md = []


    text_md.append(f'<h2>NODE: {node.id}</h2>')
    text_md.append(f'<hr/>')
    if graph_plot == 'plotly':
        imageblob = node_get_pyplot_b64(node)
        graph_plot = html_renderer.vm_Image(f'NODE "{node.id}" surrounding lyout', imageblob)

    if graph_plot:
        text_md.append(f'<h3>Plot:</h3>')
        text_md.append(graph_plot)

    status = node.status.name if not isinstance(node.status, str) else node.status
    tags = ', '.join((t for t in node.tags))

    text_md.append(f'<div><b>STATUS</b>: <span style="color:{graph_base.colorss[status]};">{status}</span></div>')
    text_md.append(f'<div><b>TAGS</b>: {tags}</div>')
    
    text_md.append(f'<hr/>')
    if issubclass(type(node), schema.BaseVersionedDocument):
        text = node.get_text(short=True)
    else:
        text = node.text

    
    if text:
        if not no_text_header:
            text_md.append('<h4>TEXT:</h4>')
        text_md.append(html_renderer.vm_Markdown('', text))
        text_md.append(f'<hr/>')

    if node.notes:
        if not no_text_header:
            text_md.append('<h4>NOTES:</h4>')
            
        text_md.append('<ul>')
        text_md += ['   <li><b>{}</b>: {}</li>'.format(k, markdown.markdown(v)) for k, v in node.notes.items()]
        text_md.append('</ul>')

    # for kw in keywords:
    #     text_md = re.sub(kw, f'** {kw} **', text_md, flags=re.IGNORECASE)


    text_md.append('<h4>PARENTS:</h4>')
    if node.has_graph:
        text_md += [' | '.join([mk_link(e.id, **kwargs) for e in node.parents])]
    else:
        text_md += ['<div style="color:red;">Can not show parents, since node is not connected to graph</div>']
    text_md.append('<h4>CHILDREN:</h4>')
    if node.has_graph:
        text_md += [' | '.join([mk_link(e.id, **kwargs) for e in node.children])]
    else:
        text_md += ['<div style="color:red;">Can not show children, since node is not connected to graph</div>']
    
    if with_doc:
        text_md.append('<hr/>')
        text_md.append('<h4>DOCUMENT PARTS:</h4>')
        text_md.append(html_doc2html(node.get_doc()))

    s = '\n\n'.join(text_md)

    for kw in keywords:
        s = re.sub(kw, f'<mark>{kw}</mark>', s, flags=re.IGNORECASE)

    return s




def html_node2md(node, keywords=None, no_text_header=False):
    keywords = [] if not keywords else keywords
    text_md = []

    if issubclass(type(node), schema.BaseVersionedDocument):
        text = node.get_text(short=True)
    else:
        text = node.text

    if text:
        if not no_text_header:
            text_md.append('#### TEXT:\n\n')
        text_md += [text]

    if node.notes:
        if not no_text_header:
            text_md.append('#### NOTES:\n\n')
        text_md += ['**{}**: {}'.format(k, v) for k, v in node.notes.items()]

    for kw in keywords:
        text_md = re.sub(kw, f'** {kw} **', text_md, flags=re.IGNORECASE)

    text_md.append('#### PARENTS:\n\n')
    text_md += [' | '.join([e.id for e in node.parents])]
    text_md.append('\n\n#### CHILDREN:\n\n')
    text_md += [' | '.join([e.id for e in node.children])]



    return '\n'.join(text_md)

def helper(docname, g, node):
    file_content = html_node2html(node, with_doc=True, graph_plot='pyplot').encode(encoding="utf-8")
    filename = docname + '.html'
    return filename, file_content

def add_docmaker_pipe():
    graph_base.docmaker_pipes['.html'] = helper




if __name__ == '__main__':
    
    test_dict = {
        "id": "R.DS.G.11",
        "_text": "DS Components Electrical Safety\nThe DS components shall be electrically safe in accordance to the applicable sections of SANS 60950-1 (for guidance refer to [301-000000-018]).",
        "data": {},
        "tags": [
        "INSP"
        ],
        "notes": {
        "imported_note_00": "SANS Report"
        },
        '_status': 'NONE',
        'error': '',
        'type': 'BaseNode'
    }
    example_doc_dc = {
        'Mytext': 'some text...',
        'Other_text': 'some other text...',
        'Myiter': [],
        'MyImage': {'type': 'image', 'value': None}
    }

    el = graph_main.construct(test_dict)
    for v in example_doc_dc.values():
        el.doc_append_part(v)

    s = html_node2html(el)

    pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\temp.html'
    
    with open(pth, 'w+') as fp:
        fp.write(s)
