import yaml, base64, time, io, copy, json, traceback, hashlib, markdown, re

def im2attachment(dc_img):
    mapper = {
            '/' : 'jpg',
            'i' : 'png',
            'R' : 'gif',
            'U' : 'webp'
        }
    filename = dc_img.get('value', None)
    imageblob = dc_img.get('imageblob')

    if not filename: 
        if ';base64' in imageblob:
            ext = imageblob.split(';base64')[0].split('/')[-1]
        elif mapper.get(imageblob[0], None):
            ext = mapper.get(imageblob[0], None)
        else:
            raise KeyError('extension could not be determined from imageblob')

        #filename = f'img_{time.time_ns()}_{str(id(dc))[-4:]}.{ext}'
        filename = f"img_{hashlib.md5(imageblob.encode('utf-8')).hexdigest()}.{ext}"
    description = dc_img.get('caption', '')
    if not description: 
        description = filename
    
    content = io.BytesIO(base64.b64decode(imageblob))

    return {"path" : content, "filename" : filename, "content_type" : "application/octet-stream", "description": description}


class DocumentRedmineFormatter:

    def __init__(self, out_format='textile') -> None:
        self.attachments = []
        self.out_format = out_format

    def handle_error(self, err, el) -> list:
        txt = 'ERROR WHILE HANDLING ELEMENT:\n{}\n\n'.format(el)
        if not isinstance(err, str):
            txt += '\n'.join(traceback.format_exception(err, limit=5)) + '\n'
        else:
            txt += err + '\n'
        txt = f"""<pre>\n{txt}\n</pre>"""

        return [txt]

    def digest_tabular(self, el, hlines=0) -> list:
        # HACK: need to implement this!
        return [self.digest_verbatim(json.dumps(el))]
    
        
    def digest_table(self, value=None, layout='', hlines=1, caption='', **kwargs) -> list:
        # HACK: need to implement this!
        return [self.digest_verbatim(json.dumps(value))]
    

    def digest_markdown(self, value='', **kwargs) -> list:
        return [value]
    
    def digest_image(self, **kwargs) -> list:
        attachment = im2attachment(kwargs)

        filename = attachment.get('filename')
        caption = attachment.get('description')
        self.attachments.append(attachment)
        s = f'!{filename}({caption})!\n**IMAGE:** attachment:"{filename}" {caption}\n'
        return [s]
    

    def digest_input(self, value='', content='', **kwargs) -> list:
        return [content]
        

    def digest_rawfile(self, value='', filecontent='', **kwargs) -> list:
        return [filecontent]


    def digest_verbatim(self, value='', **kwargs) -> list:
        txt = self.digest(value).strip('\n')
        s = f"""<pre>{txt}</pre>"""
        return [s]


    def digest_iterator(self, el) -> list:
        parts = []
        if isinstance(el, dict) and el.get('type', '') == 'iter' and isinstance(el.get('value', None), list):
            el = el['value']
        
        assert isinstance(el, list)
        for p in el:
            parts += self.digest(p)
            parts.append('\n\n')

        return parts
    
    def parse_md2html(self, s) -> str:
        return markdown.markdown(s, extensions=['extra', 'toc'])

    def parse_md2textile_line(self, line):
        r = re.match(r'([ \t]*)#+', line)
        n = r.group().count('#') if r else None
        if n:
            line = re.sub(r'([ \t]*)#+', rf'\1h{n}. ', line)

        r = re.match(r'^([ \t]*-{1}[ ]{1})', line)
        if r:
            g = r.group()
            line = line.replace(g, ('*'*len(g)) + ' ')

        return line

    

    def parse_md2textile(self, s) -> str:
        f = self.parse_md2textile_line
        lines = [f(line) for line in s.split('\n')]
        s = '\n'.join(lines)
        # code blocks
        s = re.sub(r"(```)([\s\S]*?)(?=```)(```)", r'<pre>\2</pre>', s)

        # links
        s = re.sub(r"(\[)([\s\S]*?)(?=\])(\])(\()([\s\S]*?)(?=\))(\))", r'"\2":\5', s)
        
        return s

    def digest_str(self, el) -> list:
        return [el]
        
    def digest(self, el) -> list:
        try:
            
            if not el:
                return ''
            elif isinstance(el, str):
                ret = self.digest_str(el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'iter':
                ret = self.digest_iterator(el)
            elif isinstance(el, list) and el and isinstance(el[0], list):
                ret = self.digest_tabular(el)
            elif isinstance(el, list) and el:
                ret = self.digest_iterator(el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'table':
                ret = self.digest_table(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'image':
                ret = self.digest_image(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'input':
                ret = self.digest_input(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'rawfile':
                ret = self.digest_rawfile(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'verbatim':
                ret = self.digest_verbatim(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'markdown':
                ret = self.digest_markdown(**el)
            else:
                return self.handle_error(f'the element of type {type(el)} {el=}, could not be parsed.')
            
            if self.out_format == 'html':
                ret = [self.parse_md2html(s) for s in ret]
            elif self.out_format == 'textile':
                ret = [self.parse_md2textile(s) for s in ret]

            return ret
        
        except Exception as err:
            return self.handle_error(err, el)

