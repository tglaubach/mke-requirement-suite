# import traceback
# import io

# import base64

# import docx
# from docx.shared import Inches


# import markdown

# def blue(run):
#     run.font.color.rgb = docx.shared.RGBColor(0, 0, 255)

# def red(run):
#     run.font.color.rgb = docx.shared.RGBColor(255, 0, 0)

# def add_paragraph(p1, newtext, *args, **kwargs):
#     para2 = document.add_paragraph(newtext, *args, **kwargs)
#     p1._p.addnext(para2._p)
#     return para2

# def handle_image(document, token, value, paragraph, run):
#     # Replace token with image
#     print('handling image')

#     image_data = base64.b64decode(value['imageblob'])
#     # Save the decoded image to a file
#     with open('test_img.png', 'wb') as f:
#         f.write(image_data)

#     image_width = Inches(max(1, value['width']*12))
#     image_caption = value['caption']
#     p = add_paragraph(paragraph, '')

#     fp = io.BytesIO(image_data)
#     fp.seek(0)

#     r = p.add_run()
#     r.add_picture(r"C:\Users\tglaubach\repos\mke-requirement-suite\test_img.png", width=image_width)
#     # document.add_picture(r"C:\Users\tglaubach\repos\mke-requirement-suite\src\static\images\mpifr.png",width=image_width)

#     r.alignment = 1
#     p2 = add_paragraph(p, '')
#     new_run = p2.add_run(image_caption)
#     blue(new_run)
#     new_run.alignment = 1

#     return dict(token=token, value=value, paragraph=p, run=new_run)

# def handle_str(document, token, value, paragraph, run, append=False):
#     if append:
#         r = paragraph.add_run(value)
#     else:
#         run.text = run.text.replace(token, str(value))
#         r = run
#     blue(r)
#     return dict(token=token, value=value, paragraph=paragraph, run=r)

# def handle_markdown(document, token, value, paragraph, run):
#     # Replace token with markdown content
#     markdown_content = value['value']
#     # html = markdown.markdown(markdown_content)
#     new_paragraph=add_paragraph(paragraph, markdown_content, style='Normal')
#     for r in new_paragraph.runs:
#         blue(r)
#     return dict(token=token, value=value, paragraph=new_paragraph, run=run)

# def handle_verbatim(document, token, value, paragraph, run):
    
#     # Replace token with verbatim text
#     verbatim_text = value['value']
#     new_run = paragraph.add_run(verbatim_text)
#     new_run.font.name = 'Courier New'  # Or any other monospace font
#     new_run.font.size = docx.shared.Pt(8)  # Adjust font size as needed
#     blue(new_run)
#     return dict(token=token, value=value, paragraph=paragraph, run=new_run)


# def handle_error(document, token, value, paragraph, run):
#     if not isinstance(value, str):
#         traceback.print_exception(value, limit=5)
#         value = traceback.format_exception(value, limit=5)

#     verbatim_text = value
#     new_run = paragraph.add_run(verbatim_text)
#     new_run.font.name = 'Courier New'  # Or any other monospace font
#     new_run.font.size = docx.shared.Pt(8)  # Adjust font size as needed
#     red(new_run)
#     return dict(token=token, value=value, paragraph=paragraph, run=new_run)


# def handle_iterator(document, token, value, paragraph, run):
#     for val in value:
#         digest(document, token, val, paragraph, run)
#     return dict(token=token, value=value, paragraph=paragraph, run=run)

# def digest(document, token, value, paragraph, run, make_blue=True):
#     try:
        
#         if not value:
#             return ''
#         elif isinstance(value, str):
#             ret = handle_str(document, token, value, paragraph, run, append=not token.startswith('REPLACEME:'))
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'iter':
#             ret = handle_iterator(document, token, value, paragraph, run)
#         elif isinstance(value, list) and value and isinstance(value[0], list):
#             ret = handle_tabular(document, token, value, paragraph, run)
#         elif isinstance(value, list) and value:
#             ret = handle_iterator(document, token, value, paragraph, run)
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'table':
#             ret = handle_table(document, token, value, paragraph, run)
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'image':
#             ret = handle_image(document, token, value, paragraph, run)
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'input':
#             ret = handle_input(document, token, value, paragraph, run)
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'rawfile':
#             ret = handle_rawfile(document, token, value, paragraph, run)
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'verbatim':
#             ret = handle_verbatim(document, token, value, paragraph, run)
#         elif isinstance(value, dict) and 'type' in value and value['type'] == 'markdown':
#             ret = handle_markdown(document, token, value, paragraph, run)
#         else:
#             val = f'the element of type {type(value)}, could not be parsed.'
#             ret = handle_error(document, token, val, paragraph, run)
#     except Exception as err:
#         ret = handle_error(document, token, err, paragraph, run)

#     return ret

# def replace_tokens(template_path, replacement_dict, out_path):
#     """Replaces tokens in a DOCX document with values from a dictionary.

#     Args:
#         document_path (str): The path to the DOCX document.
#         replacement_dict (dict): A dictionary containing token-value pairs.
#     """

#     doc = docx.Document(template_path)
    
#     for token, value in replacement_dict.items():
#         token = token.replace('>', '').replace('<', '').replace('_', '')
#         print('TOKEN', token)
#         for paragraph in doc.paragraphs:
#             if 'REPLACEME' in paragraph.text:
#                 print('P', paragraph.text)

#             for run in paragraph.runs:
#                 if 'REPLACEME' in run.text:
#                     print('   R', run.text)
#                 if token in run.text:
#                     print('replacing ', token)
#                     digest(doc, token, value, paragraph, run)

#             paragraph.text = paragraph.text.replace(token, "")

#     doc.save(out_path)
#     print('saved to', out_path)
    
# if __name__ == '__main__':
        
#     # Example usage:
#     document_path = r"C:\Users\tglaubach\repos\mke-requirement-suite\src\ReqTracker\exporters\templates\rep_default_template2.docx"
#     outp = "C:\\temp\\testreps\\test_rep2.docx"

#     parts = ['some text...',
#      'some other text...',
#      [{'type': 'markdown',
#        'value': "## **316-000000-050**: DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed\n\n\n**STATUS**: NEEDS_REVIEW\n\n\n**TAGS**: \n\n\n---\n\n**TEXT**: \n\n**316-000000-050**\n\n---\n\nShared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_All/applc_doc_IRR/316-000000-050 RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n\n- LTC (UTC): **2022-02-02 10:48:03**  | VERSION: **RevD**\n    - FNAME: `316-000000-050 RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_All/applc_doc_IRR](https://cloud.mpifr-bonn.mpg.de/index.php/f/17183060)\n- LTC (UTC): **2022-02-02 10:48:03**  | VERSION: **RevD**\n    - FNAME: `316-000000-050 RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_Public_Review/Applicable_Documents](https://cloud.mpifr-bonn.mpg.de/index.php/f/17183639)\n- LTC (UTC): **2019-10-16 05:59:43**  | VERSION: ****\n    - FNAME: `316-000000-050  RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Documents/official/Design_evaluation_shared/DataPack_Design_Evaluation_Consortium/Additional_Docs](https://cloud.mpifr-bonn.mpg.de/index.php/f/15350168)\n- LTC (UTC): **2019-10-16 05:59:43**  | VERSION: ****\n    - FNAME: `316-000000-050  RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_Contract/DataPack_Contract_SOW/00_Bid/Documents_Referenced_available](https://cloud.mpifr-bonn.mpg.de/index.php/f/15838617)\n- LTC (UTC): **2019-10-16 05:59:43**  | VERSION: ****\n    - FNAME: `316-000000-050  RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_DS_Evaluation_and_Design_Adoption/Design_evaluation_MTM/Additional_Docs](https://cloud.mpifr-bonn.mpg.de/index.php/f/15350169)\n\n---\n\n```{'pre': '', 'PBS': '316-000000', 'org': '', 'doc_type': '', 'doc_no': '050', 'sub': '', 'dish_no': '', 'version': 'RevD', 'title': 'DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed', 'ext': '.pdf'}```\n\n```i_ver_last: 3\ntickets: []\nversions:\n- checksum: ''\n  fileid: '15350168'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/15350168\n  ltc: '2019-10-16T05:59:43Z'\n  path: Documents/official/Design_evaluation_shared/DataPack_Design_Evaluation_Consortium/Additional_Docs/316-000000-050  RevD\n    DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: ''\n  fileid: '15838617'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/15838617\n  ltc: '2019-10-16T05:59:43Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_Contract/DataPack_Contract_SOW/00_Bid/Documents_Referenced_available/316-000000-050  RevD\n    DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: ''\n  fileid: '15350169'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/15350169\n  ltc: '2019-10-16T05:59:43Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_DS_Evaluation_and_Design_Adoption/Design_evaluation_MTM/Additional_Docs/316-000000-050  RevD\n    DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: SHA1:f53bdc25fbd39f5eac37100f7e87b977417d3b7c\n  fileid: '17183060'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/17183060\n  ltc: '2022-02-02T10:48:03Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_All/applc_doc_IRR/316-000000-050\n    RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: SHA1:f53bdc25fbd39f5eac37100f7e87b977417d3b7c\n  fileid: '17183639'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/17183639\n  ltc: '2022-02-02T10:48:03Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_Public_Review/Applicable_Documents/316-000000-050\n    RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n```\n\n\n---\n\n**NOTES**:\n\n**TICKETS**:\n\n---\n"}],
#      {'type': 'image',
#       'value': None,
#       'imageblob': 'iVBORw0KGgoAAAANSUhEUgAAAGQAAABnCAYAAAD2duf6AAAABGdBTUEAALGeYUxB9wAAACBjSFJNAACHEAAAjBIAAP1NAACBPgAAWesAARIPAAA85gAAGc66ySIyAAABGWlDQ1BJQ0MgUHJvZmlsZQAAKM9jYGBSSCwoyGESYGDIzSspCnJ3UoiIjFJgf8DAxsDEIMAgwaCWmFxc4BgQ4MMABDAaFXy7xsAIoi/rgsxiIA1wpaQWJwPpP0CcnVxQVMLAwJgBZCuXlxSA2D1AtkhSNpi9AMQuAjoQyN4CYqdD2CfAaiDsO2A1IUHOQPYHIJsvCcxmAtnFlw5hC4DYUHtBQNAxJT8pVQHkew1DS0sLTQYqg5LUihIQ7ZxfUFmUmZ5RouAIDKlUBc+8ZD0dBSMDQzMGBlC4Q1R/DgSHJ6PYGYQYAiDE5kgwMPgvZWBg+YMQM+llYFigw8DAPxUhpmbIwCCgz8Cwb05yaVEZ1BhGJmMGBvx8BgYAChlKTWZAi7MAAAAJcEhZcwAAuIoAALiKAUz1/7QAAAAHdElNRQfgBgIOHDA16czyAAAfa0lEQVR4Xu1dB3hUZbqeTC/plWSAtElvJIQ0MIjUS9fFRUUWdRXLruu67lrQS1MR3FVWt1iurNgBcRWwgdICCQRSIEQIIUB679P7fb+TkywBhJCZSSbc+z7PPOec7z/1//rfhscZIZiWlRXk6ec3LykpqaqiokLPkm86cNmtU+PJJ5+U1LW1bVerVJ/U1NRsZMk3JUaEhmi12pX4LTEajRyrxZIYERm5r66urpotvqng9BoyMS0tQ6NSPW2xWDgCPr8bW15XV9e6bdu2jRhzeyNwaoasWrVK2N7V9brFahWJxeLd8jFj7gDZYDIYblm3bt2SnrNuLji1lCm7un6j1eke4rq46Hz8/JYeOnQoVy6XjzGaTOMtJtO4jMzMD8+dO6djT78p4LQaMm3atCClSvW81WrliCWSv+fl5RUQPUguf8mFy201mc2hcPB/ZE6+ieC0DGmor38G/sKfx+OdC1co1rNkzp49e2pcXV03uGBfo1b/bsqUKVE9JTcHnJIhqOQ4g8HwEO3DeXz41VdftTEFLBQKxT+4fP4paI9rc1PTiyz5poBTMqStpeVPZrNZgh/HoNPdCXiwRQy2b9+u9fT0/G8XFxeOXq+/MystbQZbNOLhdAwh7UDOcRf5DpgrDnxF4tmzZ//EFvfh2LFjO4RC4Td0HiKxlygiY4tGNJyOIS1NTY9RmCsQCg94eXquogqHr/j9bbfdFsue0gcvb+/noSVak8k0YdeOHQ+y5BENp2LI5MmTR+v0+ntoXyaT/augqGgtGHMSTJE1Nja+zJx0CXJzc09KJZJ3OGCaUq1+Yd68eQFs0YiFUzGks739HlS+J5fHq4Lj3kk0+IoVFFHBVyxMTU2dR7RLERwauh5hcL3FbA68eP78syz5/2Erli9fLoiKiDipCAuzJsbHv8SSGcTGxHwRDnp0ZGTJvffeK2PJfRiXkPAYlSvCw7UTJ05MYskjEk6jISdOnLjFbLEkWjkcg5uHx2csmYGfv//zyNaVyNATSkpLH2fJfVhwxx3vCQSC49AucUdb2zqW/P+wBbHR0W9Awq1RUVE/sKR+SEhIWM1ogULRPn369FCW3If09PTppF10zvjx4xew5BEHp9AQMkNGo3EO7YvF4q8Y4mVITEx8nbJ2q8Xi1VBXt4Yl9yE/P/8HhMHbaF+pVL4IEyhlCkYYnIIhp0+fToO5CceuDk58dw+1Pz755JNuDze3F8jBa3W6JRMmTJjaU/IfBAQGrkQYrISDTyjIz/8NSx5RcAqGIIK6lYOsm8/nHz9w4MB5lnwFjhcVbUMY/D3CXK6yu/uVy5PB/fv3n5XKZG+yYfDTM2bMGMMWjRg4BUOQ2E2krYDP3w8Jh1//eSAZfA5hrs5kNE7Y+dVXy1lyH8aMGfMayi9aLBbfurq6F1jyiMGwM4SSObPROJ72hWJxPkO8BvLy8k5IpNJ/EtfUSAapmb6npAfffPNNh7ur62qmnUunuz87KyuTLRoRGHaGtDY2xsNcecKHdMvl8hMs+ZoIDg5+hcvl1iBMDmhoaPhvltyHZ1as+ATmLwf3FLS0t6/D1ikswYhAfHz87yjcjVQojt5IxSUnJz9A1+FnuJoWZGRkTEQIbGTC4HHj7mbJTo9hlxzY+hjaIrErg5mxMMQB4LnnnvugTws6Ol65fNDD0aNHc4Ui0ce0r1Kr11zehO+sGHaGIK/oTfLOsdsB4Ze//KWZHDx2TUa9fvL69evv7Sn5D8aOHbsGpq3dbDZHlJeVPcmSnRrDyhBIrRARlpz2eVzuDY+zOnLkSJ5EInmfHLxKpVqzcOFCn56SHuzevbtSIhb/mco1Gs2TkydPVvSUOC+GlSEwN64wWUyTuVgma2KIN4jgkJA1XB6vyWIyBZ+vqLiitTcqJubvYPYZi9Xq3trcvJolOy2GlSEGPl8MpnjgZxWJRK0s+YaAMLdOJpGsIy3Q6XS/nZqdndBT0oPPP/9c5ebhQRk8R28w3JOZmXlFhm8PpKWlzUlKSvpLdnZ2BEsaFIaNIVOmTAm/UFLyGnYp29ZRkwdTMAgsuP32t5FUFkLbxHVNTf/ImDRp8oIFCzzZYk5hYeF2lH8Df+XS3t7+0jvvvCNgi2zGjBkzFHFxce+3tbZ+rddqn+poa/slWzQoUNPQkCM5MfEBlUbzEnUqmcxmjlAoVKVnZCR+/PHHF9lTbhjp48dPb+3o2A2mIFHnUn98OUzVUalEstfi4nLQ29vbvbKycq+L1ern7un5UFFR0XvspYMCDQA/cODAo2qV6mkEDYzZhZZ/KRKLnywuLq5iThoEhpQhy5Yt8ywsKPgrTMsyWCnkgy6tcOpeqLxORUREApxwA3vqoBAXE/OpVqe7m0arsEwhR8WBOVOLhMKDJosl1qDXh6DiasanpiZ9+umnHeylN4RMmKfOrq61BqMxhY75AsFpqVT6IhixhTnBBgzZUFLqyasoL/+3Xqf7LzpGdLRRLJFs0Gm1S5BPcCIiI/956tSpbubkQSI+IeGESqlcCmZLZFLpATBkl9XFxRdMCUBWHwHmM2aM/FZzU1Pa2OBgTmRkpLqqqqqducF1MGfatAhowMZupXIDmB4IgVK7urr+Be++HNpynD3NJgwJQ9LT02+Fjd1hNBojUPnd7h4eD58sKXk1KjRUqtJqf0tO3dXN7e1z584NSmJ7AZPUMXr0aK7ZZJqKCuPHxMUtQ8b+Rm1t7Q9IPC8iC+VyXVw8USYGg0KhLQuV3d2/9vf3nxkwalRocHCwNT4+Xnn+/Hkte0sG1F8DjXuiqaVlE7Qii2jQsi+QBy2Ff/qsrKys3/m2wOEmi/otujo7v4B0eggFgrMBgYH3HTx48CiVISJJqKutLSbzNdrfPyEnP/8Mc5ENoMo7fuzYMTwvFjnI1tLTp+9iixggF4luaGjYZjQYEkgQUNGMzyHQe0CrKnB8VCqT/YjtIZCjoHUvQ5iS6RwyT25S6dqC4uKtdGxvODTKykxNzSJmQGI9hCJRYXBg4KxeZhDc3d27sNFD9Xlai6UvKrIFCAzUHp6eNF6Lo9PrF0M7GRPZCzy/zMvL6xnyL/iZvb28VsN0PoX9g7imC9qjMBgM9+K9N3d2dBS1t7XtAvOSUaaCeVoXExOT7ShmEBzGEIrH25XKbaQZAqGwGLZ6/u6DByvZYgb+XG4HpLCNKs9iNPqzZJsBBfkK2rgDERcHlfoKggkxW8QgPz//O5iwL7DL71apppaWlr5+trz81gSFIibAz285yqi5hfppKEfiwW/s8vP3z4SZff7yccb2hkMYQiFhc3Pz+yaDQY6KqZSPHr1o586d9WxxH2aJRBowhIms9Hp9IEO0E7x8fJ7HvdWo2KSSkpLHWHIffNzdKVnUIMO/JTk5mRnJUtfREdPe2bkY5smbhIQAzWmfcttt9x85cqSUITgYDnHqarV6PaKpxTDOWh8vr0U5hw9ftZ/j89OnraMCAmag0uJ48C8tLS172CKbUVNT0wJBcDUZjbfA5CDKTd0CZ90XxVXX1bUEyeUSlGXDRCUGBgZmwMFvgFaFgQmNcNrleK9AaEvhd9999w/2MofD7hqSmZmZrdVofk8SJpFKnz9y/HgOW3RVIEdgWnmRJEYyBDti7Nixf0Zy2Nude8VIlfC4uHchNB0wTXKtVnsnzJMF+crWUYGBWVweLx8axuHx+SfZ04cEdmUIjT6EE1xPdhfRyF6YijfYop8FpPEn2sJMxF9tVKItoO5cmUz2AgkHNHbZpIyMyWwRBxoz/+zJk/+GFnjRMZ2DYGDF6bKyuw4dOnQRJpQ5F0wZUC+mvWBXhsCZLoK0Ue+dwdvb+1l85HU7nBDxUNirwy+kurra7lpSWFz8GYTjR9yf29LRsXbq1KnJsbGxH3S0t++AuUpFXtQMs1SFcpqLkkrXUDsbNDaCsnwwtIS50RDBbgxZtWoVX6tWM51AUql0S++cwOtBoVDUQjw74EJdOjs701my3QChsEI4qCNLj0Qwu+rixcM6rfZX5LLFYvE2mKcMH1/fJXSe3mBYRG1iXV1dwTgW4OK6MWPGnGVuNESwm1PX6XRT4cypP8Lo7ePzazjVxp6SawMadQ8klUa9U4KmamltpXDUrsC71I8OCvI3mc3p8CeIwoUnYJ5+e/LkybXQyk5k8jWBAQHhKE/Sm0zh0BpPfE8aNOdwTk7Ov9jbDAnspiFqpXIxqbgI5oGG6rDka4J8hl6rpUUBuJQzINrJnjNnDmPT7Y2QgICX4eDruPAVYpFoX0FBQT/GB/r6roZAdEI4shBtPUo0nD+kDp1gF4bQAAI4wZm0D6n6nCEOAOVlZfdZrNYYVJIWlaGBlgQ11ddPYYvtil379jVJZLK1MEMctUbzaFZWFuMverH38OELUonkNYqsoLU8cvICkaiYLR4y2IUhlZWVqQhfR+On8fbz28eSr4klS5a4w2f8wQrNcPPw2AC/8zHZdbVWS6s1OAQLFy78F+xVHhgv6Wxvf4XMJFvEIDg09A2Eu6dIiwBVQECAXVpwbwR2YQicOSNtiGaK9+3bN6DBCrDfy8yUhPH5nYh63kRFfU10JHLzFixY4JAxuWvWrDG5ubmRg7cYjMZp48ePZ6bP9WLnzp1KsUDwPphChwKlUjnkY4Pt5UPiSKbI5lK00kP6eSxatMgbecEfaB9m4l3qKEI0c8CFx6vCxe6VFy70a6G1J44jUYUP+YDCXLVKtfYKn+XiMprK4NNECI2HfJEbmxmCcJeL6CSEuIAwqbyHem0g9HwIAUAIdltgrv5KNJJOiUj0Ee1rdboH7Z0kXopAuXw13rXVbDKFVdfW9k25JhNmNJlSiSEEJKuT1r/88lLmYIhgM0N++uknEZwg01ILUbpuqLtw6lQfxPmPw5lzpDLZW8iK+7pt/QIC3sdGCemMPHXihE2DBa6FH3/8sdpNKn2F9rUqFS3PEUf78+fP96cWA+waJVJpCbX4qjSa1aTRVD4UsJkhiOnFMFPuJFVcgeC6XbAVDQ3L4TvklCHDTL3Fkhns3bv3AnzJR3QvjV7/lCNnQYVHRr7F5fNPIKiQNTU1MVOuGxoaovEt3nDqHYqxY++DCa6BcASfO3duyBa5sZkhyB1c8FGIFrkcN4nkmk0lCI9H6Y3G39G+RCz++7fffnuFRvn5+ZEJU+Gecfn5+ff3UO0PWp7D3d19BQfvjQx+waRJkybqjEbGiSM4ubjz+++LESZvoGP4mt9RTyPtOxo2M0Qul9O4Gwi9haPUaq95v3Nnz/4GFT0KEljrL5VetUkbmfE5WgyA0RK1eoUjFwNAckgdVVvpWa3Nza9yrdYFtM/n8ZiEMCUlZRO0pAQ0WUtzc7+p2o6CzQxBLkENg92USIEpPzvCfOYttwSq1eqH6YNdXV3/tufo0Z8d6ZG5aNFKSGkZ7heEiMuhwz99fX1XQkC6afCCSqlcRDQwgWlp+OCDD3QIOpj5J7AEv8jIyJhF+46EzQzZvHmznvwB7cMJjmKIV0FdW9tv4cj9UNE1Kamp1xykVvrdd+kIoy3kVBFxLYc5mcYW2R2HDx8ulyBDJ4GCmhPJ7Orh0df0Ay3aCb+2gwQJYfDLbzz+uIgtcghsZgjlHVBxZqQeXvqq41pnz54drDcYHqW2Lvr4TZs2XVU7srKywhMSEjY1t7Xtg0TG8pCggSvc1paWv106NNTeCAkL24hnnaUMHd/TmJmZ2a/J3b9nKSha5Cbl/UOHfs2SHQKbGcKC6WRC5SWBKf2aIwi1tbVPgO4Fx39+rFR6RevpAw884DYuMfE52Ol8+I0HmEZKkegLXz+/uyhfgDmJvlBR8Tp7ut1BOZCbTPYCCQDgnXfoUL9ugJyjR0+JxOK3aZ/mNULAftYS2Aq7MATRSAGpNI1dum3OnLEsmQGikxCmkgHY49d35uX1G1Sdmpp6R15ubo5SpVoHn+EDbSvz9vK650xZ2SJEWVu9vL2fJsmF6bo/OTn5YfYyu6PwxIntME20/pako7Pz1TfeeKOfafLy8nqFy+PVWM3mwJqamhUs2e6wC0NiY2MLodL1+Ek7amv7ukkJzc3N1IDowePzK+AUP2TJxKh44POOjo4vwMhx0J4OmVS6Mjo2NvNYYWHfWiew4e/DzG0ihsPpbkxLS3NIazDBx8dnBZkmWvV00+bN/aZcw9e0SMViZjlBCNhymNdxTIGzApX7IU3CjImK+jdL4syfOTMqIiJCFR4aaoVvuI9od8+d64v9dTi3myZk0i8+Nvazqy1Q1gtqRomJjs6l+0RFRtbOmDHDYTlBQmzsn2nNlMiIiMY5c+Yws7t6QdMYoqKi8umdY6Ojv2HJdoW9fAg5622M2TKZZiEqYvrGL9TWPgW/InPh8XJLSko+QFy/pKi8PA8S9hz8hBvN6YBJml96+vTd+/btO83c6Cqg0YhBcvlSRGhV0CZ5TXX11vnz5/ebn24vIINfD9NUCfMZUFVVtYolM3j44YeNTDKJ+AV+bXZ6evovekrsB7sxBGbrRxpBQjZYpVL9au7cuWNpvXYUmWF/c+NiYz/t6uz8GJFKBMxTg9TV9Y//NWfOLTBJu3rucG1Qs4qfv/9sXFup1+tpHcbts2bN8usptR9oZKJUJmNyH51We8XCA8ePH98rFAo/I+FDGLzG3o2gV0REtiA5MfGRbpXqLURG9UKBoFyj1d5K+QQq0QiJE2HbInN1/dfUqVNf3bhx4xWhLz7S5c033xRWVFSIEJmJlUqlGOGvCAyQgJFi3IPutRiM/SN2OXDCRyLl8tupN5C9hV2A9+DC9O6Ftt8qEgpzVq9dexvN+mWLmWGy9XV1hTjPzdXN7emTJ0/+mS2yGXZlyJ133ukK03TcoNdTIx1l7gydpAnJo9XDw2MnHrgXlexntlp9Ue6Kk6Q4SwK6DOfJkDzKcK0QxyIEA2IcU7RDPyGSBJpGzdyPfvQMhMfF8tGj74IGDajpf6CYBHvU2NJyCA8ReHp5/Rqa3C9cj4uLWwMNWgmr0KaIiBj/7bffDnrW1KWwK0MICGNJgrfA1rMUJnns20LCmYcy/SdUwahYYhxtCWwuwKCXhq0eWqdF+KvFtVrsU+gsA2OZac6Q4koPL6/7ESYfoGN7AWb2LZ1O9wiEqSYiMjLl66+/7puYOm3aNI/qqqoC+EiFWCzeBD9ol1VR7c4QAiKiz2FmFlFFQ4IvoOZrQKba1/O4XA00ohtcUIMhGi6f3wapz4S/mYtjPaTxSdjoi2CCDjZOrTOb1WCuQSqVGlAxBj8/P0Oct7fup/Z2L5iKwyaDIQT3Jw3UwHw8XlxcbLdhO7SwTVVlZSE1iIql0o2lpaVML2cvUpKSlnQplbRahClg1KjsvLy8Iz0lTgbkGKMRntaEhYRQSLt/w4YNbmzRVYHY+BiFmjAD77CkAWFcSsqD4Qi1Q4KDmR+F3UkJCf+05zIatMAmvRvurcV39cs9IDTc6MjI/RQGQwj30zFbNGjYLcq6FAcPHqyFpD+A8NFAjv3DzZupOf2q2jguMfGPJrN5AsxQZ1BQUN+i+wPBgnnzPhTweJSUkvmDQlpoXZNHS0+d2oPKm8SeZhMioqPfgxk9jnuLW5qa+i2wSc908/CgMNgELb4VYX2/QRNOh9SUlOUktWFI6JBIbbk8RKQ2oQiFopFdGnZQi+pPmDBhLptg6sanpHyEbS0dR4SH65KSktbdc889Ng+8o2l5uKeFviU1OXkxS+4Dvu095pkKxTlbG0EdoiG9KCgqetfbx4eZPqY3GBYXFRV9demye9XV1U9B8gKojWhsSMh1R8pfDcgLvoafoqxZBO1wi42Lmy0Si3+E1olUSuVzeObB9NRUWkt+0P6Scg+E2B/jHhyE9S9ebhKDfHzWQlvaUK6orKy0aZEbhw9xqauryw0ODm40GgzTodaRSpVqAex+GfyFS3NT03v4EKG7m9sLB3NyBh0hxUdHn+1Sqe6H8401m80/wNmvCBk7Ng4RUhxsSQANon7n7bezQsPCmvA+P7um47WA9y1GXrQMAiRXdnfrm5qb+973Qk1NF0JvKwKM6Xhecnxk5BdVdXUDmmp9OYZkzFFjY2NhWHh4IRKtbLPJRK2/i7u7u+dThxYiqsOnSkuZsbSDRWV1dQP8jxQVMgmhcKq/n1+GRqOZBWaLEWYrSbLx3EhEcksDAgLSQ0NDNc8888zF77//vi/Zux6qqqo6x8jl1KI9Ddo3PiEh4UvQ+sJgaP7J+vr6BRCKMRqj0betrW1Qg8aHhCEEvGwFbPFOrUYThI+KQwX5ULgKhpSEKxSlyMyZXsfBYOnSpT6ogAi9TjcD9/U2WywJuLdYLJF8iTB5mUAg+AjhdgCkNwpCEAHNWVxYUDBbLpe7xcfHt128eHFAEzknLFxY3F5VtQBaMgZhfVBLayuzTjABCbExOCSkge4NAUiIio4+jG+64aVCHJKHXA8IEY8hy6XIijmGJOuFAsFBkUTyDSQ6H8ll2bvvvktTpq+KVatWSffs2ROmVakSjRbLdKNePx0VLac0kiItHp/PCfD1vT8vP39zzxU9mDhx4q3t7e3LYT5vR8zKzMzFs7t5AsEBJHe73N3dc3Nycq45Vx6R1Fwkvruo4rw9PeceKyrq1+qLMHgHLMF8vkCQP+quuyYdXLPGxBYNCEPOEHzQzM6ODvrnA56rTPYaDS6A5E4ibSFAumgyfw2OL0Kya1Bh7TBDGuxTBXrj3EBUfCgyZDnKJOxFNGqyWQjnbtDpsi1Wa7hEKn0VidwzTPllyMzMjIc/WAzG/MJkMsXgPmwJR4VnU+9gHhLNApr4Cd9ReXmXc2xMzP9AQx7EO5V8/MknkJ/UvmaJ6dOnj7t44UIuPkPq5ur62ImSkn5jz66HIWUIJJu/dcuWXJisNHzsl6fPnLmD+hjwwZPUavU8mBvyAcmoFD6d3/tyPQ0ojDQTw5h92qLSzvGRI0C7vnf18PiRRkGOHzfuV13d3R/gZENgUFAmaEXMBVcBjcC/cOFCNvzZPPifbPgGpg2uF3iGiRgNPa6B1tXgWY0cLrcBWhgE0/sYTnCRSCSfwuyeMZrNQRaTKQgMHoXvS8FbIjATdiYmJUVu3769hb3ldTGkDElJSroPYeP7eKg+UC7PhHnoN/+Cuk23bt0ago+K1qhUCmwDXHg8TzDPHZKvsrq4dCGEbhYLBJUiqbTM19e3aseOHZ3s5QyIwa+/9tohMDddIBLtOnPmzHy26JqgULampiZepVJNgvlLMxuNMWBQJISjz88ylQWGkVkkgaAfdS8zo+XpmDmrT4CUYpFoL/zjr6jPvod0fQwZQ+bPn+9WduZMIUxNBKTqnVM//fQIW2R3pKWlzWhvbd1Nlefj6zubVm5giwaMWbNmuava2kYZudxwnUYTBs0lE+kPJniDSRIwhUejHqE9ZpPFouFzuZSHNEEIqqVS6Xlox4UDBw5U45prjuYcNiBM/BObzXZe7e8m7A1kz1/S86Kjoo7RdG2W7PRwaKbei5kzZwYiqnqK9iVi8d9++OGHQa8cN1AEBAauhjnRw+xNKCgoYPrzRwKGhCH1dXV/gIoHwP7XhikUzHwQR2P//v0nhWLxe2TnEVGtcNRkUnvD4QyZMmVKFDLkRyh6QRj4qqNX07kUcPrr8dxW+K2Q2urqJ1iyU8PhDGlqaqI/GHaFdpRmh4fbtPDkjYK6AaTIdShy0Wi1T0ydOjWsp8R54VCGpAN6ne5u0g6Zq+uLG7dvt9tSeANFSEjIPxA6l8NkejY3Nj7Pkv9vIjo6+luKdKKionKGevLkpaDxYPQeirAwQ0ZGRhpL/r8FKMds6tDBz4K84DaWPCwgYUD4e5iYEhMdfcM5yYgHxf2ogJ4hl7GxfUNLhxN9vX54p9TU1AFl78MBh/gQZMZLjCZTGjJlvZ+f31qWPKygXj+RULidmjVUSuXqxx088WawsDtDaK6HVqNZQW07YrF4MyKdIV0A7Frw8vFZgwCDJt4k5+bmOnTizWBhd0eLaOYJg15/F5fL7QwYNWop9bSxRcMOZh3GUaO8ob2ZJqMxMTMr66Py8nINW+wUsKuGUBOJRq1mmkikEsnfoB39loV1BgT5+v4FwtJkNpvHVFdW/p4lOw3sypD6+nqmiQQfXBsaHj6oUSSOxu5DhxokUulfKDfSarWPT3ayf92xG0OmZGVFwXc8zCSBUumGoWwiuVEkJia+xePxTlusVve2lpaVLNkpYDeGNHV2UhOJG5fHKw2PiNjEkp0SNAHI3dWVHDwtR353ZmYms8D+TYOJEydmIMY3UoyPrNhhi8bYExAel95xubHR0T/QMVs0rLCLhrS3ta3C1/D5AkHOs88+a/dFLB0BaIfVzcODpqyZaTGzjAkTbu8pGeG4pInECtUf1iaSwSAmOvoT0pLIyMiSRYsW9YxiGUbYpCE0oKCrs3MVdQIJBIJPjxw5MqD1Fp0J/gEB9NcW7RazOaGiouKKf58eathkN1OSkh7qUirfxa4+XKFY5OfndwJZMDOER6fT0WbwYK+38S7XRXBwsCr/6NFXlErlg4i86kePGZO1f/9+u0xPGwwGzRBqQV21cuVRVDyzJB40hEYa9vvDeVtA97QHBnAfRL/MtDoZn8/neLi7P1VYXOywZTyuB1s0xGVcYuIjao3mr8h6yfTxadgNtWH1lPa/tU2q6HhYYbbMEKoqXz+/6bQYP0sfcthcT1mpqVEwUzwXkchmke7X/CqyX2PsQNQWDOH5BAa2bNmyxa5TrG8MHM7/AkXGl1KFCn/hAAAAAElFTkSuQmCC',
#       'width': 0.8,
#       'caption': 'some test image'}]
    
#     replacement_dict = {
#         "<REPLACEME_ITER:CHAPTER_INPUTS>": parts,
#         # "<REPLACEME:DISHID>": "TEST",
#         # Add more token-value pairs as needed
#     }

#     document = docx.Document(document_path)


#     replace_tokens(document_path, replacement_dict, outp)


#         # node = g['WP.CAM_INST.D.##']
    