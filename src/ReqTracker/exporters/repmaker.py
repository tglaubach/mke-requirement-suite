
import tempfile
import shutil
from io import BytesIO
import os
import json
import copy
import re
import datetime

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    


from ReqTracker.exporters.docmaker_tex import DocMaker, TexWriter, ElementFormatter, find_ids_full, find_ids, get_tikz_graph
from ReqTracker.core import graph_main, graph_base, schema, helpers

log = helpers.log


TEMPLATE_DEFAULT = os.path.join(os.path.dirname(__file__), 'templates', 'tex_rep_default_mpifr.zip')

def get_possible_templates():
    d = os.path.join(os.path.dirname(__file__), 'templates')
    return [os.path.join(d, dd) for dd in os.listdir(d)]

class BaseDocPart():

    @classmethod
    def get_default_content(cls):
        raise NotImplementedError('This is an abstract method and needs to be implemented for its child!')
    
    def __init__(self, content=None) -> None:
        self.content = self.get_default_content() if content is None else content

    def parse(self):
        return copy.deepcopy(self.content)


class Ptext(BaseDocPart):
    
    contenttype = 'str'

    @classmethod
    def get_default_content(cls):
        return ''

    def parse(self):
        return self.content
    
class PIterator(BaseDocPart):
    
    contenttype = 'list'
    
    @classmethod
    def get_default_content(cls):
        return []
    
class PTabular(BaseDocPart):
    
    contenttype = 'tabular'
    
    @classmethod
    def get_default_content(cls):
        return [[]]
    

class PTable(BaseDocPart):
    
    contenttype = 'table'
    
    @classmethod
    def get_default_content(cls):
        return dict(type=cls.contenttype, value=None, layout='', hlines=1, caption='')
    
    def __init__(self, value:str, layout:str, hlines=1, caption='') -> None:
        super().__init__(dict(type=self.contenttype, value=value, layout=layout, hlines=hlines, caption=caption))

class PImage(BaseDocPart):
    
    contenttype = 'image'
    
    @classmethod
    def get_default_content(cls):
        return dict(type=cls.contenttype, value='', width=0.8, caption='')
        
    def __init__(self, path:str, width=0.8, caption='', imageblob='') -> None:
        super().__init__(dict(type=self.contenttype, value=path, width=width, caption=caption, imageblob=imageblob))
        assert 'type' in self.content

    def parse(self):
        assert 'type' in self.content
        return copy.deepcopy(self.content)
    


class PRawFile(BaseDocPart):
    
    contenttype = 'rawfile'
    
    @classmethod
    def get_default_content(cls):
        return dict(type=cls.contenttype, value='')
    
    def __init__(self, path:str, filecontent:str='') -> None:
        super().__init__(dict(type=self.contenttype, value=path, filecontent=filecontent))

class PInput(BaseDocPart):
    
    contenttype = 'input'
    
    @classmethod
    def get_default_content(cls):
        return dict(type=cls.contenttype, value='', content='')

    def __init__(self, path:str, content='') -> None:
        super().__init__(dict(type=self.contenttype, value=path, content=content))


class PVerbatim(BaseDocPart):
    
    contenttype = 'verbatim'
    
    @classmethod
    def get_default_content(cls):
        return dict(type=cls.contenttype, value='')
        

    def __init__(self, value:str) -> None:
        if isinstance(value, (list, dict)):
            value = json.dumps(value)
        super().__init__(dict(type=self.contenttype, value=value))


class Pmarkdown(BaseDocPart):
    
    contenttype = 'markdown'

    @classmethod
    def get_default_content(cls):
        return dict(type=cls.contenttype, value='')
        

    def __init__(self, content:str='', value:str='') -> None:
        # HACK: either content OR value.... but the bug is somewhere else and I don't know where :-(
        if not content and value:
            content = value
        super().__init__(dict(type=self.contenttype, value=content))
    

class ReportMaker():

    def __init__(self, docname, long_title = '', author='', changenote='', template_dir=TEMPLATE_DEFAULT, dc_json_annex={}) -> None:

        parts = schema.Document.fun_match(docname)
        self.parts = parts if parts else docname

        self.template_dir = template_dir
        self.writer = TexWriter()
        
        self.long_title = long_title
        self.author = author
        self.data = None
        self.changenote = changenote

        self.dc_auto_json = dc_json_annex

        
    @property
    def docname(self):
        if isinstance(self.parts, str):
            return self.parts
        else:
            return '-'.join([self.parts[p] for p in schema.Document.default_keys if p in self.parts and self.parts[p]])


    def _test_if_init(self):
        assert self.data, 'need to call init() first!'


    def init(self, node=None, key_info = 'NODE_INFO', key_id='NODE_ID', key_graph='NODE_GRAPH', minimal_style=True):
    
        

        g = node.graph if not node is None else None
        if self.docname:
            try:
                docid = schema.Document.fun_get_doc_id(self.docname)
            except Exception as err:
                docid = ''
        else:
            docid = ''

        assert not self.data, 'this ReportMaker already has some data in the input dict (self.data). Either clear the data or make a new ReportMaker'
        self.data = DocMaker(self.template_dir).make_docdc(minimal=minimal_style)


        # -----------------------------------------------------------------------
        # fill report name parts:
        if isinstance(self.parts, str): 
            parts = {
                'dish_no': '',
                'title': '',
                'version': ''
            }
        else:
            parts = self.parts
        
        mapper = {
            'AUTHOR': self.author,
            'DOCNAME': self.docname,
            'DOCID': docid,
            'DISHID': parts['dish_no'],
            'DOCTITLE': parts['title'],
            'LONGTITLE': self.long_title,
            'VERSION': parts['version']
        }

        for k, v in mapper.items():
            self.data[k] = v

        # -----------------------------------------------------------------------
        # fill_version_history:
        if not g is None and  docid in g:
            node_rep = g[docid]
            if 'versionhist' in node_rep.data:
                versionhist = node_rep.data['versionhist']
            else:
                versions = docid.get_version_hist()
                f = lambda p: 'v' + v + ' ' + schema.Document.fun_match(p)
                versionhist = [[datetime.datetime.today().strftime('%Y-%m-%d'), '', f(v)] for v in versions]
        else:
            versionhist = []
        versionhist.append([parts['version'], '', self.changenote])

        self.data['VERSIONHIST'] = versionhist

        # -----------------------------------------------------------------------
        # fill_main_node
        if not node is None:
            

            writer = TexWriter()

            node_text = get_tikz_graph(node)
            if node.has_doc:
                node_text = [node_text] + node.doc_get_parts()

            self.data[key_id] = node.id
            self.data[key_graph] = node_text
            self.data[key_info] = writer.node2content(node)


            self.dc_auto_json['node_dc'] = node.to_dict(True)

        else:
            self.data[key_id] = 'NONE'
            self.data[key_graph] = ''
            self.data[key_info] = 'NONE'
            self.dc_auto_json['node_dc'] = {}

        return self
    
    def set_data(self, token_dc):
        for k, v in token_dc.items():
            if v:
                self.data[k] = v
        return self
    
    def post_proc(self, indent=1):
        self._test_if_init()
        
        # -----------------------------------------------------------------------
        # write autogen json
        self.data['AUTO_JSON'] = PVerbatim(json.dumps(self.dc_auto_json, indent=indent))

        

        for k in 'AUTHOR DOCID DISHID DOCTITLE VERSION'.split(): # these fields need an non empty string to render the latex
            if not self.data[k]:
                self.data[k] = 'NONE'
                self.data[k].replace('_', '-')
        # -----------------------------------------------------------------------
        # parse all parts
        self.data = {k:v.parse() if issubclass(type(v), BaseDocPart) else v for k, v in self.data.items()}


        return self
    

    def post_proc_rdy(self, dc_rdy, nodes=None, parse_key='ALLNODES', rep_ids=False):
        self._test_if_init()

        writer = TexWriter()

        if nodes is None:
            nodes = []

        # -----------------------------------------------------------------------
        # make nodes to hrefs
        if not nodes is None:
            nodes_in_doc = find_ids_full({k:v for k, v in dc_rdy.items() if 'AUTO_JSON' not in k}, graph_main.el2id(nodes))
            log.info(f'found {nodes_in_doc=}')

        if rep_ids:
            for node, locations in nodes_in_doc.items():
                log.info(f'making to links: "{id_}"')
                for key, ilines in locations:
                    ref = writer.grefid(node.id)
                    lines = dc_rdy[key].split('\n')
                    for i in ilines:
                        lines[i] = lines[i].replace(node.id, ref)
                    dc_rdy[key] = lines.join('\n')

        
        handled = {}
        for n in nodes:
            id_ = n.id
            if not id_ in handled and id_ in nodes_in_doc:
                log.info(f'writing: "{id_}"')
                handled[id_] = writer.node2subsec(n)
        
        subsections = [t[1] for t in sorted(handled.items())]
        dc_rdy[parse_key] = '\n\n'.join(subsections)
        to_ascii = lambda text: re.sub(r'[^\x00-\x7F]+','?', text)
        dc_rdy = {k:to_ascii(v) for k, v in dc_rdy.items()}
        return dc_rdy
    


    

    def make_report(self, out_files=None, nodes = None, verb=True, **kwargs):
        
        self._test_if_init()
        
        if out_files is None:
            out_files = []
        
        elif 'out_file' in kwargs:
            out_files.append(kwargs.get('out_file'))

        if isinstance(out_files, str):
            out_files = [out_files]

        if not out_files:
            out_files.append(None)

        fun_pp = lambda dc_rdy: self.post_proc_rdy(dc_rdy, nodes=nodes)

        for out_file in out_files:
            if out_file and os.path.isdir(out_file):
                out_dir = os.path.join(out_file, self.docname)
                DocMaker(self.template_dir, out_dir).run(self.data, fun_pp, verb=verb)

            elif not out_file or out_file.endswith('.zip'):
                with tempfile.TemporaryDirectory() as tmpdirname:
                    target_dir = os.path.join(tmpdirname, self.docname)
                    os.mkdir(target_dir)
                    if verb:
                        print('created temporary directory', target_dir)

                    DocMaker(self.template_dir, target_dir).run(self.data, fun_pp, verb=verb)
                    
                    shutil.make_archive(target_dir, 'zip', target_dir)

                    with open(target_dir + '.zip', "rb") as fh:
                        filename = os.path.basename(target_dir + '.zip')
                        filecontent = BytesIO(fh.read())
                        filecontent.seek(0)

                if not out_file:
                    return filename, filecontent
                else:
                    with open(out_file, "wb") as fh:
                        fh.write(filecontent.read())
            else:
                raise ValueError(f'{out_file=} is not understood as a valid input!')
            
    def run(self, out_file=None, node=None):
        self._test_if_init()

        if not node is None:
            nodes = node.graph.nodes
        else:
            nodes = None
        
        return self.post_proc().make_report(out_file, nodes=nodes)

def helper(docname, g, node):
    default_filename, file_content = ReportMaker(docname).init(node).run(None, node=node)
    return docname + '_tex.zip', file_content.read()

def add_docmaker_pipe():
    graph_base.docmaker_pipes['_tex.zip'] = helper


if __name__ == '__main__':

    pth = r'C:\Users\tglaubach\repos\mke-requirement-suite\scripts\req_db.sqlite'

    g = graph_main.Graph(pth)
    id_ = f'VR.OREL.P.61.BUD'
    docname = 'MKE-316-000000-MPI-IDD-3075-01-BlaBlaBla'
    outdir = 'C:\\temp\\testreps'
    
    rep = ReportMaker(docname).init(g[id_]).run(outdir, node=g[id_])
