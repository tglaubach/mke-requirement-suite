
import argparse
import base64
import os
import re
from pathlib import Path
import json
import shutil
import traceback
import sys

import tempfile
import shutil
from io import BytesIO

from dataclasses import dataclass
import zipfile

try:
    from ReqTracker.core import helpers
    print = helpers.log.info

        
    import markdown
    try:
        from ReqTracker.exporters import mdx_latex
    except Exception as err:
        from . import mdx_latex
    
    md = markdown.Markdown()
    latex_mdx = mdx_latex.LaTeXExtension()
    latex_mdx.extendMarkdown(md)


except Exception as err:
    print(err)
    print('failed to import ReqTracker... trying to make due without!')






marker_pattern = re.compile(r"(?P<COMMAND>[A-Z]+)_?(?P<tp>[A-Z]+)?_?(?P<ncols>[0-9]+)?")
find_pattern = re.compile(r'<[a-zA-Z0-9\-_]+:[a-zA-Z0-9\-_]+>')
match_pattern = re.compile(r"<(?P<COMMAND>[A-Z]+)_?(?P<tp>[A-Z]+)?_?(?P<ncols>[0-9]+)?:(?P<KEY>[a-zA-Z0-9\-_]+)>")
mine_pattern = re.compile(r"(?P<COMMAND>[A-Z]+)_?(?P<tp>[A-Z]+)?_?(?P<ncols>[0-9]+)?:?(?P<KEY>[a-zA-Z0-9\-_]+)?")

def token2dict(s):
    r = [m.groupdict() for m in mine_pattern.finditer(s)]
    # r = [rr for rr in r if {}]
    if not r:
        return mine_pattern.match(s).groupdict()
    else:
        return r[0]









def mkcolor(txt, color=None):
    if color is None:
        color=txt # special case for status
    return f'{{\\color{{{color}}}{txt}}}'

def mk_cmd(txt, cmd):
    return f'\\{cmd}{{{txt}}}'

def sanitize_id(id_):
    return id_.replace('#', 'h').replace('_', ' ')

def id2ref(id_):
    return id_.replace('#', 'x').replace('_', 'd').replace('_', ' ').replace('.', 'p')



def find_ids(docdc_rdy:dict, ids_to_test:set):
    
    found_ids = []
    ids_to_test = set(ids_to_test)
    fun = lambda l: r'\hyperref' in l or r'\label' in l

    for k, v in docdc_rdy.items():
        lines = v.split('\n')
    
        for id_ in ids_to_test:
            if [l for l in lines if id_ in l and not fun(l)]:
                found_ids.append(id_)

    return found_ids


def find_ids_full(docdc_rdy:dict, ids_to_test:set):
    
    found_ids = {}
    ids_to_test = set(ids_to_test)
    fun = lambda l: r'\hyperref' in l or r'\label' in l

    for k, v in docdc_rdy.items():
        lines = v.split('\n')
    
        for id_ in ids_to_test:
            rows = [i for i, l in enumerate(lines) if id_ in l and not fun(l)]
            if rows:
                if not id_ in found_ids:
                    found_ids[id_] = []
                found_ids[id_].append((k, rows))

    return found_ids



"""

 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄       ▄       ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌     ▐░▌     ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
 ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀  ▐░▌   ▐░▌      ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ 
     ▐░▌     ▐░▌            ▐░▌ ▐░▌       ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌          ▐░▌     ▐░▌          
     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄    ▐░▐░▌        ▐░▌   ▄   ▐░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌          ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ 
     ▐░▌     ▐░░░░░░░░░░░▌    ▐░▌         ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌     ▐░▌          ▐░▌     ▐░░░░░░░░░░░▌
     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀    ▐░▌░▌        ▐░▌ ▐░▌░▌ ▐░▌▐░█▀▀▀▀█░█▀▀      ▐░▌          ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ 
     ▐░▌     ▐░▌            ▐░▌ ▐░▌       ▐░▌▐░▌ ▐░▌▐░▌▐░▌     ▐░▌       ▐░▌          ▐░▌     ▐░▌          
     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄  ▐░▌   ▐░▌      ▐░▌░▌   ▐░▐░▌▐░▌      ▐░▌  ▄▄▄▄█░█▄▄▄▄      ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ 
     ▐░▌     ▐░░░░░░░░░░░▌▐░▌     ▐░▌     ▐░░▌     ▐░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌
      ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀       ▀       ▀▀       ▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                                                                                                                                                                 
"""



def get_tikz_graph(node, margin=(2,2), width=16, height='auto', infig=True, caption='', maxlevel=6):
    
    writer = TexWriter()
    edges = node.get_lower(as_edges=True, maxlevel=maxlevel) # + node.get_upper(maxlevel=1)
    assert node.has_graph, 'need to supply a node which is bound to a graph'
    g = node.graph


    v = g.to_view(node.id, edges)
    if not edges:
        v.default_positions = {'GRAPH_EMPTY':{'x':0, 'y':0}}

    if not caption:
        caption = 'Verification Graph Leading up to: ' + v.name

    
    return writer.get_tikz(g, v, 
                                margin=margin,
                                width=width,
                                height=height,
                                full_graph = g, 
                                infig=infig, 
                                caption = caption)
    
    
    

class TexWriter:

    def __init__(self, refs = None, cache_ids =None) -> None:
        self.refs = [] if refs is None else refs

        if cache_ids is None:
            cache_ids = False if self.refs else True
        self.cache_ids = cache_ids


        
    def grefid(self, id_, color=''):

        id_ = id_.id if not isinstance(id_, str) else id_
        
        if color:
            txt = f'{{\\color{{{color}}}{sanitize_id(id_)}}}'
        else:
            txt = sanitize_id(id_)
        
        if self.refs and not id_ in self.refs or not self.refs:
            return rf'{{{txt}}}'
        else:
            return rf'\hyperref[{id2ref(id_)}]{{{txt}}}'


    ###############################################################################################

    """
    ███    ██  ██████  ██████  ███████     ██     ██ ██████  ██ ████████ ██ ███    ██  ██████  
    ████   ██ ██    ██ ██   ██ ██          ██     ██ ██   ██ ██    ██    ██ ████   ██ ██       
    ██ ██  ██ ██    ██ ██   ██ █████       ██  █  ██ ██████  ██    ██    ██ ██ ██  ██ ██   ███ 
    ██  ██ ██ ██    ██ ██   ██ ██          ██ ███ ██ ██   ██ ██    ██    ██ ██  ██ ██ ██    ██ 
    ██   ████  ██████  ██████  ███████      ███ ███  ██   ██ ██    ██    ██ ██   ████  ██████  
                                                                                            
                                                                                            
    """


    def node2content(self, n):
        verb = lambda s: f'\\verb+{s}+'
        statuss = rf'\textbf{{Status:}} {{{mkcolor(verb(n.status.name), n.status.name)}}}' + r'\\'
        tags = ' | '.join([t.strip() for t in n.tags if t.strip()])
        tagss = rf'\textbf{{Tags:}} {{ \texttt{{{tags}}} }}' + r'\\'
        
        body = n.text.rstrip()
        
        bodys = r'\textbf{Text:}' + '\n\n' + (body + r'\\' if body else '') + '\n\n'

        ctexts = rf'\textbf{{Children:}}' + '\n / \n'.join([self.grefid(e.id, e.status.name) for e in n.children]) + r'\\'
        ptexts = rf'\textbf{{Parents:}} ' + '\n / \n'.join([self.grefid(e.id, e.status.name) for e in n.parents]) + r'\\'
        
        notess = rf'\textbf{{Notes:}}' + r'\\' + '\n / \n'.join(['{}: {}'.format(mk_cmd(k, 'textbf'), v) for k, v in n.notes.items()]) + r'\\'

        parts = [statuss, tagss, bodys]
        
        txt = '\n\n'.join(parts)
        
        txt += mk_cmd('\n\n'.join([notess, ptexts, ctexts]), 'small')
        
        return txt
            


    def node2content_md(self, n):
        raise NotImplementedError("this is a bit harder still and I need to implement this")


    def node2subsec(self, n):

        headers = rf'\subsection{{{sanitize_id(str(n))}}}\label{{{id2ref(n.id)}}}'
        return headers + '\n\n' + self.node2content(n)
            




###############################################################################################


    """
████████ ██ ██   ██ ███████      ██████  ██████   █████  ██████  ██   ██ 
   ██    ██ ██  ██     ███      ██       ██   ██ ██   ██ ██   ██ ██   ██ 
   ██    ██ █████     ███       ██   ███ ██████  ███████ ██████  ███████ 
   ██    ██ ██  ██   ███        ██    ██ ██   ██ ██   ██ ██      ██   ██ 
   ██    ██ ██   ██ ███████      ██████  ██   ██ ██   ██ ██      ██   ██ 
                                                                                                                                           
    """


    # mk_n = lambda x, y, c, l, i: fr'\Vertex[x={x},y={y},color={c},opacity=0.8,label={{\texttt{{{l}}}}}]{{{i}}}'
    # mk_n = lambda x, y, c, l, i: fr'\node [circle split,draw] at ({x},{y}),color={c},opacity=0.8,label={{\texttt{{{l}}}}}]{{{i}}}'
    def _mk_n(self, x, y, c, l, i, tc='black'):
        template = fr"\node[ellipse split, fill={c}, draw={tc}] (n{i}) at ({x},{y}) {{REPLACEME:CONTENT}};"

        frmt = lambda x: fr'\tiny{{{self.grefid(x)}}}'
        content = frmt(l) + r' \nodepart{lower} ' + frmt(sanitize_id(c))
        
        return template.replace('REPLACEME:CONTENT', content)


    # mk_e = lambda a, b: fr'\Edge[,bend=-8.531]({a})({b})'
    def _mk_e(self, a, b, straight_inout=False):
        s = '[in=270, out=90]' if straight_inout else ''
        return fr'\draw[->, very thick] (n{a}) edge{s} (n{b});'

    def _mk_ed(self, a, b, straight_inout=False):
        s = '[in=270, out=90]' if straight_inout else ''
        return fr'\draw[->, dashed, draw=gray] ({a}) edge{s} ({b});'


    def _get_xy_sub(self, v, margin=(2,2), width=16, height='auto'):
        xy = v.get_xy(normalize=True)
        
        if 'np' not in sys.modules:
            import numpy as np

        if height == 'auto':
            height = np.unique(xy[:,1]).shape[0] * 2 + 2*margin[0]
        
        if np.unique(xy[:,0]).shape[0] == 1:
            width = 9
            margin = (3, margin[1])
            xy[:,0] = 0.5

        if np.unique(xy[:,0]).shape[0] == 2:
            width = 12
            margin = (3, margin[1])

        elif np.unique(xy[:,0]).shape[0] == 3:
            width = 14
            margin = (3, margin[1])
            

        height = min(height, 16)
        width = min(width, 16)
        
        scl = (width, height)
        # xy = xy[:, ::-1]

        xy[:,0] *= scl[0] - 2*margin[0]
        xy[:,1] *= scl[1] - 2*margin[1]

        xy[:,0]  += margin[0]
        xy[:,1]  += margin[1]

        xy = np.round(xy, 2)
        return xy, scl

    def put_in_figure(self, txt, caption = ''):

        if caption:
            txt += '\n' + fr'\caption{{{caption}}}'

        template = r"""\begin{figure}[h!]
    \centering
    <REPLACEME:CONTENT>
    \end{figure}"""

        return template.replace('<REPLACEME:CONTENT>', txt)


    def get_tikz(self, g, v, margin=(2,2), width=16, height='auto', full_graph = None, infig=True, caption=''):
        
        
        xy, scl = self._get_xy_sub(v, margin, width, height)
        
        view = v.to_igraph().copy()
        
        to_delete = [vv.index for vv in view.vs if vv['name'] == v.name]
        view.delete_vertices(to_delete)

        ei = view.get_edgelist()
        ni = [vv.index for vv in view.vs]
        keys = [vv['name'] for vv in view.vs]
        stati = [g[n].status.name for n in keys]
        
        
        template = fr"""\begin{{tikzpicture}}REPLACEME:STYLE
    \clip (0,0) rectangle ({scl[0]},{scl[1]});
    REPLACEME:CONTENT
    \end{{tikzpicture}}"""
        
        
        
        
        lowest_nids = [id_ for id_ in keys if not v.get_children(id_) if id_ in v]
        

        style = r'[every text node part/.style={font=\itshape}, every lower node part/.style={font=\footnotesize}]'
        style = '[rounded corners, text centered, minimum width = 1cm]'


        content = []
        dashed_edges = []
        for (x, y), st, id_, i in zip(xy, stati, keys, ni):
            
            tc = 'blue' if id_ == v.name else 'black'
            line = self._mk_n(x, y, st, id_, i, tc)
            
            content.append(line)
            
            if id_ in lowest_nids and not full_graph is None and  id_ in full_graph:
                cc = g[id_].get_children()
                if cc:
                    line = fr'\node (s{i}) [below=0.3cm of n{i}] {{{len(cc)} more}};'
                    content.append(line)
                    dashed_edges.append((f's{i}', f'n{i}'))
                    
        content += [self._mk_e(c, p) for p, c in ei]
        content += [self._mk_ed(a, b) for a, b in dashed_edges]
        
        content = '   ' + '\n   '.join(content)
        txt = template.replace('REPLACEME:STYLE', style)
        txt = txt.replace('REPLACEME:CONTENT', content)


        if infig:
            txt = self.put_in_figure(txt, caption=caption)

        return txt











"""

 ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄       ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄    ▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░▌  ▐░▌▐░░░░░░░░░░░▌
▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌     ▐░▌░▌   ▐░▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌ ▐░▌ ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌     ▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌▐░▌▐░▌  ▐░▌          
▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌     ▐░▌ ▐░▐░▌ ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░▌░▌   ▐░█▄▄▄▄▄▄▄▄▄ 
▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░▌    ▐░░░░░░░░░░░▌
▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀      ▐░▌   ▀   ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌░▌   ▐░█▀▀▀▀▀▀▀▀▀ 
▐░▌     ▐░▌  ▐░▌          ▐░▌               ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌▐░▌  ▐░▌          
▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌               ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌ ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ 
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌               ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌  ▐░▌▐░░░░░░░░░░░▌
 ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀                 ▀         ▀  ▀         ▀  ▀    ▀  ▀▀▀▀▀▀▀▀▀▀▀ 
                                                                                           
                                                                                                                                         
"""

###########################################################################################
"""

███████  ██████  ██████  ███    ███  █████  ████████ 
██      ██    ██ ██   ██ ████  ████ ██   ██    ██    
█████   ██    ██ ██████  ██ ████ ██ ███████    ██    
██      ██    ██ ██   ██ ██  ██  ██ ██   ██    ██    
██       ██████  ██   ██ ██      ██ ██   ██    ██    
                                                     
"""
###########################################################################################

@dataclass(frozen=True)
class ElementFormatter:

    root_path:str = ''
    copy_files:bool = True



    def handle_error(self, err, el):
        txt = 'ERROR WHILE HANDLING ELEMENT:\n{}\n\n'.format(el)
        if not isinstance(err, str):
            txt += '\n'.join(traceback.format_exception(err, limit=5)) + '\n'
        else:
            txt += err + '\n'
        txt = r"""
\begin{verbatim}

<REPLACEME:VERBTEXT>

\end{verbatim}""".replace('<REPLACEME:VERBTEXT>', txt)
        txt = f'{{\\color{{red}}{txt}}}'

        return txt

    def digest_tabular(self, el, hlines=0) -> str:
        ret = ''
        for row in el:
            srow = [f'{{{self.digest(e, make_blue=True)}}}' for e in row]
            ret += ' & '.join(srow) + r'\\' + '\n'
            if hlines:
                ret += '\\hline\n'

        return ret
        
    def digest_table(self, value=None, layout='', hlines=1, caption='', **kwargs) -> str:

        template = r"""\begin{table}[h!]
%\centering
\begin{tabular}{<REPLACEME:LAYOUT>}
<<REPLACEME:CONTENT>
\end{tabular}
<<REPLACEME:CAPTION>
\end{table}"""

        if not layout:
            layout = len(value[0]) * 'l'

        txt = template.replace('<REPLACEME:LAYOUT>', layout)

        content = self.digest_tabular(value, hlines=hlines)

        txt = txt.replace('<REPLACEME:CONTENT>', content)

        if caption:
            txt += fr'\caption{{{caption}}}'
        txt = txt.replace('<REPLACEME:CAPTION>', caption)
        return txt
    

    def digest_markdown(self, value='', **kwargs) -> str:
        tex = md.convert(value).lstrip('<root>').rstrip('</root>')
        return tex

    
    def digest_image(self, value='', width=0.8, caption='', imageblob='', **kwargs) -> str:

        if not isinstance(width, str):
            width = 'width={}\\textwidth'.format(width)

        file_name = os.path.basename(value)
        abspath = os.path.join(self.root_path, 'inp', file_name)
        path = './inp/' + file_name

        if imageblob:
            if isinstance(imageblob, str):
                if ';base64, ' in imageblob:
                    imageblob = imageblob.replace(';base64, ', ';base64,')

                imageblob = imageblob.encode("utf8")

            data = imageblob.split(b";base64,")[1]
            assert not os.path.exists(abspath), f'can not write a file if it already exists! "{value}'
            s = str(data)[:25] + '...'
            print(f'writing:\n   data= {s}\n     to: {abspath}')
            if not os.path.exists(os.path.dirname(abspath)):
                os.makedirs(os.path.dirname(abspath))
            with open(abspath, "wb") as fp:
                fp.write(base64.decodebytes(data))

        elif self.copy_files:
            print(f'copying:\n   from: {value}\n     to: {abspath}')
            if os.path.isfile(value) and value.endswith('.zip'):
                with zipfile.ZipFile(value, 'r') as zip_ref:
                    zip_ref.extractall(abspath)
            else:
                shutil.copyfile(value, abspath)

        txt = fr'\includegraphics[{width}]{{{path}}}'

        if caption:
            txt += '\n' + fr'\caption{{{caption}}}'

        txt = r"\begin{figure}[h!]" + '\n' + r"\centering" '\n' + txt + '\n' + r"\end{figure}"
        return txt


    def digest_input(self, value='', content='', **kwargs) -> str:

        file_name = os.path.basename(value)
        abspath = os.path.join(self.root_path, 'inp', file_name)
        path = './inp/' + file_name

        if self.copy_files:
            if os.path.exists(value):
                shutil.copyfile(value, abspath)
                assert not content, 'can not give content of a file which exists and is not empty!'
            else:
                with open(abspath, 'w') as fp:
                    content = '% write your input in here!' if not content else content
                    fp.write(content)
         
        txt = fr'\input{{{path}}}'

        return txt


    def digest_rawfile(self, value='', filecontent='', **kwargs) -> str:
        if filecontent:
            return filecontent
        else:
            with open(value, 'r') as fp:
                txt = fp.read()
            return txt


    def digest_verbatim(self, value='', **kwargs) -> str:
        txt = self.digest(value)
        template = r"""\begin{tabular}{|p{16cm}|}
\hline
\begin{tiny}\begin{verbatim}
<REPLACEME:VERBTEXT>
\end{verbatim}\end{tiny}
\\
\hline
\end{tabular}\par"""
        txt = txt.strip('\n')
        parts = []

        while len(txt) > 2000:
            parts.append(template.replace('<REPLACEME:VERBTEXT>', txt[:2000]))
            txt = txt[2000:]
        parts.append(template.replace('<REPLACEME:VERBTEXT>', txt))

        txt = '\n\n'.join(parts)
        # if caption:
        #     caption = fr'\caption{{{caption}}}'

        # txt = txt.replace('<REPLACEME:CAPTION>', caption)

        return txt


    def digest_iterator(self, el) -> str:
        if isinstance(el, dict) and el.get('type', '') == 'iter' and isinstance(el.get('value', None), list):
            el = el['value']
        return '\n\n'.join([f'% Iterator Element {i}\n' + self.digest(e) for i, e in enumerate(el)])
    
    def digest_str(self, el):
        # if '\n' in el:
        #     return f'\n\\color{{blue}}\n{el}\n\\color{{black}}'
        # else:
        return el
        
    def digest(self, el, make_blue=False):
        blue = lambda s: f'{{\\color{{blue}}{s}}}'
        try:
            
            if not el:
                return ''
            elif isinstance(el, str):
                ret = self.digest_str(el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'iter':
                ret = self.digest_iterator(el)
            elif isinstance(el, list) and el and isinstance(el[0], list):
                ret = self.digest_tabular(el)
                make_blue = False
            elif isinstance(el, list) and el:
                ret = self.digest_iterator(el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'table':
                ret = self.digest_table(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'image':
                ret = self.digest_image(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'input':
                ret = self.digest_input(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'rawfile':
                ret = self.digest_rawfile(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'verbatim':
                ret = self.digest_verbatim(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'markdown':
                ret = self.digest_markdown(**el)
            else:
                return self.handle_error(f'the element of type {type(el)}, could not be parsed.', el)
            
            return blue(ret) if make_blue else ret
        
        except Exception as err:
            return self.handle_error(err, el)


    def format(self, docdc:dict) -> dict:
        return {key:self.digest(value, make_blue=True) for key, value in docdc.items()}













###########################################################################################
"""

██████   ██████   ██████     ███    ███  █████  ██   ██ ███████ ██████  
██   ██ ██    ██ ██          ████  ████ ██   ██ ██  ██  ██      ██   ██ 
██   ██ ██    ██ ██          ██ ████ ██ ███████ █████   █████   ██████  
██   ██ ██    ██ ██          ██  ██  ██ ██   ██ ██  ██  ██      ██   ██ 
██████   ██████   ██████     ██      ██ ██   ██ ██   ██ ███████ ██   ██ 
                                                                        
"""
###########################################################################################

@dataclass(frozen=True)
class DocMaker:

    template_dir:str = ''
    target_dir:str = ''


    def get_tokens(self, crawl_where='target_dir template_dir', minimal=True):

        if isinstance(crawl_where, str):
            crawl_where = crawl_where.split()

        d = None
        for p in crawl_where:
            if hasattr(self, p):
                p = getattr(self, p)
            if os.path.exists(p) and (os.path.isdir(p) or p.endswith('.zip')):
                d = p
                break

        assert not d is None, 'no path or the given pathes exists and is a dir!'

        if os.path.exists(d) and d.endswith('.zip'):
            input_zip=zipfile.ZipFile(d)
            dc_lines = {name: input_zip.read(name).decode('UTF-8').split('\n') for name in input_zip.namelist() if name.endswith('.tex')}
        else:
            dc_lines = {}
            files = list(Path(d).rglob("*.[tT][eE][xX]"))
            for f in files:
                with open(f) as fp:
                    dc_lines[f] = fp.readlines()
        
        res = []
        for f, lines in dc_lines.items():
            for i, l in enumerate(lines, 1):
                if not l.strip().startswith('%'):
                    for m in find_pattern.findall(l.strip()):
                        dc = dict(file = str(f).replace('\\', '/'), line=i, linetext=l, groups = token2dict(m))
                        res.append((m, dc))

        if minimal:
            return list(dict(res).keys())
        else:
            return dict(res)
        

    
    def copy_tmpl2trgt(self, rename_main=True, on_exist='raise', verb=False):
        from_path, to_path = self.template_dir, self.target_dir

        dirs_exist_ok = False

        if os.path.exists(to_path) and list(Path(to_path).rglob("*")):
            if on_exist == 'ask':
                a = ''
                while not a and not a[0] in 'cor':
                    a = input(f'The directory "{to_path}" exists and is not empty! what to do? (c="clear", o="overwrite", r="raise/return"): ')
                if a[0] == 'c':
                    on_exist = 'clear'
                elif a[0] == 'o':
                    on_exist = 'overwrite'
                elif a[0] == 'r':
                    on_exist = 'raise'
        
            if on_exist == 'raise':
                raise FileExistsError(f'The directory "{to_path}" exists and is not empty!')
            elif on_exist == 'clear':
                shutil.rmtree(to_path)
            elif on_exist == 'overwrite':
                dirs_exist_ok = True
            else:
                raise KeyError(f'"on_exist" must be one of "ask", "raise", "clear", or "overwrite", but was "{on_exist}"')
        
        if verb: print(f'copying:\n   from: {from_path}\n     to: {to_path}')
        
        if os.path.isfile(from_path) and from_path.endswith('.zip'):
            with zipfile.ZipFile(from_path, 'r') as zip_ref:
                zip_ref.extractall(to_path)
        else:
            shutil.copytree(from_path, to_path, dirs_exist_ok=dirs_exist_ok)
        
        inp = os.path.join(to_path, 'inp')
        os.makedirs(inp, exist_ok=True)

        if rename_main:
            docname = os.path.basename(to_path)
            self.rename_main(docname)

        return self


    




    def merge_exp_act_kws(self, docdc:dict, docdc_rdy:dict, tokens:set = None):
        if tokens is None:
            tokens = self.get_tokens()

        mapper = {}
        tester = {}
        for k in set(tokens):
            m = match_pattern.match(k)
            if m:
                txt = m.group()
                dc = m.groupdict()
                key = dc['KEY']
                mapper[key] = txt
                tester[key] = dc
                
        for k, v in tester.items():
            assert k in docdc and k in docdc_rdy, f'key {k=} not found in docdc or docdc_rdy {docdc.keys()=} and {docdc_rdy.keys()=}' 
            assert isinstance(docdc_rdy[k], str), f'KEY {k=} in docdc is not a string, but instead a {type(docdc_rdy[k])=} with {docdc_rdy[k]=}'
            
            if v['tp'] == None:
                pass # can be anything
            elif v['tp'] == 'ITER':
                assert isinstance(docdc[k], list),  f'KEY {k=} in inp_dc is not a list, but instead a {type(docdc[k])=} with {docdc[k]=}'
            elif v['tp'] == 'TABLE':
                assert isinstance(docdc[k], list) and isinstance(docdc[k][0], list),  f'KEY {k=} in inp_dc is not a list of lists, but instead a {type(docdc[k])=} with {docdc[k]=}'
                N = len(docdc[k][0])
                if not v['ncols'] is None:
                    Nexp = int(v['ncols'])
                    assert N == Nexp,  f'KEY {k=} column length did not match! expected { Nexp= } but got {N=}'
                wrong_rows = [i for i, row in enumerate(docdc[k]) if len(row) != N]
                assert not wrong_rows, f'KEY {k=} column length not consistent! Need { N= } but the following rows were off {wrong_rows=}'
                    
        ret = {replacetext:docdc_rdy[k] for k, replacetext in mapper.items()}
        
        missing_keys = [k for k in mapper.keys() if not k in docdc_rdy]
        additional_keys = [k for k in docdc_rdy.keys() if not k in mapper]
        inconsistent_keys = [k for k in docdc_rdy.keys() if not k in docdc]
        inconsistent_keys2 = [k for k in docdc.keys() if not k in docdc_rdy]
        
        assert not missing_keys, f'some keys are missing in the docdc compared to the keywords in the document template {missing_keys=}'
        assert not additional_keys, f'some input keys are too much and not given as keywords in the document template {additional_keys=}'
        assert not inconsistent_keys, f'some keys are inconsistent between inputs and actual replacements {inconsistent_keys=}'
        assert not inconsistent_keys, f'some keys are inconsistent between inputs and actual replacements {inconsistent_keys2=}'
        
        return ret


    def make_project(self, docdc, fun_post_proc=None, verb=True):

        main_dir = self.target_dir

        docdc_rdy = ElementFormatter(main_dir).format(docdc)

        if not fun_post_proc is None:
            docdc_rdy = fun_post_proc(docdc_rdy)

        replacement_dict = self.merge_exp_act_kws(docdc, docdc_rdy)

        files = list(Path(main_dir).rglob("*.[tT][eE][xX]"))
        
        if verb:print('')
        for f in files:
            if verb:print(f'replacing keywords in: "{f}"')
            with open(f, "r") as fp:
                txt = fp.read()
            for k, v in replacement_dict.items():
                vv = v.replace('\n', '')
                n = txt.count(k)
                if n:
                    if verb:print('   {} --> found n={} times -> "{}"'.format(k.ljust(35), n, vv if len(vv) < 30 else vv[:25] + f'... {len(vv) - 25} more chars'))
                txt = txt.replace(k, v)
            with open(f, "w") as fp:
                fp.write(txt)
        
        return self


    def make_docdc(self, tokens=None, minimal=False):
        if tokens is None:
            tokens = self.get_tokens()

        res = {}

        for k in tokens:
            tp, key = k[1:-1].split(':')
            if tp.upper() == 'REPLACEME':
                res[key] = ''
            elif tp.upper() == 'REPLACEME_ITER': 
                if minimal:
                    res[key] = []
                else:
                    res[key] = [
                        'sometext',
                        {
                            'type': 'image',
                            'value': '/path/to/some/image.jpg',
                            'width': 0.8,
                            'caption': 'Some caption text to add!'
                        },
                        'some other text',
                        {
                            'type': 'input',
                            'value': '/path/to/some/textfile.tex'
                        },
                        {
                            'type': 'input',
                            'value': 'some_none_existing_file.tex',
                            'content': '%whatever you want to write in the file!'
                        },
                    ]
            elif tp.upper().startswith('REPLACEME_TABLE'):
                tdc = token2dict(tp.upper())

                if tdc['ncols']:
                    N = int(tdc['ncols'])
                else:
                    N = None
                if minimal:
                    res[key] = [['']*N] if N else [[]]
                else:
                    N = N if N else 3
                    res[key] = [
                        [f'row-1-col-{i}' for i in range(N)],
                        [f'row-2-col-{i}' for i in range(N)],
                    ]
                    
        return res


    def read_docdc(self, fpth='', verb = False):
        if not fpth:
            fpth = os.path.join(self.target_dir, 'inp', 'docdc.json')
            
        if verb: print(f'reading: {fpth}')
        with open(fpth, 'r') as fp:
            return json.load(fp)


    def write_docdc(self, docdc, fpth='', verb = False):
        if not fpth:
            fpth = os.path.join(self.target_dir, 'inp', 'docdc.json')

        if verb: print(f'writing: {fpth}')
        os.makedirs(os.path.basename(fpth), exist_ok=True)
        with open(fpth, 'w+') as fp:
            json.dump(docdc, fp, indent=2)
    
        return self
    

    def rename_main(self, docname_new, verb = False):

        if not docname_new.endswith('.tex'):
            docname_new += '.tex'

        if docname_new:
            from_ = os.path.join(self.target_dir, 'main.tex')
            to_ = os.path.join(self.target_dir, docname_new)
            if verb: print(f'copying:\n   from: {from_}\n     to: {to_}')
            os.rename(from_, to_)

        return self
    
    def prepare(self, minimal=False, on_exist='raise', rename_main=True, verb=True):
        self.copy_tmpl2trgt(rename_main=rename_main, on_exist=on_exist, verb=verb)
        return self.write_docdc(self.make_docdc(minimal=minimal), verb=verb)


    def make(self, inp_file='', fun_post_proc=None, verb=True):
        return self.make_project(self.read_docdc(inp_file), fun_post_proc=fun_post_proc, verb=verb)


    def run(self, inp_file, fun_post_proc=None, on_exist='raise', rename_main=True, verb=True):
        self.copy_tmpl2trgt(rename_main=rename_main, verb=verb, on_exist=on_exist)
        dc = inp_file if isinstance(inp_file, dict) else self.read_docdc(inp_file)
        return self.make_project(dc, fun_post_proc=fun_post_proc, verb=verb)








###########################################################################################
"""

██████  ██████   ██████   ██████  ███████ 
██   ██ ██   ██ ██    ██ ██       ██      
██████  ██████  ██    ██ ██   ███ ███████ 
██      ██   ██ ██    ██ ██    ██      ██ 
██      ██   ██  ██████   ██████  ███████ 
                                          
"""
###########################################################################################

def crawl_(args):
    main_dir = args.main_dir
    minimal = args.minimal

    res = DocMaker(main_dir).get_tokens(minimal=minimal)
    if minimal:
        print(' '.join(res))
    else:
        print(json.dumps(res, indent=2))



def prepare_(args):
    template_dir = args.template_dir
    target_dir = args.doc_dir
    minimal = args.minimal
    on_exists = args.on_exists

    DocMaker(template_dir, target_dir).prepare(minimal=minimal, on_exist=on_exists, verb=True)


def make_(args):
    doc_dir = args.doc_dir
    inp_file = args.file
    DocMaker(target_dir=doc_dir).make(inp_file, verb=True)
    

def run_(args):
    template_dir = args.template_dir
    target_dir = args.doc_dir
    inp_file = args.file
    no_rename = args.no_rename
    docname = args.docname
    on_exists = args.on_exists

    if not docname and not no_rename:
        docname = os.path.basename(target_dir)
    elif no_rename:
        docname = ''

    DocMaker(template_dir, target_dir).run(inp_file, on_exist=on_exists, verb=True)





###########################################################################################
"""

███    ███  █████  ██ ███    ██ 
████  ████ ██   ██ ██ ████   ██ 
██ ████ ██ ███████ ██ ██ ██  ██ 
██  ██  ██ ██   ██ ██ ██  ██ ██ 
██      ██ ██   ██ ██ ██   ████ 
                                
"""
###########################################################################################


if __name__ == '__main__':

    allowed_commands = 'prepare make crawl'.split()
    minimal = False

    parser = argparse.ArgumentParser(__name__)
    subparsers = parser.add_subparsers(help="commands")
    

    crawl_parser = subparsers.add_parser('crawl')
    crawl_parser.set_defaults(func=crawl_) 
    crawl_parser.add_argument('main_dir', type=str, help='the directory to use as a template')
    crawl_parser.add_argument('-m',  '--minimal', help='only show minimal output instead of a full example output')


    prepare_parser = subparsers.add_parser('prepare')
    prepare_parser.set_defaults(func=prepare_)
    prepare_parser.add_argument('template_dir', type=str, help='the directory to use as a template')
    prepare_parser.add_argument('doc_dir', type=str, help='the directory to copy the template to')
    prepare_parser.add_argument('--outfile', type=str, help='the json file to write the keyword stuff to. Default will result in ./inp/input_mapper.json in the target_dir')
    prepare_parser.add_argument('-m',  '--minimal', help='only write minimal output instead of a full example output')
    prepare_parser.add_argument('--no_rename', help='no renaming of the "main.tex" file will take place')
    prepare_parser.add_argument('--docname', type=str, help='if this option is given it will rename main.tex file to this instead', default='')
    prepare_parser.add_argument('--on_exists', type=str, help='What to do if the target folder exists: Must be one of "ask", "raise", "clear", or "overwrite".', default='raise')

    make_parser = subparsers.add_parser('make')
    make_parser.set_defaults(func=make_)
    make_parser.add_argument('doc_dir', type=str, help='the directory holding the document to make')
    make_parser.add_argument('--file', type=str, help='the json file to use for making inputs for the keywords. Default will try to read in ./inp/input_mapper.json in the target_dir')

    run_parser = subparsers.add_parser('run')
    run_parser.set_defaults(func=run_)
    run_parser.add_argument('template_dir', type=str, help='the directory to use as a template')
    run_parser.add_argument('doc_dir', type=str, help='the directory to copy the template to')
    run_parser.add_argument('file', type=str, help='the json file to use for making inputs for the keywords.')
    run_parser.add_argument('--no_rename', help='no renaming of the "main.tex" file will take place')
    run_parser.add_argument('--docname', type=str, help='if this option is given it will rename main.tex file to this instead', default='')
    run_parser.add_argument('--on_exists', type=str, help='What to do if the target folder exists: Must be one of "ask", "raise", "clear", or "overwrite".', default='raise')

    
    args = parser.parse_args()
    args.func(args)
