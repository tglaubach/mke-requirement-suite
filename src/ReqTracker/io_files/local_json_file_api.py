import hashlib
import os, time, json



class LocalFile(object):
    @staticmethod
    def test_valid(p):
        return os.path.exists(p)
    
    def __init__(self, p) -> None:
        self.p = p
    
    def info(self):
        return f'Local File with Path="{self.p}'
    
    def get_meta(self, *args, **kwargs):
        stat = os.stat(self.p)
        return {
            'size': stat.st_size,
            'created': time.ctime(stat.st_ctime),
            'modified': time.ctime(stat.st_mtime),
            'accessed': time.ctime(stat.st_atime)
        }


    def get_data_version_id(self, meta= {}, algorithm='sha256'):
        """Calculates the checksum of a file using the specified algorithm.

        Args:
            filename: The path to the file.
            algorithm: The name of the hash algorithm to use (e.g., 'sha256', 'md5').

        Returns:
            The hexadecimal representation of the checksum.
        """

        with open(self.p, 'rb') as f:
            hash_obj = hashlib.new(algorithm)
            while True:
                chunk = f.read(1024)
                if not chunk:
                    break
                hash_obj.update(chunk)
            return hash_obj.hexdigest()

    def get_content(self):
        with open(self.p, 'r', encoding='utf-8') as fp:
            return json.load(fp)
  
    def write_content(self, content):
        with open(self.p, 'w', encoding='utf-8') as fp:
            json.dump(content, fp)
        
    def upload(self, content, comment='', description=''):
        return self.write_content(content)