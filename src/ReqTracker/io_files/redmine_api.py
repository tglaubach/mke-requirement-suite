import json, io, yaml, copy
import warnings
import re
import requests

from typing import Dict

import redminelib
from redminelib import Redmine, exceptions

import datetime
import os, inspect, sys

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

import ReqTracker
from ReqTracker.core import helpers
import ReqTracker.core.graph_base as base
from ReqTracker.core.graph_base import STATUS, log_method_calls
from ReqTracker.exporters.redmine_formatter import DocumentRedmineFormatter

log = helpers.log

server = ''
token = ''
redmine = None

def setup(url, api_key):
    global token, server, redmine
    token = api_key
    server = url
    redmine = Redmine(server, key=token)



def getRedmine(project, server):
    global redmine
    if redmine is None:
        redmine = Redmine(server, key=token)
    return redmine

def getProject(project, server):
    redmine = getRedmine(project, server)
    return redmine.project.get(project)

"""

██████  ███    ███ ███████ 
██   ██ ████  ████ ██      
██   ██ ██ ████ ██ ███████ 
██   ██ ██  ██  ██      ██ 
██████  ██      ██ ███████ 
                           
                           
"""


class RedmineDmsfFile():
    def __init__(self, id=None, session=None, redmine_url=None, api_key=None) -> None:
        self.id = id
        self.api_key = api_key if api_key else token
        self.redmine_url = redmine_url if redmine_url else server

        self.session = requests.Session() if session is None else session
        self.session.headers.update({'X-Redmine-API-Key': self.api_key})

    def info(self):
        return f'Redmine DMSF file at "{self.redmine_url}" with ID="{self.id}'

    def get(self, route, *args, **kwargs):
        return self._request('get', route, *args, **kwargs)
    
    def post(self, route, *args, **kwargs):
        return self._request('post', route, *args, **kwargs)
    
    def patch(self, route, *args, **kwargs):
        return self._request('patch', route, *args, **kwargs)
    
    def _request(self, method, route, *args, **kwargs):
        
        headers = kwargs.get('headers', {})
        if headers:
            headers.update({'X-Redmine-API-Key': self.api_key})
            kwargs['headers'] = headers

        if not route.startswith('/'):
            route = f'/{route}'
        url = f'{self.redmine_url}{route}'

        r = self.session.request(method, url, *args, **kwargs)
        if r.status_code > 499:
            log.error(r.content.decode())
        r.raise_for_status()
        
        return r
    
    def get_data_version_id(self, meta=None):
        meta = meta if meta else self.get_meta()
        return meta.get('digest')

    def get_meta(self, *args, **kwargs):
        return self.get(f'/dmsf/files/{self.id}.json').json()
    
    def get_content(self):
        r = self.get(f'/dmsf/files/{self.id}/download')
        ct = r.headers.get('Content-Type', '')
        if ct == 'application/json':
            return r.json()
        else:
            return r.content
        
    def get_project_id(self):
        meta = self.get_meta()
        return meta['dmsf_file']['project_id']
        
    def get_project(self, project_id=None):
        if not project_id:
            project_id = self.get_project_id()
        return self.get(f"/projects/{project_id}.json").json()

    def upload(self, content, comment='', description='', **kwargs):
        kwargs['comment'] = comment
        kwargs['description'] = description     
            
        if self.id:
            meta = self.get_meta().get('dmsf_file')
            proj_id = meta.get('project_id')
            file_name = meta.get('name')
            folder_id = meta.get('dmsf_folder_id', None)
            description = kwargs.get('description', meta.get('description', ''))
            comment = kwargs.get('comment', meta.get('comment', ''))
            proj_identifier = self.get_project(proj_id).get('project').get('identifier')
        else:
            proj_identifier = kwargs.pop('project')
            folder_id = kwargs.pop('folder_id') 
            if 'file_name' in kwargs:
                file_name = kwargs.pop('file_name')
            elif isinstance(content, str):
                file_name = os.path.basename(content)
            else:
                raise ValueError('"file_name" keyword argument is missing if this is a new file to upload')
            
            comment = kwargs.get('comment', '')
            description = kwargs.get('description', '')

        # compose url of the upload
        if isinstance(content, dict):
            ct = "application/octet-stream"
            stream = io.BytesIO(json.dumps(content, indent=2).encode("utf-8"))
        elif isinstance(content, str):
            ct = "application/octet-stream"
            stream = open(content, 'rb')
        elif isinstance(content, bytes):
            ct = "application/octet-stream"
            stream = io.BytesIO(content)

        params    = { "filename": file_name}
        headers   = { "Content-Type" : ct}
        route = f"/projects/{proj_identifier}/dmsf/upload.json"
        json_dict = self.post(route, params=params, data=stream, headers=headers ).json()


        # check if uploading was successfull and commit upload
        if json_dict["upload"]["token"] != "" :
            # commit upload
            token = json_dict["upload"]["token"]
            title       = os.path.basename(file_name).split('.')[0]
            data      = { 
                          "attachments"  : {
                                            "folder_id"     : int( folder_id ),
                                            "uploaded_file" : {
                                                                "token"        : token,
                                                                "title"        : title,
                                                                "name"         : file_name,
                                                                "comment"      : comment,
                                                                "description"  : description,
                                                                "version"      : "",
                                                            }
                                            }
                        }

            if folder_id:
                data['attachments']['folder_id'] = int( folder_id )

            route = f"/projects/{proj_identifier}/dmsf/commit.json"
            json_dict = self.post(route, json=data).json()

            fl = json_dict.get('dmsf_files', [])
            if not self.id and fl:
                self.id = fl[0]['id']

            if not json_dict["total_count"] > 0:
                warnings.warn('commit on upload failed! here is the response: ' + str(json_dict))
        else:
            warnings.warn('upload of file failed! here is the response: ' + str(json_dict))
            
            return {"total_count": 0, "dmsf_files": []}
        
        return json_dict



"""

██████   ██████   ██████     ██████      ██     ██ ██ ██   ██ ██ 
██   ██ ██    ██ ██               ██     ██     ██ ██ ██  ██  ██ 
██   ██ ██    ██ ██           █████      ██  █  ██ ██ █████   ██ 
██   ██ ██    ██ ██          ██          ██ ███ ██ ██ ██  ██  ██ 
██████   ██████   ██████     ███████      ███ ███  ██ ██   ██ ██ 
                                                                 
                                                                 

"""

def make_wiki_path(project_name, page_name):
    return f'{redmine.url}/projects/{project_name}/wiki/{page_name}'

def get_page_title(docname):
    docname = next(iter(docname)) if not isinstance(docname, str) else docname
    return 'wikidoc_' + docname.replace('.', '_').replace(' ', '_').replace('|', '_')
    
def doc2attachment(doc_dc):
    content = io.BytesIO(bytes(json.dumps(doc_dc, indent=2), 'ascii'))
    return {"path" : content, "filename" : 'doc_dict.json', "content_type" : "application/octet-stream"}

def bindoc2attachment(mybytes, filename):
    content = io.BytesIO(mybytes)
    return {"path" : content, "filename" : filename, "content_type" : "application/octet-stream"}

def del_wikipage(project_id, page_title, _redmine=None):
    try:
        _redmine = redmine if _redmine is None else _redmine
        page = _redmine.wiki_page.get(page_title, project_id=project_id, include=['attachments'])
        page.delete()
    except exceptions.ResourceNotFoundError as err:
        page = None
    return not page is None

def doc2wikipage(doc_dc, project_id, page_title=None, force_overwrite=False, _redmine=None, files_to_upload:Dict[str, bytes]=None):
    
    _redmine = redmine if _redmine is None else _redmine

    assert doc_dc, 'input can not be none or empty'
    docname = next(iter(doc_dc))
    doc = doc_dc[docname].get('value', [])
    formatter = DocumentRedmineFormatter()
    s = formatter.digest(doc)

    if not page_title:
        page_title = get_page_title(docname)

    text = f'h1. {docname}\n\n' + '\n'.join(s)
    files_to_upload = {} if not files_to_upload else files_to_upload
    attachments = [doc2attachment(doc_dc)] + formatter.attachments + [bindoc2attachment(bts, k) for k, bts in files_to_upload.items()]

    try:
        page = _redmine.wiki_page.get(page_title, project_id=project_id, include=['attachments'])
    except exceptions.ResourceNotFoundError as err:
        page = None
    
    #print(page)
    if not page:
        is_new = True
        page = _redmine.wiki_page.new()
    else:
        is_new = False
    
    if not is_new and not force_overwrite:
        a_dc = {a.filename:a for a in page.attachments}
        to_upload = []
        for at in attachments:
            # check file sizes for images... if same name and same size... skip the upload
            n_new = len(at.get('path').getvalue())
            filename = at.get('filename')
            ext = filename.split('.')[-1]
            if ext in 'jpg png gif webp json'.split():
                a_old = a_dc.get(filename, None)
                if not a_old is None:
                    
                    if n_new == a_old.filesize:
                        print(f'{filename} | {n_new=} | {a_old.filesize=} | Equal?: {n_new == a_old.filesize} --> SKIPPING!')
                        continue
                    else:
                        print(f'{filename} | {n_new=} | {a_old.filesize=} | Equal?: {n_new == a_old.filesize} --> UPLOADING!')
            print(filename, ' --> UPLOADING!')
            to_upload.append(at)

        filenames = [f.get('filename') for f in to_upload]
        to_delete = [a for a in page.attachments if a.filename in filenames]

        
    else:
        to_upload = attachments
        to_delete = []

    if force_overwrite:
        to_delete = [a for a in page.attachments]

    for a in to_delete:
        a.delete()
        
    if is_new:
        page.project_id = project_id
        page.title = page_title

    page.text = text
    page.uploads = to_upload
    page.comments = f'updated at {datetime.datetime.utcnow().isoformat()}'
    page.save()

    return page 


"""

███    ██  ██████  ██████  ███████     ██████      ██ ███████ ███████ ██    ██ ███████ 
████   ██ ██    ██ ██   ██ ██               ██     ██ ██      ██      ██    ██ ██      
██ ██  ██ ██    ██ ██   ██ █████        █████      ██ ███████ ███████ ██    ██ █████   
██  ██ ██ ██    ██ ██   ██ ██          ██          ██      ██      ██ ██    ██ ██      
██   ████  ██████  ██████  ███████     ███████     ██ ███████ ███████  ██████  ███████ 
                                                                                       
                                                                                       

"""
def mapstatus(s):
    
    if s in 'OPEN NONE UNHANDLED'.split():
        return 2 # New
    elif s in 'UNCLEAR_IF_POSSIBLE REJECTED DISPUTE DISCONTINUED IGNORE CORRUPTED INCONCLUSIVE FAIL ISSUE ERROR'.split():
        return 12 # Rejected
    elif s in 'IN_PREPARATION IN_CREATION TESTING_IN_PROGRESS IN_PROCESSING NEEDS_REVIEW'.split():
        return 4 # In Progress 
    elif 'PREPARED READY_FOR_TESTING AWAITING_EXTERNAL_INPUT IN_REVIEW'.split():
        return 8 # Feedback
    elif 'ACCEPTED PASS DONE DELETED'.split():
        return 10 # Closed
    else:
        raise KeyError(f'status="{s}" is unknown for mapping to redmine status')

def mapstatus_color(status_id):
    cols = {
        2: base.colors[STATUS.OPEN],
        12: base.colors[STATUS.REJECTED],
        4: base.colors[STATUS.IN_PROCESSING],
        8: base.colors[STATUS.AWAITING_EXTERNAL_INPUT],
        10: base.colors[STATUS.PASS],
    }
    return cols.get(status_id, base.colors[STATUS.ERROR])



def weight_status(s):
    return base.weight_status(s)

def node2redmine(redmine, node, project_id, issue_id=None, doc_as_wikipage=False):
    status_id = 3
    tracker_id = 14 # "Node" type "issue" in redmine
    dc = copy.deepcopy(node.to_dict(node.has_graph))

    description =  f'h1. {node.id}\n\n'
    
    description += '\n\n---\n\nh2. TEXT:\n' + dc.pop('_text')
    

    
    subject = str(node) # 
    dc.pop('id')

    _status = dc.pop('_status')
    custom_fields = [{'id': 7, 'name': 'Node-List', 'value': _status}]
    status_id = mapstatus(_status)

    if 'parents' in dc:
        dc['parents'] = list(dc['parents'])
    # for parent in parents:
    #     raise NotImplementedError('Directly adding parents is not possible YET!')
    
    notes = dc.pop('notes', [])
    if isinstance(notes, dict):
        notes_s = [f'**NOTE {t}:** {s}' for t, s in notes.items()]
    else:
        notes_s = [f'**NOTE {t}:** {s}' for t, s in notes]

    doc_dc = node.doc_get(redmine_in = redmine)
    
    if doc_dc: 
        docname = next(iter(doc_dc))
        doc = doc_dc[docname].get('value', [])
    else:
        docname = ''
        doc = []

    tags = dc.pop('tags', [])

    description += '\n\n---\n\nh5. TAGS:\n\n{}'.format(' '.join(tags))

    description += f'\n\n---\n\nh2. DATA:\n\n<pre><code class="yaml">\n{yaml.dump(dc)}\n</code></pre>'

    if doc and not doc_as_wikipage:
        formatter = DocumentRedmineFormatter()
        doc_parts = formatter.digest(doc)
        doc_txt = '\n'.join(doc_parts)
        attachments = [doc2attachment(doc_dc)] + formatter.attachments
    else:
        doc_txt = ''
        attachments = []
    
    if not doc_as_wikipage:
        description += '\n\n---\n\nh2. DOC:\n\n---\n\n' + doc_txt
        wiki_page = None
    elif doc:
        wiki_page = doc2wikipage(redmine, doc_dc, project_id)
        description += f'\n\n---\n\nh2. WIKIDOC: [[{wiki_page.title}]]\n\n'
        
    if issue_id and hasattr(issue_id, 'id') and isinstance(issue_id.id, int):
        issue_id = issue_id.id
        
    if issue_id is None and hasattr(node, 'issue_id'):
        issue_id = node.issue_id


    if issue_id is None and node.data.get('issue_id'):
        issue_id = node.data.get('issue_id')

    if issue_id is None:
        issue = redmine.issue.new()
    else:
        issue = redmine.issue.get(int(issue_id))

    issue.project_id = project_id
    issue.subject = subject
    issue.tracker_id = tracker_id
    issue.description = description
    issue.status_id = status_id

    #issue.parent_issue_id = 345
    issue.custom_fields = custom_fields

    if issue_id:

        a_dc = {a.filename:a for a in issue.attachments}

        to_upload = []
        for at in attachments:
            # check file sizes for images... if same name and same size... skip the upload
            n_new = len(at.get('path').getvalue())
            filename = at.get('filename')
            ext = filename.split('.')[-1]
            if ext in 'jpg png gif webp json'.split():
                a_old = a_dc.get(filename, None)
                if not a_old is None:
                    
                    if n_new == a_old.filesize:
                        print(f'{filename} | {n_new=} | {a_old.filesize=} | Equal?: {n_new == a_old.filesize} --> SKIPPING!')
                        continue
                    else:
                        print(f'{filename} | {n_new=} | {a_old.filesize=} | Equal?: {n_new == a_old.filesize} --> UPLOADING!')
            print(filename, ' --> UPLOADING!')
            to_upload.append(at)

        filenames = [f.get('filename') for f in to_upload]
        to_delete = [a for a in issue.attachments if a.filename in filenames]
        for a in to_delete:
            a.delete()
        
        notes_exist = {j.notes for j in issue.journals}
        notes_s = [n for n in notes_s if not n in notes_exist]
    else:
        to_upload = attachments

    issue.uploads = to_upload
    issue.save()

    for s in notes_s:
        redmine.issue.update(issue.id, notes=s)            
        
    return issue


"""

██████  ███████ ██████  ███    ███ ██ ███    ██ ███████     ███    ██  ██████  ██████  ███████ 
██   ██ ██      ██   ██ ████  ████ ██ ████   ██ ██          ████   ██ ██    ██ ██   ██ ██      
██████  █████   ██   ██ ██ ████ ██ ██ ██ ██  ██ █████       ██ ██  ██ ██    ██ ██   ██ █████   
██   ██ ██      ██   ██ ██  ██  ██ ██ ██  ██ ██ ██          ██  ██ ██ ██    ██ ██   ██ ██      
██   ██ ███████ ██████  ██      ██ ██ ██   ████ ███████     ██   ████  ██████  ██████  ███████ 
                                                                                               
                                                                                               

"""
    
def _helper(pattern, txt):
    s = next(re.finditer(pattern, txt), None)
    return s.group(1) if s and s.group(1) else ''
    

def get_doc_from_wikipage(page_title, project_id, redmine_in = None):
    try:
        if redmine_in is None:
            redmine_in = redmine
        page = redmine_in.wiki_page.get(page_title, project_id=project_id, include=['attachments'])
        attachment = next((a for a in page.attachments if a.filename == 'doc_dict.json'), None)
        dc_json = attachment.download().text
        return json.loads(dc_json) if dc_json else None
    except redminelib.exceptions.ResourceNotFoundError as err:
        return None


class RedmineNode(base.BaseNode):

    build_kwargs = 'data tags error _status notes id _text'.split()
    key = ''
    matcher_dish_id = re.compile(r'\.D\.(([#P][0-9]{1,3}|##|[0-9]{3}[A-Z]{1}))$')

    @staticmethod
    def test_is_per_dish(node_id):
        if not isinstance(node_id, str):
            node_id = node_id.id
        return len(RedmineNode.matcher_dish_id.findall(node_id)) > 0
    
    @staticmethod
    def _parse_relation(r):
        if r.relation_type == 'precedes':
            return (r.issue_id, r.issue_to_id)
        elif r.relation_type == 'follows':
            return (r.issue_to_id, r.issue_id)
        else:
            log.warning(f'unknown relation type: {r.relation_type=}')
            return None
    

    @staticmethod
    def _parse_journalentry(j):
        if j.notes.startswith('**NOTE '):
            a = _helper(r'^\*\*NOTE:?\s*([\s\S]*?)(?=:\*\*)\:\*\*', j.notes)
            return (a, j.notes[len(a):])
        else:
            return (j.created_on, f'{j.user.name}: {j.notes}')

    @staticmethod
    def _parse_description(desc):

        kwargs_yaml = _helper(r'<pre><code class=\"yaml\">([\s\S]*?)(?=</code></pre>)</code></pre>', desc)
        kwargs_yaml = kwargs_yaml.strip("'").strip("'").strip('"')
        kwargs = yaml.safe_load(kwargs_yaml) if kwargs_yaml else {}

        tags_s = _helper(r'h2. TAGS:?\s*([\s\S]*?)(?=\n---\n)\n---\n', desc)
        kwargs['tags'] = [t.strip() for t in tags_s.strip().split('|')]

        text = _helper(r'TEXT:?\s*([\s\S]*?)(?=\n---\n)\n---\n', desc)
        kwargs['_text'] = text if text else desc

        wikidoc = _helper(r'WIKIDOC:?\s*\[\[:?\s*([\s\S]*?)(?=\]\])\]\]', desc)

        return kwargs, wikidoc

    @staticmethod
    def from_redmine(issue, with_relations=False, issue_lut = None):
        
        if isinstance(issue, int) and not issue_lut:
            issue = redmine.issue.get(issue)
        elif isinstance(issue, int) and issue_lut and issue in issue_lut:
            issue = issue_lut.get(rid)
        elif isinstance(issue, redminelib.resources.Issue):
            pass
        else:
            raise ValueError(f'unknown input {type(issue)=} | {issue=} for from_redmine')

        rid = issue.id
        kwargs, doc_page = RedmineNode._parse_description(issue.description)

        cf = next((c for c in issue.custom_fields if c.name == 'Node-List'))
        kwargs['_status'] = cf.value if cf else base.STATUS.NONE.name
        kwargs['notes'] = dict([RedmineNode._parse_journalentry(j) for j in issue.journals])
        kwargs['id'] = issue.subject.split('|')[0].strip()

        if with_relations:
            kwargs['relations'] = [RedmineNode._parse_relation(r) for r in issue.relations]

        project_id = issue.project.id

        return RedmineNode(issue, doc_page=doc_page, project_id=project_id, dc=kwargs)


    def __init__(self, rid, doc_page=None, project_id=None, dc=None):


        self.locals = dc    
        self.doc_page = doc_page
        self.project_id = project_id
        self.issue = None

        if isinstance(rid, redminelib.resources.Issue):
            self.issue = rid
            rid = self.issue.id
            
        self.rid = rid

        dcs = {k:dc[k] for k in RedmineNode.build_kwargs if k in dc}

        super().__init__('', '', STATUS.OPEN, dcs)

    @property
    def dish_id(self):
        a = self.matcher_dish_id.findall(self.id)
        if len(a) and a[0] and len(a[0]):
            return a[0][0]
        else:
            return ''
    
    # def set_from_dict(self, dc):
    #     raise NotImplementedError()
    
    @property
    def has_doc(self) -> bool:
        return True if self.doc_page and self.project_id else False
    
    @property
    def status(self):
        return super().status

    @status.setter
    def status(self, value):
        raise NotImplementedError()

    @log_method_calls
    def set_deleted(self):
        raise NotImplementedError()
    
    def set_text(self, new_text):
        raise NotImplementedError()
    
    def set_status(self, new_status):
        raise NotImplementedError()
    
    def doc_get(self, redmine_in=None):
        assert self.has_doc, f'"{self}" has no document data attached!'
        assert self.project_id, f'"{self}" has no project_id!'
        return get_doc_from_wikipage(self.doc_page, self.project_id, redmine_in=redmine_in)
    

    def doc_set(self, doc:dict):
        raise NotImplementedError()
        # BaseNode will store docment directly as dict
        self._doc = doc
        return True
    
    
    # def edit(self):
    #     raise NotImplementedError()
    #     self.raise_on_not_has_graph()
    #     return self.graph.edit_nodes(self)

    # def rename_cp(self, new_id):
    #     raise NotImplementedError()
    
if __name__ == '__main__':
    import yaml

    pth = r"C:\Users\tglaubach\repos\mke-requirement-suite\src\config.yaml" # 'config.yaml'
    with open(pth) as fp:
        config = yaml.safe_load(fp)

    rm = config['sources'].get('redmine')
    with open(rm.get('login'), 'r') as fp:
        api_key = fp.read().strip()

    setup(rm.get('url'), api_key)

    api = RedmineDmsfFile(31512)

#     project_id = 'tobias-test-project'

#     g = ReqTracker.Graph(r"C:\Users\tglaubach\Nextcloud\davmount\skampieng_db\meertest_db\BU_20240506_req_db.sqlite")
    
#     doc_parts = ['some text...',
#  'some other text...',
#  [{'type': 'markdown',
#    'value': "## **316-000000-050**: DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed\n\n\n**STATUS**: NEEDS_REVIEW\n\n\n**TAGS**: \n\n\n---\n\n**TEXT**: \n\n**316-000000-050**\n\n---\n\nShared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_All/applc_doc_IRR/316-000000-050 RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n\n- LTC (UTC): **2022-02-02 10:48:03**  | VERSION: **RevD**\n    - FNAME: `316-000000-050 RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_All/applc_doc_IRR](https://cloud.mpifr-bonn.mpg.de/index.php/f/17183060)\n- LTC (UTC): **2022-02-02 10:48:03**  | VERSION: **RevD**\n    - FNAME: `316-000000-050 RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_Public_Review/Applicable_Documents](https://cloud.mpifr-bonn.mpg.de/index.php/f/17183639)\n- LTC (UTC): **2019-10-16 05:59:43**  | VERSION: ****\n    - FNAME: `316-000000-050  RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Documents/official/Design_evaluation_shared/DataPack_Design_Evaluation_Consortium/Additional_Docs](https://cloud.mpifr-bonn.mpg.de/index.php/f/15350168)\n- LTC (UTC): **2019-10-16 05:59:43**  | VERSION: ****\n    - FNAME: `316-000000-050  RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_Contract/DataPack_Contract_SOW/00_Bid/Documents_Referenced_available](https://cloud.mpifr-bonn.mpg.de/index.php/f/15838617)\n- LTC (UTC): **2019-10-16 05:59:43**  | VERSION: ****\n    - FNAME: `316-000000-050  RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf`\n    - DIR: [Shared/MeerKAT Extension/05_official_redmine_dms/MKp_DS_Evaluation_and_Design_Adoption/Design_evaluation_MTM/Additional_Docs](https://cloud.mpifr-bonn.mpg.de/index.php/f/15350169)\n\n---\n\n```{'pre': '', 'PBS': '316-000000', 'org': '', 'doc_type': '', 'doc_no': '050', 'sub': '', 'dish_no': '', 'version': 'RevD', 'title': 'DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed', 'ext': '.pdf'}```\n\n```i_ver_last: 3\ntickets: []\nversions:\n- checksum: ''\n  fileid: '15350168'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/15350168\n  ltc: '2019-10-16T05:59:43Z'\n  path: Documents/official/Design_evaluation_shared/DataPack_Design_Evaluation_Consortium/Additional_Docs/316-000000-050  RevD\n    DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: ''\n  fileid: '15838617'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/15838617\n  ltc: '2019-10-16T05:59:43Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_Contract/DataPack_Contract_SOW/00_Bid/Documents_Referenced_available/316-000000-050  RevD\n    DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: ''\n  fileid: '15350169'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/15350169\n  ltc: '2019-10-16T05:59:43Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_DS_Evaluation_and_Design_Adoption/Design_evaluation_MTM/Additional_Docs/316-000000-050  RevD\n    DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: SHA1:f53bdc25fbd39f5eac37100f7e87b977417d3b7c\n  fileid: '17183060'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/17183060\n  ltc: '2022-02-02T10:48:03Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_All/applc_doc_IRR/316-000000-050\n    RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n- checksum: SHA1:f53bdc25fbd39f5eac37100f7e87b977417d3b7c\n  fileid: '17183639'\n  link: https://cloud.mpifr-bonn.mpg.de/index.php/f/17183639\n  ltc: '2022-02-02T10:48:03Z'\n  path: Shared/MeerKAT Extension/05_official_redmine_dms/MKp_IRR_Public_Review/Applicable_Documents/316-000000-050\n    RevD DS REFLECTOR SYSTEM ALIGNMENT AND PERFORMANCE REPORT (SKA-P) - signed.pdf\n```\n\n\n---\n\n**NOTES**:\n\n**TICKETS**:\n\n---\n"}],
#  {'type': 'image',
#   'value': None,
#   'imageblob': 'iVBORw0KGgoAAAANSUhEUgAAAGQAAABnCAYAAAD2duf6AAAABGdBTUEAALGeYUxB9wAAACBjSFJNAACHEAAAjBIAAP1NAACBPgAAWesAARIPAAA85gAAGc66ySIyAAABGWlDQ1BJQ0MgUHJvZmlsZQAAKM9jYGBSSCwoyGESYGDIzSspCnJ3UoiIjFJgf8DAxsDEIMAgwaCWmFxc4BgQ4MMABDAaFXy7xsAIoi/rgsxiIA1wpaQWJwPpP0CcnVxQVMLAwJgBZCuXlxSA2D1AtkhSNpi9AMQuAjoQyN4CYqdD2CfAaiDsO2A1IUHOQPYHIJsvCcxmAtnFlw5hC4DYUHtBQNAxJT8pVQHkew1DS0sLTQYqg5LUihIQ7ZxfUFmUmZ5RouAIDKlUBc+8ZD0dBSMDQzMGBlC4Q1R/DgSHJ6PYGYQYAiDE5kgwMPgvZWBg+YMQM+llYFigw8DAPxUhpmbIwCCgz8Cwb05yaVEZ1BhGJmMGBvx8BgYAChlKTWZAi7MAAAAJcEhZcwAAuIoAALiKAUz1/7QAAAAHdElNRQfgBgIOHDA16czyAAAfa0lEQVR4Xu1dB3hUZbqeTC/plWSAtElvJIQ0MIjUS9fFRUUWdRXLruu67lrQS1MR3FVWt1iurNgBcRWwgdICCQRSIEQIIUB679P7fb+TkywBhJCZSSbc+z7PPOec7z/1//rfhscZIZiWlRXk6ec3LykpqaqiokLPkm86cNmtU+PJJ5+U1LW1bVerVJ/U1NRsZMk3JUaEhmi12pX4LTEajRyrxZIYERm5r66urpotvqng9BoyMS0tQ6NSPW2xWDgCPr8bW15XV9e6bdu2jRhzeyNwaoasWrVK2N7V9brFahWJxeLd8jFj7gDZYDIYblm3bt2SnrNuLji1lCm7un6j1eke4rq46Hz8/JYeOnQoVy6XjzGaTOMtJtO4jMzMD8+dO6djT78p4LQaMm3atCClSvW81WrliCWSv+fl5RUQPUguf8mFy201mc2hcPB/ZE6+ieC0DGmor38G/sKfx+OdC1co1rNkzp49e2pcXV03uGBfo1b/bsqUKVE9JTcHnJIhqOQ4g8HwEO3DeXz41VdftTEFLBQKxT+4fP4paI9rc1PTiyz5poBTMqStpeVPZrNZgh/HoNPdCXiwRQy2b9+u9fT0/G8XFxeOXq+/MystbQZbNOLhdAwh7UDOcRf5DpgrDnxF4tmzZ//EFvfh2LFjO4RC4Td0HiKxlygiY4tGNJyOIS1NTY9RmCsQCg94eXquogqHr/j9bbfdFsue0gcvb+/noSVak8k0YdeOHQ+y5BENp2LI5MmTR+v0+ntoXyaT/augqGgtGHMSTJE1Nja+zJx0CXJzc09KJZJ3OGCaUq1+Yd68eQFs0YiFUzGks739HlS+J5fHq4Lj3kk0+IoVFFHBVyxMTU2dR7RLERwauh5hcL3FbA68eP78syz5/2Erli9fLoiKiDipCAuzJsbHv8SSGcTGxHwRDnp0ZGTJvffeK2PJfRiXkPAYlSvCw7UTJ05MYskjEk6jISdOnLjFbLEkWjkcg5uHx2csmYGfv//zyNaVyNATSkpLH2fJfVhwxx3vCQSC49AucUdb2zqW/P+wBbHR0W9Awq1RUVE/sKR+SEhIWM1ogULRPn369FCW3If09PTppF10zvjx4xew5BEHp9AQMkNGo3EO7YvF4q8Y4mVITEx8nbJ2q8Xi1VBXt4Yl9yE/P/8HhMHbaF+pVL4IEyhlCkYYnIIhp0+fToO5CceuDk58dw+1Pz755JNuDze3F8jBa3W6JRMmTJjaU/IfBAQGrkQYrISDTyjIz/8NSx5RcAqGIIK6lYOsm8/nHz9w4MB5lnwFjhcVbUMY/D3CXK6yu/uVy5PB/fv3n5XKZG+yYfDTM2bMGMMWjRg4BUOQ2E2krYDP3w8Jh1//eSAZfA5hrs5kNE7Y+dVXy1lyH8aMGfMayi9aLBbfurq6F1jyiMGwM4SSObPROJ72hWJxPkO8BvLy8k5IpNJ/EtfUSAapmb6npAfffPNNh7ur62qmnUunuz87KyuTLRoRGHaGtDY2xsNcecKHdMvl8hMs+ZoIDg5+hcvl1iBMDmhoaPhvltyHZ1as+ATmLwf3FLS0t6/D1ikswYhAfHz87yjcjVQojt5IxSUnJz9A1+FnuJoWZGRkTEQIbGTC4HHj7mbJTo9hlxzY+hjaIrErg5mxMMQB4LnnnvugTws6Ol65fNDD0aNHc4Ui0ce0r1Kr11zehO+sGHaGIK/oTfLOsdsB4Ze//KWZHDx2TUa9fvL69evv7Sn5D8aOHbsGpq3dbDZHlJeVPcmSnRrDyhBIrRARlpz2eVzuDY+zOnLkSJ5EInmfHLxKpVqzcOFCn56SHuzevbtSIhb/mco1Gs2TkydPVvSUOC+GlSEwN64wWUyTuVgma2KIN4jgkJA1XB6vyWIyBZ+vqLiitTcqJubvYPYZi9Xq3trcvJolOy2GlSEGPl8MpnjgZxWJRK0s+YaAMLdOJpGsIy3Q6XS/nZqdndBT0oPPP/9c5ebhQRk8R28w3JOZmXlFhm8PpKWlzUlKSvpLdnZ2BEsaFIaNIVOmTAm/UFLyGnYp29ZRkwdTMAgsuP32t5FUFkLbxHVNTf/ImDRp8oIFCzzZYk5hYeF2lH8Df+XS3t7+0jvvvCNgi2zGjBkzFHFxce+3tbZ+rddqn+poa/slWzQoUNPQkCM5MfEBlUbzEnUqmcxmjlAoVKVnZCR+/PHHF9lTbhjp48dPb+3o2A2mIFHnUn98OUzVUalEstfi4nLQ29vbvbKycq+L1ern7un5UFFR0XvspYMCDQA/cODAo2qV6mkEDYzZhZZ/KRKLnywuLq5iThoEhpQhy5Yt8ywsKPgrTMsyWCnkgy6tcOpeqLxORUREApxwA3vqoBAXE/OpVqe7m0arsEwhR8WBOVOLhMKDJosl1qDXh6DiasanpiZ9+umnHeylN4RMmKfOrq61BqMxhY75AsFpqVT6IhixhTnBBgzZUFLqyasoL/+3Xqf7LzpGdLRRLJFs0Gm1S5BPcCIiI/956tSpbubkQSI+IeGESqlcCmZLZFLpATBkl9XFxRdMCUBWHwHmM2aM/FZzU1Pa2OBgTmRkpLqqqqqducF1MGfatAhowMZupXIDmB4IgVK7urr+Be++HNpynD3NJgwJQ9LT02+Fjd1hNBojUPnd7h4eD58sKXk1KjRUqtJqf0tO3dXN7e1z584NSmJ7AZPUMXr0aK7ZZJqKCuPHxMUtQ8b+Rm1t7Q9IPC8iC+VyXVw8USYGg0KhLQuV3d2/9vf3nxkwalRocHCwNT4+Xnn+/Hkte0sG1F8DjXuiqaVlE7Qii2jQsi+QBy2Ff/qsrKys3/m2wOEmi/otujo7v4B0eggFgrMBgYH3HTx48CiVISJJqKutLSbzNdrfPyEnP/8Mc5ENoMo7fuzYMTwvFjnI1tLTp+9iixggF4luaGjYZjQYEkgQUNGMzyHQe0CrKnB8VCqT/YjtIZCjoHUvQ5iS6RwyT25S6dqC4uKtdGxvODTKykxNzSJmQGI9hCJRYXBg4KxeZhDc3d27sNFD9Xlai6UvKrIFCAzUHp6eNF6Lo9PrF0M7GRPZCzy/zMvL6xnyL/iZvb28VsN0PoX9g7imC9qjMBgM9+K9N3d2dBS1t7XtAvOSUaaCeVoXExOT7ShmEBzGEIrH25XKbaQZAqGwGLZ6/u6DByvZYgb+XG4HpLCNKs9iNPqzZJsBBfkK2rgDERcHlfoKggkxW8QgPz//O5iwL7DL71apppaWlr5+trz81gSFIibAz285yqi5hfppKEfiwW/s8vP3z4SZff7yccb2hkMYQiFhc3Pz+yaDQY6KqZSPHr1o586d9WxxH2aJRBowhIms9Hp9IEO0E7x8fJ7HvdWo2KSSkpLHWHIffNzdKVnUIMO/JTk5mRnJUtfREdPe2bkY5smbhIQAzWmfcttt9x85cqSUITgYDnHqarV6PaKpxTDOWh8vr0U5hw9ftZ/j89OnraMCAmag0uJ48C8tLS172CKbUVNT0wJBcDUZjbfA5CDKTd0CZ90XxVXX1bUEyeUSlGXDRCUGBgZmwMFvgFaFgQmNcNrleK9AaEvhd9999w/2MofD7hqSmZmZrdVofk8SJpFKnz9y/HgOW3RVIEdgWnmRJEYyBDti7Nixf0Zy2Nude8VIlfC4uHchNB0wTXKtVnsnzJMF+crWUYGBWVweLx8axuHx+SfZ04cEdmUIjT6EE1xPdhfRyF6YijfYop8FpPEn2sJMxF9tVKItoO5cmUz2AgkHNHbZpIyMyWwRBxoz/+zJk/+GFnjRMZ2DYGDF6bKyuw4dOnQRJpQ5F0wZUC+mvWBXhsCZLoK0Ue+dwdvb+1l85HU7nBDxUNirwy+kurra7lpSWFz8GYTjR9yf29LRsXbq1KnJsbGxH3S0t++AuUpFXtQMs1SFcpqLkkrXUDsbNDaCsnwwtIS50RDBbgxZtWoVX6tWM51AUql0S++cwOtBoVDUQjw74EJdOjs701my3QChsEI4qCNLj0Qwu+rixcM6rfZX5LLFYvE2mKcMH1/fJXSe3mBYRG1iXV1dwTgW4OK6MWPGnGVuNESwm1PX6XRT4cypP8Lo7ePzazjVxp6SawMadQ8klUa9U4KmamltpXDUrsC71I8OCvI3mc3p8CeIwoUnYJ5+e/LkybXQyk5k8jWBAQHhKE/Sm0zh0BpPfE8aNOdwTk7Ov9jbDAnspiFqpXIxqbgI5oGG6rDka4J8hl6rpUUBuJQzINrJnjNnDmPT7Y2QgICX4eDruPAVYpFoX0FBQT/GB/r6roZAdEI4shBtPUo0nD+kDp1gF4bQAAI4wZm0D6n6nCEOAOVlZfdZrNYYVJIWlaGBlgQ11ddPYYvtil379jVJZLK1MEMctUbzaFZWFuMverH38OELUonkNYqsoLU8cvICkaiYLR4y2IUhlZWVqQhfR+On8fbz28eSr4klS5a4w2f8wQrNcPPw2AC/8zHZdbVWS6s1OAQLFy78F+xVHhgv6Wxvf4XMJFvEIDg09A2Eu6dIiwBVQECAXVpwbwR2YQicOSNtiGaK9+3bN6DBCrDfy8yUhPH5nYh63kRFfU10JHLzFixY4JAxuWvWrDG5ubmRg7cYjMZp48ePZ6bP9WLnzp1KsUDwPphChwKlUjnkY4Pt5UPiSKbI5lK00kP6eSxatMgbecEfaB9m4l3qKEI0c8CFx6vCxe6VFy70a6G1J44jUYUP+YDCXLVKtfYKn+XiMprK4NNECI2HfJEbmxmCcJeL6CSEuIAwqbyHem0g9HwIAUAIdltgrv5KNJJOiUj0Ee1rdboH7Z0kXopAuXw13rXVbDKFVdfW9k25JhNmNJlSiSEEJKuT1r/88lLmYIhgM0N++uknEZwg01ILUbpuqLtw6lQfxPmPw5lzpDLZW8iK+7pt/QIC3sdGCemMPHXihE2DBa6FH3/8sdpNKn2F9rUqFS3PEUf78+fP96cWA+waJVJpCbX4qjSa1aTRVD4UsJkhiOnFMFPuJFVcgeC6XbAVDQ3L4TvklCHDTL3Fkhns3bv3AnzJR3QvjV7/lCNnQYVHRr7F5fNPIKiQNTU1MVOuGxoaovEt3nDqHYqxY++DCa6BcASfO3duyBa5sZkhyB1c8FGIFrkcN4nkmk0lCI9H6Y3G39G+RCz++7fffnuFRvn5+ZEJU+Gecfn5+ff3UO0PWp7D3d19BQfvjQx+waRJkybqjEbGiSM4ubjz+++LESZvoGP4mt9RTyPtOxo2M0Qul9O4Gwi9haPUaq95v3Nnz/4GFT0KEljrL5VetUkbmfE5WgyA0RK1eoUjFwNAckgdVVvpWa3Nza9yrdYFtM/n8ZiEMCUlZRO0pAQ0WUtzc7+p2o6CzQxBLkENg92USIEpPzvCfOYttwSq1eqH6YNdXV3/tufo0Z8d6ZG5aNFKSGkZ7heEiMuhwz99fX1XQkC6afCCSqlcRDQwgWlp+OCDD3QIOpj5J7AEv8jIyJhF+46EzQzZvHmznvwB7cMJjmKIV0FdW9tv4cj9UNE1Kamp1xykVvrdd+kIoy3kVBFxLYc5mcYW2R2HDx8ulyBDJ4GCmhPJ7Orh0df0Ay3aCb+2gwQJYfDLbzz+uIgtcghsZgjlHVBxZqQeXvqq41pnz54drDcYHqW2Lvr4TZs2XVU7srKywhMSEjY1t7Xtg0TG8pCggSvc1paWv106NNTeCAkL24hnnaUMHd/TmJmZ2a/J3b9nKSha5Cbl/UOHfs2SHQKbGcKC6WRC5SWBKf2aIwi1tbVPgO4Fx39+rFR6RevpAw884DYuMfE52Ol8+I0HmEZKkegLXz+/uyhfgDmJvlBR8Tp7ut1BOZCbTPYCCQDgnXfoUL9ugJyjR0+JxOK3aZ/mNULAftYS2Aq7MATRSAGpNI1dum3OnLEsmQGikxCmkgHY49d35uX1G1Sdmpp6R15ubo5SpVoHn+EDbSvz9vK650xZ2SJEWVu9vL2fJsmF6bo/OTn5YfYyu6PwxIntME20/pako7Pz1TfeeKOfafLy8nqFy+PVWM3mwJqamhUs2e6wC0NiY2MLodL1+Ek7amv7ukkJzc3N1IDowePzK+AUP2TJxKh44POOjo4vwMhx0J4OmVS6Mjo2NvNYYWHfWiew4e/DzG0ihsPpbkxLS3NIazDBx8dnBZkmWvV00+bN/aZcw9e0SMViZjlBCNhymNdxTIGzApX7IU3CjImK+jdL4syfOTMqIiJCFR4aaoVvuI9od8+d64v9dTi3myZk0i8+Nvazqy1Q1gtqRomJjs6l+0RFRtbOmDHDYTlBQmzsn2nNlMiIiMY5c+Yws7t6QdMYoqKi8umdY6Ojv2HJdoW9fAg5622M2TKZZiEqYvrGL9TWPgW/InPh8XJLSko+QFy/pKi8PA8S9hz8hBvN6YBJml96+vTd+/btO83c6Cqg0YhBcvlSRGhV0CZ5TXX11vnz5/ebn24vIINfD9NUCfMZUFVVtYolM3j44YeNTDKJ+AV+bXZ6evovekrsB7sxBGbrRxpBQjZYpVL9au7cuWNpvXYUmWF/c+NiYz/t6uz8GJFKBMxTg9TV9Y//NWfOLTBJu3rucG1Qs4qfv/9sXFup1+tpHcbts2bN8usptR9oZKJUJmNyH51We8XCA8ePH98rFAo/I+FDGLzG3o2gV0REtiA5MfGRbpXqLURG9UKBoFyj1d5K+QQq0QiJE2HbInN1/dfUqVNf3bhx4xWhLz7S5c033xRWVFSIEJmJlUqlGOGvCAyQgJFi3IPutRiM/SN2OXDCRyLl8tupN5C9hV2A9+DC9O6Ftt8qEgpzVq9dexvN+mWLmWGy9XV1hTjPzdXN7emTJ0/+mS2yGXZlyJ133ukK03TcoNdTIx1l7gydpAnJo9XDw2MnHrgXlexntlp9Ue6Kk6Q4SwK6DOfJkDzKcK0QxyIEA2IcU7RDPyGSBJpGzdyPfvQMhMfF8tGj74IGDajpf6CYBHvU2NJyCA8ReHp5/Rqa3C9cj4uLWwMNWgmr0KaIiBj/7bffDnrW1KWwK0MICGNJgrfA1rMUJnns20LCmYcy/SdUwahYYhxtCWwuwKCXhq0eWqdF+KvFtVrsU+gsA2OZac6Q4koPL6/7ESYfoGN7AWb2LZ1O9wiEqSYiMjLl66+/7puYOm3aNI/qqqoC+EiFWCzeBD9ol1VR7c4QAiKiz2FmFlFFQ4IvoOZrQKba1/O4XA00ohtcUIMhGi6f3wapz4S/mYtjPaTxSdjoi2CCDjZOrTOb1WCuQSqVGlAxBj8/P0Oct7fup/Z2L5iKwyaDIQT3Jw3UwHw8XlxcbLdhO7SwTVVlZSE1iIql0o2lpaVML2cvUpKSlnQplbRahClg1KjsvLy8Iz0lTgbkGKMRntaEhYRQSLt/w4YNbmzRVYHY+BiFmjAD77CkAWFcSsqD4Qi1Q4KDmR+F3UkJCf+05zIatMAmvRvurcV39cs9IDTc6MjI/RQGQwj30zFbNGjYLcq6FAcPHqyFpD+A8NFAjv3DzZupOf2q2jguMfGPJrN5AsxQZ1BQUN+i+wPBgnnzPhTweJSUkvmDQlpoXZNHS0+d2oPKm8SeZhMioqPfgxk9jnuLW5qa+i2wSc908/CgMNgELb4VYX2/QRNOh9SUlOUktWFI6JBIbbk8RKQ2oQiFopFdGnZQi+pPmDBhLptg6sanpHyEbS0dR4SH65KSktbdc889Ng+8o2l5uKeFviU1OXkxS+4Dvu095pkKxTlbG0EdoiG9KCgqetfbx4eZPqY3GBYXFRV9demye9XV1U9B8gKojWhsSMh1R8pfDcgLvoafoqxZBO1wi42Lmy0Si3+E1olUSuVzeObB9NRUWkt+0P6Scg+E2B/jHhyE9S9ebhKDfHzWQlvaUK6orKy0aZEbhw9xqauryw0ODm40GgzTodaRSpVqAex+GfyFS3NT03v4EKG7m9sLB3NyBh0hxUdHn+1Sqe6H8401m80/wNmvCBk7Ng4RUhxsSQANon7n7bezQsPCmvA+P7um47WA9y1GXrQMAiRXdnfrm5qb+973Qk1NF0JvKwKM6Xhecnxk5BdVdXUDmmp9OYZkzFFjY2NhWHh4IRKtbLPJRK2/i7u7u+dThxYiqsOnSkuZsbSDRWV1dQP8jxQVMgmhcKq/n1+GRqOZBWaLEWYrSbLx3EhEcksDAgLSQ0NDNc8888zF77//vi/Zux6qqqo6x8jl1KI9Ddo3PiEh4UvQ+sJgaP7J+vr6BRCKMRqj0betrW1Qg8aHhCEEvGwFbPFOrUYThI+KQwX5ULgKhpSEKxSlyMyZXsfBYOnSpT6ogAi9TjcD9/U2WywJuLdYLJF8iTB5mUAg+AjhdgCkNwpCEAHNWVxYUDBbLpe7xcfHt128eHFAEzknLFxY3F5VtQBaMgZhfVBLayuzTjABCbExOCSkge4NAUiIio4+jG+64aVCHJKHXA8IEY8hy6XIijmGJOuFAsFBkUTyDSQ6H8ll2bvvvktTpq+KVatWSffs2ROmVakSjRbLdKNePx0VLac0kiItHp/PCfD1vT8vP39zzxU9mDhx4q3t7e3LYT5vR8zKzMzFs7t5AsEBJHe73N3dc3Nycq45Vx6R1Fwkvruo4rw9PeceKyrq1+qLMHgHLMF8vkCQP+quuyYdXLPGxBYNCEPOEHzQzM6ODvrnA56rTPYaDS6A5E4ibSFAumgyfw2OL0Kya1Bh7TBDGuxTBXrj3EBUfCgyZDnKJOxFNGqyWQjnbtDpsi1Wa7hEKn0VidwzTPllyMzMjIc/WAzG/MJkMsXgPmwJR4VnU+9gHhLNApr4Cd9ReXmXc2xMzP9AQx7EO5V8/MknkJ/UvmaJ6dOnj7t44UIuPkPq5ur62ImSkn5jz66HIWUIJJu/dcuWXJisNHzsl6fPnLmD+hjwwZPUavU8mBvyAcmoFD6d3/tyPQ0ojDQTw5h92qLSzvGRI0C7vnf18PiRRkGOHzfuV13d3R/gZENgUFAmaEXMBVcBjcC/cOFCNvzZPPifbPgGpg2uF3iGiRgNPa6B1tXgWY0cLrcBWhgE0/sYTnCRSCSfwuyeMZrNQRaTKQgMHoXvS8FbIjATdiYmJUVu3769hb3ldTGkDElJSroPYeP7eKg+UC7PhHnoN/+Cuk23bt0ago+K1qhUCmwDXHg8TzDPHZKvsrq4dCGEbhYLBJUiqbTM19e3aseOHZ3s5QyIwa+/9tohMDddIBLtOnPmzHy26JqgULampiZepVJNgvlLMxuNMWBQJISjz88ylQWGkVkkgaAfdS8zo+XpmDmrT4CUYpFoL/zjr6jPvod0fQwZQ+bPn+9WduZMIUxNBKTqnVM//fQIW2R3pKWlzWhvbd1Nlefj6zubVm5giwaMWbNmuava2kYZudxwnUYTBs0lE+kPJniDSRIwhUejHqE9ZpPFouFzuZSHNEEIqqVS6Xlox4UDBw5U45prjuYcNiBM/BObzXZe7e8m7A1kz1/S86Kjoo7RdG2W7PRwaKbei5kzZwYiqnqK9iVi8d9++OGHQa8cN1AEBAauhjnRw+xNKCgoYPrzRwKGhCH1dXV/gIoHwP7XhikUzHwQR2P//v0nhWLxe2TnEVGtcNRkUnvD4QyZMmVKFDLkRyh6QRj4qqNX07kUcPrr8dxW+K2Q2urqJ1iyU8PhDGlqaqI/GHaFdpRmh4fbtPDkjYK6AaTIdShy0Wi1T0ydOjWsp8R54VCGpAN6ne5u0g6Zq+uLG7dvt9tSeANFSEjIPxA6l8NkejY3Nj7Pkv9vIjo6+luKdKKionKGevLkpaDxYPQeirAwQ0ZGRhpL/r8FKMds6tDBz4K84DaWPCwgYUD4e5iYEhMdfcM5yYgHxf2ogJ4hl7GxfUNLhxN9vX54p9TU1AFl78MBh/gQZMZLjCZTGjJlvZ+f31qWPKygXj+RULidmjVUSuXqxx088WawsDtDaK6HVqNZQW07YrF4MyKdIV0A7Frw8vFZgwCDJt4k5+bmOnTizWBhd0eLaOYJg15/F5fL7QwYNWop9bSxRcMOZh3GUaO8ob2ZJqMxMTMr66Py8nINW+wUsKuGUBOJRq1mmkikEsnfoB39loV1BgT5+v4FwtJkNpvHVFdW/p4lOw3sypD6+nqmiQQfXBsaHj6oUSSOxu5DhxokUulfKDfSarWPT3ayf92xG0OmZGVFwXc8zCSBUumGoWwiuVEkJia+xePxTlusVve2lpaVLNkpYDeGNHV2UhOJG5fHKw2PiNjEkp0SNAHI3dWVHDwtR353ZmYms8D+TYOJEydmIMY3UoyPrNhhi8bYExAel95xubHR0T/QMVs0rLCLhrS3ta3C1/D5AkHOs88+a/dFLB0BaIfVzcODpqyZaTGzjAkTbu8pGeG4pInECtUf1iaSwSAmOvoT0pLIyMiSRYsW9YxiGUbYpCE0oKCrs3MVdQIJBIJPjxw5MqD1Fp0J/gEB9NcW7RazOaGiouKKf58eathkN1OSkh7qUirfxa4+XKFY5OfndwJZMDOER6fT0WbwYK+38S7XRXBwsCr/6NFXlErlg4i86kePGZO1f/9+u0xPGwwGzRBqQV21cuVRVDyzJB40hEYa9vvDeVtA97QHBnAfRL/MtDoZn8/neLi7P1VYXOywZTyuB1s0xGVcYuIjao3mr8h6yfTxadgNtWH1lPa/tU2q6HhYYbbMEKoqXz+/6bQYP0sfcthcT1mpqVEwUzwXkchmke7X/CqyX2PsQNQWDOH5BAa2bNmyxa5TrG8MHM7/AkXGl1KFCn/hAAAAAElFTkSuQmCC',
#   'width': 0.8,
#   'caption': ''}]
#     node = g['WP.CAM_INST.D.##']
    
#     node.doc_append_parts(doc_parts)
#     getRedmine('tobias-test-project', 'https://project.mpifr-bonn.mpg.de')
#     issue = node2redmine(redmine, node, project_id, issue_id=8503, doc_as_wikipage=True)

#     node = RedmineNode.from_redmine(issue)
#     dc_doc = node.doc_get()
#     print(dc_doc)




