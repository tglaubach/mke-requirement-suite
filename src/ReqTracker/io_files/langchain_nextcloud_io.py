import datetime
import io
import re
from typing import List
import dateutil
import pytz
import requests
import threading
import queue
import time

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    

from langchain_core.documents import Document

from langchain_community.document_loaders.base import BaseLoader
from langchain_community.document_loaders.blob_loaders import Blob
from langchain_community.document_loaders.parsers.pdf import PyMuPDFParser, PDFPlumberParser, PyPDFParser, PDFMinerParser
import docx2txt, pypdf



from ReqTracker.core import helpers

log = helpers.log



login_data = None
server_url = None

def setup(_server_url, _login_data):
    global server_url, login_data
    login_data = _login_data
    server_url = _server_url



# import concurrent.futures

def download_nc_by_queue(q:queue.Queue, q_out:queue.Queue, nc_auth, baseurl):
    assert nc_auth and len(nc_auth) == 2, 'no login data given!'

    with requests.session() as s:
        username, password = nc_auth
        s.verify = True
        s.auth = (username, password)

        while pi := q.get():
            url = f'{baseurl}/remote.php/dav/files/{username}/{pi}'
            p = s.request('GET', url)

            if p.status_code == 404:
                q_out.put((pi, None))
            else:
                p.raise_for_status()
                q_out.put((pi, p.content))
            q.task_done()
            


class BytesLoader(BaseLoader):
    def __init__(self, data, path, **kwargs) -> None:
        self.text_kwargs = kwargs
        self.data = data
        self.path = path

        super().__init__()

    def _tryparse_pdf(self, cls, text_kwargs):
        try:
            parser = cls(text_kwargs=text_kwargs)
            return parser.parse(Blob.from_data(self.data, path=self.path))
        except Exception as err:
            return str(err)

    def load(self, **kwargs):
        try:
            """Load file."""
            text_kwargs = {**self.text_kwargs, **kwargs}
            if self.path.endswith('.pdf'):
                
                for cls in (PyMuPDFParser, PDFPlumberParser, PyPDFParser, PDFMinerParser):
                    res = self._tryparse_pdf(cls, text_kwargs)
                    if not isinstance(res, str) or not res is None:
                        return res
                raise Exception(res)
                
        
            elif self.path.endswith('.docx'):
                return [Document(page_content=docx2txt.process(io.BytesIO(self.data)),metadata={"source": self.path})]
            else:
                return None
        except Exception as err:
            log.error(f'ERROR: {err} ({self.path})')
            return None	
        
    def loadn(self, node, **kwargs):

        node.get_latest_ver()['path']
        pages = self.load(**kwargs)
        
        ver = node.get_latest_ver()

        
        paged_dc = []
        if not pages is None and not isinstance(pages, str):
            for i, page in enumerate(pages, 1 ):
                try:
                    dc = dict(
                        node_id=node.id, 
                        title=node.title, 
                        ltc = ver['ltc'], 
                        link = ver['link'],
                        version = node.version,
                        content=page.page_content, 
                        source=page.metadata['source'] if 'source' in page.metadata else ver['path'], 
                        page=page.metadata['page'] +1 if 'page' in page.metadata else i, 
                        n_pages=page.metadata['total_pages'] if 'total_pages' in page.metadata else len(pages)
                    )
                    dc['pid'] = str(dc['node_id']) + '.PAGE.' + str(dc['page'])

                    paged_dc.append(dc)
                except Exception as err:
                    log.error(f'ERROR: {err}')
                    return None	
            
        return paged_dc
    

def fun_get_path(n):
    if hasattr(n, 'get_latest_ver'):
        return n.get_latest_ver()['path']
    elif isinstance(n, dict) and n.get('path'):
        return n.get('path')
    else:
        return str(n)


class NextcloudFileLoader():
    def __init__(self, nc_auth = None, baseurl=None, verb=False, use_threading=False) -> None:
        self.nc_auth = nc_auth if nc_auth else login_data
        self.baseurl = baseurl if baseurl else server_url
        self.verb = verb
        self.use_threading=use_threading

    def get_nc_docs_dc(self, nodes):
        
        verb, use_threading, nc_auth, baseurl = self.verb, self.use_threading, self.nc_auth, self.baseurl
        pathes = [fun_get_path(n) for n in nodes]

        if use_threading:
            n_threads = min(max(1, len(pathes) // 10), 20)
            contents = self.download_files_thread(pathes, n_workers=n_threads)
        else:
            contents = self.download_file_by_path(pathes)
        docs = [BytesLoader(d, p).loadn(node=n) for d, p, n in zip(contents, pathes, nodes) if p]
        return docs

    def get_nc_docs(self, nodes):

        verb, use_threading, nc_auth, baseurl = self.verb, self.use_threading, self.nc_auth, self.baseurl
        
        pathes = [fun_get_path(n) for n in nodes]
        if use_threading:
            n_threads = min(max(1, len(pathes) // 10), 20)
            contents = self.download_files_thread(pathes, n_workers=n_threads)
        else:
            contents = self.download_file_by_path(pathes)

        docs = [BytesLoader(d, p).load() for d, p in zip(contents, pathes)]
        
        return docs

    def download_files_thread(self, pathes:List[str], n_workers=5):
        verb, use_threading, nc_auth, baseurl = self.verb, self.use_threading, self.nc_auth, self.baseurl
        qin, qout = queue.Queue(), queue.Queue()
        ts = time.time()

        threads = []
        if verb:
            log.info(f'starting N={n_workers} threads for downloading from nextcloud...')
        for i in range(n_workers):
            t = threading.Thread(target = download_nc_by_queue, args=(qin, qout, nc_auth, baseurl))
            t.start()
            threads.append(t)
        log.info(f'enqueuing K={len(pathes)} file pathes to N={n_workers} for downloading from nextcloud...')
        for p in pathes:
            qin.put(p)
        
        for i in range(n_workers):
            # add a signal to stop twice just to be sure
            qin.put(None)
            qin.put(None)

        log.info(f'waiting for files to be downloaded...')
        for t in threads:
            t.join()

        tmp = {}
        for i in range(len(pathes)):
            p, content = qout.get()
            tmp[p] = content
        
        elapsed = time.time() - ts
        log.info(f'Finished downloading K={len(pathes)} files using N={n_workers} in dt={elapsed:.2f} seconds...')
        return [tmp[p] for p in pathes]

    def download_file_by_path(self, path):
        verb, use_threading, nc_auth, baseurl = self.verb, self.use_threading, self.nc_auth, self.baseurl
        assert nc_auth and len(nc_auth) == 2, 'no login data given!'

        with requests.session() as s:
            username, password = nc_auth
            s.verify = True
            s.auth = (username, password)
            if not isinstance(path, str) and hasattr(path, '__len__'):
                pathes = path
                is_single = False
            else:
                pathes = [path]
                is_single = True

            ret = []
            for i, pi in enumerate(pathes, 1):
                url = f'{baseurl}/remote.php/dav/files/{username}/{pi}'
                if verb:
                    log.info(f'GETTING {i}/{len(pathes)}: {url}')

                p = s.request('GET', url)
                if p.status_code == 404:
                    log.warning(f'The file "{pi}" could not be found any more, maybe it was deleted... skipping for now!')
                    ret.append(None)
                else:
                    p.raise_for_status()
                    ret.append(p.content)

        return ret[0] if is_single else ret

if __name__ == '__main__':
    pass

    # import time, getpass
    # from ReqTracker.core import graph_main, schema
    # from ReqTracker.ui import ui_state

    # g = graph_main.Graph(r"C:\Users\tglaubach\repos\mke-requirement-suite\scripts\20240217_tmp.sqlite")

    # # g.pprint()


    # for doc in get_nc_docs_dc(list(g.nodes)[:10], ui_state.NEXTCLOUD_LOGIN_DATA, verb=True):
    #     print(doc)
