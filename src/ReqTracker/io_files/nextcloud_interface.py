
import datetime
import requests

from urllib import parse
import urllib
import xmltodict
import xml.etree.ElementTree as ET

from threading import Thread
import nextcloud_client


import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

from ReqTracker.core import helpers

log = helpers.log
parser = helpers.dateutil.parser

login_data = None
server_url = None

def setup(_server_url, _login_data):
    global server_url, login_data
    login_data = _login_data
    server_url = _server_url


req_data_idx ="""<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
<d:prop>
    <d:getlastmodified />
    <d:getetag />
    <oc:fileid />
</d:prop>
</d:propfind>"""

req_data = """<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
<d:prop>
    <d:getlastmodified />
    <d:getetag />
    <d:getcontenttype />
    <d:resourcetype />
    <oc:fileid />
    <oc:permissions />
    <oc:size />
    <d:getcontentlength />
    <nc:has-preview />
    <oc:favorite />
    <oc:checksums />
    <oc:comments-unread />
    <oc:owner-display-name />
    <oc:share-types />
    <nc:contained-folder-count />
    <nc:contained-file-count />
</d:prop>
</d:propfind>"""

search_data = """<?xml version="1.0" encoding="UTF-8"?>
<d:searchrequest xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns">
    <d:basicsearch>
        <d:select>
            <d:prop>
                <d:displayname/>
                <oc:fileid />
                <oc:checksums />
                <d:getetag />
                <d:getlastmodified />
            </d:prop>
        </d:select>
        <d:from>
            <d:scope>
                <d:href>/files/{}</d:href>
                <d:depth>infinity</d:depth>
            </d:scope>
        </d:from>
        <d:where>
            <d:and>
                <d:not>
                    <d:is-collection/>
                </d:not>
                <d:gt>
                    <d:prop>
                        <d:getlastmodified/>
                    </d:prop>
                    <d:literal>{}</d:literal>
                </d:gt>
                <d:lte>
                    <d:prop>
                        <d:getlastmodified/>
                    </d:prop>
                    <d:literal>{}</d:literal>
                </d:lte>
            </d:and>
        </d:where>
        <d:orderby/>
    </d:basicsearch>
</d:searchrequest>"""

def _filedict_from_response(response, username, baseurl):

    a = response['d:href'].split('/remote.php/dav/files/' + username + '/')
    if isinstance(a, list):
        pth = a[-1]
    else:
        log.warning(f"could not split the path: {a}")
        pth = response['d:href']


    path = urllib.parse.unquote_plus(pth)
    propstat = response['d:propstat']
    
    if not isinstance(propstat, dict):
        propstat = next((d['d:prop'] for d in propstat if '200 OK' in d['d:status']))
    else:
        assert '200 OK' in propstat['d:status'], propstat['d:status']
        propstat = propstat['d:prop']

    fileid = propstat['oc:fileid'] if 'oc:fileid' in propstat else ''
    checksum = ''            
    if 'oc:checksums' in propstat and propstat['oc:checksums']:
        if 'oc:checksum' in propstat['oc:checksums']:
            checksum = propstat['oc:checksums']['oc:checksum']
    elif 'oc:checksum' in propstat and propstat['oc:checksum']:
        checksum = propstat['oc:checksum']

    
    lastmodified = ''
    if 'd:getlastmodified' in propstat:
        lastmodified = propstat['d:getlastmodified']
        lastmodified = helpers.make_zulustr(helpers.parse_timestr(lastmodified))

    link = '{}/index.php/f/{}'.format(baseurl, fileid) if fileid else ''
    ret = dict(path=path, link=link, fileid=fileid, checksum=checksum, ltc=lastmodified)
    
    return ret



def _get_dir_pathes(s, baseurl, url, verb, username, recursive=True, as_dict=True, ignore_errors=True):
    try:
        ret = []
        if verb:
            log.info('Nextcloud Loading {}'.format(url))

        r = s.request('PROPFIND', baseurl+url, data=req_data)

        r.raise_for_status()
        # dc = p.json()
        txt = r.text
        dc = xmltodict.parse(txt)
        response = dc['d:multistatus']['d:response']

        # print(response)
        if isinstance(response, dict):
            response = [response]

        for r in response:
            # print(r)
            if not r or 'd:href' not in r:
                log.debug(r)
                continue

            p = r['d:href']
            if p.endswith('.ipynb_checkpoints/'):
                continue

            ret.append(_filedict_from_response(r, username, baseurl))
            if recursive:
                if p.endswith('/') and p != url and p not in url and not p.endswith('.ipynb_checkpoints/'):
                    # must be a dir
                    ret += _get_dir_pathes(s, baseurl, p, verb, username, as_dict=as_dict, ignore_errors=True)
        return ret
    except Exception as err:
        log.exception(err)
        if ignore_errors:
            return []
        else:
            raise
        
class NextcloudApiAccessor(object):
    def __init__(self, nc_auth=None, baseurl=None, verb = True) -> None:
        self.auth = nc_auth if nc_auth else login_data
        self.baseurl = baseurl if baseurl else server_url
        self.verb = verb


    def crawl_directory(self, dir, as_dict=True, ignore_errors=True):
        baseurl, nc_auth = self.baseurl, self.auth
        # Use 'with' to ensure the session context is closed after use.
        with requests.session() as s:
            username, password = nc_auth
            s.verify = True
            s.auth = (username, password)
            p1 = f'/remote.php/dav/files/{username}'
            url = p1 + parse.quote(dir.encode('utf-8'))
            tree = _get_dir_pathes(s, baseurl, url, self.verb, username, as_dict=as_dict, ignore_errors=ignore_errors)
            # tree = [parse.unquote(f)[len(p1):] for f in tree]
            # tree = list(dict.fromkeys(tree)) # remove duplicates while maintaining order
            return tree

                
    def get_directory(self, dir):
        baseurl, nc_auth = self.baseurl, self.auth
        # Use 'with' to ensure the session context is closed after use.
        with requests.session() as s:
            username, password = nc_auth if nc_auth else login_data
            s.verify = True
            s.auth = (username, password)
            p1 = f'/remote.php/dav/files/{username}'
            url = p1 + parse.quote(dir.encode('utf-8'))
            tree = _get_dir_pathes(s, baseurl, url, self.verb, recursive=False)
            tree = [parse.unquote(f)[len(p1):] for f in tree]
            tree = list(dict.fromkeys(tree)) # remove duplicates while maintaining order
            return tree


    def test_ping(self):
        baseurl, nc_auth = self.baseurl, self.auth
        if nc_auth is None:
            nc_auth = login_data

        assert nc_auth and len(nc_auth) == 2, 'no login data given!'

        with requests.session() as s:
            username, password = nc_auth
            s.verify = True
            s.auth = (username, password)
            url = f'{baseurl}/remote.php/dav/files/{username}'
            uri = url
            p = s.request('PROPFIND', uri, headers={'Depth': '0'}, data=req_data)
            p.raise_for_status()

        return ''



    def index_all(self, folder_path_remote, depth="infinity", newer_than=''):
        baseurl, nc_auth = self.baseurl, self.auth
        
        if not folder_path_remote.startswith('/'):
            folder_path_remote = '/' + folder_path_remote
        
        if not folder_path_remote.endswith('/'):
            folder_path_remote += '/'
            
        def to_dict(tree, dc=None):
            if dc is None:
                dc = {}
                
            if tree.tag not in dc:
                dc[tree.tag] = {}

            for c in tree:
                if c.tag and c.text:
                    dc[tree.tag][c.tag] = c.text
                else:
                    to_dict(c, dc[tree.tag])
            return dc
                    
            
        def get_info(tree, dc=None):
            if dc is None:
                dc = {}
            pth = tree.get("{DAV:}href", None)
            if pth:
                p = tree.get("{DAV:}propstat", {}).get('{DAV:}prop', {})
                dc[pth] = {k:v for k,v in p.items()} if p else {}
            else:
                for k, v in tree.items():
                    get_info(v, dc)
            return dc


        def rename(props, baseurl):
            dc_props = {}
            for k, v in props.items():
                if 'fileid' in k:
                    dc_props['fileid'] = v
                elif 'getlastmodified' in k:
                    v = helpers.make_zulustr(parser.parse(v))
                    dc_props['ltc'] = v
                elif 'getetag' in k: 
                    dc_props['etag'] = v
            if 'fileid' in dc_props:
                dc_props['link'] = '{}/index.php/f/{}'.format(baseurl, dc_props['fileid'])
            
            for n in 'fileid getlastmodified getetag link'.split():
                if not n in dc_props:
                    dc_props[n] = ''
            
            if dc_props['etag'].startswith('"'): 
                dc_props['etag'] = dc_props['etag'][1:]
            if dc_props['etag'].endswith('"'): 
                dc_props['etag'] = dc_props['etag'][:1]
            
            dc_props['checksum'] = dc_props['etag']

            return dc_props

        # Use 'with' to ensure the session context is closed after use.
        with requests.session() as s:
            username, password = nc_auth if nc_auth else login_data
            s.verify = True
            s.auth = (username, password)
            
            if newer_than:
                dt_high = helpers.make_zulustr(helpers.get_utcnow(), remove_ms=True)
                dt_low = helpers.make_zulustr(newer_than, remove_ms=True) if not isinstance(newer_than, str) else newer_than
                url = f'{baseurl}/remote.php/dav/'
                res = s.request('SEARCH', url, headers={'content-Type': 'text/xml'}, data=search_data.format(username, dt_low, dt_high))
                res.raise_for_status()
            else:
                url = f'{baseurl}/remote.php/dav/files/{username}'
                res = s.request('PROPFIND', url, data=req_data_idx, headers={'Depth': str(depth)})
                res.raise_for_status()

            tree = ET.fromstring(res.content)
            files = {}
            for el in tree:
                dc_file = get_info(to_dict(el))
                for f, props in dc_file.items():
                    ff = f.split(f'{username}/{folder_path_remote}')[-1]
                    files[ff] = rename(props, baseurl)

        return files




    def get_nextcld(self, pathes, depth=0):
        baseurl, nc_auth = self.baseurl, self.auth

        if isinstance(pathes, str):
            return self.get_nextcld([pathes])[pathes]
        

        ret = {}
        # Use 'with' to ensure the session context is closed after use.
        with requests.session() as s:
            username, password = nc_auth if nc_auth else login_data
            s.verify = True
            s.auth = (username, password)
            url = f'{baseurl}/remote.php/dav/files/{username}'

            for i, path in enumerate(pathes):
                try:
                
                    if self.verb:
                        log.info('Nextcloud Loading {}/{} {}'.format(i, len(pathes), path))

                    uri = url + parse.quote(path.encode('utf-8'))
                    if self.verb: 
                        print(uri)
                    p = s.request('PROPFIND', uri, headers={'Depth': str(depth)}, data=req_data)

                    p.raise_for_status()
                    # dc = p.json()
                    txt = p.text
                    dc = xmltodict.parse(txt)

                    dc_props = dc['d:multistatus']['d:response']['d:propstat'][0]['d:prop']
                    dc_props = {k.split(':')[-1]: v for k, v in dc_props.items()}
                    dc_props = {k.replace('checksums', 'checksum'): (v if not isinstance(v, dict) or not 'oc:checksum' in v else v['oc:checksum']) for k, v in dc_props.items()}
                    dc_props['link'] = '{}/index.php/f/{}'.format(baseurl, dc_props['fileid'])
                    
                    ret[path] = dc_props
                except Exception as err:
                    log.error(err)
                    ret[path] = {}
        return ret


    def query_all(self, exclude_dirs=True):
        dt_low = datetime.datetime(1980, 1, 1, 1, 1, 1, 1, datetime.timezone.utc)
        dt_high = helpers.get_utcnow()
        return self.query_changed_since(dt_low, exclude_dirs=exclude_dirs, dt_high=dt_high)


    def query_changed_since(self, dt_low, exclude_dirs=True, dt_high=None, folder='Shared/MeerKAT Extension'):
        baseurl, nc_auth = self.baseurl, self.auth
        if dt_high is None:
            dt_high = helpers.get_utcnow()

        if dt_low is None:
            dt_low = dt_high - datetime.timedelta(days=1)

        if isinstance(dt_low, datetime.timedelta):
            dt_low = dt_high - dt_low

        if isinstance(dt_low, (int, float)):
            dt_low = dt_high - datetime.timedelta(seconds=dt_low)

        if not isinstance(dt_high, str):
            dt_high = helpers.make_zulustr(dt_high, remove_ms=True)

        if not isinstance(dt_low, str):
            dt_low = helpers.make_zulustr(dt_low, remove_ms=True)
        

        # Use 'with' to ensure the session context is closed after use.
        with requests.session() as s:
            username, password = nc_auth if nc_auth else login_data
            s.verify = True
            s.auth = (username, password)
            folder = "" if not folder else folder
            
            # ufolder = urllib.parse.quote(folder) if folder else ''
            # ufolder = (ufolder + '/') if ufolder and not not ufolder.endswith('/') else ufolder

            scope = (username + '/' + folder) if folder else username
            url = f'{baseurl}/remote.php/dav'

            if self.verb:
                log.info('Nextcloud Loading changed files since {}'.format(dt_low))

            p = s.request('SEARCH', url, headers={'content-Type': 'text/xml'}, data=search_data.format(scope, dt_low, dt_high))

            p.raise_for_status()
            # dc = p.json()
            txt = p.text
            dc = xmltodict.parse(txt)

            files = []
            if 'd:multistatus' in dc and 'd:response' in dc['d:multistatus']:
                d = dc['d:multistatus']['d:response']
                if not isinstance(d, list):
                    d = [d]

                for response in d:
                    try:
                        files.append(_filedict_from_response(response, username, baseurl))
                    except Exception as err:
                        import json
                        print(json.dumps(dc, indent=2))
                        log.debug((type(dc['d:multistatus']['d:response']), dc['d:multistatus']['d:response']))
                        log.debug(response)
                        log.exception(err)
                        
                

        if exclude_dirs:
            files = [f for f in files if not f['path'].endswith('/')]

        
        files_missing = [f['path'] for f in files if not f['link']]
        if files_missing:
            files_get = self.get_nextcld(files_missing)
            
            for f in files:
                if not f['link']:
                    files['link'] = files_get[f['path']]['link']
                    
        return files



class NextcloudFileSyncer():
    def __init__(self, up = None, baseurl=None) -> None:
        up = up if up else login_data
        baseurl = baseurl if baseurl else server_url

        self.cache = {}
        self.nc = nextcloud_client.Client(baseurl)
        self.nc.login(*up)
        
        self.running_thread = None

    def clearcache(self):
        self.cache.clear()
        

        
    def push_file_if_newer_trylog(self, path_local, path_remote, pull_ltc_after=True, verb=True, mkdir=False):
        try:
            return self.push_file_if_newer(path_local, path_remote, pull_ltc_after, verb, mkdir)
        except Exception as err:
            log.error(err, exc_info=True)
            return None
    

    def push_file_if_newer(self, path_local, path_remote, pull_ltc_after=True, verb=True, mkdir=False, push_in_thread=False):
        if push_in_thread:
            if not self.running_thread is None and self.running_thread.is_alive():
                self.running_thread.join() # only allow one thread to run at a time

            self.running_thread = Thread(target=self.push_file_if_newer_trylog, args=(path_local,path_remote))
            self.running_thread.start()
            return None
        
        res = False
        nc = self.nc


        if mkdir:
            nc.mkdir(os.path.dirname(path_remote))

        if verb:
            mylog = log.info
        else:
            def mylog(*args, **kwargs):
                pass
            
        mylog(f'syncing: {path_local} -> {path_remote} on {nc.url}')
        
        t_server = None
        if not path_remote in self.cache:
            try:
                a = nc.file_info(path_remote)
            except nextcloud_client.HTTPResponseError as err:
                if err.status_code == 404:
                    a = None
                    mylog('file does not exist on server...')
                else:
                    raise
            
            if not a is None:
                t_server = parser.parse(a.attributes['{DAV:}getlastmodified'])
                self.cache[path_remote] = t_server
                mylog(f'last time changed on server: {t_server}')

        t_local = datetime.datetime.utcfromtimestamp(os.path.getmtime(path_local)).replace(tzinfo=datetime.timezone.utc, microsecond=0)
        mylog(f'last time changed local:     {t_local}')
        
        if t_server is None or t_server < t_local:
            mylog(f'local is newer. pushing local to server...')
            res = nc.put_file(path_remote, path_local)
            assert res, f'upload of "{path_local}" to {nc.url} at "{path_remote}" failed'
            if pull_ltc_after:
                mylog(f'getting last time changed from server for caching...')
                t_local = parser.parse(nc.file_info(path_remote).attributes['{DAV:}getlastmodified'])
            else:
                mylog(f'using last time changed from local for caching...')
            mylog(f'caching last time changed for "{path_remote}={t_local}...')
            self.cache[path_remote] = t_local
            res = True
        elif t_server > t_local:
            raise Exception('Sync Conflict! Remote file has changes against local file!')
        else:
            mylog('Everything up to date...')
        
        mylog(f'returning with: {res}')
        return res








# if __name__ == '__main__':
#     import getpass
#     # res = index_all('Shared', (getpass.getuser(), getpass.getpass()), verb=True, depth=2)
#     # print(len(res))

#     # from ReqTracker.core import nextcloud_interface, node_factory


#     def get_nc_info():
#         from cryptography.fernet import Fernet

#         with open(NC_INFO_PATH, 'r') as fp:
#             k, a, b = fp.readlines()
#             FERNET = Fernet(bytes(k, 'ASCII'))
#             a = FERNET.decrypt(a).decode()
#             b = FERNET.decrypt(b).decode()
#         return (a,b)


#     NC_INFO_PATH = os.environ.get('NC_INFO_PATH', r"C:\Users\tglaubach\repos\mke-requirement-suite\scripts\info.hash")

#     import time    

#     for i in range(3):
#         print('index_all')
#         t0 = time.time()
#         res = index_all('Shared', get_nc_info(), verb=False)
#         print(time.time() - t0)
#         print(len(res))

#         # docs = {}
#         # for pth, props in res.items():
#         #     id_, cls = node_factory.doc_fun_path2id(pth, True)
#         #     props['path'] = pth

#         #     if id_ and id_ in docs:
#         #         docs[id_].add_version(props)
#         #     elif id_:
#         #         docs[id_] = cls.from_data(props)
#         #         print(id_, docs[id_])
#         #     # else:
#         #         # print(None, pth)

#         # res = get_nextcld('/Shared', get_nc_info(), depth=2)
#         print('query_all')
#         t0 = time.time()
#         res = query_all(get_nc_info())
#         print(time.time() - t0)
#         print(len(res))
