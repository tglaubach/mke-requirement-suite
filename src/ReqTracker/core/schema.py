
import datetime
import os
import re
import yaml

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

from ReqTracker.core.graph_base import BaseNode, STATUS, colors
from ReqTracker.core import helpers


log = helpers.log

##################################################
##################################################
##################################################
        

class BasePerDishNode(BaseNode):

    matcher_dish_id = re.compile(r'\.D\.(([#P][0-9]{1,3}|##|[0-9]{3}[A-Z]{1}))$')

    @staticmethod
    def test_is_per_dish(node_id):
        if not isinstance(node_id, str):
            node_id = node_id.id
        return len(BasePerDishNode.matcher_dish_id.findall(node_id)) > 0
    

    def get_key_from_id(node_id):
        assert node_id and node_id.contains('.'), "not a valid ID for a node. Needs to be a string and contain a dot"
        return node_id.split('.')[2]

    def __init__(self, node_id='', text='', status=STATUS.OPEN, dc=None):
        if dc:
            if not self.matcher_dish_id.findall(dc['id']):
                dc['id'] += '.D.##'
            super().__init__('', '', STATUS.OPEN, dc)
        else:
            if not self.matcher_dish_id.findall(node_id):
                node_id += '.D.##'
            super().__init__(node_id, text, status, dc)

    
    @property
    def dish_id(self):
        a = self.matcher_dish_id.findall(self.id)
        if len(a) and a[0] and len(a[0]):
            return a[0][0]
        else:
            return ''
        
    def set_new_dish_id(self, new_dish_id, set_children=False):

        to_rep = '.D.{}'.format(self.dish_id)
        new_id = '.D.{}'.format(new_dish_id)

        self.id = self.id.replace(to_rep, new_id)
        if set_children:
            for c in self.children():
                if issubclass(type(c), BasePerDishNode):
                    c.set_new_dish_id(new_id)



def test_node_child_state(node):
    pass_conditions = node.children       
    nopass = [c for c in pass_conditions if c.status != STATUS.PASS]
    if not nopass:
        node._state = STATUS.PASS
    else:
        no_nearpass = [c for c in pass_conditions if c.status != STATUS.NEARLY_PASS]
        fails = [c for c in pass_conditions if c.status == STATUS.FAIL]
        if fails:
            node._state = STATUS.FAIL
        if not no_nearpass:
            node._state = STATUS.NEARLY_PASS
        else:
            node._state = STATUS.OPEN

  
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
# import re, os
re_forms = {
    'PBS': r'(?P<PBS>3[0-9]{2}-[0-9]{6})',
    'org': r'(?P<org>[A-Z]{3,5})',
    'doc_type': r'(?P<doc_type>[A-Z]{2,3})',
    'doc_no':   r'(?P<doc_no>[0-9]{3,4})',
    'doc_no2':   r'(?P<doc_no>[0-9]+)',
    'version':          r'(?P<version>[Vv][0-9A-Za-z]*|[Rr][Ee][Vv][ _]?[A-Z0-9a-z]+|[0-9A-Za-z\.]+)',
    'version_strict':   r'(?P<version>[Vv][0-9A-Za-z]*|[Rr][Ee][Vv][A-Z0-9\.]{0,5}|DRAFT[0-9_]*|[0-9]{1,3}[A-Z]?)',
    'sub': r'(?P<sub>A[0-9]{2})',
    'dish_no': r'(?P<dish_no>#[0-9]{1,2}|P[0-9]{3}|[0-9]{3}[A-Z]{1})',
    'dish_no2': r'(?P<dish_no2>[0-9]{3}[A-Z]{1})',
    'title':        r'(?P<title>[0-9A-Za-z_ \-()&]*)',
    'title2':       r'(?P<title>[A-Za-z(][0-9A-Za-z_ \-()&]*)',
    'title_strict': r'(?P<title>[0-9A-Za-z_ \-]*)',
    # 'sign': r'(?P<sign>[ \-_]?[Ss][Ii][Gg][Nn]([Ee][Dd])?[ \-_]?)',
    # 'mult': r'(\([0-9]\))',    
    'ext': r'(?P<ext>\.[a-zA-Z0-9]*)',
    'ext_strict': r'(?P<ext>.pdf|.docx)',
    '-_': r'[\-_]',
    'dev_id': r'(?P<dev_id>[0-9]{3,4})',
}
def mk_re(*args):
    return ''.join([re_forms[x] if x in re_forms else str(x) for x in args])
    

class BaseVersionedDocument(BaseNode):
    is_filelike=True

    @classmethod
    def fun_version2unique(cls, version_dc):
        fields = ['path', 'fileid', 'checksum']
        return '|'.join(version_dc[k] for k in fields if k in version_dc and version_dc[k])
    
    @classmethod
    def from_data(cls, dc_data):
        id_ = cls.fun_path2id(dc_data['path'])
        return cls(dc={'id': id_, 'data': {'versions': [dc_data], 'i_ver_last': 0}})

    @classmethod
    def fun_get_md_link_s(cls, path, link, id=''):
        if not path and id:
            path = id
        if path and link:
            txt = '[{}]({})'.format(path, link)
        elif path:
            txt = '{}'.format(path)
        else:
            txt = 'NO PATH, ID OR LINK!'
        return txt


    @classmethod
    def fun_path2id(cls, pth, ret_all_matches=False, empty_on_no_match=False):
        dc = cls.fun_match(pth, ret_all_matches=ret_all_matches)
        if not dc and empty_on_no_match:
            return ''
        elif not dc:
            return os.path.basename(pth)
        elif ret_all_matches and dc:
            f = lambda dc: '-'.join([dc[k] for k in cls.id_parts if dc[k]])
            return [f(n) for n in dc]
        else:
            return '-'.join([dc[k] for k in cls.id_parts if dc[k]])

    @classmethod
    def fun_match(cls, pth, ret_all_matches=False):
        filename = os.path.basename(pth)        
        ret = []
        dc = {}

        for match in cls.matcher.finditer(filename):
            
            dc = match.groupdict()
            dc['filename'] = match.group()
            dc['path'] = pth

            merge = lambda dca, dcb, k: dcb[k] if k in dcb and dcb[k] else (dca[k] if k in dca else None)
            dc_default = {k:'' for k in cls.default_keys}
            dc = {k: merge(dc_default, dc, k) for k in dc_default.keys()}
            if not ret_all_matches:
                break
            else:
                ret.append(dc)

    
        return dc if not ret_all_matches else ret



    def __init__(self, id='', text='', status = STATUS.UNHANDLED, dc=None, trim_versions=True):
        super().__init__(id, text, status, dc)
        self.__assure_has_fields()
        if trim_versions:
            self.__trim_versions()
        self.__test_has_versions()
        
    @property
    def hint(self):
        return f'VERSION: {self.version} | PATH: {self.path}'    
    
    def __assure_has_fields(self):
        if 'versions' not in self.data:
            self.data['versions'] = []
        if 'i_ver_last' not in self.data:
            self.data['i_ver_last'] = 0
        if 'tickets' not in self.data:
            self.data['tickets'] = []

    def __trim_versions(self):
        to_del = []
        vers = self.data['versions'] 
        vers_unq = set()
        if len(vers) <= 0:
            return
    
        for i, v in enumerate(vers):
            k = self.fun_version2unique(v)
            if k in vers_unq:
                to_del.append(i)
            else:
                vers_unq.add(k)
        if to_del:
            self.data['versions'] = [vers[i] for i, _ in enumerate(vers) if i not in to_del]
            if self.data['i_ver_last'] in to_del:
                self.data['i_ver_last'] = len(self.data['versions'])

    def get_text(self, short=False):
        txt = '**{}**\n\n'.format(self.id)
        txt += '---\n\n'

        txt += self.get_md_link() + '\n\n'

        versions = self.get_version_dcs()

        nn = []
        for ver_dc in versions:
            n = {**self.fun_match(ver_dc['path']), **ver_dc}
            n['txt'] = 'LTC (UTC): **{}**  | VERSION: **{}**\n    - FNAME: `{}`\n    - DIR: '.format(n.get('ltc', ' ').replace('T', ' ').replace('Z', ''), n.get('version', ' '), os.path.basename(n['path']))
            nn.append(n)

        nn.sort(key=lambda n: n['txt'], reverse=True)
        
        links = [n['txt'] + type(self).fun_get_md_link_s(os.path.dirname(n['path']), n['link']) for n in nn]

        txt += '\n'.join(['- ' + lnk for lnk in links])

        if not short:
            txt += '\n\n---\n\n'

            txt += '```{}```\n'.format(self.get_fields())
            txt += '\n```{}```\n'.format(yaml.dump(self.data))

        if self._text:
            txt += '---\n\n'
            txt += self._text

        return txt

    def get_md_link(self, label_to_show='path'):
        label = getattr(self, label_to_show) if hasattr(self, label_to_show) else self._getter(label_to_show)
        return self.fun_get_md_link_s(label, self._getter('link'), self.id)
    
    def __test_has_versions(self):
        assert 'i_ver_last' in self.data and self.data['i_ver_last'] is not None, 'this document has no version'
        assert 'versions' in self.data and self.data['versions'], 'this document has no version'
        
    def get_version_dcs(self, ret_dict=False):
        if ret_dict:
            return {self.fun_version2unique(e):e for e in self.data['versions']}
        else:
            return [e for e in self.data['versions']]
    
    def get_version_hist(self):
        tmp = [self.fun_match(e['path']) for e in self.data['versions']]
        # fun = lambda e: '-'.join([e[k] for k in 'dish_no version'.split() if k in e]) if 'dish_no' in e and e['dish_no'] else e['version']
        fun = lambda e: e['version']
        tmp = [fun(e) for e in tmp if 'version' in e and e['version']]
        tmp.sort(key=lambda x: self.__cmp_version(x))
        
        return tmp
    
    @property
    def i_ver_last(self):
        self.__test_has_versions()
        return self.data['i_ver_last']
    
    def get_latest_ver(self):
        self.__test_has_versions()
        if self.data['i_ver_last'] >= len(self.data['versions']):
            self.data['i_ver_last'] = len(self.data['versions']) -1
        return self.data['versions'][self.data['i_ver_last']]
                
    
    def add_version(self, version_dc, is_latest = 'test', on_exist='ignore', update_status = True):
        self.__test_has_versions()
        self.__trim_versions()
        
        needed_fields = 'path link fileid checksum'.split()
        assert version_dc and isinstance(version_dc, dict), f'must give a non empty dict with the fields {needed_fields} as input!'
        assert not [k for k in needed_fields if k not in version_dc], f'must give a non empty dict with the fields {needed_fields} as input!'
        
        found = False
        versions = self.get_version_dcs(ret_dict=True)
        new_version_key = self.fun_version2unique(version_dc)

        if on_exist == 'raise':
            assert new_version_key not in versions, 'the given version already exists!'
        else:
            if new_version_key in versions:
                version_dc = versions[new_version_key]
                found = True
        
        if not found:
            self.data['versions'].append(version_dc)

            tmp = self.fun_match(version_dc['path'])
            v = tmp['version']
            # if 'dish_no' in tmp and tmp['dish_no']:
            #     v += '-' + tmp['dish_no']

            vers = ', '.join(self.get_version_hist())
            self.add_changenote('graph["{}"].add_version("{}") -> {}'.format(self.id, v, vers))

        if is_latest == 'test':
            i = self.data['i_ver_last']
            v_before = self.get_latest_ver()
            self.sort_versions()
            v_now = self.get_latest_ver()
            if (self.__cmp_version(v_now) > self.__cmp_version(v_before)) and update_status:
                log.debug('Found new version. Setting Status to "NEEDS_REVIEW"')
                self.set_status(STATUS.NEEDS_REVIEW)
        elif is_latest:
            i = self.data['i_ver_last']
            v_before = self.get_latest_ver()
            self.data['i_ver_last'] = self.data['versions'].index(version_dc)
            v_now = self.get_latest_ver()
            if i != self.data['i_ver_last']:
                self.add_changenote('graph["{}"].i_ver_last from:{} to:{}'.format(self.id, i, self.data['i_ver_last']))
                if (self.__cmp_version(v_now) > self.__cmp_version(v_before)) and update_status:
                    self.set_status(STATUS.NEEDS_REVIEW)
        return self

    def __cmp_version(self, version):
        v = str(version)
        v = re.sub(r"\s+", "", v)
        v = v.replace('p', '.').upper()
        
        non_numeric_part = ''.join(re.findall(r"[A-Z_\-]+", v))
        numeric_part= ''.join(re.findall(r"[\d\.]+", v))
        
        numeric_part = '0' if not numeric_part else str(numeric_part)
        numeric_part = numeric_part.rjust(10, '0')
        non_numeric_part = non_numeric_part.rjust(10, 'Z')
        return f'{non_numeric_part}{numeric_part}'


    def sort_versions(self):
        vers, vers2 = self.get_sorted_versions(ret_full=True)

        if vers2:
            highest_ver = vers2[-1]
            i = vers.index(highest_ver)
            if i != self.data['i_ver_last']:
                self.add_changenote('graph["{}"].sort_versions() -> i_ver_last from:{} to:{}'.format(self.id, i, self.data['i_ver_last']))
            self.data['i_ver_last'] = i
        return self.data['i_ver_last'] 

    def get_sorted_versions(self, ret_full = False, ret_mapped=False):
        vers = [self.fun_match(x['path']).get('version', '') for x in self.data['versions']]
        
        if vers:
            vers2 = vers[:]
            vers2.sort(key=lambda x: self.__cmp_version(x))
        else:
            vers, vers2 = [], []

        if ret_full and ret_mapped:
            v = [self.__cmp_version(x) for x in vers]
            return v, vers2
        elif ret_full:
            return (vers, vers2)
        else:
            return vers2


    def get_fields(self, merge_data=False):
        dc = self.fun_match(self.get_latest_ver()['path'])
        return {**dc, **self.data} if merge_data else dc
    
    def get_row(self, parent_id='', color_col='color'):
        row = super().get_row(parent_id, color_col)
        
        row['text'] = row['node_id'] + ' | {}'.format(self._getter('title'))
        # row['node_id'] += ' | {}'.format(self._getter('title'))
        return row

    def to_doc_dict(self):
        dc1 = {'id':self.id, 'text': self.text, 'tags': self.tags, 'notes': self.notes, 'tickets': self.tickets}
        dc2 = self.get_fields()
        dc3 = self.get_latest_ver()
        dc = {**dc1, **dc2, **dc3}
        return dc
    
    def _getter(self, name, default = ''):
        if name in type(self).default_keys:
            dc = self.get_fields()
        else:
            dc = self.data
        return dc[name] if name in dc else default
    
    def get_last_version_changed(self):
        itr = [v for v in self.data['versions'] if 'ltc' in v]
        itr.sort(key=lambda x: x['ltc'], reverse=True)
        return itr[0] if itr else {}
    
    @property
    def text(self):
        return self.get_text(short=False)
    
    @property
    def link(self):
        dc = self.get_latest_ver()
        if 'link' in dc:
            return dc['link']
        else:
            return ''
        
    @property
    def version(self):
        dc = self.get_fields()
        return dc['version'] if dc and 'version' in dc else ''
    
    @property
    def extension(self):
        dc = self.get_fields()
        return dc['ext'] if dc and 'ext' in dc else ''
    
    @property
    def path(self):
        dc = self.get_latest_ver()
        assert 'path' in dc, 'path not found as property in document!'
        return dc['path']
        
    @property
    def filename(self):
        return os.path.basename(self.path)
    
    @property
    def title(self):
        return self._getter('title')
    
    @property
    def last_time_changed(self):
        return self.get_last_version_changed().get('ltc', helpers.make_zulustr(datetime.datetime.fromtimestamp(0)))
    
    @property
    def ltc(self):
        return self.last_time_changed
    
    @property
    def dish_id(self):
        dish_no = self._getter('dish_no')
        if dish_no.startswith('#SKA'):
            dish_no = dish_no[1:]
        
        if not dish_no:
            dish_no = '' # make sure it's a string whatever it was before
        
        return dish_no
    
    def __str__(self) -> str:
        s = self.id
        if self.title: 
            s += ' | ' + self.title
        return s
    

class Document(BaseVersionedDocument):
    
    matcher_mke = re.compile(mk_re(r'(?P<pre>MKE)', *('- PBS -_ org -_ doc_type -_ doc_no -_ ? sub ? -_ ? dish_no ? -_ ? version ? -_ ? title ? ext ?'.split())))
    matcher_ska = re.compile(mk_re(r'(?P<pre>SKA)', '[-_ ]', 'org', '[-\.]', 'doc_type', '?', '[\-_ \.]?', 'doc_no2', '[_ -\.]?', 'version', '*', '[\-_ ]*', 'title2', 'ext'))
    matcher_pbs = re.compile(mk_re('PBS', '-', 'doc_no', '*',  '[-\. _]', 'version', '?', '[\-_ ]?', 'title', '?', 'ext'))

    # matcher_pbs = re.compile(r'(?P<pre>MKE)-(?P<PBS>316-[0-9]{6})[\-_](?P<org>[A-Z]{3,5})[\-_](?P<doc_type>[A-Z]{2,3})[\-_](?P<doc_no>[0-9]{3,4})[\-_]?(?P<sub>A[0-9]{2})?[\-_]?[\-_]?(?P<dish_no>#?P?[0-9]{1,3})?[\-_]?(?P<version>v[0-9_]*|Rev[A-Z0-9\.]{0,5}|DRAFT[0-9_]*|[0-9]{1,3}[A-Z]?)?[\-_]?(?P<title>[0-9A-Za-z_ \-]*)?(?P<ext>\.[a-zA-Z0-9]*)?')
    # matcher_ska = re.compile(r'(?P<pre>SKA)[-_ ](?P<org>[A-Z]{3})[-\.](?P<doc_type>[A-Z]{3,4})?[\-_ ]*(?P<doc_no>[0-9]+)[_ -]?(?P<version>v[0-9_]*|[Rr][Ee][Vv] ?[A-Z0-9]*|[0-9]{0,2})?[\-_ ]*(?P<title>[A-Za-z(][0-9A-Za-z_ \-()&]*)(?P<ext>.pdf|.docx)')
    # matcher_pbs = re.compile(r'(?P<PBS>3[0-9]{2}-[0-9]{6})[-](?P<doc_no>[0-9]{3,4})[-_ ]*(?P<version>[Vv][0-9_]*|[Rr][Ee][Vv][ _]?[A-Z0-9a-z]+|[0-9\.]*)?[\-_ ]?(?P<title>[0-9A-Za-z_ \-()&]*)?(?P<ext>.pdf|.docx)')
    
    default_keys = 'pre PBS org doc_type doc_no sub dish_no version title ext'.split()
    id_parts = 'pre PBS org doc_type doc_no dish_no'.split()
    key = 'DOC'

    keys = ['MKE', 'SKA', '3']

    @classmethod
    def fun_match(cls, pth, ret_all_matches=False):
        filename = os.path.basename(pth)        
        ret = []
        dc = {}
        matchers = {'MKE': cls.matcher_mke, 'SKA': cls.matcher_ska, '3XX': cls.matcher_pbs}
        

        for key, matcher in matchers.items():
            if (key == 'SKA' or key == '3XX') and filename.startswith('MKE'):
                continue
            if key == '3XX' and filename.startswith('SKA'):
                continue

            for match in matcher.finditer(filename):
                
                dc = match.groupdict()

                dish_no2 = dc.pop('dish_no2', '')
                if dish_no2 and not dc['dish_no']:
                    dc['dish_no'] = dish_no2
                elif dish_no2 and dc['dish_no']:
                    dc['title'] = dish_no2 + '-' + dc['title']
                

                dc['filename'] = match.group()
                dc['path'] = pth

                merge = lambda dca, dcb, k: dcb[k] if k in dcb and dcb[k] else (dca[k] if k in dca else None)
                dc_default = {k:'' for k in cls.default_keys}
                dc = {k: merge(dc_default, dc, k) for k in dc_default.keys()}
                if not ret_all_matches:
                    break
                else:
                    ret.append(dc)
            if dc and not ret_all_matches:
                break
        
        return dc if not ret_all_matches else ret

    @classmethod
    def fun_get_doc_id(cls, path):
        dc = Document.fun_match(path)
        return 'MKE-' + '-'.join([dc[k] for k in 'PBS org doc_type doc_no'.split()])

    def get_doc_id(self):
        try:
            return self.fun_get_doc_id(self.get_latest_ver()['path'])
        except Exception as err:
            return self.id



##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################
##############################################################################################################################################

    
class RequestForDeviation(BaseVersionedDocument):
    matcher = re.compile(mk_re(r'(?P<doc_type>RFD)', '-_', 'dev_id', '-_', 'version', '-_', 'title', '?', 'ext', '?'))
    # matcher = re.compile( r'(?P<doc_type>RFD)[\-_](?P<dev_id>[0-9]{3,4})[\-_]?(?P<version>v[0-9_]*|Rev[A-Z0-9\.]{0,5}|DRAFT[0-9_]*|[0-9]{1,3}[A-Z]?)?[\-_]?(?P<title>[0-9A-Za-z_ \-]*)?(?P<ext>\.[a-zA-Z0-9]*)?')

    default_keys = 'doc_type dev_id version title ext'.split()
    id_parts = 'doc_type dev_id'.split()
    key = 'RFD'

class RequestForWaiver(BaseVersionedDocument):
    matcher = re.compile(mk_re(r'(?P<doc_type>RFW)', '-_', 'dev_id', '-_', 'dish_no', '-_', 'version', '-_',  'title', '?', 'ext', '?'))
    # matcher = re.compile( r'(?P<doc_type>RFW)[\-_](?P<dev_id>[0-9]{3,4})[\-_]?(?P<version>v[0-9_]*|Rev[A-Z0-9\.]{0,5}|DRAFT[0-9_]*|[0-9]{1,3}[A-Z]?)?[\-_]?(?P<title>[0-9A-Za-z_ \-]*)?(?P<ext>\.[a-zA-Z0-9]*)?')

    default_keys = 'doc_type dev_id version title ext'.split()
    id_parts = 'doc_type dev_id'.split()
    key = 'RFW'

    
# if __name__ == '__main__':
    
#     pth = r"MKE-316-000000-MPI-SIP-3054-02-119A-OnSiteInspectionChecklist - signed.pdf"

#     parts = Document.fun_match(pth)

#     print(parts)

#     pth = r"Shared/MeerKAT Extension/05_official_redmine_dms/MKp_Dishes/MKE_Dish_01/SAR/MKE-316-000000-ODC-TR-0183_119A_02_Reflector System Alignment and Performance_TG.pdf"

#     parts = Document.fun_match(pth)

#     print(parts)