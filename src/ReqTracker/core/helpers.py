
import datetime
import dateutil
import pytz

import re

# import datetime
import os

import re
import dateutil.parser, datetime, pytz

import logging
import sys

log = logging.getLogger()
level = logging.DEBUG
fmt = "[ %(levelname)s - %(asctime)s - %(name)s - %(filename)s:%(lineno)s] %(message)s"
frmatter = logging.Formatter(fmt, datefmt='%Y-%m-%d %H:%M:%S%z')
log.setLevel(level)
streamHandler = logging.StreamHandler(sys.stdout)


class FifoLogHandler(logging.Handler):
    def __init__(self, n_lines_max=200)-> None:
        self.lines = list()
        self.n_lines_max = n_lines_max
        logging.Handler.__init__(self=self)

    def emit(self, record:logging.LogRecord) -> None:
        if len(self.lines) > self.n_lines_max:
            self.lines.pop(0)
        self.lines.insert(0, frmatter.format(record))
    
    @property
    def is_truncated(self):
        return len(self.lines) >= self.n_lines_max

    def get_err_info(self):
        has_err = len([l for l in self.lines if 'error' in l.lower()])
        if has_err:
            return has_err, 'danger'
        has_warn = len([l for l in self.lines if 'warning' in l.lower()])
        if has_warn:
            return has_warn, 'warning'
        
        return 0, ''
    
fifo_log = FifoLogHandler(n_lines_max=200)

streamHandler.setFormatter(frmatter)
fifo_log.setFormatter(frmatter)

log.addHandler(streamHandler)
log.addHandler(fifo_log)


from pathlib import Path
home = str(Path.home())


def tostr(*args):
    return ' '.join([str(x) for x in args])

# if __name__ == '___main__':
#     import sys, inspect, os
#     # path was needed for local testing
#     current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
#     parent_dir = os.path.dirname(current_dir)
#     sys.path.insert(0, parent_dir)

def intersperse(lst, item, is_callable=False):
    if is_callable:
        result = [item() for _ in range(len(lst) * 2 - 1)]
    else:
        result = [item] * (len(lst) * 2 - 1)
    result[0::2] = lst
    return result

# limit_len = lambda k: k if len(k) < 10 else k[:10]+'...'
def limit_len(k, n_max =10, LR='L'):
    if k is None:
        return str(None)
    if LR == 'L':
        return k if len(k) < n_max else k[:n_max]+'...'
    else:
        return k if len(k) < n_max else '...' + k[-n_max:]
    
def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i*k+min(i, m):(i+1)*k+min(i+1, m)] for i in range(n))

def splitw(a, nmax):
    ret = []
    while len(a) > nmax:
        ret.append(a[:nmax])
        a = a[nmax:]
    ret.append(a)
    return ret


def nowiso():
    return make_zulustr(get_utcnow())

def get_utcnow():
    """get current UTC date and time as datetime.datetime object timezone aware"""
    return datetime.datetime.utcnow().replace(tzinfo=pytz.utc)

def make_zulustr(dtobj:datetime.datetime, remove_ms = True) -> str:
    '''datetime.datetime object to ISO zulu style string
    will set tzinfo to utc
    will replace microseconds with 0 if remove_ms is given

    Args:
        dtobj (datetime.datetime): the datetime object to parse
        remove_ms (bool, optional): will replace microseconds with 0 if True . Defaults to True.

    Returns:
        str: zulu style string e.G. 
            if remove_ms: 
                "2022-06-09T10:05:21Z"
            else:
                "2022-06-09T10:05:21.123456Z"
    '''
    utc = dtobj.replace(tzinfo=pytz.utc)
    if remove_ms:
        utc = utc.replace(microsecond=0)
    return utc.isoformat().replace('+00:00','') + 'Z'

def parse_zulutime(s:str)->datetime.datetime:
    '''will parse a zulu style string to a datetime.datetime object. Allowed are
        "2022-06-09T10:05:21.123456Z"
        "2022-06-09T10:05:21Z" --> Microseconds set to zero
        "2022-06-09Z" --> Time set to "00:00:00.000000"
    '''
    try:
        if re.match(r'[0-9]{4}-[0-9]{2}-[0-9]{2}Z', s) is not None:
            s = s[:-1] + 'T00:00:00Z'
        return dateutil.parser.isoparse(s).replace(tzinfo=pytz.utc)
    except Exception:
        return None

def parse_timestr(s):
    try:
        return dateutil.parser.parse(s).astimezone(pytz.utc)
    except Exception:
        return None
    
def mkdir(pth, raise_ex=False, verbose=False):
    try:
        if not os.path.exists(pth):
            if verbose:
                log.info('Creating dir because it does not exist: ' + pth)
            os.makedirs(pth, exist_ok=True)
            path = Path(pth)
            path.mkdir(parents=True, exist_ok=True)
            return str(path).replace('\\', '/').replace('//', '/')

    except Exception as err:
        log.error(err)
        if raise_ex:
            raise
    return None

def join(*parts):
    return os.path.join(*parts).replace('\\', '/').replace('//', '/')

