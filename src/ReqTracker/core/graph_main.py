
from copy import deepcopy
import json
import re
import numpy as np
import datetime
import time

import igraph as ig

import os, inspect, sys

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)
    

from ReqTracker.core import schema, graph_base, helpers
from ReqTracker.core.textsearch import simple_search, simple_search_keys


log = helpers.log



def delete_vertices(view, vertices):
    nodes_to_pop_i = [view.vs['name'].index(id_) for id_ in el2id(vertices) if id_ in view.vs['name']]
    view.delete_vertices(nodes_to_pop_i)
    return view

def delete_edges(view, edges):
    f = lambda x: view.vs['name'].index(x)
    fin = lambda a, b: a in view.vs['name'] and b in view.vs['name']
    edges_to_del = [(f(a), f(b)) for a, b in el2id(edges) if fin(a,b)]
    view.delete_edges(edges_to_del)
    return view

def el2id(*args, mapper={}):
    if len(args) == 1:
        x = args[0]
        if isinstance(x, tuple):
            return tuple([el2id(xx) for xx in x])
        if hasattr(x, '__len__') and not isinstance(x, str):
            return [el2id(xx) for xx in x]
        else:
            s = x.id if not isinstance(x, str) else x
            return mapper[s] if s in mapper else s
    else:
        return [el2id(xx) for xx in args]


def id2el(id_, dc_lut):
    if hasattr(id_, '__len__') and not isinstance(id_, str):
        return [id2el(xx, dc_lut) for xx in id_]
    elif issubclass(type(id_), graph_base.BaseNode):
        return id_
    else:
        return dc_lut[id_]


"""

██   ██ ███████ ██      ██████  ███████ ██████  ███████ 
██   ██ ██      ██      ██   ██ ██      ██   ██ ██      
███████ █████   ██      ██████  █████   ██████  ███████ 
██   ██ ██      ██      ██      ██      ██   ██      ██ 
██   ██ ███████ ███████ ██      ███████ ██   ██ ███████ 
                                                        
                                                        

"""

classes = [graph_base.BaseNode, schema.BasePerDishNode, 
            schema.Document,
            schema.RequestForDeviation, schema.RequestForWaiver]


lut_constructors = {
    **{str(c):c for c in classes},  
    **{c.__name__:c for c in classes},
    **{c.key:c for c in classes if hasattr(c, 'key')}
    }

lut_per_dish_keys = [c.key for c in classes if hasattr(c, 'matcher_dish_id')]


def _get_xy_sub(positions, graph, view_id, node_id, xy, xo, yo, show_sorted):
    x0, ymax = xy
    x, ymaxi = (x0 + xo), None
    children = graph.get_children(node_id, view_id)

    for child in (sorted(children) if show_sorted else children):
        if child in positions:
            continue
        y = ymax if ymaxi is None else ymaxi + yo
        positions[child] = {"x": x, "y": y}
        ymaxi, positions = _get_xy_sub(positions, graph, view_id, child, (x, y), xo, yo, show_sorted)
        ymax = max(ymaxi, ymax)
        
    return ymax, positions

def get_xy_tree(graph, view_id, xo = 1, yo = 1, show_sorted=True):
    root_nodes = graph.get_root_nodes(view_id)
    x, y = 0, 0
    positions = {}
    for root in (sorted(root_nodes) if show_sorted else root_nodes):
        if root in positions:
            continue
        positions[root] =  {"x": x, "y": y}
        y, positions = _get_xy_sub(positions, graph, view_id, root, (x, y), xo, yo, show_sorted)
        y += 1
    return positions

def get_possible_node_info():
    return [(c.key, c.__name__) for c in classes if  hasattr(c, 'key') and c.key != 'DOC']

def from_dict(all_nodes_as_dc):
    all_nodes_as_dc = assure_has_dish_id(all_nodes_as_dc)
    return Graph.deserialize(all_nodes_as_dc)

def get_all_keys():
    keys = []
    for c in classes:
        keys += c.keys if hasattr(c, 'keys') else [c.key]
    return keys

def assure_has_dish_id(all_nodes_as_dc):

    def adjust(node_id):
        key = node_id.split('.')[0]
        if key in lut_per_dish_keys and not schema.BasePerDishNode.matcher_dish_id.findall(node_id):
            node_id += '.D.##'
        return node_id
    
    dc_new = {}

    for k, dc in all_nodes_as_dc.items():
        knew = adjust(k)
        dc['id'] = knew
        if 'parents' in dc:
            dc['parents'] = [adjust(kc) for kc in dc['parents']]

        dc_new[knew] = dc

    return dc_new


def construct(dc_node, allow_fallback=True, raise_on_error=False):

    try:
        if isinstance(dc_node, str):
            dc_node = {'id':dc_node}

        if 'type' in dc_node:
            if dc_node['type'] not in lut_constructors:
                if allow_fallback:
                    constructor = graph_base.BaseNode
                else:
                    log.warn('construction failed (skipping!): ' + str(dc_node))
                    def constructor(dc=None):
                        return None            
            else:
                constructor = lut_constructors[dc_node['type']]
        else:
            node_id = dc_node['id'].strip()
            key = node_id.split('.')[0]
            if key in lut_constructors:
                constructor = lut_constructors[key]
            elif allow_fallback:
                constructor = graph_base.BaseNode
            else:
                raise ValueError('the constructor for id={} could not be resolved!'.format(node_id))
            
        ret = constructor(dc=dc_node)

    except Exception as err:
        if raise_on_error:
            raise
        log.error('construction failed: ' + str(dc_node))
        log.exception(err)
        ret = None
    return ret


class NodeStore(dict):

    def __contains__(self, key):
        return super().__contains__(self._keytransform(key))
    
    def __iter__(self):
        return iter(super().values())
    
    def __getitem__(self, key):
        return super().__getitem__(self._keytransform(key))

    def __setitem__(self, key, value):
        super().__setitem__(self._keytransform(key), value)

    def __delitem__(self, key):
        super().__delitem__(self._keytransform(key))

    def _keytransform(self, key):
        if hasattr(key, 'id'):
            return key.id
        elif isinstance(key, dict) and 'id' in key:
            return key.get('id')
        else:
            return key
    
    def first(self):
        return next(iter(self))

    def append(self, value):
        self[self._keytransform(value)] = value

    def append_many(self, entries):
        for e in entries:
            self.append(e)

    def remove(self, value):
        return super().pop(self._keytransform(value))
        
        

def inverted_seismic_colormap(progress):
  """Converts a progress value (0-1) to an inverted seismic colormap hex code.

  Args:
    progress: A float between 0 and 1.

  Returns:
    A hex color code.
  """

  if not isinstance(progress, (int, float)):
    return '#808080'

  progress = max(0, min(progress, 1))

  if progress <= 0.5:
    p = 2 * progress
    r = int(255 * p)
    g = 255
    b = 255
  else:
    p = 2 * (progress - 0.5)
    r = 255
    g = 255
    b = int(255 * p)

  return f"#{r:02x}{g:02x}{b:02x}"

##################################################################################
##################################################################################
##################################################################################
##################################################################################

"""

██    ██ ██ ███████ ██     ██ 
██    ██ ██ ██      ██     ██ 
██    ██ ██ █████   ██  █  ██ 
 ██  ██  ██ ██      ██ ███ ██ 
  ████   ██ ███████  ███ ███  
                                                                             
"""


class View(object):
    def __init__(self, name, edges=None, positions = None, default_layout = 'tree') -> None:
        assert isinstance(edges, list), f'"edges" is not of type list! {type(edges)=}'
        self.name = name
        self.edges = edges
        self.default_positions = positions if positions else {}
        self.default_layout = default_layout

    @property
    def view(self):
        return self.to_igraph()

    def __str__(self):
        return 'View_{}_{}, N_e={}, N_p={}'.format(self.name, id(self), len(self.edges), len(self.default_positions))

    def __repr__(self):
        return str(self)

    def __contains__(self, key):
        id_ = getattr(key, 'id') if hasattr(key, 'id') else key
        return id_ in self.get_contained_nodes() # slow, but works!
    
    def to_dict(self):
        return deepcopy(dict(edges=self.edges, positions=self.default_positions))
                             
    @property
    def positions(self):
        return self.default_positions if self.default_positions else self.get_new_positions(self.default_layout, ret_numpy=False)

    def get_contained_nodes(self):
        return list(set().union(*self.edges, set(self.default_positions.keys())))


    def clone(self, new_name=None):
        new_name = new_name if new_name else self.name
        edges = deepcopy(self.edges)
        positions = deepcopy(self.default_positions)
        return View(new_name, edges, positions, self.default_layout)
    
    def rename_ids(self, old_ids, new_ids):
        edges = np.array(self.edges)
        for oid, nid in zip(old_ids, new_ids):
            edges[edges == oid] = nid
            if oid in self.default_positions:
                self.default_positions[nid] = self.default_positions.pop(oid)
        self.edges = [tuple(e) for e in edges]
        return self
    
    def clone_renamed(self, new_name, old_ids, new_ids):
        return self.clone(new_name).rename_ids(old_ids, new_ids)
        

    def get_edges(self, copy=True):
        return [(tpl[0], tpl[1]) for tpl in self.edges] if copy else self.edges    

    def add_edge(self, edge):
        return self.add_edges([edge])
    
    def add_edges(self, edges):
        if edges:
            existing = set(self.edges)
            to_add = [e for e in el2id(edges) if not e in existing ]
            self.edges += to_add
            return len(to_add)
        return 0
                     
    def del_edge(self, edge):
        self.del_edges([edge])

    def del_edges(self, edges=None):
        if edges is None:
            n = len(self.edges)
            self.edges.clear()
            return n
        else:
            to_del = set(edges)
            self.edges = [tpl for tpl in self.edges if tpl not in to_del]
            return len(to_del)
        
    def rename_id(self, old_id, new_id):
        m = np.array(self.edges)
        idx = m == old_id
        m[idx] = new_id
        self.edges = [tuple(row) for row in m]
        return np.sum(idx.astype(int), axis=None)
    
    def get_children(self, node_id):
        return tuple([c for (p, c) in self.edges if p == node_id])

    def get_parents(self, node_id):
        return tuple([p for (p, c) in self.edges if c == node_id])
    
    def get_node_edges(self, node_id):
        return tuple([e for e in self.edges if node_id in e])
    
    def get_nodes_edges(self, node_ids):
        return tuple([e for e in self.edges if e[0] in node_ids or e[1] in node_ids])

    def get_root_nodes(self):
        return [n for n in self.get_contained_nodes() if len(self.get_parents(n)) == 0]

    def del_node(self, node_id):
        to_del = [tpl for tpl in self.edges if node_id in tpl]
        n1 = self.del_edges(to_del)

        while node_id in self.default_positions:
            self.default_positions.pop(node_id)
            n1 += 1
        return n1
    
    def add_node(self, node_id, xy=None, parents=None, children=None):
        assert not node_id in self, f'the node with {node_id=} already esists in this view'
        xy = dict(x=0, y=0) if xy is None else xy
        parents = parents if parents else []
        children = children if children else []
        self.default_positions[node_id] = xy
        my_nodes = set(self.get_contained_nodes())

        edges_to_add = []
        edges_to_add += [(p, node_id) for p in parents if p in my_nodes]
        edges_to_add += [(node_id, c) for c in children if c in my_nodes]

        if edges_to_add:
            self.add_edges(edges_to_add)

    def move_node(self, node_id, xy_new):
        nid = el2id(node_id)
        if xy_new and not isinstance(xy_new, dict):
            xy_new = dict(x=xy_new[0], y=xy_new[1])

        self.default_positions[nid] = xy_new
        return xy_new
    
    def to_igraph(self, with_root=False) -> ig.Graph:
        g = ig.Graph(directed=True)
        vertices, edges = self.get_contained_nodes(), self.get_edges(copy=False)
        if not with_root and (len(vertices) > 0 and self.name not in vertices):
            while self.name in vertices:
                vertices.remove(self.name)
            while 'root' in vertices:
                vertices.remove('root', None)
            edges = [e for e in edges if not 'root' in e and not self.name in e]
        
        g.add_vertices(vertices)
        g.add_edges(edges)
        return g
        
    def to_hirachy(self, with_root=True):
        def helper(n):
            return {c:helper(c) for c in sorted(self.get_children(n)) if c != self.name or with_root}
        
        return {c:helper(c) for c in sorted(self.get_root_nodes()) if c != self.name or with_root}
    
    def reverse(self):
        self.edges = [(c, p) for (p, c) in self.edges]


    def get_combined_positions(self, lyout=None, invertY=True, normalize=False, view=None, ret_numpy=False, with_root=False):
        return self.get_new_positions(lyout=lyout, invertY=invertY, normalize=normalize, view=view, ret_numpy=ret_numpy, fixed_pos=self.default_positions, with_root=with_root)
    
    def get_xy(self, lyout=None, invertY=True, normalize=False, view = None):
        return self.get_new_positions(lyout=lyout, invertY=invertY, normalize=normalize, view = view, ret_numpy=True, fixed_pos=None, with_root=False)

    def get_new_positions(self, lyout=None, invertY=True, normalize=False, view = None, ret_numpy=False, fixed_pos=None, with_root=False):
        
        lyout = lyout if lyout else self.default_layout
        view = self.to_igraph() if view is None else view

        ids = list(view.vs['name'])

        if fixed_pos:
            new_vs = [nid for nid in fixed_pos if not nid in ids]
            if not with_root:
                nonos = ('root', self.name)
                new_vs = [n for n in new_vs if not n in nonos]
                fixed_pos = {k:v for k, v in fixed_pos.items() if not k in nonos}

            view.add_vertices(new_vs)
            ids = list(view.vs['name'])
        
        nr_vertices = len(view.vs['name'])
        if lyout == 'point':
            lay = [(0.,0.)] * nr_vertices
        else:
                
            try:                   
                lay = view.layout(lyout)
                if fixed_pos:
                    for k, v in fixed_pos.items():
                        lay[ids.index(k)] = np.array([v.get('x'), v.get('y')], dtype=float)
            except Exception as err:
                # log.error(f'ERROR while trying to set layout "{lyout}" falling back to "auto" (err: {err})')
                raise ValueError(f'ERROR while trying to set layout "{lyout}"')

        xy = np.array([lay[k] for k in range(nr_vertices)])
        
        log.debug(f'{nr_vertices=}, {xy.shape=}')

        if nr_vertices > 0:
            
            if invertY:
                M = np.max(xy[:,1])
                xy[:,1] = 2*M - xy[:,1]


            if normalize:
                xy[:, 0] -= np.min(xy[:,0])
                if np.max(xy[:,0]) != 0:
                    xy[:, 0] /= np.max(xy[:,0])

                xy[:, 1] -= np.min(xy[:,1])
                if np.max(xy[:,1]) != 0:
                    xy[:, 1] /= np.max(xy[:,1])

            xy[np.isnan(xy)] = 0

        if ret_numpy:
            return xy
        else:
            return {id_:dict(x=x, y=y) for id_, (x,y) in zip(ids, xy)} 
    

    def plot(self, lyout='auto', ax=None, nodes_to_highlite=None, n_annotations_max=500, *arg, **kwargs):        
        if not 'plot_pyplot' in globals():
            try:
                from ReqTracker.plotting.plt_pyplot import plot_pyplot
            except Exception as err:
                log.exception(err)
                raise
            
        return plot_pyplot(self, lyout, ax, nodes_to_highlite, n_annotations_max, *arg, **kwargs)
    

    def plot_ly(self, lyout='auto', nodes_to_highlite=None, n_annotations_max=500, figsize=1200, as_dict=False, **kwargs):
        if not 'plot_ly' in globals():
            try:
                from ReqTracker.plotting.plt_plotly import plot_ly
            except Exception as err:
                log.exception(err)
                raise
        
        r = plot_ly(self, lyout=lyout, figsize=figsize, nodes_to_highlite=nodes_to_highlite, 
             n_annotations_max=n_annotations_max, **kwargs)
        return r.to_json() if as_dict else r

##################################################################################
##################################################################################
##################################################################################
##################################################################################





"""

 ██████  ██████   █████  ██████  ██   ██ 
██       ██   ██ ██   ██ ██   ██ ██   ██ 
██   ███ ██████  ███████ ██████  ███████ 
██    ██ ██   ██ ██   ██ ██      ██   ██ 
 ██████  ██   ██ ██   ██ ██      ██   ██ 
"""                                         
                                         


class Graph():

    @staticmethod
    def deserialize(dc_saved, clean_on_load=True):
        
        nodes = dc_saved.get('nodes', None)
        views = dc_saved.get('views', None)

        assert nodes, 'this dict has no "nodes" field!'
        assert views, 'this dict has no "views" field!'

        if isinstance(nodes, dict):
            nodes = list(nodes.values())
        
        assert isinstance(nodes, list), '"nodes" must be of type list'
        assert isinstance(views, dict), '"views" must be of type dict'


        f = lambda v: v.id if hasattr(v, 'id') else v['id']
        all_nodes_dc = {f(v):v for v in nodes if v['_status'] != graph_base.STATUS.DELETED.name}
        edges = []
        for my_id, dc in all_nodes_dc.items():
            if 'parents' in dc:
                child_id = my_id
                for parent_id in dc['parents']:
                    edges.append((parent_id.strip(), child_id.strip()))
            elif 'children' in dc:
                parent_id = my_id
                for child_id in dc['children']:
                    edges.append((parent_id.strip(), child_id.strip()))

        if edges:
            if not 'imported' in views:
                views['imported'] = {'edges': edges, 'positions': None}
        if clean_on_load:
            ids = set([n.get('id') for n in nodes])
            for k, v in views.items():
                n_before = len(v['edges'])
                v['edges'] = [e for e in v['edges'] if e[0] in ids and e[1] in ids]
                n_after = len(v['edges'])
                n = n_before - n_after
                if n_before != n_after:
                    log.warning(f'Removed {n=} IDs from view["{k}"].edges which were not available graph')
                
                n_before = len(v['positions'])
                v['positions'] = {kk:vv for kk, vv in v['positions'].items() if kk in ids}
                n_after = len(v['positions'])
                n = n_before - n_after
                if n_before != n_after:
                    log.warning(f'Removed {n=} IDs from view["{k}"].positions which were not available graph')
                

        g = Graph(dict(nodes=nodes, views=views))

        return g


    def __init__(self, data=None, copy=True, data_version_id=''):
        data = dict(nodes=[], views={}) if not data else data
        self.initial_data = deepcopy(data) if copy else data
        self.nodes = NodeStore()
        self.changes = []
        self.views = {}

        self.data_version_id = data_version_id
        self.reset_to_initial() # will load self.initial_data

    @property
    def has_changes(self):
        return True if self.changes else False
    

    def serialize(self):
        return {
            'nodes': [n.to_dict() for n in self],
            'views': {k:v.to_dict() for k, v in self.views.items()}
        }
        

    def add(self, nodes, edges, view_id=None):
        name = view_id if view_id else f'imported_{helpers.nowiso()}'
        
        
        self.nodes.append_many(nodes)
        self.add_view(name, edges)
        self.set_me_as_graph()

    def load_data(self, data):
        f = lambda v: v.id if hasattr(v, 'id') else v['id']
        all_nodes_dc = {f(v):v for v in data['nodes'] if v['_status'] != graph_base.STATUS.DELETED.name}

        all_nodes = [construct(v) for k, v in all_nodes_dc.items()]
        all_nodes = [n for n in all_nodes if n is not None]
        all_nodes.sort(key=lambda x: x.id)

        self.nodes.clear()
        self.nodes.append_many(all_nodes)
        self.set_me_as_graph()

        self.views.clear()
        for name, viewdata in data['views'].items():
            self.add_view(name, viewdata)
    
    def set_me_as_graph(self, nodes=None):
        if not nodes:
            nodes = self.nodes
        elif issubclass(type(nodes), graph_base.BaseNode):
            nodes = [nodes]

        for n in nodes:
            n.graph = self
    
    def clone(self):
        o = Graph(self.serialize())
        o.changes = deepcopy(self.changes)
        o.initial_data = deepcopy(self.initial_data)
        o.data_version_id = self.data_version_id

        return o


    def clone_new_dish(self, dish_id_new, dish_id_from='##'):

        sfrom = '.D.{}'.format(dish_id_from)
        sto = '.D.{}'.format(dish_id_new)

        def replace_from_to(n):
            dc = n.to_dict()
            s = json.dumps(dc)
            sn = s.replace(sfrom, sto)
            dcn = json.loads(sn)
            return construct(dcn)


        nodes_to_copy = [n for n in self.nodes if sfrom in n.id]
        new_nodes = [replace_from_to(n) for n in nodes_to_copy]
        self.nodes.append_many(new_nodes)
        old_ids = [n.id for n in nodes_to_copy]
        new_ids = [n.id for n in new_nodes]

        new_views = []

        for k, v in self.views.items():
            if sfrom in v.name:
                # keep original one as is and clone the whole view
                new_name = v.name.replace(sfrom, sto)
                vnew = v.clone_renamed(new_name, old_ids, new_ids)
                new_views.append[vnew]
            elif [n for n in v.get_contained_nodes() if sfrom in n]:
                log.warning(f'WARNING! view with {v.name=} has the key "{sfrom}" in its nodes, but not in its name. So only partial renaming is attempted!')
                # copy the matching positions and edges
                edges_to_rn = np.array(v.get_nodes_edges(old_ids))
                for oid, nid in zip(old_ids, new_ids):
                    edges_to_rn[edges_to_rn == oid] = nid
                    if oid in v.default_positions:
                        v.default_positions[nid] = deepcopy(v.default_positions[oid])
                v.add_edges([tuple(e) for e in edges_to_rn])
                
        for v in new_views:
            self.views[v.name] = v

        self.add_changenote(f'graph.clone_new_dish({dish_id_new=}, {dish_id_from=})')

        return self
    
    def __str__(self):
        return 'Graph_{}{}, N_n={}, N_v={}'.format(id(self), '*' if self.has_changes else '', len(self.nodes), len(self.views))

    def __repr__(self):
        return str(self)

    ##################################################################################
    ##################################################################################
    # access
    ##################################################################################
    ##################################################################################

    def __len__(self):
        return len(self.nodes)
    
    def __iter__(self):
        return self.nodes.__iter__()

    def __contains__(self, key):
        id_ = getattr(key, 'id') if hasattr(key, 'id') else key
        return id_ in self.nodes
    
    def __getitem__(self, node_id) -> graph_base.BaseNode:
        if isinstance(node_id, list):
            return [self[id_] for id_ in node_id]
        if isinstance(node_id, tuple):
            return tuple([self[id_] for id_ in node_id])
        if isinstance(node_id, set):
            return set([self[id_] for id_ in node_id])
        
        node_id = getattr(node_id, 'id') if hasattr(node_id, 'id') else node_id

        try:
            return self.nodes[node_id]
        except KeyError as err:
            closest_ids = simple_search_keys([n.id for n in self.nodes], str(node_id), fuzzy=True)
            closest_ids = ' '.join(closest_ids)
            raise KeyError(f'key="{node_id}" was not found in graph. Closest ids: to input: {closest_ids}')
        
    ##################################################################################
    ##################################################################################
    # methods
    ##################################################################################
    ##################################################################################

    def get_ltc_newest_doc(self):
        if not self.nodes:
            return helpers.get_utcnow()
        m = np.array([n.get_last_version_changed().get('ltc') for n in self.nodes if hasattr(n, 'get_last_version_changed')])
        m = np.max(m[m != None])
        return m

    def get_edges(self, view_id, as_str=False, copy=True):
        edges = self.get_view(view_id).get_edges(copy=copy)
        return edges if as_str else [(self[a], self[b]) for a,b in edges]

    def set_node(self, obj):
        assert issubclass(type(obj), graph_base.BaseNode), f'need to give a object which inherits from BaseNode, but given was {type(obj)=} instead.'
        self.nodes[obj.id] = obj
        self.add_changenote(f'graph.add_node("{obj.id}", obj)')

    def add_node(self, obj):
        self.set_node(obj)

    def del_node(self, node_id):
        node_id = el2id(node_id)
        node = self.nodes.pop(node_id)
        for k, v in self.views.items():
            v.del_node(node)

        raise NotImplementedError('the method del_node is not implemented yet')
    
    def reset_to_initial(self):
        self.load_data(self.initial_data)
        self.changes.clear()

    def mark_saved(self):
        raise NotImplementedError('the method mark_saved is not implemented yet')

    def add_changenote(self, s, ts=None, *args, **kwargs):
        if isinstance(s, datetime.datetime) and ts:
            ts, s = s, ts
        if ts is None:
            ts = helpers.nowiso()
        log.debug(s)
        self.changes.append(f'[{ts}] {s}')

    def reverse(self, inplace = False):
        g = self if inplace else self.clone()
        for k, v in g.views.items():
            v.reverse()
        g.add_changenote('graph.reverse()')
        return g
    
    ##################################################################################
    ##################################################################################
    # Node methods
    ##################################################################################
    ##################################################################################

    def get_node(self, node_id):
        return self[node_id]
    
    def add_note(self, node_id, text):
        self[node_id].add_note(text)
        self.add_changenote('node[{}].add_note("{}")'.format(node_id, helpers.limit_len(text, 25)))
        
    def get_parents(self, node_id, view_id, as_id=True):
        parents = self.get_view(view_id).get_parents(el2id(node_id))
        if parents:
            return parents if as_id else self[parents]
        else:
            return []
        
    def get_children(self, node_id, view_id, as_id=True):
        children = self.get_view(view_id).get_children(el2id(node_id))
        if children:
            return children if as_id else self[children]
        else:
            return []

    def rename_node(self, node_id, new_id):
        obj = self[node_id]
        old_id = obj.id
        obj.id = new_id
        self.nodes[new_id] = obj
        self.nodes.pop(old_id)
        for v in self.views.values():
            v.rename_id(old_id, new_id)
        self.add_changenote(f'rename_node({node_id=}, {new_id=})')
        return obj
    
    ##################################################################################
    ##################################################################################
    # view methods
    ##################################################################################
    ##################################################################################

    
    def get_view(self, view_id, create_on_missing=False) -> View:
        if not view_id in self.views and create_on_missing:
            self.add_view(view_id)
        return self.views[view_id]
    
    def get_view_progress(self, view_id):
        nodes = self.get_view(view_id).get_contained_nodes()
        fun = lambda x: graph_base.weight_status(self[x].status.name)
        weights = [fun(node_id) for node_id in nodes]
        return np.mean(weights)
        
    def add_view(self, name, viewdata=None) -> View:
        viewdata = {} if viewdata is None else viewdata
        edges = viewdata if isinstance(viewdata, list) else viewdata.get('edges', [])
        positions = {} if isinstance(viewdata, list) else viewdata.get('positions', {})
        self.views[name] =  View(name, edges, positions)
        return self.get_view(name)

    def get_positions(self, view_id, lyout=None, invertY=True, normalize=False, ret_numpy=False):
        return self.get_view(view_id).get_new_positions(lyout=lyout, invertY=invertY, normalize=normalize, ret_numpy=ret_numpy)

    def get_root_nodes(self, view_id):
        return self.get_view(view_id).get_root_nodes()

    def view_merge(self, view_name='merged', exclude_all_view=True, default_layout='auto') -> View:

        positions = {}
        edges = []
        for k, v in self.views.items():
            if exclude_all_view and k == 'ALL':
                continue

            for nid in v.default_positions:
                positions[nid] = v.default_positions[nid]
            edges += v.edges
        
        edges = list(set(edges))

        return View(view_name, edges, positions, default_layout=default_layout)

    def to_view(self, name, edges):
        return View(name, edges)
    
    def view2row(self, view_id, parent_id=""):

        full_id = parent_id + "|" + view_id if parent_id else view_id
        progress = self.get_view_progress(view_id)  # Assuming get_view_progress is defined
        color = inverted_seismic_colormap(progress)  # Assuming inverted_seismic_colormap is defined

        return {
            "id": full_id,
            "color": color,
            # "is_match": False,  # Unclear purpose of this line
            "node_id": view_id,
            "text": "View Container: " + view_id,
            "tags": "",
            "n_notes": 0,
            "status": "",
            "parent": parent_id,
            "type": "ViewContainer",
        }
    
    def views2rows(self, include_all=False, parent_id='root'):
        ret = [self.view2row(parent_id)] if parent_id else []
        for view_id, _ in self.views.items():  # Iterate over keys only
            if view_id == 'ALL' and not include_all:
                continue
            ret += self.view2rows(view_id, parent_id)
        return ret
    
    def view2rows(self, view_id, parent_id='root'):
        view_row = self.view2row(view_id, parent_id)
        ret = [view_row]
        for nid in self.views[view_id].get_contained_nodes():
            ret += self.node_get_rows(nid, view_id, parent_id=view_id)
        return ret
    
    def node_get_rows(self, node_or_nid, view_id, parent_id=""):
        
        node = self[node_or_nid]
        if not node:
            print(f"WARNING: Node with ID '{node_or_nid}' not found. Skipping.")  # Using print for warnings
            return []

        my_row = self[node_or_nid].get_row(parent_id)
        full_id = my_row["id"]
        ret = [my_row]
        children = self.get_children(node, view_id)

        for child_id in children:
            child = self[child_id]
            if not child:
                log.warning(f"WARNING: Child with ID '{child_id}' not found (child of '{node.id}')")
            else:
                ret += self.node_get_rows(child, view_id, full_id)

        return ret

    def add_edge(self, view_id, edge):
        self.get_view(view_id, True).add_edge(edge)
        self.add_changenote('graph[{}].edges += ({}, {})'.format(view_id, edge[0], edge[1]))

    def add_edges(self, view_id, edges):
        self.get_view(view_id, True).add_edges(edges)
        self.add_changenote('graph[{}].edges += (N={} edges)'.format(view_id, len(edges)))
        
    def del_edge(self, view_id, edge):
        self.get_view(view_id).del_edge(edge)
        self.add_changenote('graph[{}].edges -= ({}, {})'.format(view_id, edge[0], edge[1]))
    
    def del_edges(self, view_id, edges):
        self.get_view(view_id).del_edges(edges)
        self.add_changenote('graph[{}].edges -= (N={} edges)'.format(view_id, len(edges)))
        
    def move_node(self, node_id, view_id, xy_new:dict):
        xy = self.get_view(view_id).move_node(node_id, xy_new)
        self.add_changenote('graph[{}].move_node("{}", x={}, y={})'.format(view_id, node_id, xy.get('x'), xy.get('y')))
        
    def place_node(self, node_id, view_id, xy=None, parents=None, children=None):
        self.get_view(view_id, True).add_node(node_id, xy=xy, parents=parents, children=children)
        self.add_changenote(f'graph["{view_id}"].place_node("{node_id}", {xy=}, {parents=}, {children=})')
        
    ##################################################################################
    ##################################################################################
    # search
    ##################################################################################
    ##################################################################################

    def search_fuzzy(self, searchtext:str, k_max = 3, matchcase=False):
        return self.search(searchtext, k_max=k_max, only_found=False, matchcase=matchcase)
    
    def search(self, searchtext:str, k_max = -1, only_found=True, matchcase=False, as_dict=False):
        fun = lambda el: el.to_dict(False)
        all_dcs = {el.id:fun(el) for el in self.nodes}

        matches_ids = simple_search(all_dcs, searchtext, k_max=k_max, only_found=only_found, matchcase=matchcase)
        matches_objects = [self[mid] for mid in matches_ids]
        matches = matches_objects[:]
        return matches
    

    def filtt(self, key):
        if isinstance(key, str):
            return tuple((n for n in self.nodes if n.id.startswith(key)))
        else:
            return tuple((n for n in self.nodes if isinstance(n, key)))
        
    def filti(self, key, ignore_case=True):
        if ignore_case:
            fun = lambda n: key in n.id
        elif not isinstance(key, str):
            fun = lambda n: isinstance(n, key)
        else:
            fun = lambda n: key.lower() in n.id.lower()

        return tuple((n for n in self.nodes if fun(n)))


    ##################################################################################
    ##################################################################################
    # UIs
    ##################################################################################
    ##################################################################################

    def to_plot_recs(self):
        return self.views2rows(include_all=False, parent_id='')

    def dlg_edit_nodes(self, node_ids=None, i_start = 0):
        if not 'ipy' in globals():
            from ReqTracker.core import ipy
        else:
            ipy = globals('ipy')

        return ipy.get_edit_dlg(node_ids if node_ids else self, i_start=i_start)


    def plot_sunburst(self, maxdepth=10, figsize=1200, as_dict=False, **kwargs):
        if not 'make_sunburst_plot' in globals():
            try:
                from ReqTracker.plotting.plt_dash import make_sunburst_plot
            except Exception as err:
                log.exception(err)
                raise
        fig = make_sunburst_plot(self, maxdepth=maxdepth)
        fig.update_layout(
            height=figsize,
        )
        r = fig

        return r.to_json() if as_dict else r

    def plot_treemap(self, maxdepth=10, figsize=1200, as_dict=False, **kwargs):
        if not 'make_treemap_plot' in globals():
            try:
                from ReqTracker.plotting.plt_dash import make_treemap_plot
            except Exception as err:
                log.exception(err)
                raise
        fig = make_treemap_plot(self, maxdepth=maxdepth)
        fig.update_layout(
            height=figsize,
        )
        r = fig
        return r.to_json() if as_dict else r


    def plot(self, lyout='default', ax=None, nodes_to_highlite=None, n_annotations_max=500, plot_merged=False, exclude_all_view=False, *arg, **kwargs):        
        if not 'plot_pyplot' in globals():
            from ReqTracker.plotting.plt_pyplot import plot_pyplot

        g = self.view_merge(default_layout=lyout, exclude_all_view=exclude_all_view) if plot_merged else self
        return plot_pyplot(g, lyout, ax, nodes_to_highlite, n_annotations_max, *arg, **kwargs)
    

    def plot_ly(self, lyout='auto', nodes_to_highlite=None, n_annotations_max=500, figsize=1200, as_dict=False, **kwargs):        
        g = self.view_merge(default_layout=lyout)
        return g.plot_ly(lyout, nodes_to_highlite=nodes_to_highlite, n_annotations_max=n_annotations_max, figsize=figsize, as_dict=as_dict, **kwargs)

    def to_tree_map(self, view_id=None, as_mat=True):
        if view_id:
            positions = get_xy_tree(self, view_id)
            if as_mat:
                if not 'pd' in locals() or 'pd' in globals():
                    import pandas as pd
                if not 'np' in locals() or 'np' in globals():
                    import numpy as np
                positions.pop(view_id, None)
                df = pd.DataFrame(positions).T
                df['x'] *= -1
                df['x'] -= df['x'].min()
                df['y'] -= df['y'].min()
                xmax = df['x'].max()
                ymax = df['y'].max()

                mat = np.full((ymax+1, xmax+1), '', dtype=object)
                x = df['x'].values
                y = df['y'].values
                ids = df.index.values
                mat[y, x] = ids
                return mat
            return positions
        else:
            return {k:self.get_hirachy_map(k, as_mat=True) for k in self.views if k != 'ALL'}

    def to_hirachy(self, with_root=False):
        return {k: v.to_hirachy(with_root=with_root) for k, v in self.views.items()}

if __name__ == '__main__':
        
    pth = r'C:/Users/tglaubach/Nextcloud/davmount/skampieng_db/meertest_db/req_db.json'

    with open(pth, 'r') as fp:
        dc = json.load(fp)

    g = Graph.deserialize(dc)

    print(g)