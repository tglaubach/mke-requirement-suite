import os, inspect, sys

from ReqTracker.io_files import nextcloud_interface
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(os.path.dirname(current_dir))
    
if __name__ == '__main__':
    sys.path.insert(0, parent_dir)

from ReqTracker.core import helpers, schema, graph_main, graph_base

log = helpers.log

SEPERATOR_NEXTCLOUD = '/Nextcloud/'

def test_new_doc(pth):
    link = None
    ltc = None
    fileid = None
    checksum = None

    if isinstance(pth, dict) and 'path' in pth:
        dc = pth
        link = dc.get('link')
        ltc = dc.get('ltc')
        fileid = dc.get('fileid')
        checksum = dc.get('checksum')
        pth = dc.get('path')

    for cls in [schema.Document, schema.RequestForDeviation, schema.RequestForWaiver]:
        dc = cls.fun_match(pth, ret_all_matches=False)
        if dc:
            id_ = '-'.join([dc[k] for k in cls.id_parts if dc[k]])
            return dict(id=id_, version=dc.get('version'), path=pth, cls=cls.__name__, parts=dc, link=link, ltc=ltc, fileid=fileid, checksum=checksum)

    return {}


def doc_fun_path2id(pth, ignore_if_schema_not_matches=True):
    id_ = ''
    cls = None
    for cls in [schema.Document, schema.RequestForDeviation, schema.RequestForWaiver]:
        id_ = cls.fun_path2id(pth, empty_on_no_match=ignore_if_schema_not_matches)

        if id_:
            break

    return id_, cls

def construct_doc_from_dc_data(dc_data, g:graph_main.Graph, on_exist='add_version', ignore_if_schema_not_matches=True):
    pth = dc_data['path']
    id_, cls = doc_fun_path2id(pth, ignore_if_schema_not_matches=ignore_if_schema_not_matches)
    if ignore_if_schema_not_matches and not id_:
        log.debug('pth={} is not match... skipping'.format(os.path.basename(pth)))
        return ''

    if id_ in g and on_exist == 'add_version':
        log.debug('id_={} found in sink {}'.format(id_, g))
        node = g[id_]
        assert issubclass(type(node), schema.BaseVersionedDocument), f'only document types can be constructed, but the given node {node} is of type {type(node)}'
        node.add_version(dc_data, on_exist='ignore')
        p = dc_data.get('path', '')
        folder, name = os.path.basename(os.path.dirname(p)), os.path.basename(p)
        g.add_changenote(f'graph["{id_}"].add_version("{dc_data.get("version")}") "{folder}/{name}"')

    elif id_ not in g:
        log.debug('id_={} not   in sink {} ... adding'.format(id_, g))
        node = cls.from_data(dc_data)
        g.add_node(node)
    else:
        if not on_exist == 'ignore':
            raise KeyError(f'id {id_} already exist in sink: {g}')
        
    return id_

def construct_doc(file_path, nextcloud_login_data=(), graph=None, on_exist='raise', lookups=[], schema_must_match=False):
    pth = file_path.replace('\\', '/')
    pth = pth.lstrip('"').rstrip('"')
    pth = pth.lstrip("'").rstrip('"')
    pth = pth.split(SEPERATOR_NEXTCLOUD)[-1]

    assert not pth.endswith('.lnk'), 'the given file is a link/shortcut to another file! This can not be added!'
    
    if not isinstance(lookups, (list, tuple)):
        lookups = [lookups]
    
    log.debug('Linking: ' + pth)

    id_, cls = doc_fun_path2id(pth, schema_must_match)
    if schema_must_match:
        assert id_, 'id could not be matched!'

    node = None
    if graph is not None:
        if on_exist == 'raise':
            assert id_ not in graph, 'the document with id="{}" is already within the graph'.format(id_)
        else:
            if id_ in graph:
                node = graph[id_]
    
    data = {}
    try:
        if node is not None:
            pass
        else:
            for db_source in lookups:
                if hasattr(db_source, 'get_node_as_dict'):
                    node = db_source.get_node_as_dict(id_)
                    if node:
                        node['parents'] = []
                        node = graph_main.construct(node)
                        log.debug('Found Match in File Index: ' + str(node))
                        if 'versions' in node.data:
                            data_vec = [ver for ver in node.data['versions'] if ver['path'] == file_path]
                        else:
                            data_vec = []
                        
                        data = data_vec[0] if data_vec else node.get_latest_ver()

            
    except Exception as err:
        log.error(err)

    try:
        if not data: # try get from nextcloud
            if nextcloud_login_data:
                dc_nxtcld = nextcloud_interface.get_nextcld(pth, nextcloud_login_data)
            else:
                dc_nxtcld = {'link': '', 'fileid': '', 'checksum': ''}
            data['path'] = pth
            data['link'] = dc_nxtcld['link'] if 'link' in dc_nxtcld else ''
            data['fileid'] = dc_nxtcld['fileid'] if 'fileid' in dc_nxtcld else ''
            data['checksum'] = dc_nxtcld['checksum'] if 'checksum' in dc_nxtcld else ''
    except Exception as err:
        log.error(err)

    if node is None:
        node = cls.from_data(data)
    else:
        node.add_version(data, on_exist='ignore')
        
    if graph is not None and node not in graph:
        graph.add_node(node)
        
    return node



if __name__ == "__main__":
    pass
