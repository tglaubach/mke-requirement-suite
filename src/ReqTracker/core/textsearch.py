
import json
import difflib

def simple_search_keys(keys, searchtext, k_max = 5, fuzzy=False, matchcase = False):
    if fuzzy:
        matches = difflib.get_close_matches(searchtext, keys, k_max, 0.5)
    else:
        matches = [el for el in keys if searchtext in (el.lower() if matchcase else el)]

    return matches

def simple_search(all_dcs, searchtext, k_max = -1, only_found=True, matchcase = False):
    
    def to_lists(el):
        if isinstance(el, dict):
            return [to_lists(x) for x in el.values()]
        elif isinstance(el, list):
            return [to_lists(x) for x in el]
        else:
            return el
        
    if not matchcase:
        f = lambda el: json.dumps(to_lists(el)).lower()
        searchtext = searchtext.lower()
    else:
        f = lambda el: json.dumps(to_lists(el))

    filtfun = lambda a, b: a in b
    if searchtext.startswith('~'):
        searchtext = searchtext[1:]
        filtfun = lambda a, b: a not in b
    if searchtext.startswith('!='):
        searchtext = searchtext[2:]
        filtfun = lambda a, b: a not in b


        
    all_keys = set().union(*(d.keys() for d in all_dcs.values()))

    col_match = [c for c in all_keys if searchtext.startswith(f'{c}:')]
    col_match = col_match[0] if col_match else None

    if col_match:
        all_dcs = {k:{col_match:(el[col_match] if col_match in el else '')} for k, el in all_dcs.items()}
        searchtext = searchtext[(len(col_match) + 1):]
        
    json_strs = {k:f(el) for k, el in all_dcs.items()}
    json_strs_inv = {v:k for k, v in json_strs.items()}
    if only_found:
        matches = [txt for txt in json_strs.values() if filtfun(searchtext, txt)] 
    else:
        matches = json_strs.values()
    
    if k_max <= 0:
        k_max = len(matches)
    if k_max <= 0:
        k_max = 0.01
        
    # print(searchtext, matches, k_max, 0.0)
    if matches:
        matches = difflib.get_close_matches(searchtext, matches, k_max, 0.0)
        matches_ids = [json_strs_inv[m] for m in matches]
        return matches_ids
    else:
        return []
    
        
