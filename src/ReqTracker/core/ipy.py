
import traceback
import json
import os
from base64 import b64decode

def is_notebook() -> bool:
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter



class DocumentMarkdownFormatter:

    def __init__(self, use_ipython=False) -> None:
        self.use_ipython = use_ipython

    def handle_error(self, err, el) -> list:
        txt = 'ERROR WHILE HANDLING ELEMENT:\n{}\n\n'.format(el)
        if not isinstance(err, str):
            txt += '\n'.join(traceback.format_exception(err, limit=5)) + '\n'
        else:
            txt += err + '\n'
        txt = r"""
```

<REPLACEME:VERBTEXT>

```""".replace('<REPLACEME:VERBTEXT>', txt)

        return [txt]

    def digest_tabular(self, el, hlines=0) -> list:
        # HACK: need to implement this!
        return [json.dumps(el)]
    
        
    def digest_table(self, value=None, layout='', hlines=1, caption='', **kwargs) -> list:
        # HACK: need to implement this!
        return [json.dumps(value)]
    

    def digest_markdown(self, value='', **kwargs) -> list:
        return [value]
    
    def digest_image(self, value='', width=0.8, caption='', imageblob='', **kwargs) -> list:
        if self.use_ipython:
            from IPython.display import Image            
            src = 'binary' if imageblob else 'file'
            if value and os.path.exists(value):
                im = Image(filename=value)
            elif imageblob:
                imageblob = imageblob.split(',')[-1]
                im = Image(b64decode(imageblob))
            else:
                im = ''
            return ['---', f'**Image** ({src}): `{value}` **CAPTION**: ' + caption, im, '---']
        
        else:
            src = 'binary' if imageblob else 'file'
            return [f'Image ({src}) from: `{value}` with caption "{caption}"']
    

    def digest_input(self, value='', content='', **kwargs) -> list:
        return [content]
        


    def digest_rawfile(self, value='', filecontent='', **kwargs) -> list:
        return [filecontent]


    def digest_verbatim(self, value='', **kwargs) -> list:
        txt = self.digest(value)
        template = r"""```<REPLACEME:VERBTEXT>```"""
        txt = txt.strip('\n')
        s = template.replace('<REPLACEME:VERBTEXT>', txt)
        return [s]


    def digest_iterator(self, el) -> list:
        parts = []
        if isinstance(el, dict) and el.get('type', '') == 'iter' and isinstance(el.get('value', None), list):
            el = el['value']
        
        assert isinstance(el, list)
        for p in el:
            parts += self.digest(p)
            parts.append('\n\n')

        return parts
    
    def digest_str(self, el) -> list:
        return [el]
        
    def digest(self, el) -> list:
        try:
            
            if not el:
                return ''
            elif isinstance(el, str):
                ret = self.digest_str(el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'iter':
                ret = self.digest_iterator(el)
            elif isinstance(el, list) and el and isinstance(el[0], list):
                ret = self.digest_tabular(el)
            elif isinstance(el, list) and el:
                ret = self.digest_iterator(el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'table':
                ret = self.digest_table(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'image':
                ret = self.digest_image(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'input':
                ret = self.digest_input(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'rawfile':
                ret = self.digest_rawfile(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'verbatim':
                ret = self.digest_verbatim(**el)
            elif isinstance(el, dict) and 'type' in el and el['type'] == 'markdown':
                ret = self.digest_markdown(**el)
            else:
                return self.handle_error(f'the element of type {type(el)} {el=}, could not be parsed.')
            
            return ret
        
        except Exception as err:
            return self.handle_error(err, el)


    def format(self, docdc:dict, joiner='\n\n') -> dict:
        f = lambda value: '\n\n'.join([str(s) for s in self.digest(value)])
        return {key:f(value) for key, value in docdc.items()}


    def format_s(self, docdc:dict, joiner='\n\n') -> str:
        parts = []
        for key, value in docdc.items():
            # print(f'digesting {key=} with val={type(value)} and n={len(value)} items')
            parts += self.digest(value)

        return joiner.join(parts)
    

    def show_ipy(self, docdc:dict):
        from IPython.display import display, Markdown, Latex
        parts = []
        for key, value in docdc.items():
            # print(f'digesting {key=} with val={type(value)} and n={len(value)} items')
            parts += self.digest(value)

        txt = ''
        pts = []
        # print(f'found {len(parts)=}')
        for p in parts:
            if isinstance(p, str):
                txt += '\n\n' + p
            else:
                if txt:
                    pp = Markdown(txt)
                    pts.append(pp)
                    display(pp)
                    txt = ''
                pts.append(p)
                display(p)
        if txt:
            pp = Markdown(txt)
            pts.append(pp)
            display(pp)
        # return pts



class iPythonDocumentFormatter:
    pass # TODO Implement later


def node2md(el):
    assert is_notebook(), 'can only be executed in IPython Notebook'
    from IPython.display import display, Markdown, Latex
    if not isinstance(el, str):
        el = el.to_md()
    return Markdown(el)



def get_edit_dlg(g, i_start=0):
    # if not 'widgets' in globals():
    import ipywidgets as widgets
    from ipywidgets import Layout
    import markdown
    
    assert is_notebook(), 'can only be executed in IPython Notebook'

    if hasattr(g, 'vn'):
        els = g.vn
        print('STARTING as: graph (viewnodes)...')
    elif hasattr(type(g), '_text') and hasattr(type(g), '_status'):
        els = [g]
        print('STARTING as: single_node...')
    elif hasattr(g, '__len__'):
        els = [el for el in g]
        print('STARTING as: list...')
        
    el = els[i_start]
    d = get_node_edit_dlg(el, pre='NODE: {}/{} | '.format(i_start, len(els))) 
    
    dlg = widgets.Box((d,), layout=Layout(width='100%'))
    
    dd_node = widgets.Dropdown(
        options=[n.id for n in els], 
        value=el.id,
        description = 'SEL NODE: '
    )
    def dd_node_cb(change):
        elnew = el.graph[change.new]
        d = get_node_edit_dlg(elnew, pre='NODE: {}/{}  | '.format(els.index(el), len(els)))
        dlg.children = (d,)

    dd_node.observe(dd_node_cb, names='value')
    
    node_last_b = widgets.Button(description="<- Last")
    def node_last_b_cb(b):
        dd_node.index = (dd_node.index - 1) % len(dd_node.options)
    node_last_b.on_click(node_last_b_cb)

    node_next_b = widgets.Button(description="Next ->")
    def node_next_b_cb(b):
        dd_node.index = (dd_node.index + 1) % len(dd_node.options)
    node_next_b.on_click(node_next_b_cb)

    
    rows = [
        widgets.HBox([node_last_b, dd_node, node_next_b], layout=Layout(width='100%')), 
        dlg
    ]
       
    return widgets.VBox(rows, layout=Layout(border='solid 1px', width='100%'))
    
    
def get_node_edit_dlg(el, g=None, pre=''):
    if not 'widgets' in globals():
        import ipywidgets as widgets
        from ipywidgets import Layout
        import markdown

    ttl = widgets.HTML('<h3>{}Edit: {} ({})</h3>'.format(pre, el.title, el.classname))

    STATUS = type(el._status)
    dd_status = widgets.Dropdown(
        options=[el.name for el in STATUS], 
        value=el._status.name,
        description = 'Status: '
    )

    if g is None and el.has_graph:
        g = el.graph
        
    assert g is not None, 'No graph given!'
        
    tags_possible = list(set().union(*[n.tags for n in g.vn]))

    sel_tags_l = widgets.HTML(value="Tags: ")
    sel_tags = widgets.TagsInput(
        value=el.tags,
        allowed_tags=tags_possible,
        allow_duplicates=False, 
    )
    def sel_tags_cb(change):
        el.tags = change.new
    sel_tags.observe(sel_tags_cb, names='value')
    
    text_s = widgets.HTML(markdown.markdown(el.text, extensions=['extra', 'toc']), layout=Layout(width='98%', height='200px'))
    text_e = widgets.Textarea(
        value=el.text,
        placeholder='Type something as markdown',
        description='',
        disabled=False,
        layout=Layout(width='98%', height='200px')
    )
    text_e_bs = widgets.Button(description="Apply")

    def text_e_bs_cb(b):
        el.set_text(text_e.value)
        text_s.value = markdown.markdown(el.text, extensions=['extra', 'toc'])
    text_e_bs.on_click(text_e_bs_cb)

    text_e_br = widgets.Button(description="Reset Default")
    def text_e_br_cb(b):
        text_e.value = el.text
    text_e_br.on_click(text_e_br)

    # text_lnk = widgets.jslink((text_s, 'value'), (text_e, 'value'))

    text_t = widgets.Tab()
    text_t.children = [text_s, widgets.VBox([text_e, widgets.HBox([text_e_bs, text_e_br])])]
    text_t.titles = ['Text', 'Edit Text']


    sel_parents_l = widgets.HTML(
        value='',
        placeholder='Parents: ',
        description='Parents: ',
    )
    sel_parents = widgets.TagsInput(
        allowed_tags=[n.id for n in g.vn],
        value=[e.id for e in el.parents],
        allow_duplicates=False, 
    )
    def parent_change_cb(change):
        p_old, p_new = [n.id for n in el.parents], change.new
        to_del = [p for p in p_old if p not in p_new]
        to_add = [p for p in p_new if p not in p_old]
        if to_del:
            g.del_edges([(p, el.id) for p in to_del])
        if to_add:
            g.add_edges([(p, el.id) for p in to_add])


    sel_parents.observe(parent_change_cb, names='value')


    sel_children_l = widgets.HTML(
        value='',
        placeholder='Children: ',
        description='Children: ',
    )
    sel_children = widgets.TagsInput(
        allowed_tags=[n.id for n in g.vn],
        value=[e.id for e in el.children],
        allow_duplicates=False, 
    )
    def children_change_cb(change):
        p_old, p_new = [n.id for n in el.children], change.new
        to_del = [p for p in p_old if p not in p_new]
        to_add = [p for p in p_new if p not in p_old]
        if to_del:
            g.del_edges([(el.id, p) for p in to_del])
        if to_del:
            g.add_edges([(el.id, p) for p in to_add])

    sel_children.observe(children_change_cb, names='value')
    
    md = lambda lab, s: widgets.HTML(markdown.markdown(s, extensions=['extra', 'toc']), description=lab, style={'description_width': '20%'})
    notes_l = widgets.VBox([md(k, v) for k, v in el.notes.items()])
    innote_t = widgets.Text(
            value='',
            placeholder="type a text to add as note... (enter to commit)",
            description='Add New Note:', style={'description_width': '20%'},
            disabled=False,
            continuous_update=False,
            layout=Layout(width='100%')
        ) 
    def innote_b_cb(txt):
        if txt.new:
            el.add_note(txt.new)
            notes_l.children = [md(k, v) for k, v in el.notes.items()]
            innote_t.value = ''
    innote_t.observe(innote_b_cb, 'value')
    
    # inewfile_t = widgets.Text(
    #         value='',
    #         placeholder="insert nextcloud filepath to add document as node. e.G: /Shared/.../mydoc.txt",
    #         description='Add New File:',
    #         disabled=False,
    #         layout=Layout(width='85%')
    #     )
    # inewfile_b = widgets.Button(description="Add")

    rows = [
        ttl,
        widgets.HTML('<hr>'),
        text_t,
        widgets.HTML('<hr>'),
        widgets.HBox([dd_status, sel_tags_l, sel_tags]),
        widgets.HTML('<hr>'),
        notes_l,
        innote_t,
        # widgets.HTML('<hr>'),
        # widgets.HBox([inewfile_t, inewfile_b]), 
        widgets.HTML('<hr>'),
        widgets.HBox([sel_parents_l, sel_parents]),
        widgets.HTML('<hr>'),
        widgets.HBox([sel_children_l, sel_children])
    ]
    return widgets.VBox(rows, layout=Layout(border='solid 1px', width='100%'))

