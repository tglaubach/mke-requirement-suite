import copy
import json
import os
import textwrap
import numpy as np
import sys

import datetime
import enum

from ReqTracker.core import helpers
from ReqTracker.core import ipy

limit_len = helpers.limit_len

#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################

docmaker_pipes = {}

MAXSIZE = sys.maxsize

LOG_CHANGES = True
PRINT_CHANGELOG_FALLBACK = True

log = helpers.log

class STATUS(enum.IntEnum):
    """
    Status codes for the requirement and tests
    """
    
    CORRUPTED = -5
    INCONCLUSIVE = -4
    FAIL = -3
    ISSUE = -2
    ERROR = -1
    

    DELETED = -100
    DISCONTINUED = -101
    UNCLEAR_IF_POSSIBLE = -102
    

    UNHANDLED = -10
    IN_PREPARATION = 10
    IN_CREATION = 11
    OPEN = 12

    PREPARED = 100
    READY_FOR_TESTING = 1_000
    TESTING_IN_PROGRESS = 10_000
    IN_PROCESSING = 10_001
    NEARLY_PASS = 100_001
    
    AWAITING_EXTERNAL_INPUT = 2_000
    IN_REVIEW = 2_001
    NEEDS_REVIEW = 2_002
    
    PASS = 0
    IGNORE = 3


    DONE = 14
    NONE = 13

    ACCEPTED = 100_000
    REJECTED = -100_000
    DISPUTE = -100_001
    

    @classmethod
    def _missing_(cls, value):
        return cls.NONE

status_ok = set([STATUS.PASS,STATUS.IGNORE,STATUS.DONE,STATUS.NONE,STATUS.ACCEPTED ])
status_ok = set([STATUS.PASS,STATUS.IGNORE,STATUS.DONE,STATUS.NONE,STATUS.ACCEPTED ])

def test_status_ok(status, ret_color = False):
    ret = status in status_ok
    if ret_color:
        return colors[STATUS.PASS] if ret else colors[STATUS.OPEN]



set_status_ok = [s for s in STATUS if s in [STATUS.PASS, STATUS.IGNORE, STATUS.DONE, STATUS.ACCEPTED]]
set_status_not_ok = [s for s in STATUS if s < 0]
def join_status_many(stati, ret_color=False):
    stati = [STATUS[s] if isinstance(s, str) else s for s in stati]
        
    if np.all([s in set_status_ok for s in stati]):
           return colors[STATUS.PASS] if ret_color else STATUS.PASS
    elif np.any([s in set_status_not_ok for s in stati]):
        return colors[STATUS.FAIL] if ret_color else STATUS.FAIL
    else:
        return colors[STATUS.OPEN] if ret_color else STATUS.OPEN
    

colors = {
    STATUS.FAIL: "#fc0303",
    STATUS.ISSUE: '#960f5d',
    STATUS.ERROR: '#ff03fb',
    
    STATUS.UNHANDLED: '#d17321',
    STATUS.IN_PREPARATION: '#99721f',
    STATUS.IN_CREATION: '#a67607',
    STATUS.OPEN: '#d4d4d4',

    STATUS.PREPARED: '#4b91d6',
    STATUS.READY_FOR_TESTING: '#03fcf4',
    STATUS.TESTING_IN_PROGRESS: '#fceea7',

    STATUS.IN_PROCESSING: "#0769a6",
    STATUS.NEARLY_PASS: '#FFE916',

    STATUS.AWAITING_EXTERNAL_INPUT: '#6a55df',
    STATUS.IN_REVIEW: '#5d82d1',
    STATUS.NEEDS_REVIEW: '#99721f',

    STATUS.PASS: '#07a614',
    STATUS.DONE: '#07a614',
    STATUS.NONE: '#d17321',

    STATUS.ACCEPTED: '#07a614',
    STATUS.REJECTED: '#800080',
    STATUS.DISPUTE: '#960f5d',

    STATUS.IGNORE: '#555555',
    STATUS.DELETED: '#555555',

    STATUS.DISCONTINUED: '#555555',
    STATUS.UNCLEAR_IF_POSSIBLE: '#800080',
    STATUS.INCONCLUSIVE: '#960f5d',
    STATUS.CORRUPTED: '#960f5d',
}
colorss = {k.name:v for k, v in colors.items()}
colors_inv = {v:k for k, v in colors.items()}

def join_color_many(colors, ret_color=False):
    return join_status_many([colors_inv[s] for s in colors], ret_color=ret_color)

def weight_status(s):
    s = s.name if not isinstance(s, str) and hasattr(s, 'name') else s
    
    if s in 'OPEN NONE UNHANDLED'.split():
        return 0.5 # New
    elif s in 'UNCLEAR_IF_POSSIBLE REJECTED DISPUTE DISCONTINUED IGNORE CORRUPTED INCONCLUSIVE FAIL ISSUE ERROR'.split():
        return -1 # Rejected
    elif s in 'IN_PREPARATION IN_CREATION TESTING_IN_PROGRESS IN_PROCESSING NEEDS_REVIEW'.split():
        return  0.65 #In Progress 
    elif 'PREPARED READY_FOR_TESTING AWAITING_EXTERNAL_INPUT IN_REVIEW'.split():
        return 0.85 # Feedback
    elif 'ACCEPTED PASS DONE DELETED'.split():
        return 1.0 # Closed
    else:
        raise KeyError(f'status="{s}" is unknown for mapping to redmine status')


#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################

# limit_len = lambda k: k if len(k) < 10 else k[:10]+'...'
def limit_len(k, n_max =10):
    if not isinstance(k, str):
        k = str(k)
    return k if len(k) < n_max else k[:n_max]+'...'


#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################

def log_method_calls(f):
    def inner_func(calling_obj, *args, **kwargs):        
        ret = f(calling_obj, *args, **kwargs)  

        if hasattr(calling_obj, 'add_changenote'):
            parts = []
            parts += ['None' if arg is None else str(arg) for arg in args]
            parts += ['{}={}'.format(k, 'None' if v is None else str(v)) for k, v in kwargs.items()]
            co = calling_obj.id if hasattr(calling_obj, 'id') else calling_obj
            log_str = '{}.{}({})'.format(co, f.__name__, ', '.join(parts))
            # log_str = '{}.{}({}, {})'.format(calling_obj, f.__name__, args, kwargs)
            calling_obj.add_changenote(log_str)
        
        return ret       
    
    return inner_func


def add_changenote(obj, s, ts=None):
    ts = helpers.get_utcnow() if ts is None else ts
    for fun in add_changenote.listeners:
        fun(ts, s)
    if hasattr(obj, '_changelog'):
        obj._changelog.append((ts, s))
    elif PRINT_CHANGELOG_FALLBACK:
        log.info('{} {}'.format(helpers.make_zulustr(ts, remove_ms=False), s))

add_changenote.listeners = []

class DocManager():
    def __init__(self, node) -> None:
        self.node = node
        self.doc = None
        self.odoc = None
        self.has_changes = None
    
    @property
    def key(self):
        if self.doc is None:
            return ''
        return next(iter(self.doc))

    def doc_get(self):
        doc = self.node.doc_get()
        if doc is None:
            doc = {self.node.id + '.doc': {'type': 'iter', 'value':[]}}
        return doc
    
    def get_doc(self):
        return self.doc_get()

    def doc_get_parts(self):
        return self.doc[self.key]['value']
    
    def append_part(self, part):
        return self.append_parts([part])
    
    def append_parts(self, parts):
        if parts:
            self.doc[self.key]['value'] += parts
            self.has_changes = True
        return self.has_changes
    
    def update_prop(self, key, value):
        return self.update_props({key:value})
    
    def update_props(self, **kwargs):
        def setter(target, source, has_change=False):
            assert isinstance(source, dict), f'expected dict for source, but got {type(source)=}'
            assert isinstance(target, dict), f'expected dict for target, but got {type(target)=}'
            for k in source:
                if k in target and isinstance(target[k], dict) and isinstance(source[k], dict):
                    has_change |= setter(target[k], source[k])
                elif k in target and k in source and target[k] == source[k]:
                    pass # is same
                else:
                    target[k] = source[k]
                    has_change = True
            return has_change
        
        assert not self.has_changes is None, 'must call enter() first!'
        self.has_changes = setter(self.doc[self.key], kwargs)
        return self.doc

    def __enter__(self):
        self.odoc = self.doc_get()
        self.doc = copy.deepcopy(self.odoc)
        self.has_changes = False

        return self
    
    def __exit__(self, exception_type, exception_value, exception_traceback):
        if self.has_changes:
            self.node.doc_set(self.doc)


class BaseNode(object):
    key = ''
    
    hoverdata = ['text', 'status', 'tags', 'data_keys', 'notes', 'parents', 'children' ]
    keys_no_init = 'has_graph color n_sub full_id dish_id linked_ids linked parent parents children hovertext'.split()
    props_to_log = 'text data tags notes status error'.split()
    do_serialize=True
    is_filelike=False

    def to_dict(self, no_doc=False, *args, **kwargs):

        if self.data is None:
            data = {}
        else:
            data = {k:v for k, v in self.data.items()}
            if no_doc:
                _ = data.pop('doc', None)
            
        dc = {
            'type': self.classname,
            'id': self.id,
            '_text': self._text,
            'data': data,
            '_doc': self._doc,
            'doc_src': self.doc_src,
            'tags': self.tags,
            'notes': self.notes,
            '_status': str(self._status.name),
            'error': str(self.error),
        }

        
        return dc
    
    def clone(self):
        return type(self)(dc=self.to_dict())

    def __init__(self, node_id='', text='', status=STATUS.OPEN, dc=None):
        self._logging_active = False
        assert node_id or dc, 'need to give an id or a dict for construction'

        self.id = node_id
        self._text = text

        self.graph = None
        self.is_match = False # store info for searching

        self._doc = None
        self.doc_src = None

        self.data = {}
        
        self.tags = []
        self.notes = dict()

        self._status  = status
        self.error = ''

        
        if dc is not None:
            self.set_from_dict(dc)

        self._logging_active = LOG_CHANGES

    def set_from_dict(self, dc):
        doc = None
        for k, v in dc.items():
            if k not in BaseNode.keys_no_init:
                if hasattr(self, k):
                    if k == '_status':
                        if not v:
                            v = STATUS.NONE
                        else:
                            v = STATUS[v]    
                    if k == 'text':
                        k = '_text'
                    if isinstance(v, str):
                        v = v.strip()
                    if k == 'data' and v.get('doc'):
                        doc = v.get('doc')
                        v = {kk:vv for kk, vv in v.items() if kk != 'doc'}

                    setattr(self, k, v)
        if doc:
            self.doc_set(doc)

    def __setattr__(self, name, value):
        
        
        super().__setattr__(name, value)

        if self.do_serialize:
            if not name.startswith('_') and name in self.props_to_log and hasattr(self, name) and self._logging_active:
                log_str = '{}.{}={} -> {}'.format(self, name, getattr(self, name), value)
                self.add_changenote(log_str)
            
        
    def info(self):
        return self.to_dict()
    
    def infojson(self):
        return json.dumps(self.info(), indent=2)
    

    def add_changenote(self, s, ts=None):
        if self._logging_active:
            if isinstance(s, datetime.datetime) and ts:
                ts, s = s, ts
            
            if self.has_graph:
                self.graph.add_changenote(s)
            
            # add_changenote(self, s)

    @property
    def hint(self):
        return limit_len(self.text, 500)
    
    @property
    def has_doc(self) -> bool:
        return True if self._doc or self.doc_src else False
    



    # @property
    # def doc(self) -> dict:
    #     default_ = {self.id + '.doc': {'type': 'iter', 'value':[]}}
    #     doc = self.data.get('doc', {})
    #     # clean up any non dict document styles (legacy behaviour!)
    #     while isinstance(doc, list) and doc and isinstance(doc[0], list):
    #         doc = doc[0]
    #     if isinstance(doc, list):
    #         doc = {self.id + '.doc': doc}
    #     if not doc:
    #         doc = default_
    #     return doc

    @property
    def title(self):
        return limit_len(self.text.split('\n')[0], 50)
    
    @property
    def text(self):
        return self._text
    
    @property
    def status(self):
        if self.error:
            return STATUS.ERROR
        
        return self._status
    
    
    @status.setter
    def status(self, value):
        self._status = value

    @property
    def has_graph(self):
        return self.graph is not None
    
    def raise_on_not_has_graph(self, g=None):
        if not self.has_graph and g is None:
            raise ValueError(self.id + ' | Error: whatever action was called, it can not be done, since this node is not connected to a graph')
    
    def test_same_graph(self, other):
        return self.graph == other.graph

    def raise_on_not_same_graph(self, other):
        if not self.test_same_graph(other):
            raise ValueError(self.id + ' | ' + other.id + ' | Error: whatever action was called, it can not be done, since the two nodes given do not share the same graph!')
    

    def __hash__(self):
        return hash(self.id)
    
    def __contains__(self, b):
        b_id = b.id if not isinstance(b, str) else b
        return b_id in self.linked_ids
    
    @property
    def classname(self):
        return self.__class__.__name__
    
    @property
    def color(self):
        return colors[self.status] # if self.status is not None else colors[STATUS.UNHANDLED]
    
    @property
    def dish_id(self):
        return ''
    
    @property
    def tickets(self):
        if 'tickets' in self.data:
            return [tuple(x) for x in self.data['tickets']]
        else:
            return []

    @property
    def tickets_md(self):
        return ['[{}]({})'.format(k, lnk) for k, lnk in self.tickets]

    @property
    def tagss(self):
        return ' '.join(self.tags)
#####################################################################
#####################################################################

        
    def set_text(self, new_text):
        if self.text != new_text:
            log_str = '{}.{}={} -> {}'.format(self, 'text', getattr(self, 'text'), new_text)
            self._text = new_text
            self.add_changenote(log_str)
            return True
        else:
            return False
        
    # stupid workaround because otherwise logging does not seem to work...
    def set_status(self, new_status):
        if isinstance(new_status, (str)):
            new_status = STATUS[new_status]
        
        assert isinstance(new_status, STATUS), 'must give a string int or STATUS object to set the status, but given was {}={}'.format(type(new_status), new_status)
        
        if self._status != new_status:
            log_str = '{}.{}={} -> {}'.format(self, 'status', getattr(self, '_status').name, new_status.name)
            self._status = new_status
            self.add_changenote(log_str)
            return True
        else:
            return False
    
    @log_method_calls
    def set_doc_src(self, new_doc_src):
        assert isinstance(new_doc_src, str), 'new doc_str is not of string type!'
        self.add_changenote(f'doc_src changed from {self.doc_src=}  to {new_doc_src=}')
        self.doc_src = new_doc_src
        


    def doc_append_part(self, part):
        with self.doc_open() as doc:
            r = doc.append_part(part) if not (isinstance(part, list)) else doc.append_parts(part)
            self.add_changenote('appended elements to doc')
            return r

    def doc_append_parts(self, parts):
        with self.doc_open() as doc:
            r = doc.append_parts(parts)
            self.add_changenote('appended elements to doc')
            return r



    def doc_get(self, **kwargs):
        if self.doc_src:
            src = self.doc_src
            if '/projects/' in src and '/wiki/' in src and src.startswith('http'):
                server, s = src.split('/projects/')

                if not 'get_doc_from_wikipage' in locals():
                    from ReqTracker.io_files.redmine_api import get_doc_from_wikipage
                
                project_id, page_name = s.split('/projects/')[-1].split('/wiki/')

                try:
                    doc = get_doc_from_wikipage(page_name, project_id, **kwargs)
                except Exception as err:
                    log.exception(err)
                    doc = None
                return self._doc if not doc else doc
            if '/api/v1/get_doc' in src:
                raise NotImplementedError('need to imoplement this!')
            elif os.path.exists(src):
                with open(src, 'r') as fp:
                    return fp.read()
        elif isinstance(self._doc, list) and not self.doc_src:
            return {self.id + '.doc': doc}
        elif not self._doc and not self.doc_src:
            return {self.id + '.doc': {'type': 'iter', 'value':[]}}
        elif self._doc:
            return self._doc
        else:
            raise ValueError(f'{type(self._doc)=} must be either dict or list')
        
    def get_doc(self, **kwargs):
        """synonym for doc_get"""
        return self.doc_get(**kwargs)

    def doc_get_parts(self):
        with self.doc_open() as doc:
            return doc.doc_get_parts()
        
    def get_doc_parts(self):
        """synonym for doc_get_parts"""
        return self.doc_get_parts()
    
    def doc_set_value(self, docval):
        with self.doc_open() as doc:
            return doc.update_props(value=docval)
        
    def doc_update_parts(self, docval):
        return self.doc_set_value(docval)
        
    def doc_set(self, doc:dict):

        if self.doc_src:
            from ReqTracker.io_files.redmine_api import get_doc_from_wikipage, getRedmine, doc2wikipage, get_page_title

            log.info(f'uploading doc to {self.doc_src=}')
            src = self.doc_src

            page_name = get_page_title(doc)    
            docname = page_name.replace('Wikidoc_', '')

            files_to_upload = {}
            if self.graph:
                for ext, pipe in docmaker_pipes.items():
                    try:
                        filename, content = pipe(docname, self.graph, self)
                        files_to_upload[filename] = content
                    except Exception as err:
                        log.exception(err)  
                        raise

            if '/projects/' in src and '/wiki/' in src and src.startswith('http'):
                
                server, s = src.split('/projects/')
                project_id, page_name = s.split('/projects/')[-1].split('/wiki/')
                try:
                    doc = doc2wikipage(doc, project_id, page_name, files_to_upload=files_to_upload)
                except Exception as err:
                    log.exception(err)
                    doc = None

            elif os.path.exists(src):
                
                with open(src, 'w') as fp:
                    json.dump(doc, fp, indent=2)

                d = os.path.dirname(src)
                for fn, content in files_to_upload.items():
                    with open(os.path.join(d, fn), 'w') as fp:
                        fp.write(content)

        else:
            log.info(f'saving doc in object')
            # BaseNode will store docment directly as dict
            self._doc = doc

        self.add_changenote('doc_set called!')
        return True
    
    def doc_del(self):
        if self.doc_src:
            src = self.doc_src
            if '/projects/' in src and '/wiki/' in src and src.startswith('http'):
                from ReqTracker.io_files.redmine_api import get_doc_from_wikipage, getRedmine, doc2wikipage, del_wikipage
                server, s = src.split('/projects/')
                project_id, page_name = s.split('/projects/')[-1].split('/wiki/')

                try:
                    return del_wikipage(project_id, page_name)
                except Exception as err:
                    log.exception(err)
                    return False
            if '/api/v1/get_doc' in src:
                raise NotImplementedError('need to implement this!')
            elif os.path.exists(src):
                os.remove(src)
                return True
        else:
            # BaseNode will store docment directly as dict
            self._doc = None
        return True

    def doc_open(self):
        return DocManager(self)
    
    @log_method_calls
    def add_note(self, note_text, timestamp=None):
        if timestamp is None:
            timestamp = helpers.get_utcnow()
        elif isinstance(timestamp, (int, float)):
            timestamp = datetime.datetime.utcfromtimestamp(timestamp)
            timestamp = timestamp.replace(tzinfo=datetime.timezone.utc)

        assert isinstance(timestamp, datetime.datetime), "timestamp must be datetime object with timezone or unix timestamp (in UTC)!"
        key = helpers.make_zulustr(timestamp, remove_ms=False)
        while key in self.notes:
            timestamp -= datetime.timedelta(microseconds=1)
            key = helpers.make_zulustr(timestamp, remove_ms=False)
            
        self.notes[key] = note_text

        return (key, note_text)
    
    @log_method_calls
    def set_data(self, new_data):
        assert isinstance(new_data, dict), 'new data is not of dict type!'
        self.dat = new_data 
        

    @log_method_calls
    def add_data(self, data_key, data_value):
        self.data[data_key] = data_value

    @log_method_calls
    def add_ticket(self, ticket_id, ticket_link):
        if not 'tickets' in self.data:
            self.data['tickets'] = []
        self.data['tickets'].append(ticket_id, ticket_link)

    @log_method_calls
    def del_ticket(self, i):
        if not 'tickets' in self.data:
            self.data['tickets'] = []
        return self.data['tickets'].pop(i)



    #####################################################################
    #####################################################################
    # methods
    #####################################################################
    #####################################################################


    def get_row(self, parent_id='', color_col='color'):

        if parent_id:
            full_id = parent_id + '|' + self.id
        else:
            full_id = self.id

        row = {
            'id': full_id, 
            'color': getattr(self, color_col) if hasattr(self, color_col) else '', 
            'is_match': self.is_match,
            'node_id': self.id, 
            'text': limit_len(self.text, 120),
            'tags': ' | '.join(self.tags),
            'data-keys': ' | '.join([limit_len(k) for k in self.data.keys()]),
            'n-notes': len(self.notes),
            'status': str(self.status.name),
            # 'parents': limit_len(' | '.join([e.id for e in n.parents]), 100),
            # 'children': limit_len(' | '.join([e.id for e in n.children]), 100),
            'parent': parent_id,
            'type': self.key if hasattr(self, 'key') else 'root'
        }

        return row



#####################################################################
#####################################################################

    
    def __repr__(self):
         return str(self.id)
    
    def __str__(self) -> str:
        s = self.id
        if self.text: 
            s += ' | ' + helpers.limit_len(self.text.split('\n')[0], 35)
        return s
    
    def as_str(self, n=100) -> str:
        s = self.id
        if self.text: 
            s += ' | ' + helpers.limit_len(self.text, n)
        return s
    
    # def pprint(self, ret=False, offst='', graph=None):
    #     graph = self.graph if graph is None else graph
    #     lines = [offst + str(self)] 
    #     offst += '.'
    #     for c in graph.get_children(self):
    #         lines += c.pprint(ret=True, offst=offst)
    #     if ret:
    #         return lines
    #     else:
    #         print('\n'.join(lines))

    # def nng(self, nn_max=30):
    #     """get nearest neighbors as graph

    #     Args:
    #         nn_max (int, optional): number of neighbors to get. Defaults to 30.

    #     Returns:
    #         graph_main.GraphView: graph with nn's
    #     """
    #     return self.graph.filt_by_nn(self, str(self) + f'|nn{nn_max}', n_max=nn_max)



    # def dlg_edit(self, nn_max=10):
    #     self.raise_on_not_has_graph()
    #     nns = self.get_nn(nn_max)
    #     i = nns.index(self)
    #     return self.graph.dlg_edit_nodes(nns, i)

    def to_md(self, with_doc=True):
        self.title
        txt = ''
        txt += f'## **{self.id}**: {self.title}\n\n'
        
        txt += f'\n**STATUS**: {self.status.name}\n\n'
        txt += '\n**TAGS**: ' + ', '.join(self.tags) + '\n\n'

        txt += '\n---\n'
        txt += '\n**TEXT**: ' + ', '.join(self.tags) + '\n\n'
        txt += self.text
        txt += '\n\n---\n'

        notes_md = ['**{}**:\n{}'.format(k, v) for k, v in self.notes.items()]
        txt += '\n**NOTES**:'
        txt += '\n- '.join([n for n in notes_md])
        txt += '\n'
        txt += '\n**TICKETS**:'
        txt += '\n- '.join([n for n in self.tickets_md])
        txt += '\n'

        txt += '\n---\n'

        if with_doc:
            
            txt += '\n---\n'
            txt += '\n### DOC:\n\n'
            frmatter = ipy.DocumentMarkdownFormatter(use_ipython=False)
            if self.has_doc:
                doc = self.get_doc()
            else:
                doc = {self.id + '.doc': {'type': 'iter', 'value':[]}}
            txt += frmatter.format_s(doc)
        return txt
    

    def show(self, render_images=True):
        if ipy.is_notebook():
            if render_images:
                txt = self.to_md(with_doc=False)
                txt += '\n---\n'
                txt += '\n**DOC**:\n\n'
                frmatter = ipy.DocumentMarkdownFormatter(use_ipython=True)
                if self.has_doc:
                    doc = self.get_doc()
                else:
                    doc = {self.id + '.doc': {'type': 'iter', 'value':[]}}

                doc[self.id + '.doc']['value'].insert(0, txt)
                return frmatter.show_ipy(doc)
            else:
                return ipy.node2md(self.to_md(with_doc=True))
        else:
            self.pprint()
            


    @property
    def hovertext(self):
        lines = self.text.split('\n')
        header = '<b>{} </b>: {}'.format(self.id, lines[0])
        if hasattr(self, 'status'):
            status = 'STATUS: ' + str(self.status.name)
        elif hasattr(self, '_status'):
            status = 'STATUS: ' + str(self._status.name)
        else:
            status = ''
        tags = 'TAGS: ' + ' | '.join(self.tags)
        dkeys = [limit_len(k) for k in self.data.keys()]
        nres = f'DATA-KEYS: {dkeys}' 
        lnew = [header, '<br>', status, tags, nres, '<br>' ]
        for l in lines[1:]:
            lnew += textwrap.wrap(l)
        return '<br>'.join(lnew)
    

    def get_children(self):
        els = []
        for k, v in self.graph.views.items():
            if self.id in v:
                els += v.get_children(self.id)
        return list(set(els))

    def get_parents(self):
        els = []
        for k, v in self.graph.views.items():
            if self.id in v:
                els += v.get_parents(self.id)
        return list(set(els))



    def get_upper(self, as_edges=False, maxlevel=-1):
        return self.get_ancestors(as_edges=as_edges, maxlevel=maxlevel)

    def get_ancestors(self, as_edges=False, maxlevel=-1):
        els = self.get_parents()
        all_ancestors = []

        
        if maxlevel != 0:
            maxlevel -= 1
            for e in els:
                el = self.graph[e] 
                n = [(el.id, self.id)] if as_edges else [el]
                all_ancestors += n + el.get_ancestors(as_edges=as_edges, maxlevel=maxlevel)
            
        return all_ancestors
    
    def get_lower(self, as_edges=False, maxlevel=-1):
        return self.get_offsprings(as_edges=as_edges, maxlevel=maxlevel)

    def get_offsprings(self, as_edges=False, maxlevel=-1):
        els = self.get_children()
        all_offsprings = []

        
        if maxlevel != 0:
            maxlevel -= 1
            for e in els: 
                el = self.graph[e]
                n = [(self.id, el.id)] if as_edges else [el]
                all_offsprings += n + el.get_offsprings(as_edges=as_edges, maxlevel=maxlevel)
        return all_offsprings

    def get_down_tree(self, maxlevel=-1):
        els = self.get_children()
        dc = {}
        if maxlevel != 0:
            maxlevel -= 1
            dc = {n:n.get_down_tree(maxlevel=maxlevel) for n in els}

        return dc

    @property
    def children(self):
        return tuple([self.graph[e] for e in self.get_children() if e in self.graph])
    

    @property
    def parents(self):
        return tuple([self.graph[e] for e in self.get_parents() if e in self.graph])
    
