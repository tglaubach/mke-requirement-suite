import os, json


from ReqTracker.core import helpers
from ReqTracker.io_files import nextcloud_interface, redmine_api, langchain_nextcloud_io


log = helpers.log
from sys import platform


""" 
 █████  ██    ██ ████████ ██   ██ 
██   ██ ██    ██    ██    ██   ██ 
███████ ██    ██    ██    ███████ 
██   ██ ██    ██    ██    ██   ██ 
██   ██  ██████     ██    ██   ██ 
    """                              


if platform == "linux" or platform == "linux2":
    NC_INFO_PATH = '/home/jovyan/db/info.hash'
    RM_INFO_PATH = '/home/jovyan/db/info.rm'
    LOGIN_SOURCE = '/tmp/reqtrack/logins_auth.json'
else:
    # pdir = r'c:\Users\tglaubach\repos\mke-requirement-suite'
    pdir = r'C:\Users\tobia\source\repos\mke-requirement-suite'
    NC_INFO_PATH = pdir + r'\scripts\info.hash'
    RM_INFO_PATH = pdir + r'\scripts\info.rm'
    LOGIN_SOURCE = pdir + r'\scripts\logins_auth.json'

nc_info = ('', '')
ncfs = None


def _get_nc_info(pth):
    from cryptography.fernet import Fernet

    with open(pth, 'r') as fp:
        k, a, b = fp.readlines()
        FERNET = Fernet(bytes(k, 'ASCII'))
        a = FERNET.decrypt(a).decode()
        b = FERNET.decrypt(b).decode()
    return (a,b)

def _get_rm_info(pth):
    with open(pth, 'r') as fp:
        key = fp.read().strip()

    return key




def setup(config):

    try:
        nc = config['sources'].get('nextcloud')
        url = nc.get('url')
        login_data = _get_nc_info(nc.get('login'))
        nextcloud_interface.setup(url, login_data)
        langchain_nextcloud_io.setup(url, login_data)
        log.info('Sucessfully connected to Nextcloud')
    except Exception as err:
        log.error(err)


    try:
        rm = config['sources'].get('redmine')
        api_key = _get_rm_info(rm.get('login'))
        redmine_api.setup(rm.get('url'), api_key)

        log.info('Sucessfully connected to redmine')
    except Exception as err:
        log.error(err)

    
def login_user(username, pw):
    try:
        with open(LOGIN_SOURCE, 'r') as fp:
            USERNAME_PW_PAIRS = json.load(fp)

        if not username in USERNAME_PW_PAIRS:
            return False
        if not USERNAME_PW_PAIRS[username] == pw:
            return False
        
        return True 
    except Exception as err:
        log.info('could not find any username info... keeping the app open')
        log.exception(err, exc_info=True)
        return False 


