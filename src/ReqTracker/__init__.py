__version__ = '2.1.5'

from ReqTracker.core.graph_main import Graph, View

from ReqTracker.core.graph_base import BaseNode, STATUS, colors, colors_inv

from ReqTracker.core.helpers import log

from ReqTracker.core import schema, helpers


def load(path):
    import json
    with open(path, 'r', encoding='utf-8') as fp:
        dc = json.load(fp)
    return Graph.deserialize(dc)
