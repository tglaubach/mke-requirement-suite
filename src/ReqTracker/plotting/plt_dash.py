import numpy as np
import pandas as pd
import textwrap


# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

from ReqTracker.core import helpers, graph_main, graph_base, schema



COLORS = {k.name:v for k,v in graph_base.colors.items()}

def make_sunburst_plot(graph, maxdepth=10):
    
    recs = graph.to_plot_recs()
    data = pd.DataFrame(recs)

    # print(data['color'])

    hoverdata = None
    if recs:
        hoverdata = graph_base.BaseNode.hoverdata
        hoverdata = {c:c in hoverdata for c in data.columns}

    fig = px.sunburst(
        data,
        names='node_id',
        parents='parent',
        # values='perc',
        hover_data=hoverdata,
        color='color',
        # hover_template=hovertemplate,
        hover_name='node_id',
        ids='id',
        maxdepth=maxdepth,
        color_discrete_map=COLORS
    )


    return fig


def make_treemap_plot(graph, maxdepth=10):
    
    recs = graph.to_plot_recs()
    data = pd.DataFrame(recs)

    # print(data['color'])

    hoverdata = None
    if recs:
        hoverdata = graph_base.BaseNode.hoverdata
        hoverdata = {c:c in hoverdata for c in data.columns}
    
    fig = px.treemap(
        data,
        names='node_id',
        parents='parent',
        # values='perc',
        color='color',
        hover_data=hoverdata,
        # hover_template=hovertemplate,
        hover_name='node_id',
        ids='id',
        maxdepth=maxdepth,
        color_discrete_map=COLORS,
        
    )


    return fig


