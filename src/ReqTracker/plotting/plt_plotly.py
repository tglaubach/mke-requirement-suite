
import numpy as np
import pandas as pd
import textwrap


# from dash import Dash, dcc, html, Input, Output, State, callback, callback_context, ALL, MATCH, ctx, no_update
# from dash.exceptions import PreventUpdate
# import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go


from ReqTracker.core import helpers, graph_main, graph_base, schema


log = helpers.log

# https://igraph.org/python/doc/api/igraph.Graph.html#layout

LAYOUTS_POSSIBLE = """auto
circle
davidson_harel
drl
fr
grid
graphopt
kk
large
mds
random
tree
rt_circular
star
sugiyama
point""".split('\n')



SIZEFUNS_POSSIBLE = """pagerank
degree
indegree
outdegree
eccentricity
log(pagerank)
log(degree)
log(indegree)
log(outdegree)
log(eccentricity)""".split('\n')

SIZEFUNS_ALLOWED = SIZEFUNS_POSSIBLE + ['25', '15', '5']

ALL_SYMBOLS = ['circle', 'square', 'diamond', 'cross', 'x', 'pentagon', 'hexagon', 'star-triangle-up', 'star-square', 'square-cross', 'diamond-wide', 'star-diamond', 'hourglass', 'diamond-tall',  'bowtie', 'circle-cross', 'square-x', 'diamond-cross', 'diamond-x', 'cross-thin', 'x-thin', 'asterisk', 'hash', 'y-up', 'y-down', 'y-left', 'y-right', 'line-ew', 'line-ns', 'line-ne', 'line-nw', 'arrow-up', 'arrow-down', 'arrow-left', 'arrow-right', 'arrow-bar-up', 'arrow-bar-down', 'arrow-bar-left', 'arrow-bar-right']
CLASSSYMBOLS = {c.__name__:ALL_SYMBOLS[i] for i, c in enumerate(graph_main.classes)}
CLASSSYMBOLS[schema.RequirementContainer.__name__] = 'circle'
CLASSSYMBOLS[graph_main.ViewContainer.__name__] = 'circle-x'
                
MAX_SIZE = 40
# apply_log = True


def plot_ly(g, lyout='auto', nodes_to_highlite=None, n_annotations_max=500, figsize=1200, *arg, **kwargs):

    view = g.to_igraph()
    nodes = view.vs['name']
    ei = view.get_edgelist()

    if not lyout or lyout == 'default':
        # pos = g.positions
        # xy = [tuple(pos[nid].values()) for nid in nodes]
        xy = g.get_combined_positions('auto', normalize=True, view=view, ret_numpy=True)
    else:
        xy = g.get_new_positions(lyout, normalize=True, view=view, ret_numpy=True)
    
    if lyout == 'tree':
        xy = xy[:, ::-1]

    return plot_ly_sub(nodes, ei, xy, nodes_to_highlite=nodes_to_highlite, n_annotations_max=n_annotations_max, figsize=figsize, *args, **kwargs)

def plot_ly_sub(nodes, ei, xy, nodes_to_highlite=None, n_annotations_max=500, figsize=1200, *arg, **kwargs):

    fig = go.Figure()
    n_annotations_max = float('inf') if n_annotations_max < 0 else n_annotations_max
    nodes_to_highlite = [] if nodes_to_highlite is None else nodes_to_highlite

    size = max(25.0 - len(nodes)/100.0, 10.0)
    textfont_size = max(15.0 - len(nodes)/10.0, 8.0)

    nan = float("nan")
    # no arrows
    x, y = [], []
    for a, b in ei:
        p0 = xy[a,:]
        p1 = xy[b,:]
        x += [p0[0], p1[0], nan]
        y += [p0[1], p1[1], nan]


    if len(ei) > n_annotations_max:
        ids = None
        textfont = None
        symbols = None
        annotations = None       

    else:
        ids = [n.id for n in nodes]
        textfont = {"color":['#0437F2' if n.id in nodes_to_highlite else '#000000' for n in nodes]}
        symbols = [CLASSSYMBOLS[n.classname] for n in nodes]
        annotations = None

    if len(ei) < 50:
        annotations = [ab2arrow(a, b, xy) for a, b in ei]
        fig.update_layout(annotations=annotations)
    
    else:
        fig.add_trace(go.Scatter(x=x,
                y=y,
                mode='lines',
                line=dict(color='rgb(80,80,80)', width=0.5),
                hoverinfo='none',
                ))
        

    fig.add_trace(go.Scatter(x=xy[:,0],
            y=xy[:,1],
            mode='markers+text',
            textposition='top right',
            marker=dict(symbol=symbols,
                            line=dict(color='rgb(50,50,50)', width=1),
                            size=size
                            ),
            text=ids,
            textfont=textfont,

            # hoverinfo='hovertext',
            hovertext=[str(n) for n in nodes],
            ids=ids,
            # opacity=0.8
            ))

    fig.update_traces(textfont_size=textfont_size)
    fig.update_layout(height=figsize)

    return fig


def ab2arrow(a, b, xy):
    (x0,y0), (x1,y1) = xy[a,:], xy[b,:]
    arrow = go.layout.Annotation(dict(
                    x=x0,
                    y=y0,
                    xref="x", yref="y",
                    text="",
                    showarrow=True,
                    axref="x", ayref='y',
                    ax=x1,
                    ay=y1,
                    arrowhead=5,
                    arrowwidth=2.0,
                    arrowcolor='rgb(80,80,80)')
                )
    return arrow







