import matplotlib.pyplot as plt
import numpy as np



def plot_pyplot(g, lyout='auto', ax=None, nodes_to_highlite=None, n_annotations_max=500, exclude_all_view=False, *arg, **kwargs):
    if hasattr(g, 'views'):
        minus = 1 if 'ALL' in g.views and exclude_all_view else 0
        f, axs = plt.subplots(len(g.views)-minus, 1, figsize=(16, len(g.views)*8))
        if not hasattr(axs, '__len__'):
            axs = [axs]
        for ax, (k, v) in zip(axs, g.views.items()):
            plot_pyplot(v, lyout=lyout, ax=ax, nodes_to_highlite=nodes_to_highlite, n_annotations_max=n_annotations_max, *arg, **kwargs)
            ax.set_ylabel(k)
        return axs

    view = g.to_igraph()
    nodes = view.vs['name']
    ei = view.get_edgelist()

    if not lyout or lyout == 'default':
        # xy = [tuple(pos[nid].values()) for nid in nodes]
        xy = g.get_combined_positions('auto', normalize=True, view=view, ret_numpy=True)
    else:
        xy = g.get_new_positions(lyout, normalize=True, view=view, ret_numpy=True)
    
    if lyout == 'tree':
        xy = xy[:, ::-1]

    return plot_pyplot_sub(nodes, ei, xy, ax=ax, nodes_to_highlite=nodes_to_highlite, n_annotations_max=n_annotations_max, *arg, **kwargs)

def plot_pyplot_sub(nodes, ei, xy, ax=None, nodes_to_highlite=None, n_annotations_max=500, *arg, **kwargs):

    if ax is None:
        f, ax = plt.subplots(*arg, **kwargs)
    
    nan = float("nan")
    if n_annotations_max > 0 and len(nodes) > n_annotations_max:
        if len(ei) > n_annotations_max:
            # no arrows
            x, y = [], []
            for a, b in ei:
                p0 = xy[a,:]
                p1 = xy[b,:]
                x += [p0[0], p1[0], nan]
                y += [p0[1], p1[1], nan]
            ax.plot(x, y, color='k', alpha=0.2)
        ax.scatter(xy[:,0], xy[:,1], marker='o')

    else:

        ax.scatter(xy[:,0], xy[:,1], s = 60, marker='o', edgecolor='black')
        for i, txt in enumerate(nodes):
            if nodes_to_highlite and txt in nodes_to_highlite:
                col = 'blue'
            else:
                col = None
            ax.annotate(txt, (xy[i,0], xy[i,1]), color=col)
        
        # with arrows
        for a, b in ei:
            p0 = xy[a,:]
            p1 = xy[b,:]
            ax.annotate("", xy=p0, xytext=p1,arrowprops=dict(arrowstyle="->", alpha=0.5))
    
    return ax