
import datetime
import time, os, json, yaml, io

import numpy as np

from ReqTracker.core import graph_main, helpers, node_factory, schema
from ReqTracker import login
from ReqTracker.io_files import redmine_api, nextcloud_interface, langchain_nextcloud_io, local_json_file_api

log = helpers.log



def load_data(config, key='project_file', with_data_version_id=False):
    p = config[key].get('url')
    log.debug(p)

    if local_json_file_api.LocalFile.test_valid(p):
        api = local_json_file_api.LocalFile(p)

        log.info('Loading from ' + api.info())
        if with_data_version_id:
            return api.get_content(), api.get_data_version_id()
        else:
            return api.get_content()
        
    for k, v in config['sources'].items():
        log.debug((k, v))

        if v.get('url') in p:
            if v.get('type') == 'redmine_dmsf':
                fid = int(p.split('/')[-1])
                api = redmine_api.RedmineDmsfFile(fid)
                log.info('Loading from ' + api.info())
                if with_data_version_id:
                    return api.get_content(), api.get_data_version_id()
                else:
                    return api.get_content()
                
            elif v.get('type') == 'nextcloud':
                raise NotImplementedError('Nextcloud files are currently not supported!')
            else:
                raise ValueError(f'the given type source type: {v} is not supported!')
    url = locals().get('v', {}).get('url')
    raise ValueError(f'the given source for "{url}" in "{p=}" was not found!')


def save_data(config, content, comment='', description='', key='project_file', data_version_id = None):
    p = config[key].get('url')
    log.debug(p)
    comment = comment if comment else ''
    description = description if description else ''
    api = None

    if local_json_file_api.LocalFile.test_valid(p):
        api = local_json_file_api.LocalFile(p)
        
    for k, v in config['sources'].items():
        log.debug((k, v))
        if v.get('url') in p:
            if v.get('type') == 'redmine_dmsf':
                fid = int(p.split('/')[-1])
                api = redmine_api.RedmineDmsfFile(fid)
                break
            elif v.get('type') == 'nextcloud':
                return {'success': False, 'info': 'Nextcloud files are currently not supported!', 'sink': p, 'response': {}, 'version': '', 'data_version_id': data_version_id}
            else:
                return {'success': False, 'info': f'the given type source type: {v} is not supported!', 'sink': p, 'response': {}, 'version': '', 'data_version_id': data_version_id}
    
    if api:
        log.info('Saving: saving to ' + api.info())
        if data_version_id:
            log.info('Saving: testing for remote changes...')
            can_upload = data_version_id == api.get_data_version_id()
            if not can_upload:
                return {'success': False, 'info': f'The data source has changed since it was pulled. Writing this data might overwrite changes. If this is intended, please set data_version_id to null or empty', 'sink': p, 'response': {}, 'version': '', 'data_version_id': data_version_id}
            log.info('Saving: testing for remote changes... OK')

        log.info('Saving: uploading... ')
        api.upload(content, comment, description)
        log.info('Saving: uploading... DONE')
        log.info('Saving: getting new file info... ')
        meta = api.get_meta()
        new_data_version_id = api.get_data_version_id(meta)
        log.info('Saving ... done ')
        return {'success': True, 'info': 'successfully wrote the content to file.', 'sink': p, 'response': meta, 'version': meta.get('version'), 'data_version_id': new_data_version_id}
    else:
        i = f'the given source for "{p}" was not found!'
        log.error(f'Saving ... error {i} ')
        return {'success': False, 'info': i, 'sink': p, 'response': {}, 'version': '', 'data_version_id': data_version_id}


def get_new_layout(config, edges, nodes, lyout='tree'):
    positions = {nid:dict(x=0, y=0) for nid in set(nodes)}
    v = graph_main.View('dummyname', edges, positions, default_layout=lyout)
    data = v.get_new_positions(lyout=lyout, normalize=True, invertY=True, ret_numpy=False)
    return {'success': True, 'info': f'Successfully generated new layout', 'lyout':lyout, 'data':data}




def get_graph(config) -> graph_main.Graph:
    dc, data_version_id = load_data(config, with_data_version_id=True)
    return graph_main.Graph(dc, copy=False, data_version_id=data_version_id)
    

class GetGraphHold():
    def __init__(self, config, force_overwrite=False):
        self.config = config
        self.obj = None
        self.force_overwrite = force_overwrite

        log.debug(f'INIT DONE GetGraphHold')

    def __enter__(self) -> graph_main.Graph: 
        dc, data_version_id = load_data(self.config, with_data_version_id=True)
        self.obj = graph_main.Graph(dc, copy=False, data_version_id=data_version_id)
        return self.obj
    
    def __exit__(self, exception_type, exception_value, exception_traceback):
        log.debug('EXITING "GetGraphHold" for object "{}"'.format(self.obj))
        
        comments = 'CHANGES: ' + ', '.join(self.obj.changes)

        if self.obj.has_changes:
            save_data(self.config, self.obj.serialize(), comment=comments, data_version_id=None if self.force_overwrite else self.obj.data_version_id)
            log.info(f'saving graph {self.obj} with K={len(self.obj.changes)} changes')

    


# def crawl(config, force_clean=False, verb=True, limit=-1):
    
#     t = time.time()

#     with GetGraphHold(config) as g:
#         nodes_docs = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]
#         gname = g.name

#     ltcs = [helpers.parse_zulutime(n.last_time_changed) for n in nodes_docs]
#     ltc = np.max(ltcs)
#     if verb:
#         helpers.log.info('STARTING AT: ' + helpers.make_zulustr(ltc))

#     updated_ids = update_nextcloud(ltc)
#     helpers.log.info('FOUND N={} updated documents since : {}'.format(len(updated_ids), helpers.make_zulustr(ltc)))

#     with GetGraphHold(config) as g:
#         if not force_clean:
#             nodes_docs = [g[id_] for id_ in updated_ids]
#         else:
#             nodes_docs = [n for n in g.nodes if issubclass(type(n), schema.BaseVersionedDocument)]

#     if limit and limit > 0:
#         nodes_docs = nodes_docs[:limit]
    
#     nodes_docs = [n for n in nodes_docs if n.extension in '.pdf .docx']
#     api = langchain_nextcloud_io.NextcloudFileLoader(verb=True)
#     docs = api.get_nc_docs_dc(nodes_docs)

#     missing = {n.id:n.get_latest_ver()['path'] for n, d in zip(nodes_docs, docs) if n is None or d is None}
#     if verb and missing:
#         log.info('START OF MISSING DOCS'.rjust(50, '-').ljust(100, '-'))

#         for m, pth in missing.items():
#             log.info(m.ljust(50), pth)
        
#         log.info('END OF MISSING DOCS'.rjust(50, '-').ljust(100, '-'))

#     dc = {n.id:(n, d) for n, d in zip(nodes_docs, docs) if n is not None and d is not None}
#     # woosh_api.incremental_index(ui_state.DEFAULT_FINDEX_PATH, dc, force_clean=force_clean, verb=verb)
#     if docs:
#         elasticsearch_api.update_index(docs)
#     elapsed = time.time()-t
#     if verb:
#         log.info((elapsed, len(docs), len(dc)))

#     return gname, missing, elapsed, docs, dc, ltc


def get_newest_doc_ltc(config):
    g = get_graph(config)
    return g.get_ltc_newest_doc()
    


def query_changed_since(config, t_last):
    if nextcloud_interface.login_data:
        
        if t_last is None:
            t_last = get_newest_doc_ltc(config)
        elif isinstance(t_last, str):
            t_last = helpers.parse_zulutime(t_last)
        
        api = nextcloud_interface.NextcloudApiAccessor()
        return t_last, api.query_changed_since(t_last)
    else:
        return t_last, []


def check_new_docs(config, t_last=None):
    new_docs = []

    if nextcloud_interface.login_data:
        
        if t_last is None:
            t_last = get_newest_doc_ltc(config)
        elif isinstance(t_last, str):
            t_last = helpers.parse_zulutime(t_last)
        
        t_now = helpers.get_utcnow()

        api = nextcloud_interface.NextcloudApiAccessor()
        files = api.query_changed_since(t_last)

        new_docs = [node_factory.test_new_doc(f) for f in files]
        new_docs = [nd for nd in new_docs if nd]

        new_docs.sort(key=lambda n: n.get('ltc'))
        
        t_last = t_now

    return t_last, new_docs


def graph_merge_new_docs(g:graph_main.Graph):
    t_last = helpers.parse_zulutime(g.get_ltc_newest_doc())
    t_last, updated_docs_dc = check_new_docs(None, t_last)
    log.info(f'Found N={len(updated_docs_dc)} new docs! since ltc="{helpers.make_zulustr(t_last)}"')
    f = lambda d: node_factory.construct_doc_from_dc_data(d, g)
    ids = [f(x) for x in updated_docs_dc]
    ids = [x for x in ids if x]
    if ids:
        g.add_changenote(f'Successfully updated N={len(ids)} docs!')
    return ids

def merge_new_docs(config):
    with GetGraphHold(config) as g:
        return graph_merge_new_docs(g)


def update_nextcloud(config, dt_elapsed):

    log.debug('TICK nextcloud update')
    dt_get = dt_elapsed + datetime.timedelta(seconds=60)
    api = nextcloud_interface.NextcloudApiAccessor()
    files = api.query_changed_since(dt_get)


    with GetGraphHold(config) as graph:
        ids = [(d, node_factory.construct_doc_from_dc_data(d, [graph])) for d in files]
        updated_nodes = [graph[id_].add_version(dc_data, on_exist='ignore') for dc_data, id_ in ids if id_ in graph]
        return [n.id for n in updated_nodes]




def update_nextcloud_tick(config, t_last, do_tick=None):
    t_now = helpers.get_utcnow()

    DT_CHECK_FILEUPDATES_SEC_RND = config.get('crawler').get('DT_CHECK_FILEUPDATES_SEC_RND')
    DT_CHECK_FILEUPDATES_SEC = config.get('crawler').get('DT_CHECK_FILEUPDATES_SEC')

    if nextcloud_interface.login_data:
        
        if t_last is None:
            do_tick = True
            t_fallback = t_now - datetime.timedelta(DT_CHECK_FILEUPDATES_SEC_RND)
            t_last = helpers.parse_zulutime(t_last=helpers.make_zulustr(t_fallback))
        elif isinstance(t_last, str):
            t_last = helpers.parse_zulutime(t_last)

        if t_last is None or not isinstance(t_last, datetime.datetime):
            t_last = t_fallback
        if t_now is None or not isinstance(t_now, datetime.datetime):
            t_now = helpers.get_utcnow()
        
        dt_elapsed = t_now - t_last

        if do_tick is None:
            dt_fuzzy = np.random.default_rng().normal(0, DT_CHECK_FILEUPDATES_SEC_RND)
            dt_fuzzy = np.clip(dt_fuzzy, -DT_CHECK_FILEUPDATES_SEC_RND*3, DT_CHECK_FILEUPDATES_SEC_RND*3)
            dt = datetime.timedelta(seconds=DT_CHECK_FILEUPDATES_SEC + dt_fuzzy)
            do_tick = (dt_elapsed > dt)
        # log.debug((dt_elapsed, dt))
        
        if do_tick:
            updated_ids = update_nextcloud(config, dt_elapsed)
            t_last = t_now



    return t_last, updated_ids


def get_pyplot(config, lyout='auto', plot_merged=False, exclude_all_view=True):

    if not 'plt' in locals():
        import matplotlib.pyplot as plt
    g = get_graph(config)
    
    g.plot(lyout=lyout, plot_merged=plot_merged, exclude_all_view=exclude_all_view)
    plt.tight_layout()

    # Save the figure as a PNG image
    image_buffer = io.BytesIO()
    plt.savefig(image_buffer, format="png")
    image_buffer.seek(0)

    # Return the image as a response
    return image_buffer.read()

def set_docwriter_pipes(config):
    from ReqTracker.exporters import html_formatter, repmaker
    pipes = config.get('doc_writing', {}).get('pipes')
    log.info(f'setting pipes: {pipes=}')

    for key in pipes:
        log.info(f'setting pipe: {key}')
        if key == 'html':
            log.info(f'setting pipe: {key} -> HTML')
            html_formatter.add_docmaker_pipe()
        elif key == '_tex.zip':
            log.info(f'setting pipe: {key} -> repmaker')
            repmaker.add_docmaker_pipe()
        else:
            raise KeyError(f'setting pipe: {key} -> repmaker NOT FOUND')