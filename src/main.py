import datetime
import traceback
from typing import Optional
from fastapi import Body, FastAPI, Query, Request, Response, HTTPException, Path
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from pydantic import BaseModel

import yaml, json, os, time, random

import asyncio
from contextlib import asynccontextmanager

import app_helpers

from ReqTracker.core import graph_base, helpers
from ReqTracker import login
from ReqTracker.io_files import redmine_api, nextcloud_interface

log = helpers.log

T_STARTED = helpers.get_utcnow()

pth = 'config.yaml'
with open(pth) as fp:
    config = yaml.safe_load(fp)

login.setup(config)

app_helpers.set_docwriter_pipes(config)

def crawl_tick():
    with app_helpers.GetGraphHold(config=config) as g:
        app_helpers.graph_merge_new_docs(g)
        return st_reindex(force_clean=False)
    
async def crawl():
    t_fuzzy = 0
    log.info('crawler starting...')
    i = 0
    try:
        while True:
            try:
                if i % 200 == 0:
                    log.info(f'crawler: still running. now at tick {i}...')
                
                log.debug('tick crawl...')
                ret = crawl_tick()
                i += 1
                log.debug(f'tick crawl DONE with {ret=}')
            except Exception as err:
                log.error('crawl TICK ERROR:' + str(err))

            t_fuzzy = int(random.random() * 2 * 60)
            await asyncio.sleep(15 * 60 + t_fuzzy)  # Sleep for 15 minutes
    finally:
        log.info('ERROR crawler exiting')


@asynccontextmanager
async def lifespan(app: FastAPI):
    log.info('STARTING crawler')
    asyncio.create_task(crawl())

    yield

    log.info('END')



app = FastAPI(lifespan=lifespan)

templates = Jinja2Templates(directory="templates")  # You can use this for more complex templates
app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/css", StaticFiles(directory="static/css"), name="css")
app.mount("/images", StaticFiles(directory="static/images"), name="images")



@app.get("/ui")
@app.get("/ui/v2")
async def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})
  # Use this for more complex templates
    # return {"message": "Hello World"}  # For simple text response

def m_ping():
    return f'ReqTracker Server v2, status: RUNNING, local time: {helpers.nowiso()} running since: {helpers.make_zulustr(T_STARTED)}'





@app.route("/ping")
@app.route("/")
@app.route("/api/v1/ping")
@app.route("/api/v2/ping")
def ping(request: Request):
    accept_header = request.headers.get("Accept")
    if accept_header and "application/json" in accept_header:
        return dict(response=m_ping())
    else:
        return HTMLResponse(f'<a href="/ui">{m_ping()}</a>')


@app.get("/api/v2/get_updated_docs_since")
async def get_updated_docs_since(t_last_s: str = None):
    try:
        t_last, updated_ids = app_helpers.check_new_docs(config, helpers.parse_zulutime(t_last_s))
        if t_last is not None:
            t_last_s = helpers.make_zulustr(t_last, remove_ms=False)
            return {'success': True, 't_last': t_last_s, 'updated_ids': updated_ids, 'info': f'successfully checked for updates and found N={len(updated_ids)} updated documents'}
        else:
            return {'success': False, 't_last': t_last_s, 'updated_ids': updated_ids, 'info': '"t_last_s" was None or empty'}
    except Exception as err:
        log.exception(err)
        return {'success': False, 'info': f'ERROR: {err}', 't_last': '', 'response': {}}


# @app.get("/api/v2/query_changed_since")
# async def query_changed_since(t_last_s: str = None):
#     try:
#         t_last, files = app_helpers.query_changed_since(config, helpers.parse_zulutime(t_last_s))
#         if t_last is not None:
#             t_last_s = helpers.make_zulustr(t_last, remove_ms=False)
            
#         return {'success': True, 't_last': t_last_s, 'files': files, 'info': f'successfully checked for updated files and found N={len(files)} updated files'}
#     except Exception as err:
#         log.exception(err)
#         return {'success': False, 'info': f'ERROR: {err}', 'files': []}

@app.get("/api/v2/doc_versions")
async def doc_versions():
    g = app_helpers.get_graph(config)
    return {n.id: n.get_sorted_versions(ret_full=False, ret_mapped=False) for n in g.nodes if hasattr(n, 'get_sorted_versions')}





@app.get("/api/v2/data")
async def get_data(merge_first: int = Query(0, description="set to anything but zero to trigger a merging event before getting the data")):
    has_merged = False
    ids_updated = []
    if merge_first:
        ids_updated = app_helpers.merge_new_docs(config)
        has_merged = len(ids_updated) > 0
        log.info(f'successfully ran "merge_new_docs" with {len(ids_updated)=}')

    
    data, data_version_id = app_helpers.load_data(config, with_data_version_id=True)
    return {'success': True, 'data': data, 'data_version_id': data_version_id, 'has_merged': has_merged, 'ids_updated': ids_updated}


class NewProjectFileVersion(BaseModel):
    content: dict
    description: str | None = None
    comment: str | None = None
    data_version_id: str | None = None



@app.post("/api/v2/new_version")
async def upload_data(item: NewProjectFileVersion):
    try:
        return app_helpers.save_data(config, item.content, item.comment, item.description, data_version_id=item.data_version_id)
    except Exception as err:
        log.exception(err)
        return {'success': False, 'info': f'ERROR: {err}', 'sink': '', 'response': {}}

    

class GetPositionsForViewRequest(BaseModel):
    edges: list
    nodes: list
    lyout: str = 'tree'

@app.post("/api/v2/new_layout")
async def new_layout(r: GetPositionsForViewRequest):
    try:
        log.info(f'getting new layout with {r.lyout} and {len(r.nodes)=} and {len(r.edges)}')
        return app_helpers.get_new_layout(config, r.edges, r.nodes, lyout=r.lyout)
    except Exception as err:
        log.exception(err)
        return {'success': False, 'info': f'ERROR: {err}', 'data':{}}

    
    

@app.get("/api/v2/graph_as_image")
def get_image(  lyout: str = Query('auto', description="the layout function to use"),
                plot_merged: int = Query(0, description=" 1|0 plot all graphs in one figure?"), 
              exclude_all: int = Query(1, description="1|0  exclude the view called 'ALL'")):
    bts = app_helpers.get_pyplot(config, lyout=lyout, plot_merged=plot_merged, exclude_all_view=exclude_all)
    
    # Return the image as a response
    return Response(bts, media_type="image/png")

@app.get('/api/v1/graph')
async def graph():
    return await get_data(0)


@app.get('/api/v1/nodes')
async def getNode(id: str = Query('', description="the node id to get (empty for all)")):
    with app_helpers.GetGraphHold(config) as graph:
        return [n.to_dict() for n in graph.nodes]



@app.get('/api/v1/node')
async def getNode(id: str = Query('', description="the node id to get (empty for all)")):
    with app_helpers.GetGraphHold(config) as graph:
        if not id:
            return [n.to_dict() for n in graph.nodes]
        elif id not in graph:
            raise HTTPException(status_code=404, detail=f'"{id}" could not be found in graph')
        else:
            return [graph[id].to_dict()]


@app.post("/api/v1/node/{action}", response_model=dict)
@app.patch("/api/v1/node/{action}", response_model=dict)
async def update_item(action: str, dc: dict):

    log.info(f'{action=} | {len(dc)=}, {type(dc)=}')
    with app_helpers.GetGraphHold(config) as graph:
        id = dc.get('id')
        if not id:
            raise HTTPException(status_code=422, detail="Missing required field: 'id'")
        if not id in graph:
            raise HTTPException(status_code=404, detail=f'"{id}" could not be found in graph')

        node = graph[id]

        if 'set_status' in action:
            status = dc['status']
            if isinstance(status, str):
                found = status in graph_base.STATUS.__members__
            elif isinstance(status, int):
                found = status in graph_base.STATUS
            else:
                found = False
            if not found:
                allowed = ' '.join([s.name for s in graph_base.STATUS])
                raise HTTPException(status_code=400,  detail=f'the given status "{status}" could not be found in allowed status list: {allowed}')
            suc = node.set_status(status)
            return {'success': suc, 'node': node.to_dict()}
        elif 'set_text' in action:
            suc = node.set_text(dc['text'])
            return {'success': suc, 'node': node.to_dict()}
        elif 'add_note' in action:
            ret = node.add_note(dc['text'])
            return {'success': True, 'ret':ret, 'node': node.to_dict()}
        elif 'add_data' in action:
            node.add_data(dc['key'], dc['value'])
            return {'success': True, 'node': node.to_dict()}
        elif 'append_to_doc' in action:

            if config['doc_writing']['set_empty_to_redmine'] and not node.doc_src and not node._doc:
                set_doc_src_redmine(node, upload=True)

            ret = graph[id].doc_append_part(dc['value'])
            return {'success': ret, 'node': node.to_dict()}
        elif 'set_doc' in action:
            if config['doc_writing']['set_empty_to_redmine'] and not node.doc_src and not node._doc:
                set_doc_src_redmine(node, upload=True)

            if not isinstance(dc['value'], list):
                typ = type(dc['value'])
                raise HTTPException(status_code=400, detail=f'can only set a document with a list type object, but got {typ=}')
            
            ret = node.doc_set_value(dc['value'])
            return {'success': True, 'ret': ret, 'node': node.to_dict()}
        else:
            raise HTTPException(status_code=404, detail=f'"{action=}" is an unknown command!')


def set_doc_src_redmine(node, upload=False):
    doc = node.get_doc()
    project_name = config['sources']['redmine']['default_project']
    pagetitle = redmine_api.get_page_title(doc)
    new_src = redmine_api.make_wiki_path(project_name, pagetitle)
    node.set_doc_src(new_src)
    if upload:
        suc = node.doc_set(doc)
    else:
        suc = False

    return new_src, doc, suc


@app.patch("/api/v2/node/set_doc_source")
async def node(dc: dict):
    id = dc.get('id')
    src = dc.get('src', 'redmine')

    with app_helpers.GetGraphHold(config) as graph:
        if not id:
            raise HTTPException(status_code=422, detail="Missing required field: 'id'")
        if not id in graph:
            raise HTTPException(status_code=404, detail=f'"{id}" could not be found in graph')

        
        node = graph[id]

        assert src == 'redmine', 'only setting to redmine src is currently allowed'
        new_src, doc, suc = set_doc_src_redmine(node, upload=True)
        return {'success': suc, 'new_src': new_src, 'doc': doc, 'uploaded': suc}
    

@app.get("/api/v2/node/get_doc")
async def node(id: str = Query('', description="the node id to get (empty for all)")):


    with app_helpers.GetGraphHold(config) as graph:
        if not id:
            raise HTTPException(status_code=422, detail="Missing required field: 'id'")
        if not id in graph:
            raise HTTPException(status_code=404, detail=f'"{id}" could not be found in graph')

        node = graph[id]
        return node.get_doc()
    
@app.get("/api/v2/crawl")
async def st_crawl():
    return crawl_tick()

@app.get("/api/v2/st/update")
async def st_upate(n: int = Query(0, description="Maximum number of docs to crawl")):
    return st_reindex(force_clean=False, n_max=n)

@app.get("/api/v2/st/reindex")
async def st_reindex(n: int = Query(0, description="Maximum number of docs to crawl")):
    return st_reindex(force_clean=True, n_max=n)

@app.get("/api/v2/st/db")
async def st_db(preload: int = Query(0, description="1|0  weather or not to load the file into the backend first")):

    if not 'app_st_search' in locals():
        import app_search_repo
    if preload > 0:
        repo = app_search_repo.get_repo(config)
        preload_res = app_search_repo.save(config, repo, 'loaded and saved again')
    else:
        preload_res = {}
        
    data, data_version_id = app_search_repo.load_data(config, with_data_version_id=True)
    return {'content': data['content'], 'data_version_id': data_version_id, 'preload_res': preload_res}

@app.get("/api/v2/st/info")
async def st_info(n: int = Query(0, description="Maximum number of docs to crawl")):

    if not 'app_st_search' in locals():
        import app_search_repo
    return {"info": app_search_repo.load(config).info()}

def st_reindex(force_clean=False, n_max=0):
    t=time.time()
    
    try:
        if not 'app_st_search' in locals():
            import app_search_repo

        model = app_search_repo.load(config)

        n_updates, updated_docs = model.reindex_new_docs(config, force_clean=force_clean, n_max=n_max)


        changes = f'updated n={n_updates} entries (pages) in index from docs: {",".join(updated_docs)}'

        save_res = app_search_repo.save(config, model, changes)

        assert save_res.get('success'), f'saving failed with: {save_res=}'
        serr = ''
        success = True

    except Exception as err:
        log.error(err, exc_info=True)
        success = False
        n_updates = 0
        changes = ''
        serr = str(err)
        save_res = {}

    elapsed = (time.time() - t)*1000

    ret = {
        'success': success,
        'info': changes,
        'error': str(serr),
        'N': n_updates,
        't_elapsed': elapsed,
        'save_res': save_res
    }

    return ret

if __name__ == "__main__":
    # log.setLevel('DEBUG')

    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8080, reload=True)